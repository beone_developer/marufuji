PGDMP     .                     x            beone_byi_r    10.6    10.6 P   �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    354919    beone_byi_r    DATABASE     �   CREATE DATABASE beone_byi_r WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_Indonesia.1252' LC_CTYPE = 'English_Indonesia.1252';
    DROP DATABASE beone_byi_r;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    354920    beone_adjustment    TABLE     =  CREATE TABLE public.beone_adjustment (
    adjustment_id bigint NOT NULL,
    adjustment_no character varying(20),
    adjustment_date date,
    keterangan character varying(100),
    item_id integer,
    qty_adjustment bigint,
    posisi_qty integer,
    update_by integer,
    update_date date,
    flag integer
);
 $   DROP TABLE public.beone_adjustment;
       public         postgres    false    3            �            1259    354923 "   beone_adjustment_adjustment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_adjustment_adjustment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.beone_adjustment_adjustment_id_seq;
       public       postgres    false    196    3            �           0    0 "   beone_adjustment_adjustment_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.beone_adjustment_adjustment_id_seq OWNED BY public.beone_adjustment.adjustment_id;
            public       postgres    false    197            �            1259    354925 	   beone_coa    TABLE     S  CREATE TABLE public.beone_coa (
    coa_id integer NOT NULL,
    nama character varying(50),
    nomor character varying(50),
    tipe_akun integer,
    debet_valas double precision,
    debet_idr double precision,
    kredit_valas double precision,
    kredit_idr double precision,
    dk character varying,
    tipe_transaksi integer
);
    DROP TABLE public.beone_coa;
       public         postgres    false    3            �            1259    354931    beone_coa_coa_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_coa_coa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.beone_coa_coa_id_seq;
       public       postgres    false    198    3            �           0    0    beone_coa_coa_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.beone_coa_coa_id_seq OWNED BY public.beone_coa.coa_id;
            public       postgres    false    199            �            1259    354933    beone_coa_jurnal    TABLE     �   CREATE TABLE public.beone_coa_jurnal (
    coa_jurnal_id integer NOT NULL,
    nama_coa_jurnal character varying(100),
    keterangan_coa_jurnal character varying(1000),
    coa_id integer,
    coa_no character varying(50)
);
 $   DROP TABLE public.beone_coa_jurnal;
       public         postgres    false    3            �            1259    354939 "   beone_coa_jurnal_coa_jurnal_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq;
       public       postgres    false    200    3            �           0    0 "   beone_coa_jurnal_coa_jurnal_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq OWNED BY public.beone_coa_jurnal.coa_jurnal_id;
            public       postgres    false    201            �            1259    354941    beone_country    TABLE     �   CREATE TABLE public.beone_country (
    country_id integer NOT NULL,
    nama character varying(50),
    country_code character varying(10),
    flag integer
);
 !   DROP TABLE public.beone_country;
       public         postgres    false    3            �            1259    354944    beone_country_country_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_country_country_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.beone_country_country_id_seq;
       public       postgres    false    3    202            �           0    0    beone_country_country_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.beone_country_country_id_seq OWNED BY public.beone_country.country_id;
            public       postgres    false    203            �            1259    354946    beone_custsup    TABLE     b  CREATE TABLE public.beone_custsup (
    custsup_id integer NOT NULL,
    nama character varying(100),
    alamat character varying(200),
    tipe_custsup integer,
    negara integer,
    saldo_hutang_idr double precision,
    saldo_hutang_valas double precision,
    saldo_piutang_idr double precision,
    saldo_piutang_valas double precision,
    flag integer,
    pelunasan_hutang_idr double precision,
    pelunasan_hutang_valas double precision,
    pelunasan_piutang_idr double precision,
    pelunasan_piutang_valas double precision,
    status_lunas_piutang integer,
    status_lunas_hutang integer
);
 !   DROP TABLE public.beone_custsup;
       public         postgres    false    3            �            1259    354949    beone_custsup_custsup_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_custsup_custsup_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.beone_custsup_custsup_id_seq;
       public       postgres    false    3    204            �           0    0    beone_custsup_custsup_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.beone_custsup_custsup_id_seq OWNED BY public.beone_custsup.custsup_id;
            public       postgres    false    205            �            1259    354951    beone_export_detail    TABLE     �  CREATE TABLE public.beone_export_detail (
    export_detail_id integer NOT NULL,
    export_header_id integer,
    item_id integer,
    qty double precision,
    satuan_qty integer,
    price double precision,
    pack_qty double precision,
    satuan_pack integer,
    origin_country integer,
    doc character varying(50),
    volume double precision,
    netto double precision,
    brutto double precision,
    flag integer
);
 '   DROP TABLE public.beone_export_detail;
       public         postgres    false    3            �            1259    354954 (   beone_export_detail_export_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_export_detail_export_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_export_detail_export_detail_id_seq;
       public       postgres    false    206    3            �           0    0 (   beone_export_detail_export_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_export_detail_export_detail_id_seq OWNED BY public.beone_export_detail.export_detail_id;
            public       postgres    false    207            �            1259    354956    beone_export_header    TABLE       CREATE TABLE public.beone_export_header (
    export_header_id integer NOT NULL,
    jenis_bc integer,
    car_no character varying(50),
    bc_no character varying(50),
    bc_date date,
    kontrak_no character varying(50),
    kontrak_date date,
    jenis_export integer,
    invoice_no character varying(50),
    invoice_date date,
    surat_jalan_no character varying(50),
    surat_jalan_date date,
    receiver_id integer,
    country_id integer,
    price_type character varying(10),
    amount_value double precision,
    valas_value double precision,
    insurance_type character varying(10),
    insurance_value double precision,
    freight double precision,
    flag integer,
    status integer,
    delivery_date date,
    update_by integer,
    update_date date,
    delivery_no character varying(50),
    vessel character varying(100),
    port_loading character varying(100),
    port_destination character varying(100),
    container character varying(10),
    no_container character varying(25),
    no_seal character varying(25)
);
 '   DROP TABLE public.beone_export_header;
       public         postgres    false    3            �            1259    354962 (   beone_export_header_export_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_export_header_export_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_export_header_export_header_id_seq;
       public       postgres    false    3    208            �           0    0 (   beone_export_header_export_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_export_header_export_header_id_seq OWNED BY public.beone_export_header.export_header_id;
            public       postgres    false    209            �            1259    354964    beone_gl    TABLE     �  CREATE TABLE public.beone_gl (
    gl_id bigint NOT NULL,
    gl_date date,
    coa_id integer,
    coa_no character varying(20),
    coa_id_lawan integer,
    coa_no_lawan character varying(20),
    keterangan character varying(100),
    debet double precision,
    kredit double precision,
    pasangan_no character varying(50),
    gl_number character varying(50),
    update_by integer,
    update_date date
);
    DROP TABLE public.beone_gl;
       public         postgres    false    3            �            1259    354967    beone_gl_gl_id_seq    SEQUENCE     {   CREATE SEQUENCE public.beone_gl_gl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.beone_gl_gl_id_seq;
       public       postgres    false    210    3            �           0    0    beone_gl_gl_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.beone_gl_gl_id_seq OWNED BY public.beone_gl.gl_id;
            public       postgres    false    211            �            1259    354969    beone_gudang    TABLE     �   CREATE TABLE public.beone_gudang (
    gudang_id integer NOT NULL,
    nama character varying(100),
    keterangan character varying(200),
    flag integer
);
     DROP TABLE public.beone_gudang;
       public         postgres    false    3            �            1259    354972    beone_gudang_detail    TABLE     �  CREATE TABLE public.beone_gudang_detail (
    gudang_detail_id bigint NOT NULL,
    gudang_id integer,
    trans_date date,
    item_id integer,
    qty_in double precision,
    qty_out double precision,
    nomor_transaksi character varying(50),
    update_by integer,
    update_date date,
    flag integer,
    keterangan character varying(100),
    kode_tracing character varying(50)
);
 '   DROP TABLE public.beone_gudang_detail;
       public         postgres    false    3            �            1259    354975 (   beone_gudang_detail_gudang_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq;
       public       postgres    false    213    3            �           0    0 (   beone_gudang_detail_gudang_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq OWNED BY public.beone_gudang_detail.gudang_detail_id;
            public       postgres    false    214            �            1259    354977    beone_gudang_gudang_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_gudang_gudang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.beone_gudang_gudang_id_seq;
       public       postgres    false    3    212            �           0    0    beone_gudang_gudang_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.beone_gudang_gudang_id_seq OWNED BY public.beone_gudang.gudang_id;
            public       postgres    false    215            �            1259    354979    beone_pemakaian_bahan_header    TABLE     T  CREATE TABLE public.beone_pemakaian_bahan_header (
    pemakaian_header_id bigint NOT NULL,
    nomor_pemakaian character varying(50),
    keterangan character varying(250),
    tgl_pemakaian date,
    update_by integer,
    update_date date,
    nomor_pm character varying(50),
    amount_pemakaian double precision,
    status integer
);
 0   DROP TABLE public.beone_pemakaian_bahan_header;
       public         postgres    false    3            �            1259    354982 4   beone_header_pemakaian_bahan_pemakaian_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_header_pemakaian_bahan_pemakaian_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 K   DROP SEQUENCE public.beone_header_pemakaian_bahan_pemakaian_header_id_seq;
       public       postgres    false    3    216            �           0    0 4   beone_header_pemakaian_bahan_pemakaian_header_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_header_pemakaian_bahan_pemakaian_header_id_seq OWNED BY public.beone_pemakaian_bahan_header.pemakaian_header_id;
            public       postgres    false    217            �            1259    354984    beone_hutang_piutang    TABLE     �  CREATE TABLE public.beone_hutang_piutang (
    hutang_piutang_id bigint NOT NULL,
    custsup_id integer,
    trans_date date,
    nomor character varying(50),
    keterangan character varying(100),
    valas_trans double precision,
    idr_trans double precision,
    valas_pelunasan double precision,
    idr_pelunasan double precision,
    tipe_trans integer,
    update_by integer,
    update_date date,
    flag integer,
    status_lunas integer
);
 (   DROP TABLE public.beone_hutang_piutang;
       public         postgres    false    3            �            1259    354987 *   beone_hutang_piutang_hutang_piutang_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq;
       public       postgres    false    218    3            �           0    0 *   beone_hutang_piutang_hutang_piutang_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq OWNED BY public.beone_hutang_piutang.hutang_piutang_id;
            public       postgres    false    219            �            1259    354989    beone_import_detail    TABLE     �  CREATE TABLE public.beone_import_detail (
    import_detail_id integer NOT NULL,
    item_id integer,
    qty double precision,
    satuan_qty integer,
    price double precision,
    pack_qty double precision,
    satuan_pack integer,
    origin_country integer,
    volume double precision,
    netto double precision,
    brutto double precision,
    hscode character varying(50),
    tbm double precision,
    ppnn double precision,
    tpbm double precision,
    cukai integer,
    sat_cukai integer,
    cukai_value double precision,
    bea_masuk character varying(100),
    sat_bea_masuk integer,
    flag integer,
    item_type_id integer,
    import_header_id integer
);
 '   DROP TABLE public.beone_import_detail;
       public         postgres    false    3            �            1259    354992 (   beone_import_detail_import_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_import_detail_import_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_import_detail_import_detail_id_seq;
       public       postgres    false    220    3            �           0    0 (   beone_import_detail_import_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_import_detail_import_detail_id_seq OWNED BY public.beone_import_detail.import_detail_id;
            public       postgres    false    221            �            1259    354994    beone_import_header    TABLE     C  CREATE TABLE public.beone_import_header (
    import_header_id integer NOT NULL,
    jenis_bc integer,
    car_no character varying(50),
    bc_no character varying(50),
    bc_date date,
    invoice_no character varying(50),
    invoice_date date,
    kontrak_no character varying(50),
    kontrak_date date,
    purpose_id character varying(100),
    supplier_id integer,
    price_type character varying(10),
    "from" character varying(10),
    amount_value double precision,
    valas_value double precision,
    value_added double precision,
    discount double precision,
    insurance_type character varying(10),
    insurace_value double precision,
    freight double precision,
    flag integer,
    update_by integer,
    update_date date,
    status integer,
    receive_date date,
    receive_no character varying(50)
);
 '   DROP TABLE public.beone_import_header;
       public         postgres    false    3            �            1259    354997 (   beone_import_header_import_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_import_header_import_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_import_header_import_header_id_seq;
       public       postgres    false    3    222                        0    0 (   beone_import_header_import_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_import_header_import_header_id_seq OWNED BY public.beone_import_header.import_header_id;
            public       postgres    false    223            �            1259    354999    beone_inventory    TABLE     �  CREATE TABLE public.beone_inventory (
    intvent_trans_id bigint NOT NULL,
    intvent_trans_no character varying(50),
    item_id integer,
    trans_date date,
    keterangan character varying(100),
    qty_in double precision,
    value_in double precision,
    qty_out double precision,
    value_out double precision,
    sa_qty double precision,
    sa_unit_price double precision,
    sa_amount double precision,
    flag integer,
    update_by integer,
    update_date date
);
 #   DROP TABLE public.beone_inventory;
       public         postgres    false    3            �            1259    355002 $   beone_inventory_intvent_trans_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_inventory_intvent_trans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.beone_inventory_intvent_trans_id_seq;
       public       postgres    false    3    224                       0    0 $   beone_inventory_intvent_trans_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.beone_inventory_intvent_trans_id_seq OWNED BY public.beone_inventory.intvent_trans_id;
            public       postgres    false    225            �            1259    355004 
   beone_item    TABLE     S  CREATE TABLE public.beone_item (
    item_id integer NOT NULL,
    nama character varying(100),
    item_code character varying(20),
    saldo_qty double precision,
    saldo_idr double precision,
    keterangan character varying(200),
    flag integer,
    item_type_id integer,
    hscode character varying(50),
    satuan_id integer
);
    DROP TABLE public.beone_item;
       public         postgres    false    3            �            1259    355007    beone_item_item_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_item_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.beone_item_item_id_seq;
       public       postgres    false    3    226                       0    0    beone_item_item_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.beone_item_item_id_seq OWNED BY public.beone_item.item_id;
            public       postgres    false    227            �            1259    355009    beone_item_type    TABLE     �   CREATE TABLE public.beone_item_type (
    item_type_id integer NOT NULL,
    nama character varying(100),
    keterangan character varying(200),
    flag integer
);
 #   DROP TABLE public.beone_item_type;
       public         postgres    false    3            �            1259    355012     beone_item_type_item_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_item_type_item_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_item_type_item_type_id_seq;
       public       postgres    false    228    3                       0    0     beone_item_type_item_type_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_item_type_item_type_id_seq OWNED BY public.beone_item_type.item_type_id;
            public       postgres    false    229            �            1259    355014    beone_kode_trans    TABLE     �   CREATE TABLE public.beone_kode_trans (
    kode_trans_id integer NOT NULL,
    nama character varying,
    coa_id integer,
    kode_bank character varying(20),
    in_out integer,
    flag integer
);
 $   DROP TABLE public.beone_kode_trans;
       public         postgres    false    3            �            1259    355020 "   beone_kode_trans_kode_trans_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_kode_trans_kode_trans_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.beone_kode_trans_kode_trans_id_seq;
       public       postgres    false    3    230                       0    0 "   beone_kode_trans_kode_trans_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.beone_kode_trans_kode_trans_id_seq OWNED BY public.beone_kode_trans.kode_trans_id;
            public       postgres    false    231            �            1259    355022    beone_konfigurasi_perusahaan    TABLE     G  CREATE TABLE public.beone_konfigurasi_perusahaan (
    nama_perusahaan character varying(100),
    alamat_perusahaan character varying(500),
    kota_perusahaan character varying(100),
    provinsi_perusahaan character varying(100),
    kode_pos_perusahaan character varying(50),
    logo_perusahaan character varying(1000)
);
 0   DROP TABLE public.beone_konfigurasi_perusahaan;
       public         postgres    false    3            �            1259    355028 	   beone_log    TABLE     �   CREATE TABLE public.beone_log (
    log_id bigint NOT NULL,
    log_user character varying(100),
    log_tipe integer,
    log_desc character varying(250),
    log_time timestamp without time zone
);
    DROP TABLE public.beone_log;
       public         postgres    false    3            �            1259    355031    beone_log_log_id_seq    SEQUENCE     }   CREATE SEQUENCE public.beone_log_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.beone_log_log_id_seq;
       public       postgres    false    233    3                       0    0    beone_log_log_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.beone_log_log_id_seq OWNED BY public.beone_log.log_id;
            public       postgres    false    234            �            1259    355033    beone_opname_detail    TABLE       CREATE TABLE public.beone_opname_detail (
    opname_detail_id integer NOT NULL,
    opname_header_id integer,
    item_id integer,
    qty_existing double precision,
    qty_opname double precision,
    qty_selisih double precision,
    status_opname integer,
    flag integer
);
 '   DROP TABLE public.beone_opname_detail;
       public         postgres    false    3            �            1259    355036 (   beone_opname_detail_opname_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_opname_detail_opname_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_opname_detail_opname_detail_id_seq;
       public       postgres    false    235    3                       0    0 (   beone_opname_detail_opname_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_opname_detail_opname_detail_id_seq OWNED BY public.beone_opname_detail.opname_detail_id;
            public       postgres    false    236            �            1259    355038    beone_opname_header    TABLE     �  CREATE TABLE public.beone_opname_header (
    opname_header_id integer NOT NULL,
    document_no character varying(50),
    opname_date date,
    total_item_opname integer,
    total_item_opname_match integer,
    total_item_opname_plus integer,
    total_item_opname_min integer,
    update_by integer,
    flag integer,
    keterangan character varying(100),
    update_date date
);
 '   DROP TABLE public.beone_opname_header;
       public         postgres    false    3            �            1259    355041 (   beone_opname_header_opname_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_opname_header_opname_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_opname_header_opname_header_id_seq;
       public       postgres    false    3    237                       0    0 (   beone_opname_header_opname_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_opname_header_opname_header_id_seq OWNED BY public.beone_opname_header.opname_header_id;
            public       postgres    false    238            �            1259    355043    beone_peleburan    TABLE     !  CREATE TABLE public.beone_peleburan (
    peleburan_id integer NOT NULL,
    peleburan_no character varying(20),
    peleburan_date date,
    keterangan character varying(100),
    item_id integer,
    qty_peleburan bigint,
    update_by integer,
    update_date date,
    flag integer
);
 #   DROP TABLE public.beone_peleburan;
       public         postgres    false    3            �            1259    355046     beone_peleburan_peleburan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_peleburan_peleburan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_peleburan_peleburan_id_seq;
       public       postgres    false    3    239                       0    0     beone_peleburan_peleburan_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_peleburan_peleburan_id_seq OWNED BY public.beone_peleburan.peleburan_id;
            public       postgres    false    240            �            1259    355048    beone_pemakaian_bahan_detail    TABLE     �   CREATE TABLE public.beone_pemakaian_bahan_detail (
    pemakaian_detail_id bigint NOT NULL,
    pemakaian_header_id integer,
    item_id integer,
    qty double precision,
    unit_price double precision
);
 0   DROP TABLE public.beone_pemakaian_bahan_detail;
       public         postgres    false    3            �            1259    355051 4   beone_pemakaian_bahan_detail_pemakaian_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 K   DROP SEQUENCE public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq;
       public       postgres    false    3    241            	           0    0 4   beone_pemakaian_bahan_detail_pemakaian_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq OWNED BY public.beone_pemakaian_bahan_detail.pemakaian_detail_id;
            public       postgres    false    242            �            1259    355053    beone_pm_header    TABLE     �   CREATE TABLE public.beone_pm_header (
    pm_header_id bigint NOT NULL,
    no_pm character varying(50),
    tgl_pm date,
    customer_id integer,
    qty double precision,
    keterangan_artikel character varying,
    status integer
);
 #   DROP TABLE public.beone_pm_header;
       public         postgres    false    3            �            1259    355059     beone_pm_header_pm_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_pm_header_pm_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_pm_header_pm_header_id_seq;
       public       postgres    false    3    243            
           0    0     beone_pm_header_pm_header_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_pm_header_pm_header_id_seq OWNED BY public.beone_pm_header.pm_header_id;
            public       postgres    false    244            �            1259    355061    beone_po_import_detail    TABLE     �   CREATE TABLE public.beone_po_import_detail (
    purchase_detail_id bigint NOT NULL,
    purchase_header_id integer,
    item_id integer,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer
);
 *   DROP TABLE public.beone_po_import_detail;
       public         postgres    false    3            �            1259    355064 -   beone_po_import_detail_purchase_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq;
       public       postgres    false    245    3                       0    0 -   beone_po_import_detail_purchase_detail_id_seq    SEQUENCE OWNED BY        ALTER SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq OWNED BY public.beone_po_import_detail.purchase_detail_id;
            public       postgres    false    246            �            1259    355066    beone_po_import_header    TABLE     3  CREATE TABLE public.beone_po_import_header (
    purchase_header_id bigint NOT NULL,
    purchase_no character varying(50),
    trans_date date,
    supplier_id integer,
    keterangan character varying(250),
    grandtotal double precision,
    flag integer,
    update_by integer,
    update_date date
);
 *   DROP TABLE public.beone_po_import_header;
       public         postgres    false    3            �            1259    355069 -   beone_po_import_header_purchase_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_po_import_header_purchase_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public.beone_po_import_header_purchase_header_id_seq;
       public       postgres    false    3    247                       0    0 -   beone_po_import_header_purchase_header_id_seq    SEQUENCE OWNED BY        ALTER SEQUENCE public.beone_po_import_header_purchase_header_id_seq OWNED BY public.beone_po_import_header.purchase_header_id;
            public       postgres    false    248            �            1259    355071    beone_produksi_detail    TABLE     �   CREATE TABLE public.beone_produksi_detail (
    produksi_detail_id bigint NOT NULL,
    produksi_header_id integer,
    pemakaian_header_id integer
);
 )   DROP TABLE public.beone_produksi_detail;
       public         postgres    false    3            �            1259    355074 ,   beone_produksi_detail_produksi_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_produksi_detail_produksi_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_produksi_detail_produksi_detail_id_seq;
       public       postgres    false    3    249                       0    0 ,   beone_produksi_detail_produksi_detail_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_produksi_detail_produksi_detail_id_seq OWNED BY public.beone_produksi_detail.produksi_detail_id;
            public       postgres    false    250            �            1259    355076    beone_produksi_header    TABLE     ^  CREATE TABLE public.beone_produksi_header (
    produksi_header_id bigint NOT NULL,
    nomor_produksi character varying(20),
    tgl_produksi date,
    tipe_produksi integer,
    item_produksi integer,
    qty_hasil_produksi double precision,
    keterangan character varying(250),
    flag integer,
    updated_by integer,
    udpated_date date
);
 )   DROP TABLE public.beone_produksi_header;
       public         postgres    false    3            �            1259    355079 ,   beone_produksi_header_produksi_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_produksi_header_produksi_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_produksi_header_produksi_header_id_seq;
       public       postgres    false    251    3                       0    0 ,   beone_produksi_header_produksi_header_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_produksi_header_produksi_header_id_seq OWNED BY public.beone_produksi_header.produksi_header_id;
            public       postgres    false    252            �            1259    355081    beone_purchase_detail    TABLE     �   CREATE TABLE public.beone_purchase_detail (
    purchase_detail_id bigint NOT NULL,
    purchase_header_id integer,
    item_id integer,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer
);
 )   DROP TABLE public.beone_purchase_detail;
       public         postgres    false    3            �            1259    355084 ,   beone_purchase_detail_purchase_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq;
       public       postgres    false    253    3                       0    0 ,   beone_purchase_detail_purchase_detail_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq OWNED BY public.beone_purchase_detail.purchase_detail_id;
            public       postgres    false    254            �            1259    355086    beone_purchase_header    TABLE     �  CREATE TABLE public.beone_purchase_header (
    purchase_header_id bigint NOT NULL,
    purchase_no character varying(50),
    trans_date date,
    supplier_id integer,
    keterangan character varying(200),
    update_by integer,
    update_date date,
    flag integer,
    ppn integer,
    subtotal double precision,
    ppn_value double precision,
    grandtotal double precision
);
 )   DROP TABLE public.beone_purchase_header;
       public         postgres    false    3                        1259    355089 ,   beone_purchase_header_purchase_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_purchase_header_purchase_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_purchase_header_purchase_header_id_seq;
       public       postgres    false    255    3                       0    0 ,   beone_purchase_header_purchase_header_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_purchase_header_purchase_header_id_seq OWNED BY public.beone_purchase_header.purchase_header_id;
            public       postgres    false    256                       1259    355091    beone_received_import    TABLE     �  CREATE TABLE public.beone_received_import (
    received_id bigint NOT NULL,
    nomor_aju character varying(50),
    nomor_dokumen_pabean character varying(10),
    tanggal_received date,
    status_received integer,
    flag integer,
    update_by integer,
    update_date date,
    nomor_received character varying(50),
    tpb_header_id integer,
    kurs double precision,
    po_import_header_id integer
);
 )   DROP TABLE public.beone_received_import;
       public         postgres    false    3                       1259    355094 %   beone_received_import_received_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_received_import_received_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public.beone_received_import_received_id_seq;
       public       postgres    false    3    257                       0    0 %   beone_received_import_received_id_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE public.beone_received_import_received_id_seq OWNED BY public.beone_received_import.received_id;
            public       postgres    false    258                       1259    355096 
   beone_role    TABLE     �  CREATE TABLE public.beone_role (
    role_id integer NOT NULL,
    nama_role character varying(50),
    keterangan character varying(200),
    master_menu integer,
    pembelian_menu integer,
    penjualan_menu integer,
    inventory_menu integer,
    produksi_menu integer,
    asset_menu integer,
    jurnal_umum_menu integer,
    kasbank_menu integer,
    laporan_inventory integer,
    laporan_keuangan integer,
    konfigurasi integer,
    import_menu integer,
    eksport_menu integer
);
    DROP TABLE public.beone_role;
       public         postgres    false    3                       1259    355099    beone_role_role_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_role_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.beone_role_role_id_seq;
       public       postgres    false    259    3                       0    0    beone_role_role_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.beone_role_role_id_seq OWNED BY public.beone_role.role_id;
            public       postgres    false    260                       1259    355101    beone_role_user    TABLE     �  CREATE TABLE public.beone_role_user (
    role_id integer NOT NULL,
    nama_role character varying(50),
    keterangan character varying(200),
    master_menu integer,
    user_add integer,
    user_edit integer,
    user_delete integer,
    role_add integer,
    role_edit integer,
    role_delete integer,
    customer_add integer,
    customer_edit integer,
    customer_delete integer,
    supplier_add integer,
    supplier_edit integer,
    supplier_delete integer,
    item_add integer,
    item_edit integer,
    item_delete integer,
    jenis_add integer,
    jenis_edit integer,
    jenis_delete integer,
    satuan_add integer,
    satuan_edit integer,
    satuan_delete integer,
    gudang_add integer,
    gudang_edit integer,
    gudang_delete integer,
    master_import integer,
    po_add integer,
    po_edit integer,
    po_delete integer,
    tracing integer,
    terima_barang integer,
    master_pembelian integer,
    pembelian_add integer,
    pembelian_edit integer,
    pembelian_delete integer,
    kredit_note_add integer,
    kredit_note_edit integer,
    kredit_note_delete integer,
    master_eksport integer,
    eksport_add integer,
    eksport_edit integer,
    eksport_delete integer,
    kirim_barang integer,
    menu_penjualan integer,
    penjualan_add integer,
    penjualan_edit integer,
    penjualan_delete integer,
    debit_note_add integer,
    debit_note_edit integer,
    debit_note_delete integer,
    menu_inventory integer,
    pindah_gudang integer,
    stockopname_add integer,
    stockopname_edit integer,
    stockopname_delete integer,
    stockopname_opname integer,
    adjustment_add integer,
    adjustment_edit integer,
    adjustment_delete integer,
    pemusnahan_add integer,
    pemusnahan_edit integer,
    pemusnahan_delete integer,
    recal_inventory integer,
    menu_produksi integer,
    produksi_add integer,
    produksi_edit integer,
    produksi_delete integer,
    menu_asset integer,
    menu_jurnal_umum integer,
    jurnal_umum_add integer,
    jurnal_umum_edit integer,
    jurnal_umum_delete integer,
    menu_kas_bank integer,
    kas_bank_add integer,
    kas_bank_edit integer,
    kas_bank_delete integer,
    menu_laporan_inventory integer,
    menu_laporan_keuangan integer,
    menu_konfigurasi integer
);
 #   DROP TABLE public.beone_role_user;
       public         postgres    false    3                       1259    355104    beone_role_user_role_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_role_user_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.beone_role_user_role_id_seq;
       public       postgres    false    3    261                       0    0    beone_role_user_role_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.beone_role_user_role_id_seq OWNED BY public.beone_role_user.role_id;
            public       postgres    false    262                       1259    355106    beone_sales_detail    TABLE     �   CREATE TABLE public.beone_sales_detail (
    sales_detail_id bigint NOT NULL,
    sales_header_id integer,
    item_id integer,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer
);
 &   DROP TABLE public.beone_sales_detail;
       public         postgres    false    3                       1259    355109 &   beone_sales_detail_sales_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_sales_detail_sales_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.beone_sales_detail_sales_detail_id_seq;
       public       postgres    false    3    263                       0    0 &   beone_sales_detail_sales_detail_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.beone_sales_detail_sales_detail_id_seq OWNED BY public.beone_sales_detail.sales_detail_id;
            public       postgres    false    264            	           1259    355111    beone_sales_header    TABLE     �  CREATE TABLE public.beone_sales_header (
    sales_header_id bigint NOT NULL,
    sales_no character varying(50),
    trans_date date,
    customer_id integer,
    keterangan character varying(200),
    ppn double precision,
    subtotal double precision,
    ppn_value double precision,
    grandtotal double precision,
    update_by integer,
    update_date date,
    flag integer,
    biaya double precision,
    status integer
);
 &   DROP TABLE public.beone_sales_header;
       public         postgres    false    3            
           1259    355114 &   beone_sales_header_sales_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_sales_header_sales_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.beone_sales_header_sales_header_id_seq;
       public       postgres    false    3    265                       0    0 &   beone_sales_header_sales_header_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.beone_sales_header_sales_header_id_seq OWNED BY public.beone_sales_header.sales_header_id;
            public       postgres    false    266                       1259    355116    beone_satuan_item    TABLE     �   CREATE TABLE public.beone_satuan_item (
    satuan_id integer NOT NULL,
    satuan_code character varying(5),
    keterangan character varying(100),
    flag integer
);
 %   DROP TABLE public.beone_satuan_item;
       public         postgres    false    3                       1259    355119    beone_satuan_item_satuan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_satuan_item_satuan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.beone_satuan_item_satuan_id_seq;
       public       postgres    false    267    3                       0    0    beone_satuan_item_satuan_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.beone_satuan_item_satuan_id_seq OWNED BY public.beone_satuan_item.satuan_id;
            public       postgres    false    268                       1259    355121    beone_subkon_in_detail    TABLE     �   CREATE TABLE public.beone_subkon_in_detail (
    subkon_in_detail_id bigint NOT NULL,
    subkon_in_header_id integer,
    item_id integer,
    qty double precision,
    flag integer
);
 *   DROP TABLE public.beone_subkon_in_detail;
       public         postgres    false    3                       1259    355124 .   beone_subkon_in_detail_subkon_in_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_subkon_in_detail_subkon_in_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 E   DROP SEQUENCE public.beone_subkon_in_detail_subkon_in_detail_id_seq;
       public       postgres    false    269    3                       0    0 .   beone_subkon_in_detail_subkon_in_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_subkon_in_detail_subkon_in_detail_id_seq OWNED BY public.beone_subkon_in_detail.subkon_in_detail_id;
            public       postgres    false    270                       1259    355126    beone_subkon_in_header    TABLE     ~  CREATE TABLE public.beone_subkon_in_header (
    subkon_in_id bigint NOT NULL,
    no_subkon_in character varying(50),
    no_subkon_out character varying(50),
    trans_date date,
    keterangan character varying(250),
    nomor_aju character varying(50),
    biaya_subkon double precision,
    supplier_id integer,
    flag integer,
    update_by integer,
    update_date date
);
 *   DROP TABLE public.beone_subkon_in_header;
       public         postgres    false    3                       1259    355129 '   beone_subkon_in_header_subkon_in_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_subkon_in_header_subkon_in_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE public.beone_subkon_in_header_subkon_in_id_seq;
       public       postgres    false    271    3                       0    0 '   beone_subkon_in_header_subkon_in_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE public.beone_subkon_in_header_subkon_in_id_seq OWNED BY public.beone_subkon_in_header.subkon_in_id;
            public       postgres    false    272                       1259    355131    beone_subkon_out_detail    TABLE     �   CREATE TABLE public.beone_subkon_out_detail (
    subkon_out_detail_id bigint NOT NULL,
    subkon_out_header_id integer,
    item_id integer,
    qty double precision,
    unit_price double precision,
    flag integer
);
 +   DROP TABLE public.beone_subkon_out_detail;
       public         postgres    false    3                       1259    355134 0   beone_subkon_out_detail_subkon_out_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_subkon_out_detail_subkon_out_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 G   DROP SEQUENCE public.beone_subkon_out_detail_subkon_out_detail_id_seq;
       public       postgres    false    273    3                       0    0 0   beone_subkon_out_detail_subkon_out_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_subkon_out_detail_subkon_out_detail_id_seq OWNED BY public.beone_subkon_out_detail.subkon_out_detail_id;
            public       postgres    false    274                       1259    355136    beone_subkon_out_header    TABLE     5  CREATE TABLE public.beone_subkon_out_header (
    subkon_out_id bigint NOT NULL,
    no_subkon_out character varying(50),
    trans_date date,
    supplier_id integer,
    keterangan character varying(250),
    nomor_aju character varying(50),
    flag integer,
    update_by integer,
    update_date date
);
 +   DROP TABLE public.beone_subkon_out_header;
       public         postgres    false    3                       1259    355139 )   beone_subkon_out_header_subkon_out_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_subkon_out_header_subkon_out_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 @   DROP SEQUENCE public.beone_subkon_out_header_subkon_out_id_seq;
       public       postgres    false    275    3                       0    0 )   beone_subkon_out_header_subkon_out_id_seq    SEQUENCE OWNED BY     w   ALTER SEQUENCE public.beone_subkon_out_header_subkon_out_id_seq OWNED BY public.beone_subkon_out_header.subkon_out_id;
            public       postgres    false    276                       1259    355141    beone_tipe_coa    TABLE     i   CREATE TABLE public.beone_tipe_coa (
    tipe_coa_id integer NOT NULL,
    nama character varying(50)
);
 "   DROP TABLE public.beone_tipe_coa;
       public         postgres    false    3                       1259    355144    beone_tipe_coa_tipe_coa_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq;
       public       postgres    false    3    277                       0    0    beone_tipe_coa_tipe_coa_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq OWNED BY public.beone_tipe_coa.tipe_coa_id;
            public       postgres    false    278                       1259    355146    beone_tipe_coa_transaksi    TABLE     �   CREATE TABLE public.beone_tipe_coa_transaksi (
    tipe_coa_trans_id integer NOT NULL,
    nama character varying(50),
    flag integer
);
 ,   DROP TABLE public.beone_tipe_coa_transaksi;
       public         postgres    false    3                       1259    355149 .   beone_tipe_coa_transaksi_tipe_coa_trans_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 E   DROP SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq;
       public       postgres    false    3    279                       0    0 .   beone_tipe_coa_transaksi_tipe_coa_trans_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq OWNED BY public.beone_tipe_coa_transaksi.tipe_coa_trans_id;
            public       postgres    false    280                       1259    355151    beone_transfer_stock    TABLE       CREATE TABLE public.beone_transfer_stock (
    transfer_stock_id bigint NOT NULL,
    transfer_no character varying(50),
    transfer_date date,
    coa_kode_biaya integer,
    keterangan character varying(200),
    update_by integer,
    update_date date,
    flag integer
);
 (   DROP TABLE public.beone_transfer_stock;
       public         postgres    false    3                       1259    355154    beone_transfer_stock_detail    TABLE     u  CREATE TABLE public.beone_transfer_stock_detail (
    transfer_stock_detail_id bigint NOT NULL,
    transfer_stock_header_id integer,
    tipe_transfer_stock character varying(10),
    item_id integer,
    qty double precision,
    gudang_id integer,
    biaya double precision,
    update_by integer,
    update_date date,
    flag integer,
    persen_produksi integer
);
 /   DROP TABLE public.beone_transfer_stock_detail;
       public         postgres    false    3                       1259    355157 8   beone_transfer_stock_detail_transfer_stock_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 O   DROP SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq;
       public       postgres    false    3    282                       0    0 8   beone_transfer_stock_detail_transfer_stock_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq OWNED BY public.beone_transfer_stock_detail.transfer_stock_detail_id;
            public       postgres    false    283                       1259    355159 *   beone_transfer_stock_transfer_stock_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq;
       public       postgres    false    281    3                       0    0 *   beone_transfer_stock_transfer_stock_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq OWNED BY public.beone_transfer_stock.transfer_stock_id;
            public       postgres    false    284                       1259    355161 
   beone_user    TABLE     �   CREATE TABLE public.beone_user (
    user_id integer NOT NULL,
    username character varying(50),
    password character varying(50),
    nama character varying(100),
    role_id integer,
    update_by integer,
    update_date date,
    flag integer
);
    DROP TABLE public.beone_user;
       public         postgres    false    3                       1259    355164    beone_user_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_user_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.beone_user_user_id_seq;
       public       postgres    false    3    285                       0    0    beone_user_user_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.beone_user_user_id_seq OWNED BY public.beone_user.user_id;
            public       postgres    false    286                       1259    355166    beone_voucher_detail    TABLE     B  CREATE TABLE public.beone_voucher_detail (
    voucher_detail_id bigint NOT NULL,
    voucher_header_id integer,
    coa_id_lawan integer,
    coa_no_lawan character varying(20),
    jumlah_valas double precision,
    kurs double precision,
    jumlah_idr double precision,
    keterangan_detail character varying(200)
);
 (   DROP TABLE public.beone_voucher_detail;
       public         postgres    false    3                        1259    355169 *   beone_voucher_detail_voucher_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq;
       public       postgres    false    3    287                        0    0 *   beone_voucher_detail_voucher_detail_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq OWNED BY public.beone_voucher_detail.voucher_detail_id;
            public       postgres    false    288            !           1259    355171    beone_voucher_header    TABLE     ;  CREATE TABLE public.beone_voucher_header (
    voucher_header_id bigint NOT NULL,
    voucher_number character varying(20),
    voucher_date date,
    tipe integer,
    keterangan character varying(200),
    coa_id_cash_bank integer,
    coa_no character varying(20),
    update_by integer,
    update_date date
);
 (   DROP TABLE public.beone_voucher_header;
       public         postgres    false    3            "           1259    355174 *   beone_voucher_header_voucher_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_voucher_header_voucher_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_voucher_header_voucher_header_id_seq;
       public       postgres    false    289    3            !           0    0 *   beone_voucher_header_voucher_header_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_voucher_header_voucher_header_id_seq OWNED BY public.beone_voucher_header.voucher_header_id;
            public       postgres    false    290            �           2604    355176    beone_adjustment adjustment_id    DEFAULT     �   ALTER TABLE ONLY public.beone_adjustment ALTER COLUMN adjustment_id SET DEFAULT nextval('public.beone_adjustment_adjustment_id_seq'::regclass);
 M   ALTER TABLE public.beone_adjustment ALTER COLUMN adjustment_id DROP DEFAULT;
       public       postgres    false    197    196            �           2604    355177    beone_coa coa_id    DEFAULT     t   ALTER TABLE ONLY public.beone_coa ALTER COLUMN coa_id SET DEFAULT nextval('public.beone_coa_coa_id_seq'::regclass);
 ?   ALTER TABLE public.beone_coa ALTER COLUMN coa_id DROP DEFAULT;
       public       postgres    false    199    198            �           2604    355178    beone_coa_jurnal coa_jurnal_id    DEFAULT     �   ALTER TABLE ONLY public.beone_coa_jurnal ALTER COLUMN coa_jurnal_id SET DEFAULT nextval('public.beone_coa_jurnal_coa_jurnal_id_seq'::regclass);
 M   ALTER TABLE public.beone_coa_jurnal ALTER COLUMN coa_jurnal_id DROP DEFAULT;
       public       postgres    false    201    200            �           2604    355179    beone_country country_id    DEFAULT     �   ALTER TABLE ONLY public.beone_country ALTER COLUMN country_id SET DEFAULT nextval('public.beone_country_country_id_seq'::regclass);
 G   ALTER TABLE public.beone_country ALTER COLUMN country_id DROP DEFAULT;
       public       postgres    false    203    202            �           2604    355180    beone_custsup custsup_id    DEFAULT     �   ALTER TABLE ONLY public.beone_custsup ALTER COLUMN custsup_id SET DEFAULT nextval('public.beone_custsup_custsup_id_seq'::regclass);
 G   ALTER TABLE public.beone_custsup ALTER COLUMN custsup_id DROP DEFAULT;
       public       postgres    false    205    204            �           2604    355181 $   beone_export_detail export_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_export_detail ALTER COLUMN export_detail_id SET DEFAULT nextval('public.beone_export_detail_export_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_export_detail ALTER COLUMN export_detail_id DROP DEFAULT;
       public       postgres    false    207    206            �           2604    355182 $   beone_export_header export_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_export_header ALTER COLUMN export_header_id SET DEFAULT nextval('public.beone_export_header_export_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_export_header ALTER COLUMN export_header_id DROP DEFAULT;
       public       postgres    false    209    208            �           2604    355183    beone_gl gl_id    DEFAULT     p   ALTER TABLE ONLY public.beone_gl ALTER COLUMN gl_id SET DEFAULT nextval('public.beone_gl_gl_id_seq'::regclass);
 =   ALTER TABLE public.beone_gl ALTER COLUMN gl_id DROP DEFAULT;
       public       postgres    false    211    210            �           2604    355184    beone_gudang gudang_id    DEFAULT     �   ALTER TABLE ONLY public.beone_gudang ALTER COLUMN gudang_id SET DEFAULT nextval('public.beone_gudang_gudang_id_seq'::regclass);
 E   ALTER TABLE public.beone_gudang ALTER COLUMN gudang_id DROP DEFAULT;
       public       postgres    false    215    212            �           2604    355185 $   beone_gudang_detail gudang_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_gudang_detail ALTER COLUMN gudang_detail_id SET DEFAULT nextval('public.beone_gudang_detail_gudang_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_gudang_detail ALTER COLUMN gudang_detail_id DROP DEFAULT;
       public       postgres    false    214    213            �           2604    355186 &   beone_hutang_piutang hutang_piutang_id    DEFAULT     �   ALTER TABLE ONLY public.beone_hutang_piutang ALTER COLUMN hutang_piutang_id SET DEFAULT nextval('public.beone_hutang_piutang_hutang_piutang_id_seq'::regclass);
 U   ALTER TABLE public.beone_hutang_piutang ALTER COLUMN hutang_piutang_id DROP DEFAULT;
       public       postgres    false    219    218            �           2604    355187 $   beone_import_detail import_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_import_detail ALTER COLUMN import_detail_id SET DEFAULT nextval('public.beone_import_detail_import_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_import_detail ALTER COLUMN import_detail_id DROP DEFAULT;
       public       postgres    false    221    220            �           2604    355188 $   beone_import_header import_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_import_header ALTER COLUMN import_header_id SET DEFAULT nextval('public.beone_import_header_import_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_import_header ALTER COLUMN import_header_id DROP DEFAULT;
       public       postgres    false    223    222            �           2604    355189     beone_inventory intvent_trans_id    DEFAULT     �   ALTER TABLE ONLY public.beone_inventory ALTER COLUMN intvent_trans_id SET DEFAULT nextval('public.beone_inventory_intvent_trans_id_seq'::regclass);
 O   ALTER TABLE public.beone_inventory ALTER COLUMN intvent_trans_id DROP DEFAULT;
       public       postgres    false    225    224            �           2604    355190    beone_item item_id    DEFAULT     x   ALTER TABLE ONLY public.beone_item ALTER COLUMN item_id SET DEFAULT nextval('public.beone_item_item_id_seq'::regclass);
 A   ALTER TABLE public.beone_item ALTER COLUMN item_id DROP DEFAULT;
       public       postgres    false    227    226            �           2604    355191    beone_item_type item_type_id    DEFAULT     �   ALTER TABLE ONLY public.beone_item_type ALTER COLUMN item_type_id SET DEFAULT nextval('public.beone_item_type_item_type_id_seq'::regclass);
 K   ALTER TABLE public.beone_item_type ALTER COLUMN item_type_id DROP DEFAULT;
       public       postgres    false    229    228            �           2604    355192    beone_kode_trans kode_trans_id    DEFAULT     �   ALTER TABLE ONLY public.beone_kode_trans ALTER COLUMN kode_trans_id SET DEFAULT nextval('public.beone_kode_trans_kode_trans_id_seq'::regclass);
 M   ALTER TABLE public.beone_kode_trans ALTER COLUMN kode_trans_id DROP DEFAULT;
       public       postgres    false    231    230            �           2604    355193    beone_log log_id    DEFAULT     t   ALTER TABLE ONLY public.beone_log ALTER COLUMN log_id SET DEFAULT nextval('public.beone_log_log_id_seq'::regclass);
 ?   ALTER TABLE public.beone_log ALTER COLUMN log_id DROP DEFAULT;
       public       postgres    false    234    233            �           2604    355194 $   beone_opname_detail opname_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_opname_detail ALTER COLUMN opname_detail_id SET DEFAULT nextval('public.beone_opname_detail_opname_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_opname_detail ALTER COLUMN opname_detail_id DROP DEFAULT;
       public       postgres    false    236    235            �           2604    355195 $   beone_opname_header opname_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_opname_header ALTER COLUMN opname_header_id SET DEFAULT nextval('public.beone_opname_header_opname_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_opname_header ALTER COLUMN opname_header_id DROP DEFAULT;
       public       postgres    false    238    237            �           2604    355196    beone_peleburan peleburan_id    DEFAULT     �   ALTER TABLE ONLY public.beone_peleburan ALTER COLUMN peleburan_id SET DEFAULT nextval('public.beone_peleburan_peleburan_id_seq'::regclass);
 K   ALTER TABLE public.beone_peleburan ALTER COLUMN peleburan_id DROP DEFAULT;
       public       postgres    false    240    239            �           2604    355197 0   beone_pemakaian_bahan_detail pemakaian_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_detail ALTER COLUMN pemakaian_detail_id SET DEFAULT nextval('public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq'::regclass);
 _   ALTER TABLE public.beone_pemakaian_bahan_detail ALTER COLUMN pemakaian_detail_id DROP DEFAULT;
       public       postgres    false    242    241            �           2604    355198 0   beone_pemakaian_bahan_header pemakaian_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_header ALTER COLUMN pemakaian_header_id SET DEFAULT nextval('public.beone_header_pemakaian_bahan_pemakaian_header_id_seq'::regclass);
 _   ALTER TABLE public.beone_pemakaian_bahan_header ALTER COLUMN pemakaian_header_id DROP DEFAULT;
       public       postgres    false    217    216            �           2604    355199    beone_pm_header pm_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_pm_header ALTER COLUMN pm_header_id SET DEFAULT nextval('public.beone_pm_header_pm_header_id_seq'::regclass);
 K   ALTER TABLE public.beone_pm_header ALTER COLUMN pm_header_id DROP DEFAULT;
       public       postgres    false    244    243            �           2604    355200 )   beone_po_import_detail purchase_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_po_import_detail ALTER COLUMN purchase_detail_id SET DEFAULT nextval('public.beone_po_import_detail_purchase_detail_id_seq'::regclass);
 X   ALTER TABLE public.beone_po_import_detail ALTER COLUMN purchase_detail_id DROP DEFAULT;
       public       postgres    false    246    245            �           2604    355201 )   beone_po_import_header purchase_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_po_import_header ALTER COLUMN purchase_header_id SET DEFAULT nextval('public.beone_po_import_header_purchase_header_id_seq'::regclass);
 X   ALTER TABLE public.beone_po_import_header ALTER COLUMN purchase_header_id DROP DEFAULT;
       public       postgres    false    248    247            �           2604    355202 (   beone_produksi_detail produksi_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_produksi_detail ALTER COLUMN produksi_detail_id SET DEFAULT nextval('public.beone_produksi_detail_produksi_detail_id_seq'::regclass);
 W   ALTER TABLE public.beone_produksi_detail ALTER COLUMN produksi_detail_id DROP DEFAULT;
       public       postgres    false    250    249            �           2604    355203 (   beone_produksi_header produksi_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_produksi_header ALTER COLUMN produksi_header_id SET DEFAULT nextval('public.beone_produksi_header_produksi_header_id_seq'::regclass);
 W   ALTER TABLE public.beone_produksi_header ALTER COLUMN produksi_header_id DROP DEFAULT;
       public       postgres    false    252    251            �           2604    355204 (   beone_purchase_detail purchase_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_purchase_detail ALTER COLUMN purchase_detail_id SET DEFAULT nextval('public.beone_purchase_detail_purchase_detail_id_seq'::regclass);
 W   ALTER TABLE public.beone_purchase_detail ALTER COLUMN purchase_detail_id DROP DEFAULT;
       public       postgres    false    254    253            �           2604    355205 (   beone_purchase_header purchase_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_purchase_header ALTER COLUMN purchase_header_id SET DEFAULT nextval('public.beone_purchase_header_purchase_header_id_seq'::regclass);
 W   ALTER TABLE public.beone_purchase_header ALTER COLUMN purchase_header_id DROP DEFAULT;
       public       postgres    false    256    255            �           2604    355206 !   beone_received_import received_id    DEFAULT     �   ALTER TABLE ONLY public.beone_received_import ALTER COLUMN received_id SET DEFAULT nextval('public.beone_received_import_received_id_seq'::regclass);
 P   ALTER TABLE public.beone_received_import ALTER COLUMN received_id DROP DEFAULT;
       public       postgres    false    258    257            �           2604    355207    beone_role role_id    DEFAULT     x   ALTER TABLE ONLY public.beone_role ALTER COLUMN role_id SET DEFAULT nextval('public.beone_role_role_id_seq'::regclass);
 A   ALTER TABLE public.beone_role ALTER COLUMN role_id DROP DEFAULT;
       public       postgres    false    260    259            �           2604    355208    beone_role_user role_id    DEFAULT     �   ALTER TABLE ONLY public.beone_role_user ALTER COLUMN role_id SET DEFAULT nextval('public.beone_role_user_role_id_seq'::regclass);
 F   ALTER TABLE public.beone_role_user ALTER COLUMN role_id DROP DEFAULT;
       public       postgres    false    262    261            �           2604    355209 "   beone_sales_detail sales_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_sales_detail ALTER COLUMN sales_detail_id SET DEFAULT nextval('public.beone_sales_detail_sales_detail_id_seq'::regclass);
 Q   ALTER TABLE public.beone_sales_detail ALTER COLUMN sales_detail_id DROP DEFAULT;
       public       postgres    false    264    263            �           2604    355210 "   beone_sales_header sales_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_sales_header ALTER COLUMN sales_header_id SET DEFAULT nextval('public.beone_sales_header_sales_header_id_seq'::regclass);
 Q   ALTER TABLE public.beone_sales_header ALTER COLUMN sales_header_id DROP DEFAULT;
       public       postgres    false    266    265            �           2604    355211    beone_satuan_item satuan_id    DEFAULT     �   ALTER TABLE ONLY public.beone_satuan_item ALTER COLUMN satuan_id SET DEFAULT nextval('public.beone_satuan_item_satuan_id_seq'::regclass);
 J   ALTER TABLE public.beone_satuan_item ALTER COLUMN satuan_id DROP DEFAULT;
       public       postgres    false    268    267            �           2604    355212 *   beone_subkon_in_detail subkon_in_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_subkon_in_detail ALTER COLUMN subkon_in_detail_id SET DEFAULT nextval('public.beone_subkon_in_detail_subkon_in_detail_id_seq'::regclass);
 Y   ALTER TABLE public.beone_subkon_in_detail ALTER COLUMN subkon_in_detail_id DROP DEFAULT;
       public       postgres    false    270    269            �           2604    355213 #   beone_subkon_in_header subkon_in_id    DEFAULT     �   ALTER TABLE ONLY public.beone_subkon_in_header ALTER COLUMN subkon_in_id SET DEFAULT nextval('public.beone_subkon_in_header_subkon_in_id_seq'::regclass);
 R   ALTER TABLE public.beone_subkon_in_header ALTER COLUMN subkon_in_id DROP DEFAULT;
       public       postgres    false    272    271            �           2604    355214 ,   beone_subkon_out_detail subkon_out_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_subkon_out_detail ALTER COLUMN subkon_out_detail_id SET DEFAULT nextval('public.beone_subkon_out_detail_subkon_out_detail_id_seq'::regclass);
 [   ALTER TABLE public.beone_subkon_out_detail ALTER COLUMN subkon_out_detail_id DROP DEFAULT;
       public       postgres    false    274    273            �           2604    355215 %   beone_subkon_out_header subkon_out_id    DEFAULT     �   ALTER TABLE ONLY public.beone_subkon_out_header ALTER COLUMN subkon_out_id SET DEFAULT nextval('public.beone_subkon_out_header_subkon_out_id_seq'::regclass);
 T   ALTER TABLE public.beone_subkon_out_header ALTER COLUMN subkon_out_id DROP DEFAULT;
       public       postgres    false    276    275            �           2604    355216    beone_tipe_coa tipe_coa_id    DEFAULT     �   ALTER TABLE ONLY public.beone_tipe_coa ALTER COLUMN tipe_coa_id SET DEFAULT nextval('public.beone_tipe_coa_tipe_coa_id_seq'::regclass);
 I   ALTER TABLE public.beone_tipe_coa ALTER COLUMN tipe_coa_id DROP DEFAULT;
       public       postgres    false    278    277            �           2604    355217 *   beone_tipe_coa_transaksi tipe_coa_trans_id    DEFAULT     �   ALTER TABLE ONLY public.beone_tipe_coa_transaksi ALTER COLUMN tipe_coa_trans_id SET DEFAULT nextval('public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq'::regclass);
 Y   ALTER TABLE public.beone_tipe_coa_transaksi ALTER COLUMN tipe_coa_trans_id DROP DEFAULT;
       public       postgres    false    280    279            �           2604    355218 &   beone_transfer_stock transfer_stock_id    DEFAULT     �   ALTER TABLE ONLY public.beone_transfer_stock ALTER COLUMN transfer_stock_id SET DEFAULT nextval('public.beone_transfer_stock_transfer_stock_id_seq'::regclass);
 U   ALTER TABLE public.beone_transfer_stock ALTER COLUMN transfer_stock_id DROP DEFAULT;
       public       postgres    false    284    281            �           2604    355219 4   beone_transfer_stock_detail transfer_stock_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_transfer_stock_detail ALTER COLUMN transfer_stock_detail_id SET DEFAULT nextval('public.beone_transfer_stock_detail_transfer_stock_detail_id_seq'::regclass);
 c   ALTER TABLE public.beone_transfer_stock_detail ALTER COLUMN transfer_stock_detail_id DROP DEFAULT;
       public       postgres    false    283    282            �           2604    355220    beone_user user_id    DEFAULT     x   ALTER TABLE ONLY public.beone_user ALTER COLUMN user_id SET DEFAULT nextval('public.beone_user_user_id_seq'::regclass);
 A   ALTER TABLE public.beone_user ALTER COLUMN user_id DROP DEFAULT;
       public       postgres    false    286    285            �           2604    355221 &   beone_voucher_detail voucher_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_voucher_detail ALTER COLUMN voucher_detail_id SET DEFAULT nextval('public.beone_voucher_detail_voucher_detail_id_seq'::regclass);
 U   ALTER TABLE public.beone_voucher_detail ALTER COLUMN voucher_detail_id DROP DEFAULT;
       public       postgres    false    288    287            �           2604    355222 &   beone_voucher_header voucher_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_voucher_header ALTER COLUMN voucher_header_id SET DEFAULT nextval('public.beone_voucher_header_voucher_header_id_seq'::regclass);
 U   ALTER TABLE public.beone_voucher_header ALTER COLUMN voucher_header_id DROP DEFAULT;
       public       postgres    false    290    289            �          0    354920    beone_adjustment 
   TABLE DATA               �   COPY public.beone_adjustment (adjustment_id, adjustment_no, adjustment_date, keterangan, item_id, qty_adjustment, posisi_qty, update_by, update_date, flag) FROM stdin;
    public       postgres    false    196   O�      �          0    354925 	   beone_coa 
   TABLE DATA               �   COPY public.beone_coa (coa_id, nama, nomor, tipe_akun, debet_valas, debet_idr, kredit_valas, kredit_idr, dk, tipe_transaksi) FROM stdin;
    public       postgres    false    198   l�      �          0    354933    beone_coa_jurnal 
   TABLE DATA               q   COPY public.beone_coa_jurnal (coa_jurnal_id, nama_coa_jurnal, keterangan_coa_jurnal, coa_id, coa_no) FROM stdin;
    public       postgres    false    200   �       �          0    354941    beone_country 
   TABLE DATA               M   COPY public.beone_country (country_id, nama, country_code, flag) FROM stdin;
    public       postgres    false    202   �      �          0    354946    beone_custsup 
   TABLE DATA               4  COPY public.beone_custsup (custsup_id, nama, alamat, tipe_custsup, negara, saldo_hutang_idr, saldo_hutang_valas, saldo_piutang_idr, saldo_piutang_valas, flag, pelunasan_hutang_idr, pelunasan_hutang_valas, pelunasan_piutang_idr, pelunasan_piutang_valas, status_lunas_piutang, status_lunas_hutang) FROM stdin;
    public       postgres    false    204   F      �          0    354951    beone_export_detail 
   TABLE DATA               �   COPY public.beone_export_detail (export_detail_id, export_header_id, item_id, qty, satuan_qty, price, pack_qty, satuan_pack, origin_country, doc, volume, netto, brutto, flag) FROM stdin;
    public       postgres    false    206   �      �          0    354956    beone_export_header 
   TABLE DATA               �  COPY public.beone_export_header (export_header_id, jenis_bc, car_no, bc_no, bc_date, kontrak_no, kontrak_date, jenis_export, invoice_no, invoice_date, surat_jalan_no, surat_jalan_date, receiver_id, country_id, price_type, amount_value, valas_value, insurance_type, insurance_value, freight, flag, status, delivery_date, update_by, update_date, delivery_no, vessel, port_loading, port_destination, container, no_container, no_seal) FROM stdin;
    public       postgres    false    208         �          0    354964    beone_gl 
   TABLE DATA               �   COPY public.beone_gl (gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date) FROM stdin;
    public       postgres    false    210   �      �          0    354969    beone_gudang 
   TABLE DATA               I   COPY public.beone_gudang (gudang_id, nama, keterangan, flag) FROM stdin;
    public       postgres    false    212   4      �          0    354972    beone_gudang_detail 
   TABLE DATA               �   COPY public.beone_gudang_detail (gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan, kode_tracing) FROM stdin;
    public       postgres    false    213   �      �          0    354984    beone_hutang_piutang 
   TABLE DATA               �   COPY public.beone_hutang_piutang (hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas) FROM stdin;
    public       postgres    false    218   "       �          0    354989    beone_import_detail 
   TABLE DATA                 COPY public.beone_import_detail (import_detail_id, item_id, qty, satuan_qty, price, pack_qty, satuan_pack, origin_country, volume, netto, brutto, hscode, tbm, ppnn, tpbm, cukai, sat_cukai, cukai_value, bea_masuk, sat_bea_masuk, flag, item_type_id, import_header_id) FROM stdin;
    public       postgres    false    220   �       �          0    354994    beone_import_header 
   TABLE DATA               ]  COPY public.beone_import_header (import_header_id, jenis_bc, car_no, bc_no, bc_date, invoice_no, invoice_date, kontrak_no, kontrak_date, purpose_id, supplier_id, price_type, "from", amount_value, valas_value, value_added, discount, insurance_type, insurace_value, freight, flag, update_by, update_date, status, receive_date, receive_no) FROM stdin;
    public       postgres    false    222   �       �          0    354999    beone_inventory 
   TABLE DATA               �   COPY public.beone_inventory (intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) FROM stdin;
    public       postgres    false    224   !      �          0    355004 
   beone_item 
   TABLE DATA               �   COPY public.beone_item (item_id, nama, item_code, saldo_qty, saldo_idr, keterangan, flag, item_type_id, hscode, satuan_id) FROM stdin;
    public       postgres    false    226   �#      �          0    355009    beone_item_type 
   TABLE DATA               O   COPY public.beone_item_type (item_type_id, nama, keterangan, flag) FROM stdin;
    public       postgres    false    228   h(      �          0    355014    beone_kode_trans 
   TABLE DATA               `   COPY public.beone_kode_trans (kode_trans_id, nama, coa_id, kode_bank, in_out, flag) FROM stdin;
    public       postgres    false    230   E)      �          0    355022    beone_konfigurasi_perusahaan 
   TABLE DATA               �   COPY public.beone_konfigurasi_perusahaan (nama_perusahaan, alamat_perusahaan, kota_perusahaan, provinsi_perusahaan, kode_pos_perusahaan, logo_perusahaan) FROM stdin;
    public       postgres    false    232    *      �          0    355028 	   beone_log 
   TABLE DATA               S   COPY public.beone_log (log_id, log_user, log_tipe, log_desc, log_time) FROM stdin;
    public       postgres    false    233   Q*      �          0    355033    beone_opname_detail 
   TABLE DATA               �   COPY public.beone_opname_detail (opname_detail_id, opname_header_id, item_id, qty_existing, qty_opname, qty_selisih, status_opname, flag) FROM stdin;
    public       postgres    false    235   l,      �          0    355038    beone_opname_header 
   TABLE DATA               �   COPY public.beone_opname_header (opname_header_id, document_no, opname_date, total_item_opname, total_item_opname_match, total_item_opname_plus, total_item_opname_min, update_by, flag, keterangan, update_date) FROM stdin;
    public       postgres    false    237   �,      �          0    355043    beone_peleburan 
   TABLE DATA               �   COPY public.beone_peleburan (peleburan_id, peleburan_no, peleburan_date, keterangan, item_id, qty_peleburan, update_by, update_date, flag) FROM stdin;
    public       postgres    false    239   �,      �          0    355048    beone_pemakaian_bahan_detail 
   TABLE DATA               z   COPY public.beone_pemakaian_bahan_detail (pemakaian_detail_id, pemakaian_header_id, item_id, qty, unit_price) FROM stdin;
    public       postgres    false    241   �,      �          0    354979    beone_pemakaian_bahan_header 
   TABLE DATA               �   COPY public.beone_pemakaian_bahan_header (pemakaian_header_id, nomor_pemakaian, keterangan, tgl_pemakaian, update_by, update_date, nomor_pm, amount_pemakaian, status) FROM stdin;
    public       postgres    false    216   s-      �          0    355053    beone_pm_header 
   TABLE DATA               t   COPY public.beone_pm_header (pm_header_id, no_pm, tgl_pm, customer_id, qty, keterangan_artikel, status) FROM stdin;
    public       postgres    false    243   }.      �          0    355061    beone_po_import_detail 
   TABLE DATA               {   COPY public.beone_po_import_detail (purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag) FROM stdin;
    public       postgres    false    245   �.      �          0    355066    beone_po_import_header 
   TABLE DATA               �   COPY public.beone_po_import_header (purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, grandtotal, flag, update_by, update_date) FROM stdin;
    public       postgres    false    247   9/      �          0    355071    beone_produksi_detail 
   TABLE DATA               l   COPY public.beone_produksi_detail (produksi_detail_id, produksi_header_id, pemakaian_header_id) FROM stdin;
    public       postgres    false    249   �/      �          0    355076    beone_produksi_header 
   TABLE DATA               �   COPY public.beone_produksi_header (produksi_header_id, nomor_produksi, tgl_produksi, tipe_produksi, item_produksi, qty_hasil_produksi, keterangan, flag, updated_by, udpated_date) FROM stdin;
    public       postgres    false    251   �0      �          0    355081    beone_purchase_detail 
   TABLE DATA               z   COPY public.beone_purchase_detail (purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag) FROM stdin;
    public       postgres    false    253   s3      �          0    355086    beone_purchase_header 
   TABLE DATA               �   COPY public.beone_purchase_header (purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, update_by, update_date, flag, ppn, subtotal, ppn_value, grandtotal) FROM stdin;
    public       postgres    false    255   �3      �          0    355091    beone_received_import 
   TABLE DATA               �   COPY public.beone_received_import (received_id, nomor_aju, nomor_dokumen_pabean, tanggal_received, status_received, flag, update_by, update_date, nomor_received, tpb_header_id, kurs, po_import_header_id) FROM stdin;
    public       postgres    false    257   �3      �          0    355096 
   beone_role 
   TABLE DATA               	  COPY public.beone_role (role_id, nama_role, keterangan, master_menu, pembelian_menu, penjualan_menu, inventory_menu, produksi_menu, asset_menu, jurnal_umum_menu, kasbank_menu, laporan_inventory, laporan_keuangan, konfigurasi, import_menu, eksport_menu) FROM stdin;
    public       postgres    false    259   ;4      �          0    355101    beone_role_user 
   TABLE DATA               �  COPY public.beone_role_user (role_id, nama_role, keterangan, master_menu, user_add, user_edit, user_delete, role_add, role_edit, role_delete, customer_add, customer_edit, customer_delete, supplier_add, supplier_edit, supplier_delete, item_add, item_edit, item_delete, jenis_add, jenis_edit, jenis_delete, satuan_add, satuan_edit, satuan_delete, gudang_add, gudang_edit, gudang_delete, master_import, po_add, po_edit, po_delete, tracing, terima_barang, master_pembelian, pembelian_add, pembelian_edit, pembelian_delete, kredit_note_add, kredit_note_edit, kredit_note_delete, master_eksport, eksport_add, eksport_edit, eksport_delete, kirim_barang, menu_penjualan, penjualan_add, penjualan_edit, penjualan_delete, debit_note_add, debit_note_edit, debit_note_delete, menu_inventory, pindah_gudang, stockopname_add, stockopname_edit, stockopname_delete, stockopname_opname, adjustment_add, adjustment_edit, adjustment_delete, pemusnahan_add, pemusnahan_edit, pemusnahan_delete, recal_inventory, menu_produksi, produksi_add, produksi_edit, produksi_delete, menu_asset, menu_jurnal_umum, jurnal_umum_add, jurnal_umum_edit, jurnal_umum_delete, menu_kas_bank, kas_bank_add, kas_bank_edit, kas_bank_delete, menu_laporan_inventory, menu_laporan_keuangan, menu_konfigurasi) FROM stdin;
    public       postgres    false    261   �4      �          0    355106    beone_sales_detail 
   TABLE DATA               q   COPY public.beone_sales_detail (sales_detail_id, sales_header_id, item_id, qty, price, amount, flag) FROM stdin;
    public       postgres    false    263   �4      �          0    355111    beone_sales_header 
   TABLE DATA               �   COPY public.beone_sales_header (sales_header_id, sales_no, trans_date, customer_id, keterangan, ppn, subtotal, ppn_value, grandtotal, update_by, update_date, flag, biaya, status) FROM stdin;
    public       postgres    false    265   
5      �          0    355116    beone_satuan_item 
   TABLE DATA               U   COPY public.beone_satuan_item (satuan_id, satuan_code, keterangan, flag) FROM stdin;
    public       postgres    false    267   '5      �          0    355121    beone_subkon_in_detail 
   TABLE DATA               n   COPY public.beone_subkon_in_detail (subkon_in_detail_id, subkon_in_header_id, item_id, qty, flag) FROM stdin;
    public       postgres    false    269   6      �          0    355126    beone_subkon_in_header 
   TABLE DATA               �   COPY public.beone_subkon_in_header (subkon_in_id, no_subkon_in, no_subkon_out, trans_date, keterangan, nomor_aju, biaya_subkon, supplier_id, flag, update_by, update_date) FROM stdin;
    public       postgres    false    271   86      �          0    355131    beone_subkon_out_detail 
   TABLE DATA               }   COPY public.beone_subkon_out_detail (subkon_out_detail_id, subkon_out_header_id, item_id, qty, unit_price, flag) FROM stdin;
    public       postgres    false    273   U6      �          0    355136    beone_subkon_out_header 
   TABLE DATA               �   COPY public.beone_subkon_out_header (subkon_out_id, no_subkon_out, trans_date, supplier_id, keterangan, nomor_aju, flag, update_by, update_date) FROM stdin;
    public       postgres    false    275   r6      �          0    355141    beone_tipe_coa 
   TABLE DATA               ;   COPY public.beone_tipe_coa (tipe_coa_id, nama) FROM stdin;
    public       postgres    false    277   �6      �          0    355146    beone_tipe_coa_transaksi 
   TABLE DATA               Q   COPY public.beone_tipe_coa_transaksi (tipe_coa_trans_id, nama, flag) FROM stdin;
    public       postgres    false    279   �6      �          0    355151    beone_transfer_stock 
   TABLE DATA               �   COPY public.beone_transfer_stock (transfer_stock_id, transfer_no, transfer_date, coa_kode_biaya, keterangan, update_by, update_date, flag) FROM stdin;
    public       postgres    false    281   �7      �          0    355154    beone_transfer_stock_detail 
   TABLE DATA               �   COPY public.beone_transfer_stock_detail (transfer_stock_detail_id, transfer_stock_header_id, tipe_transfer_stock, item_id, qty, gudang_id, biaya, update_by, update_date, flag, persen_produksi) FROM stdin;
    public       postgres    false    282   �7      �          0    355161 
   beone_user 
   TABLE DATA               n   COPY public.beone_user (user_id, username, password, nama, role_id, update_by, update_date, flag) FROM stdin;
    public       postgres    false    285   8      �          0    355166    beone_voucher_detail 
   TABLE DATA               �   COPY public.beone_voucher_detail (voucher_detail_id, voucher_header_id, coa_id_lawan, coa_no_lawan, jumlah_valas, kurs, jumlah_idr, keterangan_detail) FROM stdin;
    public       postgres    false    287   �9      �          0    355171    beone_voucher_header 
   TABLE DATA               �   COPY public.beone_voucher_header (voucher_header_id, voucher_number, voucher_date, tipe, keterangan, coa_id_cash_bank, coa_no, update_by, update_date) FROM stdin;
    public       postgres    false    289   `>      "           0    0 "   beone_adjustment_adjustment_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.beone_adjustment_adjustment_id_seq', 10, true);
            public       postgres    false    197            #           0    0    beone_coa_coa_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.beone_coa_coa_id_seq', 1, false);
            public       postgres    false    199            $           0    0 "   beone_coa_jurnal_coa_jurnal_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.beone_coa_jurnal_coa_jurnal_id_seq', 12, true);
            public       postgres    false    201            %           0    0    beone_country_country_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.beone_country_country_id_seq', 3, true);
            public       postgres    false    203            &           0    0    beone_custsup_custsup_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.beone_custsup_custsup_id_seq', 34, true);
            public       postgres    false    205            '           0    0 (   beone_export_detail_export_detail_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_export_detail_export_detail_id_seq', 38, true);
            public       postgres    false    207            (           0    0 (   beone_export_header_export_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_export_header_export_header_id_seq', 33, true);
            public       postgres    false    209            )           0    0    beone_gl_gl_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.beone_gl_gl_id_seq', 2522, true);
            public       postgres    false    211            *           0    0 (   beone_gudang_detail_gudang_detail_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.beone_gudang_detail_gudang_detail_id_seq', 6203, true);
            public       postgres    false    214            +           0    0    beone_gudang_gudang_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.beone_gudang_gudang_id_seq', 6, true);
            public       postgres    false    215            ,           0    0 4   beone_header_pemakaian_bahan_pemakaian_header_id_seq    SEQUENCE SET     c   SELECT pg_catalog.setval('public.beone_header_pemakaian_bahan_pemakaian_header_id_seq', 42, true);
            public       postgres    false    217            -           0    0 *   beone_hutang_piutang_hutang_piutang_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.beone_hutang_piutang_hutang_piutang_id_seq', 432, true);
            public       postgres    false    219            .           0    0 (   beone_import_detail_import_detail_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_import_detail_import_detail_id_seq', 36, true);
            public       postgres    false    221            /           0    0 (   beone_import_header_import_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_import_header_import_header_id_seq', 30, true);
            public       postgres    false    223            0           0    0 $   beone_inventory_intvent_trans_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_inventory_intvent_trans_id_seq', 1207, true);
            public       postgres    false    225            1           0    0    beone_item_item_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.beone_item_item_id_seq', 6001, true);
            public       postgres    false    227            2           0    0     beone_item_type_item_type_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.beone_item_type_item_type_id_seq', 16, true);
            public       postgres    false    229            3           0    0 "   beone_kode_trans_kode_trans_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.beone_kode_trans_kode_trans_id_seq', 25, true);
            public       postgres    false    231            4           0    0    beone_log_log_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.beone_log_log_id_seq', 406, true);
            public       postgres    false    234            5           0    0 (   beone_opname_detail_opname_detail_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.beone_opname_detail_opname_detail_id_seq', 6037, true);
            public       postgres    false    236            6           0    0 (   beone_opname_header_opname_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_opname_header_opname_header_id_seq', 13, true);
            public       postgres    false    238            7           0    0     beone_peleburan_peleburan_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.beone_peleburan_peleburan_id_seq', 6, true);
            public       postgres    false    240            8           0    0 4   beone_pemakaian_bahan_detail_pemakaian_detail_id_seq    SEQUENCE SET     c   SELECT pg_catalog.setval('public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq', 60, true);
            public       postgres    false    242            9           0    0     beone_pm_header_pm_header_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.beone_pm_header_pm_header_id_seq', 14, true);
            public       postgres    false    244            :           0    0 -   beone_po_import_detail_purchase_detail_id_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public.beone_po_import_detail_purchase_detail_id_seq', 102, true);
            public       postgres    false    246            ;           0    0 -   beone_po_import_header_purchase_header_id_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public.beone_po_import_header_purchase_header_id_seq', 100, true);
            public       postgres    false    248            <           0    0 ,   beone_produksi_detail_produksi_detail_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_produksi_detail_produksi_detail_id_seq', 55, true);
            public       postgres    false    250            =           0    0 ,   beone_produksi_header_produksi_header_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_produksi_header_produksi_header_id_seq', 53, true);
            public       postgres    false    252            >           0    0 ,   beone_purchase_detail_purchase_detail_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_purchase_detail_purchase_detail_id_seq', 96, true);
            public       postgres    false    254            ?           0    0 ,   beone_purchase_header_purchase_header_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_purchase_header_purchase_header_id_seq', 65, true);
            public       postgres    false    256            @           0    0 %   beone_received_import_received_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_received_import_received_id_seq', 221, true);
            public       postgres    false    258            A           0    0    beone_role_role_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.beone_role_role_id_seq', 6, true);
            public       postgres    false    260            B           0    0    beone_role_user_role_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.beone_role_user_role_id_seq', 3, true);
            public       postgres    false    262            C           0    0 &   beone_sales_detail_sales_detail_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_sales_detail_sales_detail_id_seq', 24, true);
            public       postgres    false    264            D           0    0 &   beone_sales_header_sales_header_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_sales_header_sales_header_id_seq', 18, true);
            public       postgres    false    266            E           0    0    beone_satuan_item_satuan_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.beone_satuan_item_satuan_id_seq', 3, true);
            public       postgres    false    268            F           0    0 .   beone_subkon_in_detail_subkon_in_detail_id_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public.beone_subkon_in_detail_subkon_in_detail_id_seq', 8, true);
            public       postgres    false    270            G           0    0 '   beone_subkon_in_header_subkon_in_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_subkon_in_header_subkon_in_id_seq', 4, true);
            public       postgres    false    272            H           0    0 0   beone_subkon_out_detail_subkon_out_detail_id_seq    SEQUENCE SET     _   SELECT pg_catalog.setval('public.beone_subkon_out_detail_subkon_out_detail_id_seq', 37, true);
            public       postgres    false    274            I           0    0 )   beone_subkon_out_header_subkon_out_id_seq    SEQUENCE SET     X   SELECT pg_catalog.setval('public.beone_subkon_out_header_subkon_out_id_seq', 18, true);
            public       postgres    false    276            J           0    0    beone_tipe_coa_tipe_coa_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.beone_tipe_coa_tipe_coa_id_seq', 1, false);
            public       postgres    false    278            K           0    0 .   beone_tipe_coa_transaksi_tipe_coa_trans_id_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq', 20, true);
            public       postgres    false    280            L           0    0 8   beone_transfer_stock_detail_transfer_stock_detail_id_seq    SEQUENCE SET     h   SELECT pg_catalog.setval('public.beone_transfer_stock_detail_transfer_stock_detail_id_seq', 644, true);
            public       postgres    false    283            M           0    0 *   beone_transfer_stock_transfer_stock_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.beone_transfer_stock_transfer_stock_id_seq', 294, true);
            public       postgres    false    284            N           0    0    beone_user_user_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.beone_user_user_id_seq', 22, true);
            public       postgres    false    286            O           0    0 *   beone_voucher_detail_voucher_detail_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.beone_voucher_detail_voucher_detail_id_seq', 215, true);
            public       postgres    false    288            P           0    0 *   beone_voucher_header_voucher_header_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.beone_voucher_header_voucher_header_id_seq', 142, true);
            public       postgres    false    290            �           2606    355224 &   beone_adjustment beone_adjustment_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.beone_adjustment
    ADD CONSTRAINT beone_adjustment_pkey PRIMARY KEY (adjustment_id);
 P   ALTER TABLE ONLY public.beone_adjustment DROP CONSTRAINT beone_adjustment_pkey;
       public         postgres    false    196            �           2606    355226 &   beone_coa_jurnal beone_coa_jurnal_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.beone_coa_jurnal
    ADD CONSTRAINT beone_coa_jurnal_pkey PRIMARY KEY (coa_jurnal_id);
 P   ALTER TABLE ONLY public.beone_coa_jurnal DROP CONSTRAINT beone_coa_jurnal_pkey;
       public         postgres    false    200            �           2606    355228    beone_coa beone_coa_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.beone_coa
    ADD CONSTRAINT beone_coa_pkey PRIMARY KEY (coa_id);
 B   ALTER TABLE ONLY public.beone_coa DROP CONSTRAINT beone_coa_pkey;
       public         postgres    false    198            �           2606    355230     beone_country beone_country_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.beone_country
    ADD CONSTRAINT beone_country_pkey PRIMARY KEY (country_id);
 J   ALTER TABLE ONLY public.beone_country DROP CONSTRAINT beone_country_pkey;
       public         postgres    false    202            �           2606    355232 ,   beone_export_detail beone_export_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_export_detail
    ADD CONSTRAINT beone_export_detail_pkey PRIMARY KEY (export_detail_id);
 V   ALTER TABLE ONLY public.beone_export_detail DROP CONSTRAINT beone_export_detail_pkey;
       public         postgres    false    206            �           2606    355234    beone_gl beone_gl_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.beone_gl
    ADD CONSTRAINT beone_gl_pkey PRIMARY KEY (gl_id);
 @   ALTER TABLE ONLY public.beone_gl DROP CONSTRAINT beone_gl_pkey;
       public         postgres    false    210            �           2606    355236 ,   beone_gudang_detail beone_gudang_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_gudang_detail
    ADD CONSTRAINT beone_gudang_detail_pkey PRIMARY KEY (gudang_detail_id);
 V   ALTER TABLE ONLY public.beone_gudang_detail DROP CONSTRAINT beone_gudang_detail_pkey;
       public         postgres    false    213            �           2606    355238    beone_gudang beone_gudang_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.beone_gudang
    ADD CONSTRAINT beone_gudang_pkey PRIMARY KEY (gudang_id);
 H   ALTER TABLE ONLY public.beone_gudang DROP CONSTRAINT beone_gudang_pkey;
       public         postgres    false    212            �           2606    355240 >   beone_pemakaian_bahan_header beone_header_pemakaian_bahan_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_header
    ADD CONSTRAINT beone_header_pemakaian_bahan_pkey PRIMARY KEY (pemakaian_header_id);
 h   ALTER TABLE ONLY public.beone_pemakaian_bahan_header DROP CONSTRAINT beone_header_pemakaian_bahan_pkey;
       public         postgres    false    216            �           2606    355242 .   beone_hutang_piutang beone_hutang_piutang_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_hutang_piutang
    ADD CONSTRAINT beone_hutang_piutang_pkey PRIMARY KEY (hutang_piutang_id);
 X   ALTER TABLE ONLY public.beone_hutang_piutang DROP CONSTRAINT beone_hutang_piutang_pkey;
       public         postgres    false    218            �           2606    355244 ,   beone_import_detail beone_import_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_import_detail
    ADD CONSTRAINT beone_import_detail_pkey PRIMARY KEY (import_detail_id);
 V   ALTER TABLE ONLY public.beone_import_detail DROP CONSTRAINT beone_import_detail_pkey;
       public         postgres    false    220            �           2606    355246 ,   beone_import_header beone_import_header_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_import_header
    ADD CONSTRAINT beone_import_header_pkey PRIMARY KEY (import_header_id);
 V   ALTER TABLE ONLY public.beone_import_header DROP CONSTRAINT beone_import_header_pkey;
       public         postgres    false    222            �           2606    355248 $   beone_inventory beone_inventory_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.beone_inventory
    ADD CONSTRAINT beone_inventory_pkey PRIMARY KEY (intvent_trans_id);
 N   ALTER TABLE ONLY public.beone_inventory DROP CONSTRAINT beone_inventory_pkey;
       public         postgres    false    224            �           2606    355250    beone_item beone_item_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.beone_item
    ADD CONSTRAINT beone_item_pkey PRIMARY KEY (item_id);
 D   ALTER TABLE ONLY public.beone_item DROP CONSTRAINT beone_item_pkey;
       public         postgres    false    226            �           2606    355252 $   beone_item_type beone_item_type_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_item_type
    ADD CONSTRAINT beone_item_type_pkey PRIMARY KEY (item_type_id);
 N   ALTER TABLE ONLY public.beone_item_type DROP CONSTRAINT beone_item_type_pkey;
       public         postgres    false    228            �           2606    355254 &   beone_kode_trans beone_kode_trans_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.beone_kode_trans
    ADD CONSTRAINT beone_kode_trans_pkey PRIMARY KEY (kode_trans_id);
 P   ALTER TABLE ONLY public.beone_kode_trans DROP CONSTRAINT beone_kode_trans_pkey;
       public         postgres    false    230            �           2606    355256    beone_log beone_log_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.beone_log
    ADD CONSTRAINT beone_log_pkey PRIMARY KEY (log_id);
 B   ALTER TABLE ONLY public.beone_log DROP CONSTRAINT beone_log_pkey;
       public         postgres    false    233            �           2606    355258 ,   beone_opname_detail beone_opname_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_opname_detail
    ADD CONSTRAINT beone_opname_detail_pkey PRIMARY KEY (opname_detail_id);
 V   ALTER TABLE ONLY public.beone_opname_detail DROP CONSTRAINT beone_opname_detail_pkey;
       public         postgres    false    235            �           2606    355260 ,   beone_opname_header beone_opname_header_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_opname_header
    ADD CONSTRAINT beone_opname_header_pkey PRIMARY KEY (opname_header_id);
 V   ALTER TABLE ONLY public.beone_opname_header DROP CONSTRAINT beone_opname_header_pkey;
       public         postgres    false    237            �           2606    355262 $   beone_peleburan beone_peleburan_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_peleburan
    ADD CONSTRAINT beone_peleburan_pkey PRIMARY KEY (peleburan_id);
 N   ALTER TABLE ONLY public.beone_peleburan DROP CONSTRAINT beone_peleburan_pkey;
       public         postgres    false    239            �           2606    355264 >   beone_pemakaian_bahan_detail beone_pemakaian_bahan_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_detail
    ADD CONSTRAINT beone_pemakaian_bahan_detail_pkey PRIMARY KEY (pemakaian_detail_id);
 h   ALTER TABLE ONLY public.beone_pemakaian_bahan_detail DROP CONSTRAINT beone_pemakaian_bahan_detail_pkey;
       public         postgres    false    241            �           2606    355266 $   beone_pm_header beone_pm_header_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_pm_header
    ADD CONSTRAINT beone_pm_header_pkey PRIMARY KEY (pm_header_id);
 N   ALTER TABLE ONLY public.beone_pm_header DROP CONSTRAINT beone_pm_header_pkey;
       public         postgres    false    243            �           2606    355268 2   beone_po_import_detail beone_po_import_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_po_import_detail
    ADD CONSTRAINT beone_po_import_detail_pkey PRIMARY KEY (purchase_detail_id);
 \   ALTER TABLE ONLY public.beone_po_import_detail DROP CONSTRAINT beone_po_import_detail_pkey;
       public         postgres    false    245            �           2606    355270 2   beone_po_import_header beone_po_import_header_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_po_import_header
    ADD CONSTRAINT beone_po_import_header_pkey PRIMARY KEY (purchase_header_id);
 \   ALTER TABLE ONLY public.beone_po_import_header DROP CONSTRAINT beone_po_import_header_pkey;
       public         postgres    false    247            �           2606    355272 0   beone_produksi_detail beone_produksi_detail_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_produksi_detail
    ADD CONSTRAINT beone_produksi_detail_pkey PRIMARY KEY (produksi_detail_id);
 Z   ALTER TABLE ONLY public.beone_produksi_detail DROP CONSTRAINT beone_produksi_detail_pkey;
       public         postgres    false    249            �           2606    355274 0   beone_produksi_header beone_produksi_header_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_produksi_header
    ADD CONSTRAINT beone_produksi_header_pkey PRIMARY KEY (produksi_header_id);
 Z   ALTER TABLE ONLY public.beone_produksi_header DROP CONSTRAINT beone_produksi_header_pkey;
       public         postgres    false    251            �           2606    355276 0   beone_purchase_detail beone_purchase_detail_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_purchase_detail
    ADD CONSTRAINT beone_purchase_detail_pkey PRIMARY KEY (purchase_detail_id);
 Z   ALTER TABLE ONLY public.beone_purchase_detail DROP CONSTRAINT beone_purchase_detail_pkey;
       public         postgres    false    253            �           2606    355278 0   beone_purchase_header beone_purchase_header_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_purchase_header
    ADD CONSTRAINT beone_purchase_header_pkey PRIMARY KEY (purchase_header_id);
 Z   ALTER TABLE ONLY public.beone_purchase_header DROP CONSTRAINT beone_purchase_header_pkey;
       public         postgres    false    255            �           2606    355280 0   beone_received_import beone_received_import_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY public.beone_received_import
    ADD CONSTRAINT beone_received_import_pkey PRIMARY KEY (received_id);
 Z   ALTER TABLE ONLY public.beone_received_import DROP CONSTRAINT beone_received_import_pkey;
       public         postgres    false    257            �           2606    355282    beone_role beone_role_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.beone_role
    ADD CONSTRAINT beone_role_pkey PRIMARY KEY (role_id);
 D   ALTER TABLE ONLY public.beone_role DROP CONSTRAINT beone_role_pkey;
       public         postgres    false    259            �           2606    355284 $   beone_role_user beone_role_user_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.beone_role_user
    ADD CONSTRAINT beone_role_user_pkey PRIMARY KEY (role_id);
 N   ALTER TABLE ONLY public.beone_role_user DROP CONSTRAINT beone_role_user_pkey;
       public         postgres    false    261            �           2606    355286 *   beone_sales_detail beone_sales_detail_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.beone_sales_detail
    ADD CONSTRAINT beone_sales_detail_pkey PRIMARY KEY (sales_detail_id);
 T   ALTER TABLE ONLY public.beone_sales_detail DROP CONSTRAINT beone_sales_detail_pkey;
       public         postgres    false    263            �           2606    355288 *   beone_sales_header beone_sales_header_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.beone_sales_header
    ADD CONSTRAINT beone_sales_header_pkey PRIMARY KEY (sales_header_id);
 T   ALTER TABLE ONLY public.beone_sales_header DROP CONSTRAINT beone_sales_header_pkey;
       public         postgres    false    265            �           2606    355290 (   beone_satuan_item beone_satuan_item_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.beone_satuan_item
    ADD CONSTRAINT beone_satuan_item_pkey PRIMARY KEY (satuan_id);
 R   ALTER TABLE ONLY public.beone_satuan_item DROP CONSTRAINT beone_satuan_item_pkey;
       public         postgres    false    267                        2606    355292 2   beone_subkon_in_detail beone_subkon_in_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_subkon_in_detail
    ADD CONSTRAINT beone_subkon_in_detail_pkey PRIMARY KEY (subkon_in_detail_id);
 \   ALTER TABLE ONLY public.beone_subkon_in_detail DROP CONSTRAINT beone_subkon_in_detail_pkey;
       public         postgres    false    269                       2606    355294 2   beone_subkon_in_header beone_subkon_in_header_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY public.beone_subkon_in_header
    ADD CONSTRAINT beone_subkon_in_header_pkey PRIMARY KEY (subkon_in_id);
 \   ALTER TABLE ONLY public.beone_subkon_in_header DROP CONSTRAINT beone_subkon_in_header_pkey;
       public         postgres    false    271                       2606    355296 4   beone_subkon_out_detail beone_subkon_out_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_subkon_out_detail
    ADD CONSTRAINT beone_subkon_out_detail_pkey PRIMARY KEY (subkon_out_detail_id);
 ^   ALTER TABLE ONLY public.beone_subkon_out_detail DROP CONSTRAINT beone_subkon_out_detail_pkey;
       public         postgres    false    273                       2606    355298 4   beone_subkon_out_header beone_subkon_out_header_pkey 
   CONSTRAINT     }   ALTER TABLE ONLY public.beone_subkon_out_header
    ADD CONSTRAINT beone_subkon_out_header_pkey PRIMARY KEY (subkon_out_id);
 ^   ALTER TABLE ONLY public.beone_subkon_out_header DROP CONSTRAINT beone_subkon_out_header_pkey;
       public         postgres    false    275                       2606    355300 "   beone_tipe_coa beone_tipe_coa_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.beone_tipe_coa
    ADD CONSTRAINT beone_tipe_coa_pkey PRIMARY KEY (tipe_coa_id);
 L   ALTER TABLE ONLY public.beone_tipe_coa DROP CONSTRAINT beone_tipe_coa_pkey;
       public         postgres    false    277                       2606    355302 <   beone_transfer_stock_detail beone_transfer_stock_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_transfer_stock_detail
    ADD CONSTRAINT beone_transfer_stock_detail_pkey PRIMARY KEY (transfer_stock_detail_id);
 f   ALTER TABLE ONLY public.beone_transfer_stock_detail DROP CONSTRAINT beone_transfer_stock_detail_pkey;
       public         postgres    false    282            
           2606    355304 .   beone_transfer_stock beone_transfer_stock_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_transfer_stock
    ADD CONSTRAINT beone_transfer_stock_pkey PRIMARY KEY (transfer_stock_id);
 X   ALTER TABLE ONLY public.beone_transfer_stock DROP CONSTRAINT beone_transfer_stock_pkey;
       public         postgres    false    281                       2606    355306    beone_user beone_user_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.beone_user
    ADD CONSTRAINT beone_user_pkey PRIMARY KEY (user_id);
 D   ALTER TABLE ONLY public.beone_user DROP CONSTRAINT beone_user_pkey;
       public         postgres    false    285                       2606    355308 .   beone_voucher_detail beone_voucher_detail_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_voucher_detail
    ADD CONSTRAINT beone_voucher_detail_pkey PRIMARY KEY (voucher_detail_id);
 X   ALTER TABLE ONLY public.beone_voucher_detail DROP CONSTRAINT beone_voucher_detail_pkey;
       public         postgres    false    287                       2606    355310 .   beone_voucher_header beone_voucher_header_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_voucher_header
    ADD CONSTRAINT beone_voucher_header_pkey PRIMARY KEY (voucher_header_id);
 X   ALTER TABLE ONLY public.beone_voucher_header DROP CONSTRAINT beone_voucher_header_pkey;
       public         postgres    false    289            �      x������ � �      �      x��}�r$;r�:����u/�7��Q�`1�dfv>�Xc��E�͘�e2m���̗̏	����A�Z2�+w<�q Ϡ�l���F􏽈���~#e��~�"���e�yڈ�9���~�^��WZv_�������]��?�e#Da���7�'�X����-ț�>!���6¾tΆǾ���a��N	�n �i���c�����=:��Q���r�Qy�ĳe�[w�������+@�7���/���}o����zg���>Z���'+��]�|/z�����M��x�k��}ɠ�tF�^�u!
P������Q=F�(7_��N�N��xg����CV-��K(��g\���9�K��u_b�/���%��W�uLQ�ɰ��_w���S���(W�ͨ���R7���D����;e��E�X����%�5�@KŝA�*�E<�G-�<#��\�J�Ю Aɢ���/nN���\����}���ἻD�\����4�~m�,b�VA8�'�.�� E'J�k+QP�@I�_��k�>~j�bH�������)n��?������o����w_��������d���� {e���+�R4j��łe������x�x��2�%�m���|;���+��o���p�_bW������m���#.�e#ԭ_wI5�8�4�H�����^&|�<�
��Qe���n![��	l�pyO*0��O?��v�ï�q�a
j��m�-�ƅ�X�y.�i7�E?��n�-�w\��Hܞz)���q���K�������6���W���D���n����}OKm�Q�1�[�E6ck3�˙ѳ=q4���
�F#ns��o�}�lA3�X�Lܣ�cB1v�9G����m�/��x�3���n��^.Ӛ�[K�Zǰs8܇��1-6Q"�]���l$h$7���n;wjj�ܨ���鵋�~<���I_,��B��-���:�������e1swh��}�R<j�3f����/ݷ�!2�ދ~���4��St��o1��1ivܟ��B��"�!%M}H��Uf���$��k�Z�0����Qm���i{31K�
��F���|0}�r?�ƀ^��(>__���9Q��|"@��H�o��}A:�C���q؏�q���B��|߿���HqC�q�.u�����4�^ߣ���xQ�3����i�=<-kDc_lo�Q���ġ�7��y�CXZ���ao���6o��i#��9��><�v~���{{�bӗ�y�^Qo���le=�����.�L��{�}�a71@�����(����ꆗ�[ �B䴟L�o�Gț��Z:g��y���	���B7���qU��b��Z�t
���+�Ø��6�t {әX�)q�����cW��J�6.�!Ƭ{�z���͏�e�}�U�-���K���9FOa�2?~u�_��㍗3�� 2n���|1W�"\��E�N#w"A�6WNn�KW6���m�Q9>wMmn��Alz3���Tvy�[��Y��b�jxcb�nC�	�#�s�#�/��q�}{�m�Y�����O ���~�=�v��vj6o��N�t
��s�YЙ�!`:Ϳ��퀼��o���x�x�K�D�8Y���K<��3o���*�|)��;�χBg�b8����P:�cw�S��){��ˮ����Q���u��6��Ʉ,M�}/L��j7��8���S��q-N�J�Y��0�?	�'Bn׉�5"����$xz/1��0��#r  X>�(�֢=����B1� ��mB-a�B��s�(&|�ÿt�*�4�SR+a�4��!(40�ǘx����;Nc�ZpyT�&>��;�0ؾ{߮C�D��	����ߏ��n�.Y����O�ʐQƃ�t��=e� �#�FY�(�"���qy�>K6z�����x��#�����%^��5��x���8]���7�^?D�ă��9^z�x�NI^��%�:�[hk�S3V��z��	*�P*N��R�ظ�?vO�~i�r�)b\�mj|ˋ��c�����q�og�H��zs��s��YfOWA4n	��]�f�_��M＿7��N�����|�9�8�j����9���:�f�������vݾ��0�x�0��1�g�� ��}��#$G̍o���=o�ۍ%klx�y}[�;bY����i_0G�E�f���>J�;tߺ��,�5E9�	�Mr���i���h3�t5�d��*���,W*�Flf�.7%� ^�-��~�2/�۔��Xl=�(��a=w@2����EN, � ���qb)��c<�c�׀s�d10]N��l@p������۔	:\/�he�s��y|��n:v,@π���4����@T�l�����tZ�#e7A�ɦX��/3�p��$�|q��[Ŕ<��m����_J7p7�-0�2el���1^0�a,�Ԕ���1����<���������-T ���@�략�	8��`	�����̌�ܻ��h��m���[>w��2���C+����K�����} d�@�m�<��A�p��Rg����~�`�&�&%1K#���{����sWɼf�P[gC���Ş�.K��X�JC92-�[�i��]M+�#�~N��������H�V��(R$�g�CcȴK-�x�����ҔQdI	ύ��lNR|���56O�8��\c�4�#� ���	�D)��@m�1����q����=%��p�xo�GU�{Zb��eܷ�C��kl��2�Lѯ��]c�z��}?<��1���[í��w��]cg��Qy���K��C[@�]c���\sK�u;�A��4��'�AGY�#T&���@�_^�'�A�*G�cT�I�a8B/Y�f"ko���
ͤB�5�8)��1�:�o�o���h�};��b�t8���ފ����4��	�Ⱦ_3A�|2��܋OMT���r�BI�	����j9&4'��k�*�Sś5xM��'�����9��[Ög��	L�2�:�F4��_�bqT�-�e�LL��*�FK��%��:�mD���*%�'���VXe�-p;����B����t��0���6�:
����!B�ܧ&�N����*D"BX�A���k�2D"d/��$D"d/�;��3�^�ai� D�~��<D"d�JE"�_eb#D"d�JH"�_�$�ٯ���)1�o��o��B�!B
񹍬N	B�r��)ԧ&�N�!�^�P�	B�f�9DH"�]C�!B�!����B����������B�\e"��JE"$
r���!Q����d!B�!W)IC�D!B����I��:�/��i���t���J�B��-8
�V�sT�BW�@�p�D�s
�%[�2(�jAIP((�v��r�(��Q�+$}�d��B�WM��+$y�dXC�
�]5�Ƥ���u�jT�
�\7ٶd?g(�~����J�]~t�!�3��5ҷV��0��uMխ���YCW����m�+mk�m�Z~��5���`�l���CI����M�t�}rfрQYk$k��W�F�6-bQk$j��UC�iڴ��$���M�`T�)ڴ8V
Z#A��d����eD�^���҄UxV��J�7�T�(�+�XCW����l�+A������7e���`)h�"��4�D�(W,�m��>93�k����m�� A�����k�!h��Z�b�6HЮE0*h��Z+m���B��{=t��T/����r@ʶH������3�$��gf��-Һs�e��}ziZ��o���;H�'9X�3H�B,���i��Cz
Y�H��Zz
i�i��(T�O�c�E1�JY<,
�S�6��EQ�J\N,
'�S�ҸbQ\񟲷0��vr8�O0�8O�CIˊu(|���E��C�"��T���dU���X�<��,}����7@�t���^3�C�N�M�V4��T��V��\��T��D��R��:�A�S:0�9 9�SBP�9�0�SN��r@P*�4\�]7�z�ڝ]�P[hK�����}d���0@��Tz\h�:�ħ���W:�@�*�    * �Y�HR���-��:UzH `"T���3t�=sҤ����H�b�u\�)Y�Ў��#Q��5�푾�
���=��\�U�G��+,�G �#�߮�Y�G4�;��(PH��f�
��e�ƃ��AzIX�Ru�����2RE��"CzPh�#����CzRh(cD@1"�*�,�@P�H��n�>p��������B����<n7�gD%�#��>�i#�A�gtea$�0�>#,�%��gˀP@I��/��s���&
����D�6�ԫP��J��	"
���NѴPu���Z���C�R?�^+��8��S�����P��J�]���Tz���oΞ�`��$P(T���X�P��O�5�B�?e�����B%~ʬѐ�*��Y#"	

�)���" (Tݧ#c<��/H
)��>e���DTק�c��P����a���{#d�Wa ��)Ø�}� �j��eL�2�B>e�� Tŧ,�\�:��\��i	Hŏ���ms�K�)�&>��S�͵��Qٞ�m�1ѣ�=eۤ��G{ʶyW��(T����q��	<*�S����g��=��f4;*�S�1�DW����������c�[�f��z<��f`)rT��#ٌ$GUx�1�%��'`����F�wʷx�ō��o�HU�)��UCب�N����Q���-�QQ�;�[�%��딧4�����Qu��l����딧tKp�jT]�<�]��sYJ���t���T��˞��Qu�
�o	Y*Uש@Y��Dڨ�NJ��q��)(���Uש�dW7��S�I1"oT]�B�a}��:�DcG�u�oR�*U��ɶZ⨾N��p��(T^�{�9�����tO)�t�Pq��)���m���=%���B�u��lc?�Q��N��g��5
��R��F��:-(��j*�ӂ�	��F��:-l�bFEuZ4�D��j�h��!dTR�E�TLƨ�N����QA�f�Fu:�j�<�����Q1�N�����tz�(�T���N�g
�rɦ�W����@Qz�RE5s:=G�R��\N�ׇD$�*�tzp��eO|���B���D�qZ�pY��8-M�$QU�V�%9��8� Y�Q-�V�.T��N+��Z��N����_��˵���z^���_�5�J�tzX7���*�tzhY�JEp:=
�Z���e��ZF*��7����ȂF�o:=���F�o:=�,���7�����}�KO-8��M��x�#*|��3�����޴���e��̀(�?#*��M�ϨJ�*w��3�ނ�9ET��C�q��>��*y����LQћN�Gc*{�鍀�*w<*�ǁ�� *}��A�𕕏��tzȐR��M���u�8�������q` �`T�-��,*�Ӗ����i˹�t�����`JDep�rzP��B8m9C��P5�N�������'=�΀�P1�Ny���,T�Sڞ�
Q)�Ni��r�՘��^��锵��:Q�N){-U���tJ�s,Q+���)e�\g��+)aρT��N�6׸�Q	�vm�5�
8��\c�F�oڵ��ԍ*ߴoӍ��i�fQ;*}�)���������S9*v�)�O�Y^��M��}�QU��6���W��jN)��T���t����zQ��N��)U�
�tJ�gQ+*E�)_��~���{Pu�r387�*Q���D���L��BT>�'S*ӁӃ�����BԆ��t���׿��W߻�0�^��Π�0§&�$�3}�b�hѠ�0Ӌ�,T]`�4�\1P�Ӡ�0ӫ5�I�Յ�^���Tfz����kP]�)~l��gߌ=����E�(��k�c�6�.��k�UnP]�k��z7�"̈5R�Tf�I0�"̈5aU�����@5�P�&��?@({J���O��z��e�X�)��A�:�~S����7�����ДR�ग़X�)����S
�cb��j�Se�Z�ߑ��V��t$Ĭ
GV��[J��z*��'����Ց��5,q�B�����"�P#)/^�*W\&)+��J�X���O~�X,KJv/��$gL�l/�Z3j&e����l,�M�Z/͉r,T��D`�P<�2�֏��є�i�B!i��%��)#��,����(e��t�]���I9()�0.�AMiOaDT�J��qU���JY9(+#���+ue$E���2�Bja9(,����/�1�=����2�L[j�0J��rP\�1��AuF*/�e5��ԗe쨞<�X�����L��C��q��ߤ�0�g��~�R�Mu�j�MJCt��/��)�R!��7)Q��f1��~�2�Xj�����H"MT�oRf���>93RN�¨PQ5�q-^q��Z~�Z�"�E��Ƶx�P/��7�E/&bT�o\�`T˨�߸�jI�
~C���}��z
q����o�sY]����/����Q)����g�pyӯ���Jᨠ��o�W޳�QM��_�/��֡�~C?�_���Qi���鯽�rG���~��DRţC?�_�G5��~��4At����<�oHU��q~hg�/*�7����:� ����0����P��I	���r8C�J����J�M�Gg�9��7)Mp�;�ΰJԨ�ߤt�+k��۔rΐJ¨�ߦ,s��}�r˥�Y��dߦlrP��Z}�srpy�"}�svU��|�sr4Ĉ��m�9�4���m�YB��
�mωR*��۔�˥{�����Wm�CtC@�V�2�L��զd0W��
mJl)ŀ�6%��פǀn�6e�	�8?t��)7L��0��ڔ%fN�G@��QD��r�l���4����:�b��ke�MX��{�l���6�ۯ�Z�t�������M�ʚ\�ݽx�(R2�[�~�H��[�WmP]�����sU7��U�j�R9�'[%���Ŏn�V�Ux�ytg�J������*�I��'eV�4�[�U��� ݥ�Z'%	�Fm�:'�ݫ�Z�&�vm�:9i�@wl���Y�tӶ�Pt����m�jc�2���j��4.�{�զ	���p�-BVQ ݥ�v�c?�E[���<�>[�H]�����/�C��.��@�p=�k�5�:D��ldNC��jl$-�[)D�����@�EWakj"���	�2lMhB�V�u�� �>х�Z�BV.�<��J��Rl�B��8��j *�	��� �&�[�e_|խ*Lx;��/\��~la�,��!�4T	��ц�ޒ"�$�';ĝZ�𮜿4���Zj�0Q�͟�o�ZA��6[��HQM������=�j��<�W�E5�6F��βE��6?�AK�_�?ϰD¨���/���/�J�T<R9�Z`��\�F5�ַ�F��j��os�!pT)l}�rL�b��6��Q��m�ՒG�6v�6��~&�_�b���A@�]^~ɇQ�$�R�~ɃQ�B���+�6h�I��Kދ"�rW�_�]��%���%�E� 
m�%�E1�9��v�qm�֋
�ռ�o�q�1E]������_�aDIP��/I0
�4(����Pt��8�N�q8]�aYJ�S"̰��P�<�^�/�e�,]X�S&D�_�I�������O!`���Ț�K>�B���p_W�~�����*\�N���Up�:!8�rB�ꄬ�P����e*�L��U�R����	S5'\�NX� [g�&\=��;<w�����C�JSQ
 JQ/4�� ���RA
 HY�t!+d%�U~�������}�_���q��)Y������_d̀�(���%�K }I��k�.���.r�V�3�Q��I7��zBI!'}ٺ2�N�N��q�"��q�_�f�l�SbnXj%x��sK"��qJ%��&;�8��f_��x:��H�Ŭ �]V�*ez�Z��%w�Z����s�ZV�������U�?J�8�o�~>!n�D�����B(=�L�)?QR�iU���s�j��WE	�M��Y��.��Ҵ�)Jv8�R[�T��p���]�6!5���c�_�H�<.#|�&/'%,ʀ8�W�S�=���IH�����<<?O_����ʋ�,0yY�+�v����Fębi P  �ېo�>���[
%<���a����ǜ�|��ȜG�s�� �)��=gT�����+U�칸|�z�����:�-u�>��ɍ�����\$Ga<[�ݩ%�	�H��Ūr���9[,+Q
��sŪ�׈�XPFz��;��%�X��O�t_w�v��%�ܔ�#�L3��sSj/!(�Q2�MɼQ��L��w	PQ%�ܔ���3�Q��MٹԸ$3J��)!�Z6�4���p��l��֔q�Fy�38�t���R_N������t��X;C%S��TG(E�|��\H()�|��DI(�|�腔��/�<'�|#9�|���g_(׾�s�k#��_%�\4�-��O����}?N3u��-����ZD w)����=h�~��`7�xs"�E#3��M���8�!r*�;����0���G�!�x=5��c|{���������q�\�&� ��o������.�������6��!v������qY#I���U����D}M��g8��'�̊���.E��H���2�;�݉}���p�]����1fPf��������ifM�HN����I�5$�63�&|䵏�'���E_7/�$���u�bϙ!�{BR�>[?��xr�\n^��/�W3���~TW�N�����۾-s-xg	�#�Ւ���A"�؝w�É�g}����:��H	7NQ�i-Ӡ$�(��æD~\"��r�/���B�j��??����ǿ��������$���wχ��dc)���L-��8�F1I�Gw9ĥ?&�wIc�!��6���=\O�� �v$��8)S�~�����z9G��`���및}�e0�$�l�-�"dy��X��0(A�vx��俕�ؒ)�a�4���t>,�E��@7�=�8˂��a�H:\��f�*��i�XoIyjX��+�^��ު�(��N2$�B���4���6�_�V:,pl��o@��B���ʆ���~<,!�y,g�3�kYBn��}N��M�V`/5Ks6Z����=#�&�x�a|`����-��N�3�ϲC�i�(�b���7�T�|��$F�����zWA�|����1bM�%�͌!��v�\Z������]�      �     x�}RKn�0]�O1'@�K����c�-�j��9j��R	y1�7�&���+���tuw��rl|�����9(r)I�D����7��4GxY@N��(h������S�Q�H5�Ӎ�n�@��3j�8E�wg��t��tq�S���;���%����g���4T��)�/��2o���ޢ|R��pſ@�ܕJPP�C]i���C�X�����2�U�����V!�>����>��*�|��\n\��W�tZ|��DN�b`�k��D�e��_      �   >   x�3���quQqq��4�2�t
r����t�t��9=�\��\���.@�=... �pd      �      x��\�n�H�}���|�� J�E+�S�L�)��Ų
�E��ؚ*[/����{"�Z�J���Q�,��Eĉk೰Y�&�c�Tk^�jg3Y��-��G<	s��y!��L�<��<��k%�\d�Ï�����A��76B���O|U\��JYs�7<,�K���=O�0k>�U!"g?�ʣ�n*%R^�*q�H�yl݊�ױ�Y�f�W]$_Ȭ,h���Z��2ĐE���3O����h$-�s=���"Eo�ơ{��0V����,z:���s�mR��I���E��x*r���P���c��q6z�Q����2��"-�)m�Lػ%`^�`]��a�t�t����_���^��vs�O������j��ux��6O��c�ģݓe�{�8ƛy��EѬ��xYQ6�u=UbQh��Y-�W�,� ����Z}�]�\.U��a\���w�ʬ�'z�TZ��>$N�
5���UU7{�ZsQI�A�gE<Ъ�s`�=�'�Ӝ�N*�*^E�����Z��`WV��<�
�L�03��U�W�ń���BV�f�#ict<aIg[V��S�J��}/�Z����T�]VE}'k܅U�i�a���$��$?�jdļ��3Ej�"��+�M�Sг�tb��LM��q�;����1\țb�d윗�ۑ�"���v"�E�ժ4Zc�e}���R`Z�|�#hI�t����i��'��w=�^+sh�J#�
4��D�J���v�7�KY�8�J��,U�mN�dN|��ns�&���Z��ލ�e2:������G5Q�x%�܃-�x�琢m��q��C����>��cR�u+�tE���hF�|�g�n4d�d�����84�>�&��Z���s��@R�W���m,�8b�v���7�hLŦ"kZɊ�$QT� �>�ȸ���U�}����V��9p,%�<�)���4Ի��b�#�@xj�F�b5���[m�_���3��d�7�I8����0�JC{�X5�_��ߨH����zg+�����.���� ��C��&����*�8�D���P�܂���2�.�"������h<�>�D�Ȥ���!��F,��� C�s������O�	X<`�����E�ߏa�o��02}�?ÄGe0�}l�#�(
����"_[/c��:�2���n��Z&�:�*A�U
�\��6���yCW[�2��i/<.Q+N��'��Dqr1�ط�=q�-JQ����p�����V6�pw�7�a"+͑�ݵ֛|�Z ����ܓ �� I`*5/�sJ�,q��e��	� �"$�&S�k�\P�B�uEvd�^H"�Q��F<ĹYL���U��5%��CN0|@��M8�N۱.l���{R�c��]�J�5AL0����M��;��w�皜�B�Gǁ� 59�� Ck0�T�Rb*oU��iQT|�p�F&�d�tM�@+�2�C�K���fy��"I.b҆��˫����^��q�;l';u���c�����u�7����Tj�:6:V��A��]��@e�6�'����������|'�f�!�
�k�ef�}�FͧـQm��=/���<-�����FA �N�Ԃ�TGu��l�h�_D�*�C:��sA&c:d%����I&�E����CO�D\��m��k�e	��l��c�i��#!�(8
$��JV��/��F�+{�33�(N^�nTǬ��G����)���x�g�U��sR�l3N�R����,C�8��rO�s=Ǥ1���p L�Y�j��	1��Lx:Ŕu��W�9"�_в�C��U>�_�,z�bo�n�a`��Yy�{��?�:�ǖm���15�m��>����6h�v5˔8U$h`	�J���B�ϮG@�d8l�YnD��$p����\B�y�%�xH�6J� <Ǭ	���u	�j���;8�_����l�����NR"gp�3Ց��<��{BU�J�Ay��7n3�ԭV>��&&AB2�a���sGl	�P���l�-��7�y:��;:c�M$H��@�.Iex�A�:y�RR��7�J�	<�6M���N��;�y�<����-�6nx��m����3/����v��]2�����uX(M ����N��[=�%r���	�>�/O���e�0IG)�y�1Q�,��H��`��^��$j��M�DT:��5�����T�6`	���/bp�e��F�8i���1�$,T^�m������g2�͗"L�1��F�>x���"�b��]�==o�!T}2"D1��ƈ�o N'S��/1����+��T����}��+"�
H'3Y�MɌT���	��|QT����XNֱ=&~mԗ��}�{���wk���}R���{�Zn��`@�wY���7^o��<�V��7����m��e5+?�簳�)*Vn^`���q����o\}�=�m""7k��Zا^�l���m˟7��[>��5dm��t@R��<�xZ�:��:�7����5^����f"�Z�Z�^�z�������U�F�<�n�p�[B5>�.cB�M%]�`�
iT�2ۼ�>o')O��ƛBG3�BP7W��8�t��ǯwϏo���]4f���6LN%L��s��6���\����!=2�M��C��1o_�^7Z�������+��u p�cį!� 3"�,��`��q�<p���i��Q�0�����g�ZG�X�Jr�ʻ�;쎫k��{�l}~���K�L�}�MaQDY������؟����y�㷗���)�L͂!�D�:�Q�珢�\8Ԉ5T���[�%47/n��H��)G�{��|0f5 $]�Ge������*-1�8��Z��9{������=n0�����|0@~�Ҧ4�ZiҦ)�V|�fj�><�ȁ��b�������o�Z�����e��Y�\;K���^�S�l���$�k��d�E~I%Ń���f�#�'��c�h���k�
�7Ā��1�{�?�J�F������ j>o�h�"�Z~�6��.�ǟ��	gǳH����=��L*��ŧ��� q��,��n��@�3Z�X�(�ȱ��Բ���Eb�2�]�>ͣ�����u���i�:Tо��-�r������
���O'��_E���P��.�Sp��fǽ�K�BI�^bZ�j�ZZi��;U�cޥ*����C�Ӯ����=��������x���xzy��k6�?�F=��iT�o����i�.���׹��7�_��n�+���Y��X����B(
h�mĪ"��3)Bo8�u�\��
�CEVJʧ�d{Jhyǯ�7�M�X]\˼�T:�����$S��6���X�mU�h6k_ޔĄIH�2�c�P����G�_X�,-0�|��$?�f��<F�~.~��wF�.��|�;�D�v�8?��g�>
X�4]SR6ө6V=�t}�<�e�؇�7��T^�}V��N�&E��aq4`��a�~R����4���Ӽ=��S�[��wϯ<ھ�:�Ь��ѐ2Θ���:mJ� _�A�˴j��x����礒ʥ�vI�Luu�����N8���
�ltzMQz�,���t��=r�N�����f˿�m���<!�B��_���?��9|�١(��"6E�f�P�Kޘw<�2���/Ֆ�ij��$5�'@5-��"n��N;%���X�iZ�G;��=�X���m������[����a���D�DE�Y�3�u|ƞ� ��������H[zS�I��/tuW7�6�X'����M	�n�� '�'�e0䡦�i���nUn���RQ���=�~�����e��E�	�%v�Z�������#�OҝOP{�!a���c6eO����S���doA�i`=:鼢��.���.���1��"PӮs�ț��q~E\�ȸ��Q��O�k��3�?���L����P2��1qA�V0��.��b��gtt�"�ӧ�1S��>Zp�T+:auU�g8���_�x5m�0ϻ�0W^>l^�ק����p����þգM�?�{����%5Ho2dm�#U�ε6�FQ�lvC2��D;r�����"�Lw�� H  e�4a`rӥc	�u�«=Q����}��Y�/B&S���+�[��?�kU�*<F���%]�=�@i�MZ��q�2���a��W�>��Z� �@�ǫ�d#���OGqպ C�}��q�|h���
9I�';"O}�n}vf�G�u	A�?Y���w: g�{I[ޥ_�e ���'z3Dkw�d:dkQD⤳�jO#.�M�#��V�
N-WJdc,K��%~(��C7�����:�o��l��N5�Tm�:S}�N Xi���-��d���ⴐ�ﴚy�)��Yo�[�,���˾HA.������'#���*�k]���(+U�uoa7M�G>���@��R��/�ꁊ%"�����j������� ����|WU�h�o���U��˪c&�H�l��ә�j�
��0DVݫ�`o�J�X51�ф�$����`���h�R�#>Z��cJ:T��5SJ!�ޡ����=��V�\Ո�!����&���ExScGH�6����t�g�:E�IrJ9�R�R�X��f
"��e����nD�"�ٗ3���]AM3@oڧ)�Xw��g�H��Bm~]�1�[�׀�ͷԈ�����W�����h����	��H�v�wy�w���^��p���`���0�ʷ��ֻo���e��@�2�8|V��y޽�R���3��^}�N6f�̬M��+���n̓&���ێ����-h�2���)�.�`�	��:�D��P4���L4�ʩ�`VTqQDu1]��V�P�g��=8������%Y:�%pY54�!�֥�O�!�>������r�wȄ_�M�ij��~ͨ��R�UY�F���l]�g����=�����)�!.�Gh:�{��W?���N�޿ިJ`�x~�i���&��~�wA��ج*t��ķ뼦�b��y�K)�K�P��`<�ޛ�u��6Ր̈́Jc~��w��׵*)z�@8��1uw�bk`r�S-�H��8���z��
�t@<z��\%��߿�};�����k����~aR���H�.LnX���K���]��9���	]�s�0�Qp7�92�c^T���4�$�a����#�NY��m |��E�ik8==��]�Ӭ�QPe�?�>f禾�fԘ��9�
ֵP͊Xs>�A])�J9�������5W仦	��=���ƅ�S0���"�qe�.�=߰�Eo�/�վ��~tg�IH�v�a��2}�>����U�A�\����!v��\�Sih�Ro����=B��ڦ~��	hv͌�=> �]���IΊs��z�i�)�l�W�O5�+����X�|��T�	*p��yP
��e��"�r�ϋZ7ͩ͞�C&�RtMR5FQ����@jgm�����?"{�J
�a�M����ؑK�Pg�.�:eA��#�C��4O4^@��ǧ�?<'�՚L�#��0����6����d{.џ0�Ȓ�M�P���Ԥ�y�O�9ӎ9�L����������D�M,��՗j;���85����ֹ��2p��G���=�e�j�.Im�]�;�Qg�t:Te	������l�y�<���0�sլ�'� k�e�*��=m��\����X���-$��wS���2����ࠃ��d�����C�uy���8�^:��-�5������G<)���w�DA"�k��2��[j'4��q'��C�"�ԧ�{毗����~��!u �ҍ��|���8���}5H(,\���F�Z;b)�d�~9 � ����(|����{Cu0e�N������&bi�2|�T70�y[��i�n��\VKj�6���lfc�fEݾ뜶��N������?, .S��{X����Y-�"S�ٽ�Ǩ���?�1�h�E�FΩt.(p�_��_~�u      �   l   x����1C��0Q��v��sԹ�Ԩ_5!�0��Q30��øZ�:��hޡ����%�o�g+�_����	jT��!]Jp<P��̶�C2~�R��#SV��ͱ�jf�e�%�      �   �   x���I
�0E��)r�,�	4Pt�����3C�/��=���pm�0 ���+�	�p��2Slevh���� Rۊ����ߡ+I攩~�#e@�T��I����<�#^��f��ZT���Z� �P�"+����9��.pl�d5�Ȟ��p���$0MӶ
L2�{�{�?^_z      �   >  x���Mo�0��ʯ�q=�%E}�6�wi��6�2�=���۰_?)NlJ�S�m��i�|^R�+G*���sps�R �0|�"!1��l.���� B�҆À�R�&eb*p�=5$,�`&��\FBN4e2�	y��!�v#�J9 M�e4J$�8�ޝʕX�ddCdl��&J\��CB#�)Yon�n�mu�[$�Ec�h�zuY���B̰�cR�!�Bx�C��bl;6�za�."/U��<�oV�ۏ�.������bQ��A����r�\L�	��8�=P'pe���%�3}� �X/?.���,Wg�ru՜q�����<���!�r�5�\�h��V�$���@��yg�_/W�m�喃����&�������p*W󬂖�{�C9������ ���dԨ�r#�����&*Wa����-67�/Ka<�*�$G��5���rq:�����,$2r2��������a���\_o���1�
�6n=��߄ �0�`�	}�>�4�Wa�[��%�
0���Y�<��4���Y�aO#�.�b��0�j���\u�U� �AI��zP�S ����}���Z��j�����?%�B��� ������C��[[IOG�e��pj8J>)5&�����^a4f�ǯ���ھJ���<�q�C$��}���g�#$�t pԾI��������+l�/iT��O�'�<A�_�o�^���c����K��Ɇ���NZ0���}W�!k��8�qS|� ��랼��#��m����r6�_���E��T���1n�=�i�.�mx��b�0IzŶr����f�i+D�      �   �   x�m�K� D������'��`e���#T%m�����7��$Ƣ���^Kc��1�l�(<'�nN.6
E�N@�m���~��Jg���&G�&Yp��`��v�F.[$���\��4^�k����_��y ���Ec      �   E  x����N�0���S�E?���q�&����P���x�n�nM�zJ������%&�5B� ��dM�yh������Js�!���9����+���}�l$7��]�c���Y�,�2F$�n�Ź�In c�y��iO\
�}�#
8
mq-��*���J��h u2��B�*m2S&��e2�L�%����O��֫���ap��;��%]ʠi7gܔx�6o����c{�<�.?w���5��k�q���6�<�a���e-�|}�s�E��|ɍ�|�� �?��0;y�a�
خ�z�2�ò�� +ٗ���u�UU� =���      �   �   x�u�K�0D��]Z�?�������������Y$i�=&����&ԉAp���Jب�:-���N,�U�=BL2(̓����[V�融�ꛭ�6�n|��p��Ǐ�m�����O���so.y]����N��#��w�C��Rz�>�      �      x������ � �      �      x������ � �      �   x  x���͎1���)���'G�EB�nժ��V}�C�(3��Rg@�3�g;;��%0`@e)ؠ0TK�PR�������t<�!�w�o����	��B\3��}wwG��j�vcЅ�^HJ1�EL�x�x�(�\5��S�������5�F������s:=�#�%V����I�Zx`~N�t��ףs�Nr-�}��7��at�afYQPnn���C\�������T���pa@ר����`/e@���u+�:������ܨV�di��>Ȼ;��r��-Ӣ��y�!BxmJ���g� ns8=�c�酈�*�SWD����A������� E�Rz����׃ ������	 �ί7�ҵ0yF}���y�i���|<|�z�h������u��K���(x���5��e�N�/Smͣ�O�#1�|#"ͲV��D�&����>�|����hs�A3�yW_�<�M�S���?�Y�o�ca�U�L��z��φ`��6�(�U�8�K�j�~��=o7���W%�#%�֐-T�eER?�l�k[���x�m�l�ġu�wSJ'�ګ<�Rq�BhL�y4�b���ђ"�- YtS����k�tBWߠ�����)Q��]Ύ9�{^�V jb��      �   �  x���Mo�6��ί ��H�u؃�Ed/ ��h[0v
7-�_�t9/GZ[�<��G���lV�/_�_�6���h���]���篃z����J�u�7F}{�LZ����`�ݕ�M�;Pv�r�(;YYb��k��(�J9�}Q�>+���N?��=�4f� 
��F�L�1GQ��ǣӯ����W�ʍe_9�20f�NH��<`* 4f�T<}�w�����V������es`a�}��|��f�^$ �C
5A�9J$�� ��E�6u�6	9uf#�v�k/������B�S��VBp�B�������VB�¢��'c�MID��AI�}��BdDg�b���I�2�β��2I�2"N�Z	���ƅܮ�	�EU��t7�g-��7�e�|w(�Z��1+Y96�v�[�����\F���zX|<E�D,��`݈�C-]��j�����G�*w8!�&� +�y�3�	e4�r��}��8��Z��+��;Y8ڄ�|�:vm�u`$���u���]�,@�x�i��('�[/����ͺX�>pZ"��9����m��:�m9\طc�ԭ�����񭲕�S����ж:�(���N]귲&+�km&;/ޛpp�P��7�P�2�u����ʦc�c��?G�l�f���f�!���eOF �n]�<{J������_��@Z��F½(l�r���u)A#嵨\B�+;lu�F#)�%	f75����J��q�,����g��2x���h�nA�@�b�u��p��[YƱ��0��`x�µz��vxο~ߞ�0�h#�0�PZ��[Q�����:�!�"��M���a�(C�̀'��m->M ��u<-�b.q�&�Q�YyY����d���d�f��v�z@mKC0c4���	�_�#�a�@�奡8{�c��F�����D�n[[���=4�p���<o���	Vh#^��˧�D�i�����!�@��>�V��2D�M3(�2Pc]��H��@�x����Z]~f�X!�#��AzfC`�B��e��@`nr�6;�		=��,��!���P(�=v������ՙ|�a�E#1�1�|�gY�1t���O�h$^*S8�P&��s���Ʊeأh4f?�1����V#�:���b������ G���C;TG����?�/O��d�nw?��o�rI.�+��ޠo��_]]�1�K      �   �   x�]�AN1E��)|����L�7S'rR!$�܀�K8mU�"�����0�B
#�s����J�Y��$u�rO�i�Fڣ�.JY��S������`,���u����ռ�#4���=�$���|
+����L ��*n�?�0tm��V��δ����o{ּf�;|��?kN\E}�Z��7����4%��t8�:������K�_1�_�"K�      �   �   x�Mб�0����)x��E{+�`"��2���V
I��;MЖ��*�&����4q�W+��̒"�H��؅�dR�ft9 �ϚՕ؏����b���m�p�Q*��	a��v��,��a�fх�<fp2�S��!c�ɀt�$��o�.��|�?Un`�}RJ� ��Kk      �   A   x��Sp
��q�tQ��s��s�t��I���KO�C0��B2sK�8��-L9���b���� �J�      �     x���Oo1����-�nw��^�7�i��rH�V�eS!���*|���I0�rA��1�y3p͓?�M�n'4�k���I-���{��VW?�j���n�EE�~?JĒAaX����c&w�aݵj��vu���n{5��]A�а��	! ��Sc���d
ԙ5�>�
$N�]j��קˮݴ�v��y)bp����6 �N�}����ԶWA�R�7��5��$�� Z`�Y��<���l^;L�t <�S�Zo��Я��;ȩ���c<e�^F�6"M%Fy�{9�#)�@����9�W����2���7Cu4b'^bd?ʨ�(#��1g�= ��2�h�/�<���#�} ���,�����!1KrJ�q�=�I��W��
,$gIJIPR��!d��͙k�H�Ή��WB�Zڗ�}}������vY�UA
���}V^+p㿃+��g�SR�l��G�$���j�prd��B�x�4�r�P2l"�0쓜��pa��ϥ���R�q���(��}�       �      x������ � �      �      x������ � �      �      x������ � �      �   �   x�=���0BϦ���$�����5��_�P�L�2����p����G����"N�c����V�|O��{�q7�c-���ބ/Y��G�8��]d���Ҡ�+ȸ;��eY��v������X���9k�G�"�ۉ��WSc����-�p�8D�ԝ�c�/r�2      �   �   x����J�0�s��@����d����--"���}YP��m�n7�E�K����0\v-z�� �a�����j��o��%R��rPLQ�3C���Z�0���4mAL��bB+R�1C�����RH�ƒ#�9�E�o�a�a���-C	�Lb�Q��ܽp��Ἳ��p�[�Έ$���ȅ�&A�Q�?���߶�ӸYd&g��*a][���2������%������U�_���,Q�������b�4H-z�      �   L   x�34����5��5��5000�42�pM�L#NC� g@��K�w�'Hސ�����I�chds��!W� ���      �   P   x�M���0��g�f��?G7ɉ����[��ER᱈+��Fb���#{�x�W1�lua`���>�\"�Yuu      �   �   x���A
1E��)����:3m�UD��vQEa6�7��`,�B�'�O�#a�r�de�ɬ/��T�z�N�� G%���c�)����!!�v��5]�U:�m�qO�� ���O
X�����Ŷ�����+svZ2��Bh      �   �   x�����0C�3Y�²$��e��cI!�8�W�@ 7�,���
�f�<�����<�G,�����1H�F!&��`�$�CL4.b�� ��^���j*7�j1pm�Zbq+Vs�X;�lnŮ���A�6d#!� -���8���l\��!�\9�
���6d#!o�lA6t�5��\�o�f�����ul������lv�]h�c7����#���E      �   �  x����n1��ާ�Ryf����&9�@ T�ʥRo=�j�_�dI��m#$$dv�c<�yȼl[�wDw<2.�lX���{<}=�W����W8�q`����R��~sz^oק����p�_}[�K� ��䉂!kS>�������"U>N)�My��֦l��2"�7 ĽLB�FQ�G�L&��2���	���2~�Ό��l��2v.v�XC���|G�hR�[[�=���*5%*�=��+#���U��z%�*A'g|��R-�b�5EAɗ5ѷ�����I�&((U���-;���A��6D������)�/�Z��ꂗ�j�{>��i}��^��:��圾<~�v|PHafvBC����P��m�9o#��r�(�P3ޖ
���6J�I�Kj��c=혷W~>(���G�K걛��(h�!8I�n���$eFJ%{[8�y�r�e��NKi��	�T�����\��jKb�)�ͺ�.�*O�\:�L��T�XB���=��*�m�	��
5��]w�+=w���P[��p�͠��JߴH]SJ+����'nq�濙uA�\���7"C����sz�B$]9������+�.n�HnE����q�.����B�f��q颿�g!��f�B���ןo�e8c��\�r�PWJ� %$�R�R^>��\ǺC      �      x������ � �      �      x������ � �      �   ~   x���KA�5}�6UE�X{���9df4Ɖ�aW��a���{KkZ�mIŪ�1KG@V�ݮ�zt���G��b	;+?��X&����*�ȡ��-f��E{��i�L����g����<.��'r�0�      �   e   x�3�p��
u�q�Cbb�1~@�eT�����	�c �!!,�SN�O_��dc��� � G_O?6v]@�e��������`�Ad���qqq �+�      �   -   x�3�q��s�ӆ�rq:�::�z;z"�*�.����� �w-�      �      x������ � �      �      x������ � �      �   �   x�E�A��0E�?�����M(q��4�?����H~����r��Qk:�#Fm����>�����x����$i�5��xJP��X3b�E�f��|5�kpFQ(g.
��Y�Ba�-)��?`�ʳ�;ʷ(���r��.�B�0K�|�Q��4+�W�ű�+{��!�Z{��
�;�Px�2�B�]�L��>�']�B�/��P,��&W����X��e���{T�      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �   ^   x�-�M
� E��pA��+Y�r�E��בA����ٟD'�rq�^��0HZt+�FYS�$�󦡋�QM�%b��:8&暁^�&����	      �   �   x�MP[�� �vN�t���9QPCC\����kh���񌙱mhB�a"��)��,A��� �#f���e[��ʑ<z(QT
*��
%��m8���guL���B���JcM�+���#�c�_�^����Ȫ�n�m�$U�ej��S~�ߤ�o�Ӗ��Bx��7n���lq�]WʒP��iD�7�F.<m.Ȯ^Sװ��1-�L���u]�тSr      �      x������ � �      �      x������ � �      �   W  x�e�Mn1FלS�HꏺB�ЍDQ���0/z�ʅ�&1$h���=�v����ں�8��������y�����h��U,��T��(�e���g���r��Y��st�fR
�P�;m9<���r)��}[�r�y�ʫ�2|�K�Oe��jS�Q���s���0��%3�1��^u���O�YF��~�Q1�ޙo=�U�����cE�T�<�ß���#��!�[��֡yj2z�#VFoY�Ǚv�����%���b��l��V�i|q�>r��k�>��d�.c���`�_��6�YIY�)�&q$P��u�QTO�`�`x_?�j��]	-Մh�����S�'�"��6M�o��4      �   �  x��V�n�F<S_1��,����2b8�-�B�s�e�flZe����)�� �������
M���o1��Q��'�1JI��Ϻ}k�Mh�*�٪�ѬGBK�m%�W�O��#�$9��M�
l�m��;Ae����Io\_��m��hWy[���fU�:�Ó�]�	-&1W� !3œ��2r��$0�A�"��))��aM�w�®ك�p��1���$��[��3�
�r%A��	���J=T�V�(���a�c�	��Rq���W�|�y���	�9�����2�L]'ҍ�)�r�a����	�s(��(���*W�L}ֻ��-��ڴ�(:S���"QJv__���$����p�N=\.��x�l\i�:YdU��abCLl^ta��~�X��=����۶�g,�n��MΝob��0B���E^�$����cTG��2���hW��l�\v�)���W��7�g�	���
��\l�����b���4�X�j���i�c���*��y�P��ku���r�-w��W�qw�'�&����T��	��D�j�1"w�4̺������DrYu?y��ɯ3�{���@��p���+v,0��/��et}�7ͺyOnc�zK�D&qޑx>�Z��rS��G�,��
*�\͡P��2���}�ݪف��%�DeŹ��3��ؓ�jy�Y;*��
O�L���ZRq�x����#��G����[g��S9�s*�&�%!V]�,+����ʑ�c�ұ�H,w���/#�Ns6��{���A�1����,9_��M��+����X���(|"i�X��a������"��.�zJ}�X���0��?b=O�K��(/��$ԋ�h�X��RHR�}�0ydgr=�8����J��"�����b�{|�/��������|
v4�H��L�%�3ɱ��2ɞ�pm�aA�u�}�\;Ty��\E���^�1;Ǎ>�"�j�1$Ic^t����L5*)��$.3��E��c�D,p�DN�w�8��86n0ғ�"9o��qa_j7�:��a��Euc�R��`��J��.5L�ͥt/�ay�ġ����z��'�P���9H�6"����xDh��o^���hQ�Ck�
��1{|��w�x����_׷@����԰�t�f����8:�`�������9�������;�N� ��Dq\�N������=�c('ϻ�d>�s<���y      �   L  x���ݎ�0��������$\�Q
�J�R�ޘ�7�E	���;��C��D$$۟�̌M0��1y��)�&#N#�1Ύ�ުRI��t���3bD��͏��v�(��<�W|��J^�����|(��������]-�ET�
u���~PL�Ă�bI�ߙ>�B�c@����20�Zꭑ���8I�߃`�,���ܹ'P�r��#�w9ī����ڨRZ�� ^�QȪ���Ҫ<r0/"p�.�3+�I�9��rH�tv@	���,ꉱ�����Td9��.�l`i�⣺2rY\��� ���ՀL*�E��MR��kVdêY���܏!�
1�i���������!��̝.gߦ3��b�3���9�!�N��35�Y��zj^
�?�3j{��f��s<aaif�y}onU?F����5����\�0��\��A]>�C#k�bZ!�����Y_L��MU���Ps��ؔDWƤ~��8���1i���|O�f�",1M���~^�����wf'(Ԥ�'�0�}�/柒���8^��|�T��}#�	�Dײ9Ѹ".?$����U��f����q���Ċ     