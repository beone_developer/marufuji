<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Form Edit Kas Bank</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">
          <form role="form" method="post">
              <div class="form-body">
                <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                            <label>Tanggal Voucher</label>
                            <div class="input-group">
                              <span class="input-group-addon input-circle-left">
                                  <i class="fa fa-calendar"></i>
                              </span>
                              <input class="form-control form-control-inline input-medium date-picker input-circle-right" name="tanggal_voucher" size="16" type="text" value="<?php echo date('m/d/Y');?>" />
                              <span class="help-block"></span>
                            </div>
                      </div>

                      <div class="form-group">
                            <label>Tipe Voucher</label>
                            <h4><?php if ($default['tipe'] == 1){echo "Debet";}else{echo "Kredit";}?></h4>
                        </div>

                          <h2><b><?php echo $default['voucher_number'];?></b></h2>

                  </div>


                  <div class="col-sm-8" id="detail_cash_bank">
                      <div class="portlet light bordered">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Akun Kas Bank</label>
                                    <div class="input-group">
                                       <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="coa_id_cash_bank" required>
                                         <?php 	foreach($list_coa as $row){ ?>
                                           <option value="<?php echo $row['coa_id'];?>"><?php echo $row['nomor']." | ".$row['nama'];?></option>
                                         <?php } ?>
                                       </select>
                                 </div>
                               </div>
                            </div>

                            <div class="col-sm-6">
                              <div class="form-group">
                                  <label>Akun Lawan Transaksi</label>
                                  <div class="input-group">
                                     <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="coa_id_lawan" required>
                                       <?php 	foreach($list_coa as $row){ ?>
                                         <option value="<?php echo $row['coa_id'];?>"><?php echo $row['nomor']." | ".$row['nama'];?></option>
                                       <?php } ?>
                                     </select>
                               </div>
                             </div>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label>Keterangan Voucher</label>
                              <input type="text" class="form-control" placeholder="Keterangan" name="keterangan_voucher" value="<?=isset($default['keterangan'])? $default['keterangan'] : ""?>" required>
                            </div>
                          </div>
                          <div class="col-sm-12">
                              <div class="row">
                                <div class="col-sm-4">
                                  <div class="form-group">
                                  <label>Jumlah Valas</label>
                                  <input type="text" class="form-control" placeholder="Jumlah Valas" name="jumlah_valas" value="<?=isset($default['jumlah_valas'])? $default['jumlah_valas'] : ""?>">
                                  </div>
                                </div>

                                <div class="col-sm-4">
                                  <div class="form-group">
                                  <label>Kurs</label>
                                  <input type="text" class="form-control" placeholder="Kurs" name="kurs" value="<?=isset($default['kurs'])? $default['kurs'] : ""?>">
                                  </div>
                                </div>

                                <div class="col-sm-4">
                                  <div class="form-group">
                                  <label>Jumlah IDR</label>
                                  <input type="text" class="form-control" placeholder="Jumlah IDR" name="jumlah_idr" value="<?=isset($default['jumlah_idr'])? $default['jumlah_idr'] : ""?>" required>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>

                      </div>
                  </div>
              </div>
            </div>

            <div class="form-actions">
                <button type="button" class="btn default">Cancel</button>
                <button type="submit" class="btn red" name="submit_voucher">Submit</button>
            </div>
          </form>
      </div>
    </div>
