<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Form Bank</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">
          <form role="form" method="post">
              <div class="form-body">
                <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                            <label>Tanggal Voucher</label>
                            <div class="input-group">
                              <span class="input-group-addon input-circle-left">
                                  <i class="fa fa-calendar"></i>
                              </span>
                              <input class="form-control form-control-inline input-medium date-picker input-circle-right" name="tanggal_voucher" size="16" type="text" value="<?php echo date('m/d/Y');?>" />
                              <span class="help-block"></span>
                            </div>
                      </div>

                      <div class="form-group">
                            <label>Tipe Voucher</label>
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="tipe_voucher" id="optionsRadios4" value="1" onChange="tampilnomor2(this.value);"> Debit
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="tipe_voucher" id="optionsRadios5" value="0" onChange="tampilnomor2(this.value);"> Credit
                                    <span></span>
                                </label>
                            </div>
                        </div>

                          <div name="txtSatuan2" id="txtSatuan2"><b>Nomor Voucher</b></div>

                  </div>


                  <div class="col-sm-8" id="detail_cash_bank">
                      <div class="portlet light bordered">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Akun Bank</label>
                                    <div class="input-group">
                                       <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="coa_id_cash_bank" required>
                                         <?php 	foreach($list_coa_bank as $row){ ?>
                                           <option value="<?php echo $row['coa_id'];?>"><?php echo $row['nomor']." | ".$row['nama'];?></option>
                                         <?php } ?>
                                       </select>
                                 </div>
                               </div>
                            </div>

                            <div class="col-sm-6">
                              <div class="form-group">
                                  <label>Akun Lawan Transaksi</label>
                                  <div class="input-group">
                                     <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="coa_id_lawan" required>
                                       <?php 	foreach($list_coa as $row){ ?>
                                         <option value="<?php echo $row['coa_id'];?>"><?php echo $row['nomor']." | ".$row['nama'];?></option>
                                       <?php } ?>
                                     </select>
                               </div>
                             </div>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label>Keterangan Voucher</label>
                              <input type="text" class="form-control" placeholder="Keterangan" name="keterangan_voucher" required>
                            </div>
                          </div>
                          <div class="col-sm-12">
                              <div class="row">
                                <div class="col-sm-4">
                                  <div class="form-group">
                                  <label>Jumlah Valas</label>
                                  <input type="text" class="form-control" placeholder="Jumlah Valas" name="jumlah_valas">
                                  </div>
                                </div>

                                <div class="col-sm-4">
                                  <div class="form-group">
                                  <label>Kurs</label>
                                  <input type="text" class="form-control" placeholder="Kurs" name="kurs" onChange="a(this.value);">
                                  </div>
                                </div>

                                <div class="col-sm-4">
                                  <div class="form-group">
                                  <label>Jumlah IDR</label>
                                  <input type="text" class="form-control" placeholder="Jumlah IDR" name="jumlah_idr" required>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>

                      </div>
                  </div>
              </div>
            </div>

            <div class="form-actions">
                <button type="button" class="btn default">Cancel</button>
                <button type="submit" class="btn red" name="submit_voucher">Submit</button>
            </div>
          </form>
      </div>
    </div>

    <script>
				function tampilnomor2(str)
				{
				if (str=="")
				  {
				  document.getElementById("txtSatuan2").innerHTML="";
				  return;
				  }
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
					document.getElementById("txtSatuan2").innerHTML=xmlhttp.responseText;
					}
				  }
				xmlhttp.open("GET","Bank_controller/get_nomor?q="+str+"&z=b",true);
				xmlhttp.send();
				}
				</script>
