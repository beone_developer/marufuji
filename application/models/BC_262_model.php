<?php

class BC_262_model extends CI_Model {

//    var $tabel = "usr";
    //   private $another;
    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
        $this->mysql = $this->load->database('mysql', TRUE);
    }

    public function select() {

        $select = "select DISTINCT th.id, th.nomor_aju,th.NAMA_PENERIMA_BARANG, th.JUMLAH_BARANG, tkemas.JUMLAH_KEMASAN,rs.URAIAN_STATUS
            from tpb_header th
            left join tpb_kemasan tkemas on th.id = tkemas.ID_HEADER
            left join referensi_status rs on th.KODE_STATUS = rs.KODE_STATUS and th.KODE_DOKUMEN_PABEAN = rs.KODE_DOKUMEN
            where th.KODE_DOKUMEN_PABEAN = 262 ";
        $data = $this->mysql->query($select);
        return $data->result();
    }
    public function delete($id) {
        
        $delete_pungutan = "delete from tpb_pungutan where ID_HEADER = '" . $id . "'";
        $data1 = $this->mysql->query($delete_pungutan);
        $delete_kontainer = "delete from tpb_kontainer where ID_HEADER = '" . $id . "'";
        $data2 = $this->mysql->query($delete_kontainer);
        $delete_kemasan = "delete from tpb_kemasan where ID_HEADER = '" . $id . "'";
        $data3 = $this->mysql->query($delete_kemasan);
        $delete_dokumen = "delete from tpb_dokumen where ID_HEADER = '" . $id . "'";
        $data4 = $this->mysql->query($delete_dokumen);
        $delete_brgtarif = "delete from tpb_barang_tarif where ID_HEADER = '" . $id . "'";
        $data5 = $this->mysql->query($delete_brgtarif);
        $delete_dtl= "delete from tpb_detil_status where ID_HEADER = '" . $id . "'";
        $data7 = $this->mysql->query($delete_dtl);
        $delete_val= "delete from hasil_validasi_header where ID_HEADER = '" . $id . "'";
        $data8 = $this->mysql->query($delete_val);
        $delete_bbdok= "delete from tpb_bahan_baku_dokumen where ID_HEADER = '" . $id . "'";
        $data10 = $this->mysql->query($delete_bbdok);
        $delete_bbtar= "delete from tpb_bahan_baku_tarif where ID_HEADER = '" . $id . "'";
        $data11 = $this->mysql->query($delete_bbtar);
        $delete_bb= "delete from tpb_bahan_baku where ID_HEADER = '" . $id . "'";
        $data9 = $this->mysql->query($delete_bb);
        $delete_brg= "delete from tpb_barang where ID_HEADER = '" . $id . "'";
        $data6 = $this->mysql->query($delete_brg);
        $delete_brgdok= "delete from tpb_barang_dokumen where ID_HEADER = '" . $id . "'";
        $data12 = $this->mysql->query($delete_brgdok);
        $delete_brgter= "delete from tpb_barang_penerima where ID_HEADER = '" . $id . "'";
        $data13 = $this->mysql->query($delete_brgter);
        $delete_jamin= "delete from tpb_jaminan where ID_HEADER = '" . $id . "'";
        $data14 = $this->mysql->query($delete_jamin);
        $delete_billing= "delete from tpb_npwp_billing where ID_HEADER = '" . $id . "'";
        $data15 = $this->mysql->query($delete_billing);
        $delete_penerima= "delete from tpb_penerima where ID_HEADER = '" . $id . "'";
        $data16 = $this->mysql->query($delete_penerima);
        $delete_respon = "delete from tpb_respon where ID_HEADER = '" . $id . "'";
        $data17 = $this->mysql->query($delete_respon);
        
        $delete = "delete from tpb_header where ID = '" . $id . "'";
        $data = $this->mysql->query($delete);
        if ($data)
            return true;
        return false;
    }

}

?>
