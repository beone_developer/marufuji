<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Recal_model extends CI_Model{

	public function recal_inventory($post){
		$tgl = $this->db->escape($post['tanggal_awal']);
		$tgl_akhir = $this->db->escape($post['tanggal_akhir']);

		$tgl_bulan = substr($tgl, 1, 2);
		$tgl_hari = substr($tgl, 4, 2);
		$tgl_tahun = substr($tgl, 7, 4);

		$tgl_akhir_bulan = substr($tgl_akhir, 1, 2);
		$tgl_akhir_hari = substr($tgl_akhir, 4, 2);
		$tgl_akhir_tahun = substr($tgl_akhir, 7, 4);

		$tanggal_awal = $tgl_akhir_tahun."-01-01";
		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;
		$tanggal_akhir = $tgl_akhir_tahun."-".$tgl_akhir_bulan."-".$tgl_akhir_hari;

		$sql_item_inventory = $this->db->query("SELECT distinct(item_id) FROM public.beone_inventory WHERE flag = 1");

		foreach($sql_item_inventory->result_array() as $row){
						$sql_list_item = $this->db->query("SELECT * FROM public.beone_inventory WHERE item_id = ".intval($row['item_id'])." AND flag = 1 AND trans_date BETWEEN '$tanggal' AND '$tanggal_akhir' ORDER BY trans_date ASC, intvent_trans_id ASC");

						//SALDO AWAL ITEM
						$sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =".intval($row['item_id']));
						$saldo_awal_item = $sai->row_array();

						//SALDO MUTASI ITEM DARI AWAL TAHUN SAMPAI SEKARANG
						$inv = $this->db->query("SELECT qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount FROM public.beone_inventory WHERE trans_date BETWEEN '$tanggal_awal' AND '$tanggal' AND item_id =".intval($row['item_id'])." ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
						$hasil_inv = $inv->row_array();

						//cek saldo awal
						$cek = $this->db->query("SELECT count(item_id) as jml FROM public.beone_inventory WHERE flag = 1 AND item_id = ".intval($row['item_id'])); //tambah filteran range tanggal sampai tgl produksi (edit lukman)
						$cek_sa = $cek->row_array();


						$urutan_transaksi = 0;
						$saldo_awal_qty = 0;
						$saldo_awal_unit_price = 0;
						$saldo_awal_amount = 0;

						foreach($sql_list_item->result_array() as $row2){//mutasi per item

								$kode_transaksi = substr($row2['intvent_trans_no'], 0, 2);

								if ($kode_transaksi == "BO"){
//******************************************************************************************************
													$sql_produksi = $this->db->query("SELECT *
																														FROM public.beone_transfer_stock
																														WHERE transfer_no = '$row2[intvent_trans_no]'");
													$hasil_produksi = $sql_produksi->row_array();

													$sql_produksi_detail = $this->db->query("SELECT *
																														FROM public.beone_transfer_stock_detail
																														WHERE item_id = $row2[item_id] AND transfer_stock_header_id = ".intval($hasil_produksi['transfer_stock_id']));
													$hasil_produksi_detail = $sql_produksi_detail->row_array();

													$urutan_transaksi = $urutan_transaksi + 1;

													if ($urutan_transaksi == 1){//kalau transaksi pertama tidak memiliki saldo awal item maka menggunakan perhitungan transaksinya
															if ($cek_sa['jml'] == 0){ //jika tidak ada mutasi maka ambil dari saldo awal item
																	$sa_qty = $saldo_awal_item['saldo_qty'];
																	$sa_amount = $saldo_awal_item['saldo_idr'];

																	if ($saldo_awal_item['saldo_qty'] == 0 OR $saldo_awal_item['saldo_idr'] == 0){
																			if ($row2['qty_in'] == 0){//transaksi keluar
																					$unit_price_awal = $row2['value_out'];
																			}else{
																					$unit_price_awal = $row2['value_in'];
																			}

																	}else{
																					$unit_price_awal = $saldo_awal_item['saldo_idr'] / $saldo_awal_item['saldo_qty'];
																	}

																	$sa_qty_akhir = ($saldo_awal_item['saldo_qty'] + $row2['qty_in']) - $row2['qty_out'];

																	if ($row2['qty_in'] == 0){//transaksi keluar
																			$sa_amount_akhir = $saldo_awal_item['saldo_idr'] - ($row2['qty_out'] * $row2['value_out']);
																			$unit_price_akhir = ($saldo_awal_item['saldo_idr'] - ($row2['qty_out'] * $row2['value_out'])) / ($saldo_awal_item['saldo_qty'] + $row2['qty_in']) - $row2['qty_out'];
																	}else{
																			$sa_amount_akhir = $saldo_awal_item['saldo_idr'] + ($row2['qty_in'] * $row2['value_in']);
																			$unit_price_akhir = ($saldo_awal_item['saldo_idr'] + ($row2['qty_in'] * $row2['value_in'])) / ($saldo_awal_item['saldo_qty'] + $row2['qty_in']) - $row2['qty_out'];
																	}

																	$unit_price_akhir = $sa_amount_akhir / $sa_qty_akhir;

																	$sql_produksi = $this->db->query("UPDATE public.beone_inventory
																																		SET sa_qty=$sa_qty_akhir, sa_unit_price=$unit_price_akhir, sa_amount=$sa_amount_akhir
																																		WHERE intvent_trans_id = $row2[intvent_trans_id]");

															}else{// jka ada mutasi diambil dari saldo terakhir urut asc

																	$sa_qty_akhir = ($saldo_awal_qty + $row2['qty_in']) - $row2['qty_out'];

																	if ($row2['qty_in'] == 0){
																			$sa_amount_akhir = $saldo_awal_amount - ($row2['qty_out']*$row2['value_out']);
																			$unit_price_akhir = ($saldo_awal_amount - ($row2['qty_out']*$row2['value_out'])) / (($saldo_awal_qty + $row2['qty_in']) - $row2['qty_out']);
																	}else{
																		//sum saldo dari bahan baku
																		$amount_bb = $this->db->query("SELECT SUM(qty_out*value_out) as amount FROM public.beone_inventory WHERE intvent_trans_id =".intval($row2['intvent_trans_id']));
																		$total_amount_bahan_baku = $amount_bb->row_array();

																		$sa_amount_akhir = (($total_amount_bahan_baku['amount'] * $hasil_produksi_detail['persen_produksi']) /100) + $hasil_produksi_detail['biaya'];
																		$unit_price_akhir = ((($total_amount_bahan_baku['amount'] * $hasil_produksi_detail['persen_produksi']) /100) + $hasil_produksi_detail['biaya']) / ($saldo_awal_qty + $row2['qty_in']) - $row2['qty_out'];
																		//$sa_amount_akhir = $saldo_awal_amount + ($row2['qty_in']*$row2['value_in']);
																		//$unit_price_akhir = ($saldo_awal_amount + ($row2['qty_in']*$row2['value_in'])) / (($saldo_awal_qty + $row2['qty_in']) - $row2['qty_out']);
																	}
																	//lukman
																	$sql_produksi = $this->db->query("UPDATE public.beone_inventory
																																		SET sa_qty=$sa_qty_akhir, sa_unit_price=$unit_price_akhir, sa_amount=$sa_amount_akhir
																																		WHERE intvent_trans_id = $row2[intvent_trans_id]");
															}
													}else{//jika urutan sudah lebih dari 1 maka ambil saldo dari transaksi sebelumnya


														$sa_qty_akhir = ($saldo_awal_qty + $row2['qty_in']) - $row2['qty_out'];

														if ($hasil_produksi_detail['tipe_transfer_stock'] == "BB"){
																$sa_amount_akhir = $saldo_awal_amount - ($row2['qty_out']*$row2['value_out']);
																$unit_price_akhir = ($saldo_awal_amount - ($row2['qty_out']*$row2['value_out'])) / (($saldo_awal_qty + $row2['qty_in']) - $row2['qty_out']);
														}else{
																//sum saldo dari bahan baku
																$amount_bb = $this->db->query("SELECT SUM(qty_out*value_out) as amount FROM public.beone_inventory WHERE intvent_trans_id =".intval($row2['intvent_trans_id']));
																$total_amount_bahan_baku = $amount_bb->row_array();

																$sa_amount_akhir = (($total_amount_bahan_baku['amount'] * $hasil_produksi_detail['persen_produksi']) /100) + $hasil_produksi_detail['biaya'];
																$unit_price_akhir = ((($total_amount_bahan_baku['amount'] * $hasil_produksi_detail['persen_produksi']) /100) + $hasil_produksi_detail['biaya']) / ($saldo_awal_qty + $row2['qty_in']) - $row2['qty_out'];

																//$sa_amount_akhir = $saldo_awal_amount + ($row2['qty_in']*$row2['value_in']);
																//$unit_price_akhir = ($saldo_awal_amount - ($row2['qty_out']*$row2['value_out'])) / (($saldo_awal_qty + $row2['qty_in']) - $row2['qty_out']);
														}

																$sql_produksi = $this->db->query("UPDATE public.beone_inventory
																																	SET sa_qty=$sa_qty_akhir, sa_unit_price=$unit_price_akhir, sa_amount=$sa_amount_akhir
																																	WHERE intvent_trans_id = $row2[intvent_trans_id]");
													}
//******************************************************************************************************
								}

								$sak = $this->db->query("SELECT intvent_trans_id, sa_qty, sa_unit_price, sa_amount FROM public.beone_inventory WHERE intvent_trans_id =".intval($row2['intvent_trans_id']));
								$hasil_sak = $sak->row_array();

								$saldo_awal_qty = $hasil_sak['sa_qty'];
								$saldo_awal_unit_price = $hasil_sak['sa_unit_price'];
								$saldo_awal_amount = $hasil_sak['sa_amount'];
						}

				}
}
}
?>
