<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Subkontrak_model extends CI_Model{
	function __construct(){
			parent::__construct();
			$this->db = $this ->load -> database('default', TRUE);
			$this->mysql = $this ->load -> database('mysql', TRUE);
	}

	public function load_import_header_subkon_out(){
			$select = "select * from tpb_header where KODE_DOKUMEN_PABEAN = 261 order by TANGGAL_AJU desc";
			$sql_data_import = $this->mysql->query($select);
			return $sql_data_import->result_array();
	}

	public function load_import_header_subkon_in(){
			$select = "select * from tpb_header where KODE_DOKUMEN_PABEAN = 262 order by TANGGAL_AJU desc";
			$sql_data_import = $this->mysql->query($select);
			return $sql_data_import->result_array();
	}

	public function load_subkontrak_out(){
		$sql = $this->db->query("SELECT * FROM public.beone_subkon_out_header WHERE flag = 1");
		return $sql->result_array();
	}

	public function kirim($post, $import_header_id){
		$session_id = $this->session->userdata('user_id');
		$tgl_kirim = $this->db->escape($post['tgl_kirim']);
		$subkontrak_no = $this->db->escape($post['subkontrak_no']);
		$supplier = $this->db->escape($post['supplier']);
		$keterangan = $this->db->escape($post['keterangan']);
		$gudang_asal = 1; //bahan baku
		$gudang_tujuan = 5; //subkontrak

		$jml_item = $this->db->escape($post['jml_item']);
		$nomor_aju = $this->db->escape($post['nomor_aju']);

		$tgl_bulan = substr($tgl_kirim, 1, 2);
		$tgl_hari = substr($tgl_kirim, 4, 2);
		$tgl_tahun = substr($tgl_kirim, 7, 4);
		$update_date = date('Y-m-d');


		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		//select header tpb
		$select = "select * from tpb_header where id = ".intval($import_header_id);
		$header_tpb_ = $this->mysql->query($select);
		$header_tpb = $header_tpb_->row_array();

		//insert table subkontrak out header
		$sql_header = $this->db->query("INSERT INTO public.beone_subkon_out_header(
																		subkon_out_id, no_subkon_out, trans_date, supplier_id, keterangan, nomor_aju, flag, update_by, update_date)
																		VALUES (DEFAULT, $subkontrak_no, '$tanggal', $supplier, $keterangan, $nomor_aju, 1, $session_id, '$update_date')");

		$header_sk = $this->db->query("SELECT subkon_out_id FROM public.beone_subkon_out_header ORDER BY subkon_out_id DESC LIMIT 1");
		$id_header = $header_sk->row_array();
		$id = $id_header['subkon_out_id'];

		$jml = ($post['jml_item']*1) - 1;

		for ($x = 0; $x <= $jml; $x++) {
			$ctr = $x + 1;

			$item = $this->db->escape($post['item_'.$ctr]);
			$qty_ = $this->db->escape($post['qty_'.$ctr]);
			$bc = $nomor_aju;
			$qty = $post['qty_'.$ctr] * 1;


			//insert mutasi gudang subkon
				$sql_out = $this->db->query("INSERT INTO public.beone_gudang_detail(
																	gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan)
																	VALUES (DEFAULT, $gudang_asal, '$tanggal', $item, 0, $qty, $subkontrak_no, $session_id, '$update_date', 1, $keterangan)");

			  /*$sql_in = $this->db->query("INSERT INTO public.beone_gudang_detail(
																	gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan)
																	VALUES (DEFAULT, $gudang_tujuan, '$tanggal', $item, $qty, 0, $subkontrak_no, $session_id, '$update_date', 1, $keterangan)");*/

				//-------------------------
				//cek saldo awal
				$inv = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = $item ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
				$hasil_inv = $inv->row_array();
				//end cek saldo awal

				//SALDO AWAL ITEM
				$sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =".intval($item));
				$saldo_awal_item = $sai->row_array();

				if ($hasil_inv['qty_in'] == NULL){
						if ($saldo_awal_item['saldo_idr'] == 0 OR $saldo_awal_item['saldo_qty'] == 0){
								$unit_price_awal = 0; // salah, harusnya amount saat ini dibagi qty saat ini
						}else{
								$unit_price_awal = $saldo_awal_item['saldo_idr'] / $saldo_awal_item['saldo_qty'];
						}

						$sa_akhir_qty = $saldo_awal_item['saldo_qty'] - $qty;
						$sa_akhir_amount = $saldo_awal_item['saldo_idr'] - ($qty * $unit_price_awal);
						$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;

						$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $subkontrak_no, $item, '$tanggal', 'SUBKONTRAK OUT', 0, 0, $qty, $unit_price_awal, $sa_akhir_qty, $unit_price_akhir, $sa_akhir_amount, 1, $session_id, '$update_date')");
				}else{
						$sa_qty = $hasil_inv['sa_qty'];
						$sa_akhir_qty = $sa_qty-$qty;

						$sa_amount = $hasil_inv['sa_amount'];
						$sa_akhir_amount = $hasil_inv['sa_amount'] - ($qty * $hasil_inv['sa_unit_price']);

						$unit_price_awal = $hasil_inv['sa_unit_price'];
						if ($sa_akhir_amount == 0 OR $sa_akhir_qty == 0){
								$unit_price_akhir = 0;
								$sa_akhir_amount = 0;
						}else{
								$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;
						}

						$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $subkontrak_no, $item, '$tanggal', 'SUBKONTRAK OUT', 0, 0, $qty, $unit_price_awal, $sa_akhir_qty, $unit_price_akhir, $sa_akhir_amount, 1, $session_id, '$update_date')");
				}
				//-------------------------

				$detail_subkon = $this->db->query("INSERT INTO public.beone_subkon_out_detail(
																						subkon_out_detail_id, subkon_out_header_id, item_id, qty, unit_price, flag)
																						VALUES (DEFAULT, $id, $item, $qty, $unit_price_akhir, 1)");
		}

		if($sql_header)
			return true;
		return false;
	}


	public function terima($post, $import_header_id){
		$session_id = $this->session->userdata('user_id');
		$tgl_penerimaan = $this->db->escape($post['tgl_penerimaan']);
		$subkontrak_in_no = $this->db->escape($post['nomor_sk_in']);
		$subkontrak_out_no = $this->db->escape($post['nomor_sk_out']);
		$supplier = $this->db->escape($post['supplier']);
		$biaya_subkontrak_ = $this->db->escape($post['biaya_subkontrak']);
		$keterangan = $this->db->escape($post['keterangan']);

		$biaya_subkontrak_ex = str_replace(".", "", $biaya_subkontrak_);
		$biaya_subkontrak = str_replace(",", ".", $biaya_subkontrak_ex);

		$gudang_asal = 1; //bahan baku
		$gudang_tujuan = 1; //subkontrak

		$jml_item = $this->db->escape($post['jml_item']);
		$nomor_aju = $this->db->escape($post['nomor_aju']);

		$tgl_bulan = substr($tgl_penerimaan, 1, 2);
		$tgl_hari = substr($tgl_penerimaan, 4, 2);
		$tgl_tahun = substr($tgl_penerimaan, 7, 4);
		$update_date = date('Y-m-d');


		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		//select header tpb
		$select = "select * from tpb_header where id = ".intval($import_header_id);
		$header_tpb_ = $this->mysql->query($select);
		$header_tpb = $header_tpb_->row_array();

		$sql_update_status_subkon_out = $this->db->query("UPDATE public.beone_subkon_out_header
																											SET flag=0 WHERE no_subkon_out = $subkontrak_out_no");

		//insert table subkontrak out header
		$sql_header = $this->db->query("INSERT INTO public.beone_subkon_in_header(
																		subkon_in_id, no_subkon_in, no_subkon_out, trans_date, keterangan, nomor_aju, biaya_subkon, supplier_id, flag, update_by, update_date)
																		VALUES (DEFAULT, $subkontrak_in_no, $subkontrak_out_no, '$tanggal', $keterangan, $nomor_aju, $biaya_subkontrak, $supplier, 1, $session_id, '$update_date')");

		$header_sk = $this->db->query("SELECT subkon_in_id FROM public.beone_subkon_in_header ORDER BY subkon_in_id DESC LIMIT 1");
		$id_header = $header_sk->row_array();
		$id = $id_header['subkon_in_id'];

		$jml = ($post['jml_item']*1) - 1;

		for ($x = 0; $x <= $jml; $x++) {
			$ctr = $x + 1;

			$item = $this->db->escape($post['item_'.$ctr]);
			$qty_ = $this->db->escape($post['qty_'.$ctr]);
			$bc = $nomor_aju;
			$qty = $post['qty_'.$ctr] * 1;


			//insert mutasi gudang subkon
				/*$sql_out = $this->db->query("INSERT INTO public.beone_gudang_detail(
																	gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan)
																	VALUES (DEFAULT, $gudang_asal, '$tanggal', $item, 0, $qty, $subkontrak_no, $session_id, '$update_date', 1, $keterangan)");*/

				$sql_in = $this->db->query("INSERT INTO public.beone_gudang_detail(
																	gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan)
																	VALUES (DEFAULT, $gudang_tujuan, '$tanggal', $item, $qty, 0, $subkontrak_in_no, $session_id, '$update_date', 1, $keterangan)");

				$detail_subkon = $this->db->query("INSERT INTO public.beone_subkon_in_detail(
																					subkon_in_detail_id, subkon_in_header_id, item_id, qty, flag)
																					VALUES (DEFAULT, $id, $item, $qty, 1)");

				//-------------------------
				//cek saldo awal
				$inv = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = $item ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
				$hasil_inv = $inv->row_array();
				//end cek saldo awal

				//SALDO AWAL ITEM
				$sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =".intval($item));
				$saldo_awal_item = $sai->row_array();

				if ($hasil_inv['qty_in'] == NULL){
						if ($saldo_awal_item['saldo_idr'] == 0 OR $saldo_awal_item['saldo_qty'] == 0){
								$unit_price_awal = 0; // salah, harusnya amount saat ini dibagi qty saat ini
						}else{
								$unit_price_awal = $saldo_awal_item['saldo_idr'] / $saldo_awal_item['saldo_qty'];
						}

						$sa_akhir_qty = $saldo_awal_item['saldo_qty'] - $qty;
						$sa_akhir_amount = $saldo_awal_item['saldo_idr'] - ($qty * $unit_price_awal);
						$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;

						$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $subkontrak_in_no, $item, '$tanggal', 'SUBKONTRAK IN', $qty, $unit_price_awal, 0, 0, $sa_akhir_qty, $unit_price_akhir, $sa_akhir_amount, 1, $session_id, '$update_date')");
				}else{
						$sa_qty = $hasil_inv['sa_qty'];
						$sa_akhir_qty = $sa_qty-$qty;

						$sa_amount = $hasil_inv['sa_amount'];
						$sa_akhir_amount = $hasil_inv['sa_amount'] - ($qty * $hasil_inv['sa_unit_price']);

						$unit_price_awal = $hasil_inv['sa_unit_price'];
						if ($sa_akhir_amount == 0 OR $sa_akhir_qty == 0){
								$unit_price_akhir = 0;
								$sa_akhir_amount = 0;
						}else{
								$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;
						}

						$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $subkontrak_in_no, $item, '$tanggal', 'SUBKONTRAK IN', $qty, $unit_price_awal, 0, 0, $sa_akhir_qty, $unit_price_akhir, $sa_akhir_amount, 1, $session_id, '$update_date')");
				}
				//-------------------------
		}

		if($sql_header)
			return true;
		return false;
	}


}
?>
