<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Item_model extends CI_Model{

	public function simpan($post){
		$session_id = $this->session->userdata('user_id');
		$nama = $this->db->escape($post['nama_item']);
		$tipe = $this->db->escape($post['item_type']);
		$gudang_id = $this->db->escape($post['gudang']);
		$code = $this->db->escape($post['item_code']);
		$hscode = $this->db->escape($post['hscode']);
		$satuan = $this->db->escape($post['satuan']);
		$saldo_qty_ = $this->db->escape($post['saldo_qty']);
		$saldo_idr_ = $this->db->escape($post['saldo_idr']);
		$keterangan = $this->db->escape($post['keterangan']);
		$update_date = date('Y-m-d');
		$tgl_tahun = substr($update_date, 0, 4);
		$tanggal = $tgl_tahun."-01-01";

		$saldo_qty_ex = str_replace(".", "", $saldo_qty_);
		$saldo_qty = str_replace(",", ".", $saldo_qty_ex);

		$saldo_idr_ex = str_replace(".", "", $saldo_idr_);
		$saldo_idr = str_replace(",", ".", $saldo_idr_ex);

		$sql = $this->db->query("INSERT INTO public.beone_item(item_id, nama, item_code, saldo_qty, saldo_idr, keterangan, flag, item_type_id, hscode, satuan_id) VALUES (DEFAULT, $nama, $code, $saldo_qty, $saldo_idr, $keterangan, 1, $tipe, $hscode, $satuan)");
		helper_log($tipe = "add", $str = "Tambah Item ".$post['nama_item']);

		if ($post['saldo_qty'] > 0){
			$sql_item_id = $this->db->query("SELECT * FROM public.beone_item WHERE flag = 1 ORDER BY item_id DESC LIMIT 1");
			$hasil_item_id = $sql_item_id->row_array();
			$item_id = $hasil_item_id['item_id'];

			$sql_gudang = $this->db->query("INSERT INTO public.beone_gudang_detail(
																			gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan)
																			VALUES (DEFAULT, $gudang_id, '$tanggal', $item_id, $saldo_qty, 0, $code, $session_id, '$update_date', 1, 'Saldo Awal')");
		}

		if($sql)
			return true;
		return false;
	}

	public function update($post, $item_id){
		$nama = $this->db->escape($post['nama_item']);
		$tipe = $this->db->escape($post['item_type']);
		$code = $this->db->escape($post['item_code']);
		$hscode = $this->db->escape($post['hscode']);
		$satuan = $this->db->escape($post['satuan']);
		$saldo_qty_ = $this->db->escape($post['saldo_qty']);
		$saldo_idr_ = $this->db->escape($post['saldo_idr']);
		$keterangan = $this->db->escape($post['keterangan']);
		$update_date = date('Y-m-d');

		$saldo_qty_ex = str_replace(".", "", $saldo_qty_);
		$saldo_qty = str_replace(",", ".", $saldo_qty_ex);
		//$saldo_qty = round($saldo_qty_round, 2);

		$saldo_idr_ex = str_replace(".", "", $saldo_idr_);
		$saldo_idr = str_replace(",", ".", $saldo_idr_ex);
		//$saldo_idr = round($saldo_idr_round, 2);

		$sql = $this->db->query("UPDATE public.beone_item SET nama=$nama, item_code=$code, saldo_qty=$saldo_qty, saldo_idr=$saldo_idr, keterangan=$keterangan, item_type_id=$tipe , hscode=$hscode, satuan_id = $satuan WHERE item_id=".intval($item_id));
		helper_log($tipe = "edit", $str = "Ubah Item ".$post['nama_item']);

		if($sql)
			return true;
		return false;
	}

	public function delete($item_id){
		$sql_item = $this->db->query("SELECT nama FROM public.beone_item WHERE item_id = ".intval($item_id));
		$hasil_item = $sql_item->row_array();
		helper_log($tipe = "delete", $str = "Hapus Item ".$hasil_item['nama']);

		$sql = $this->db->query("UPDATE public.beone_item SET flag=0 WHERE item_id =".intval($item_id));
	}


	public function simpan_satuan($post){
		$satuan_code = $this->db->escape($post['satuan_code']);
		$keterangan = $this->db->escape($post['keterangan']);

		$sql = $this->db->query("INSERT INTO public.beone_satuan_item (satuan_id, satuan_code, keterangan, flag) VALUES (DEFAULT, $satuan_code, $keterangan, 1);");
		helper_log($tipe = "add", $str = "Tambah Satuan ".$post['satuan_code']);

		if($sql)
			return true;
		return false;
	}

	public function update_satuan($post, $satuan_id){
		$satuan_code = $this->db->escape($post['satuan_code']);
		$keterangan = $this->db->escape($post['keterangan']);

		$sql = $this->db->query("UPDATE public.beone_satuan_item SET satuan_code=$satuan_code, keterangan=$keterangan WHERE satuan_id = ".intval($satuan_id));
		helper_log($tipe = "edit", $str = "Ubah Satuan ".$post['satuan_code']);

		if($sql)
			return true;
		return false;
	}

	public function delete_satuan($satuan_id){
		$sql_item = $this->db->query("SELECT satuan_code FROM public.beone_satuan_item WHERE satuan_id = ".intval($satuan_id));
		$hasil_item = $sql_item->row_array();
		helper_log($tipe = "delete", $str = "Hapus Satuan ".$hasil_item['satuan_code']);

		$sql = $this->db->query("UPDATE public.beone_satuan_item SET flag=0 WHERE satuan_id =".intval($satuan_id));
	}

	public function simpan_item_type($post){
		$nama = $this->db->escape($post['nama_tipe']);
		$keterangan = $this->db->escape($post['keterangan']);

		$sql = $this->db->query("INSERT INTO public.beone_item_type(item_type_id, nama, keterangan, flag) VALUES (DEFAULT, $nama, $keterangan, 1)");
		helper_log($tipe = "add", $str = "Tambah Tipe Item ".$post['nama_tipe']);

		if($sql)
			return true;
		return false;
	}

	public function update_item_type($post, $item_type_id){
		$nama = $this->db->escape($post['nama_tipe']);
		$keterangan = $this->db->escape($post['keterangan']);

		$sql = $this->db->query("UPDATE public.beone_item_type SET nama=$nama, keterangan=$keterangan WHERE item_type_id = ".intval($item_type_id));
		helper_log($tipe = "edit", $str = "Ubah Tipe Item ".$post['nama_tipe']);

		if($sql)
			return true;
		return false;
	}

	public function delete_item_type($item_type_id){
		$sql_item = $this->db->query("SELECT nama FROM public.beone_item_type WHERE item_type_id = ".intval($item_type_id));
		$hasil_item = $sql_item->row_array();
		helper_log($tipe = "delete", $str = "Hapus Tipe Item ".$hasil_item['nama']);

		$sql = $this->db->query("UPDATE public.beone_item_type SET flag=0 WHERE item_type_id =".intval($item_type_id));
	}

	public function get_default($item_id){
		$sql = $this->db->query("SELECT i.item_id, i.nama, i.item_code, i.saldo_qty, i.saldo_idr, i.Keterangan, i.flag, i.item_type_id, i.hscode, t.nama as ntipe, i.satuan_id, s.satuan_code FROM public.beone_item i INNER JOIN public.beone_item_type t ON i.item_type_id = t.item_type_id INNER JOIN public.beone_satuan_item s ON s.satuan_id = i.satuan_id WHERE i.item_id = ".intval($item_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function load_item(){
		$sql = $this->db->query("SELECT i.item_id, i.nama, i.item_code, i.saldo_qty, i.saldo_idr, i.Keterangan, i.hscode, i.flag, i.item_type_id, t.nama as ntipe, s.satuan_code as nsatuan
															FROM public.beone_item i INNER JOIN public.beone_item_type t ON i.item_type_id = t.item_type_id INNER JOIN public.beone_satuan_item s ON s.satuan_id = i.satuan_id
															WHERE i.flag=1 ORDER BY i.item_id ASC");
		return $sql->result_array();
	}

	public function load_satuan(){
		$sql = $this->db->query("SELECT * FROM public.beone_satuan_item WHERE flag = 1");
		return $sql->result_array();
	}

	public function get_default_satuan($satuan_id){
		$sql = $this->db->query("SELECT * FROM public.beone_satuan_item WHERE satuan_id = ".intval($satuan_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function get_default_item_type($item_type_id){
		$sql = $this->db->query("SELECT * FROM public.beone_item_type WHERE item_type_id = ".intval($item_type_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function load_item_type(){
		$sql = $this->db->query("SELECT * FROM public.beone_item_type WHERE flag=1 ORDER BY item_type_id ASC");
		return $sql->result_array();
	}

	/****************************************************************************/
	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename){
		$this->load->library('upload'); // Load librari upload

		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']	= '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;

		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		}else{
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}

	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	/*public function insert_multiple($data){
		$this->db->insert_batch('beone_coa', $data);
	}*/

	public function insert_multiple($data){
		//$this->db->insert_batch('beone_coa', $data);
		foreach($data as $row){
			$sql = $this->db->query("INSERT INTO public.beone_item VALUES ($row[item_id], '$row[nama]', '$row[item_code]', $row[saldo_qty], $row[saldo_idr], '$row[keterangan]', $row[flag], $row[item_type_id], '$row[hscode]', $row[satuan_id])");
		}}
	/****************************************************************************/

}
?>
