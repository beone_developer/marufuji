<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Komposisi_model extends CI_Model
{

	public function load_komposisi()
	{
		$sql = $this->db->query("SELECT k.komposisi_id, i.nama as item_jadi, bi.nama as item_baku, k.qty_item_baku
								FROM public.beone_komposisi k 
								INNER JOIN public.beone_item i ON i.item_id = k.item_jadi_id 
								INNER JOIN public.beone_item bi ON bi.item_id = k.item_baku_id 
								WHERE k.flag=1 ORDER BY k.komposisi_id ASC");
		return $sql->result_array();
	}

	public function simpan($post)
	{
		$item_jadi = $this->db->escape($post['item_jadi']);
		$item_baku = $this->db->escape($post['item_baku']);
		$qty = $this->db->escape($post['qty']);
		echo($item_jadi);
		echo($item_baku);

		$sql_header = $this->db->query("INSERT INTO public.beone_komposisi(komposisi_id,item_jadi_id,item_baku_id,qty_item_baku,flag)
										VALUES (DEFAULT, $item_jadi, $item_baku,$qty,1)");

	}

	public function delete($komposisi_id)
	{
		$sql = $this->db->query("DELETE FROM public.beone_komposisi WHERE komposisi_id = " . intval($komposisi_id));
		// helper_log($tipe = "delete", $str = "Hapus Pembelian, No ".$purchase_no);
	}

	public function get_default_komposisi($item_jadi_id)
	{
		$sql = $this->db->query("SELECT k.komposisi_id, k.item_baku_id, bi.nama as item_baku, k.qty_item_baku
								FROM public.beone_komposisi k 
								INNER JOIN public.beone_item bi ON bi.item_id = k.item_baku_id 
								WHERE k.flag=1 and item_jadi_id =" . intval($item_jadi_id));
		if ($sql->num_rows() > 0)
			return $sql->result_array();
		return [];
	}
}

?>
