<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Transfer_model extends CI_Model{


	public function simpan($post){
		$tgl = $this->db->escape($post['tanggal']);
		$kode_biaya = $this->db->escape($post['kode_biaya']);
		$keterangan = $this->db->escape($post['keterangan']);
		$transfer_no = $this->db->escape($post['transfer_no']);
		$update_date = date('Y-m-d');

		$tgl_bulan = substr($tgl, 1, 2);
		$tgl_hari = substr($tgl, 4, 2);
		$tgl_tahun = substr($tgl, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;
		$tgl_awal_tahun = $tgl_tahun."-01-"."01";

		$sql_header = $this->db->query("INSERT INTO public.beone_transfer_stock(
																	transfer_stock_id, transfer_no, transfer_date, coa_kode_biaya, keterangan, update_by, update_date, flag)
																	VALUES (DEFAULT, $transfer_no, '$tanggal', $kode_biaya, $keterangan, 1, '$update_date', 1);");

		$header_id = $this->db->query("SELECT * FROM public.beone_transfer_stock ORDER BY transfer_stock_id DESC LIMIT 1");
		$hasil_header_id = $header_id->row_array();
		$hid = $hasil_header_id['transfer_stock_id'];

		$amount_unit_price = 0;
		$up = 0;
		/******************************* DARI ITEM ********************************************/
		foreach ($_POST['rows'] as $key => $count ){

				$item = $_POST['item_id_'.$count];
				$qty_ = $_POST['qty_'.$count];
				$gudang = $_POST['gudang_id_'.$count];

				$qty_round = str_replace(".", "", $qty_);
				$qty = round($qty_round, 2);

				//INSERT TABEL TRANSFER STOCK
				$sql_detail = $this->db->query("INSERT INTO public.beone_transfer_stock_detail(
																					transfer_stock_detail_id, transfer_stock_header_id, tipe_transfer_stock, item_id, qty, gudang_id, biaya, update_by, update_date, flag)
																					VALUES (DEFAULT, $hid, 'BB', $item, $qty, $gudang, 0, 1, '$update_date', 1);");


				//SALDO AKHIR UNIT PRICE SAMPAI TGL SEKARANG DARI AWAL TAHUN
				//SALDO AWAL ITEM
				$sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =".intval($item));
				$saldo_awal_item = $sai->row_array();

				//SALDO MUTASI ITEM DARI AWAL TAHUN SAMPAI SEKARANG
				$sm = $this->db->query("SELECT SUM(qty_in) as qtyin, SUM(value_in) as idrin, SUM(qty_out) as qtyout, SUM(value_out) as idrout FROM public.beone_inventory WHERE trans_date BETWEEN '$tgl_awal_tahun' AND '$tanggal' AND item_id =".intval($item));
				$saldo_mutasi_item = $sm->row_array();

				//cek saldo awal
				$inv = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = ".intval($item)." AND trans_date BETWEEN '$tgl_awal_tahun' AND '$tanggal' ORDER BY trans_date DESC LIMIT 1"); //tambah filteran range tanggal sampai tgl produksi
				$hasil_inv = $inv->row_array();

				if ($hasil_inv['qty_in'] == NULL){
					//INSERT DI KARTO STOCK
							if ($saldo_awal_item['saldo_idr'] == 0 OR $saldo_awal_item['saldo_qty'] == 0){
									$unit_price = 0;
							}else{
									$unit_price_round = $saldo_awal_item['saldo_idr'] / $saldo_awal_item['saldo_qty'];
									$unit_price = round($unit_price_round, 2);
							}
					$sa_qty_round = $saldo_awal_item['saldo_qty'];
					$sa_qty = round($sa_qty_round, 2);
					$sa_akhir_qty = $sa_qty-$qty;
					$sa_akhir_amount_round = ($sa_qty-$qty)*$unit_price;
					$sa_akhir_amount = round($sa_akhir_amount_round, 2);

					$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $transfer_no, $item, '$tanggal', $keterangan, 0, 0, $qty, $unit_price, $sa_akhir_qty, $unit_price, $sa_akhir_amount, 1, 1, '$update_date')");

					$up_round = $up + $unit_price;
					$up = round($up_round, 2);

					$amount_unit_price_round = $amount_unit_price + ($qty * $unit_price);
					$amount_unit_price = round($amount_unit_price_round, 2);
				}else{
					$sa_qty_round = $hasil_inv['sa_qty'];
					$sa_qty = round($sa_qty_round, 2);
					$sa_akhir_qty = $sa_qty-$qty;
					$sa_akhir_amount_round = ($sa_qty-$qty)*$hasil_inv['sa_unit_price'];
					$sa_akhir_amount = round($sa_akhir_amount_round, 2);
					$unit_price_round = $hasil_inv['sa_unit_price'];
					$unit_price = round($unit_price_round, 2);

					$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $transfer_no, $item, '$tanggal', $keterangan, 0, 0, $qty, $unit_price, $sa_akhir_qty, $unit_price, $sa_akhir_amount, 1, 1, '$update_date')");
					$up_round = $up + $unit_price;
					$up = round($up_round, 2);
					$amount_unit_price_round = $amount_unit_price + ($qty * $unit_price);
					$amount_unit_price = round($amount_unit_price_round, 2);
				}

		}

		if ($post['kode_biaya'] == 1){// PEMAKAIAN BAHAN BAKU


			//INSERT DI KARTO STOCK
			$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																				gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																				VALUES (DEFAULT, '$tanggal', 63, '511-01', 11, '116-01', $keterangan, $amount_unit_price, 0, $transfer_no, $transfer_no, 1, '$update_date')");

			$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																				gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																				VALUES (DEFAULT, '$tanggal', 11, '116-01', 63, '511-01', $keterangan, 0, $amount_unit_price, $transfer_no, $transfer_no, 1, '$update_date')");

		}else{//PEMAKAIAN WIP
			$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																				gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																				VALUES (DEFAULT, '$tanggal', 14, '116-08', 13, '116-07', $keterangan, $amount_unit_price, 0, $transfer_no, $transfer_no, 1, '$update_date')");

			$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																				gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																				VALUES (DEFAULT, '$tanggal', 13, '116-07', 14, '116-08', $keterangan, 0, $amount_unit_price, $transfer_no, $transfer_no, 1, '$update_date')");
		}
		/******************************* END DARI ITEM ********************************************/

		/******************************* MENJADI ITEM ********************************************/
		$total_amount_wip = 0;
		$total_biaya = 0;
		foreach ($_POST['rows_hasil'] as $key => $count ){

			$item_hasil = $_POST['item_hasil_id_'.$count];

			$qty_hasil_ = $_POST['qty_hasil_'.$count];
			$qty_hasil_ex = str_replace(".", "", $qty_hasil_);
			$qty_hasil_round = str_replace(",", ".", $qty_hasil_ex);
			$qty_hasil = round($qty_hasil_round, 2);

			$gudang_hasil = $_POST['gudang_hasil_id_'.$count];

			$biaya_hasil_ = $_POST['biaya_hasil_'.$count];
			$biaya_hasil_ex = str_replace(".", "", $biaya_hasil_);
			$biaya_hasil_round = str_replace(",", ".", $biaya_hasil_ex);
			$biaya_hasil = round($biaya_hasil_round, 2);

			$persen_hasil_ = $_POST['persen_hasil_'.$count];
			$persen_hasil_ex = str_replace(".", "", $persen_hasil_);
			$persen_hasil_round = str_replace(",", ".", $persen_hasil_ex);
			$persen_hasil = round($persen_hasil_round, 2);

				$sql_detail2 = $this->db->query("INSERT INTO public.beone_transfer_stock_detail(
																					transfer_stock_detail_id, transfer_stock_header_id, tipe_transfer_stock, item_id, qty, gudang_id, biaya, update_by, update_date, flag, persen_produksi)
																					VALUES (DEFAULT, $hid, 'WIP', $item_hasil, $qty_hasil, 1, $biaya_hasil, 1, '$update_date', 1, $persen_hasil)");

					//SALDO AKHIR UNIT PRICE SAMPAI TGL SEKARANG DARI AWAL TAHUN
					//SALDO AWAL ITEM
					$sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =".intval($item_hasil));
					$saldo_awal_item = $sai->row_array();

					//cek saldo awal
					$inv = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = $item_hasil ORDER BY intvent_trans_id DESC LIMIT 1");
					$hasil_inv = $inv->row_array();

					if ($post['kode_biaya'] == 1){// PEMAKAIAN BAHAN BAKU
						//persen amount
						$persen_amount_round = ($amount_unit_price * $persen_hasil) / 100;
						$persen_amount = round($persen_amount_round, 2);

						//INSERT DI KARTO STOCK
						//$amount_hasil_round = ($persen_amount + $biaya_hasil) / $qty_hasil;
						//$amount_hasil = round($amount_hasil_round, 2);

						$sa_akhir_qty_round = $qty_hasil + $hasil_inv['sa_qty'];
						$sa_akhir_qty = round($sa_akhir_qty_round, 2);

						//$sa_akhir_amount_round = $sa_akhir_qty * $unit_price;
						$sa_akhir_amount_round = $persen_amount + $biaya_hasil;
						$sa_akhir_amount = round($sa_akhir_amount_round, 2);

						$unit_price_round = $sa_akhir_amount / $qty_hasil;
						$unit_price = round($unit_price_round, 2);

						$total_biaya_round = $total_biaya + $biaya_hasil;
						$total_biaya = round($total_biaya_round, 2);

						$sql_inventory2 = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $transfer_no, $item_hasil, '$tanggal', $keterangan, $qty_hasil, $unit_price, 0, 0, $sa_akhir_qty, $unit_price, $sa_akhir_amount, 1, 1, '$update_date')");


						$amount_wip_round = $persen_amount + $biaya_hasil;
						$amount_wip = round($amount_wip_round, 2);

						$total_amount_wip_round = $total_amount_wip + $amount_wip;
						$total_amount_wip = round($total_amount_wip_round, 2);

					}else{//PEMAKAIAN WIP

						//INSERT DI KARTO STOCK
						$persen_amount_round = ($amount_unit_price * $persen_hasil) / 100;
						$persen_amount = round($persen_amount_round, 2);

						$amount_hasil_round = ($persen_amount + $biaya_hasil) / $qty_hasil;
						$amount_hasil = round($amount_hasil_round, 2);

						$amount_wip_round = $persen_amount + $biaya_hasil;
						$amount_wip = round($amount_wip_round, 2);

						$total_biaya_round = $total_biaya + $biaya_hasil;
						$total_biaya = round($total_biaya_round, 2);

						$sql_inventory2 = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $transfer_no, $item_hasil, '$tanggal', $keterangan, $qty_hasil, $amount_hasil, 0, 0, $sa_akhir_qty, $unit_price, $sa_akhir_amount, 1, 1, '$update_date')");

						$total_amount_wip_round = $total_amount_wip + $amount_wip;
						$total_amount_wip = round($total_amount_wip_round, 2);
					}

		}

					if ($post['kode_biaya'] == 1){// PEMAKAIAN BAHAN BAKU
						$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																							gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																							VALUES (DEFAULT, '$tanggal', 13, '116-07', 61, '510-00', $keterangan, $total_amount_wip, 0, $transfer_no, $transfer_no, 1, '$update_date')");

						$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																							gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																							VALUES (DEFAULT, '$tanggal', 61, '510-00', 13, '116-07', $keterangan, 0, $total_amount_wip, $transfer_no, $transfer_no, 1, '$update_date')");

					}else{//PEMAKAIAN WIP
						$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																							gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																							VALUES (DEFAULT, '$tanggal', 14, '116-08', 61, '510-00', $keterangan, $total_biaya, 0, $transfer_no, $transfer_no, 1, '$update_date')");

						$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																							gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																							VALUES (DEFAULT, '$tanggal', 61, '510-00', 14, '116-08', $keterangan, 0, $total_biaya, $transfer_no, $transfer_no, 1, '$update_date')");
					}
		/******************************* END MENJADI ITEM ********************************************/

		if($sql_detail2)
			return true;
		return false;
	}


	public function update($post, $transfer_id){
		$session_id = $this->session->userdata('user_id');
		$update_date = date('Y-m-d');
		$sql_detail_del = $this->db->query("DELETE FROM public.beone_transfer_stock_detail WHERE transfer_stock_header_id = ".intval($transfer_id));

		$no_produksi = $this->db->query("SELECT * FROM public.beone_transfer_stock WHERE transfer_stock_id = ".intval($transfer_id));
		$hasil_no_produksi = $no_produksi->row_array();
		$transfer_no = $hasil_no_produksi['transfer_no'];
		$keterangan = $hasil_no_produksi['keterangan'];
		$kode_biaya = $hasil_no_produksi['coa_kode_biaya'];
		$hnp = $hasil_no_produksi['transfer_no'];
		$tgl = $hasil_no_produksi['transfer_date'];

		$sql_gl_del = $this->db->query("DELETE FROM public.beone_gl WHERE gl_number = '$hnp'");
		$sql_inventory_del = $this->db->query("DELETE FROM public.beone_inventory WHERE intvent_trans_no = '$hnp'");


		$tgl_tahun = substr($tgl, 0, 4);

		$tanggal = $tgl;
		$tgl_awal_tahun = $tgl_tahun."-01-"."01";

		/*$tgl = $this->db->escape($post['tanggal']);
		$kode_biaya = $this->db->escape($post['kode_biaya']);
		$keterangan = $this->db->escape($post['keterangan']);
		$transfer_no = $this->db->escape($post['transfer_no']);
		$update_date = date('Y-m-d');

		$tgl_bulan = substr($tgl, 1, 2);
		$tgl_hari = substr($tgl, 4, 2);
		$tgl_tahun = substr($tgl, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;
		$tgl_awal_tahun = $tgl_tahun."-01-"."01";

		$sql_header = $this->db->query("INSERT INTO public.beone_transfer_stock(
																	transfer_stock_id, transfer_no, transfer_date, coa_kode_biaya, keterangan, update_by, update_date, flag)
																	VALUES (DEFAULT, $transfer_no, '$tanggal', $kode_biaya, $keterangan, 1, '$update_date', 1);");

		$header_id = $this->db->query("SELECT * FROM public.beone_transfer_stock ORDER BY transfer_stock_id DESC LIMIT 1");
		$hasil_header_id = $header_id->row_array();
		$hid = $hasil_header_id['transfer_stock_id'];*/

		$amount_unit_price = 0;
		$up = 0;
		/******************************* DARI ITEM ********************************************/
		foreach ($_POST['rows'] as $key => $count ){
				$detail_id = $_POST['detail_id_'.$count];
				$item = $_POST['item_id_'.$count];
				$qty_ = $_POST['qty_'.$count];
				$gudang = $_POST['gudang_id_'.$count];

				$qty_ex = str_replace(".", "", $qty_);
				$qty_round = str_replace(",", ".", $qty_ex);
				$qty = round($qty_round, 2);

				//INSERT TABEL TRANSFER STOCK
				$sql_detail = $this->db->query("INSERT INTO public.beone_transfer_stock_detail(
																					transfer_stock_detail_id, transfer_stock_header_id, tipe_transfer_stock, item_id, qty, gudang_id, biaya, update_by, update_date, flag)
																					VALUES (DEFAULT, $transfer_id, 'BB', $item, $qty, $gudang, 0, $session_id, '$update_date', 1)");


				//SALDO AKHIR UNIT PRICE SAMPAI TGL SEKARANG DARI AWAL TAHUN
				//SALDO AWAL ITEM
				$sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =".intval($item));
				$saldo_awal_item = $sai->row_array();

				//SALDO MUTASI ITEM DARI AWAL TAHUN SAMPAI SEKARANG
				$sm = $this->db->query("SELECT SUM(qty_in) as qtyin, SUM(value_in) as idrin, SUM(qty_out) as qtyout, SUM(value_out) as idrout FROM public.beone_inventory WHERE trans_date BETWEEN '$tgl_awal_tahun' AND '$tanggal' AND item_id =".intval($item));
				$saldo_mutasi_item = $sm->row_array();

				//cek saldo awal
				$inv = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = ".intval($item)." AND trans_date BETWEEN '$tgl_awal_tahun' AND '$tanggal' ORDER BY trans_date DESC LIMIT 1"); //tambah filteran range tanggal sampai tgl produksi
				$hasil_inv = $inv->row_array();

				if ($hasil_inv['qty_in'] == NULL){
					//INSERT DI KARTO STOCK
							if ($saldo_awal_item['saldo_idr'] == 0 OR $saldo_awal_item['saldo_qty'] == 0){
									$unit_price = 0;
							}else{
									$unit_price_round = $saldo_awal_item['saldo_idr'] / $saldo_awal_item['saldo_qty'];
									$unit_price = round($unit_price_round, 2);
							}
					$sa_qty_round = $saldo_awal_item['saldo_qty'];
					$sa_qty = round($sa_qty_round, 2);
					$sa_akhir_qty = $sa_qty-$qty;
					$sa_akhir_amount_round = ($sa_qty-$qty)*$unit_price;
					$sa_akhir_amount = round($sa_akhir_amount_round, 2);

					$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, '$transfer_no', $item, '$tanggal', '$keterangan', 0, 0, $qty, $unit_price, $sa_akhir_qty, $unit_price, $sa_akhir_amount, 1, $session_id, '$update_date')");

					$up_round = $up + $unit_price;
					$up = round($up_round, 2);

					$amount_unit_price_round = $amount_unit_price + ($qty * $unit_price);
					$amount_unit_price = round($amount_unit_price_round, 2);
				}else{
					$sa_qty_round = $hasil_inv['sa_qty'];
					$sa_qty = round($sa_qty_round, 2);
					$sa_akhir_qty = $sa_qty-$qty;
					$sa_akhir_amount_round = ($sa_qty-$qty)*$hasil_inv['sa_unit_price'];
					$sa_akhir_amount = round($sa_akhir_amount_round, 2);
					$unit_price_round = $hasil_inv['sa_unit_price'];
					$unit_price = round($unit_price_round, 2);

					$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, '$transfer_no', $item, '$tanggal', '$keterangan', 0, 0, $qty, $unit_price, $sa_akhir_qty, $unit_price, $sa_akhir_amount, 1, $session_id, '$update_date')");
					$up_round = $up + $unit_price;
					$up = round($up_round, 2);
					$amount_unit_price_round = $amount_unit_price + ($qty * $unit_price);
					$amount_unit_price = round($amount_unit_price_round, 2);
				}

		}

		if ($kode_biaya == 1){// PEMAKAIAN BAHAN BAKU


			//INSERT DI KARTO STOCK
			$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																				gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																				VALUES (DEFAULT, '$tanggal', 63, '511-01', 11, '116-01', '$keterangan', $amount_unit_price, 0, '$transfer_no', '$transfer_no', $session_id, '$update_date')");

			$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																				gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																				VALUES (DEFAULT, '$tanggal', 11, '116-01', 63, '511-01', '$keterangan', 0, $amount_unit_price, '$transfer_no', '$transfer_no', $session_id, '$update_date')");

		}else{//PEMAKAIAN WIP
			$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																				gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																				VALUES (DEFAULT, '$tanggal', 14, '116-08', 13, '116-07', '$keterangan', $amount_unit_price, 0, '$transfer_no', '$transfer_no', $session_id, '$update_date')");

			$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																				gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																				VALUES (DEFAULT, '$tanggal', 13, '116-07', 14, '116-08', '$keterangan', 0, $amount_unit_price, '$transfer_no', '$transfer_no', $session_id, '$update_date')");
		}
		/******************************* END DARI ITEM ********************************************/

		/******************************* MENJADI ITEM ********************************************/
		$total_amount_wip = 0;
		$total_biaya = 0;
		foreach ($_POST['rows_hasil'] as $key => $count ){

			$item_hasil = $_POST['item_hasil_id_'.$count];
			$qty_hasil_ = $_POST['qty_hasil_'.$count];
			$qty_hasil_ex = str_replace(".", "", $qty_hasil_);
			$qty_hasil_round = str_replace(",", ".", $qty_hasil_ex);
			$qty_hasil = round($qty_hasil_round, 2);

			$gudang_hasil = $_POST['gudang_hasil_id'.$count];

			$biaya_hasil_ = $_POST['biaya_hasil_'.$count];
			$biaya_hasil_ex = str_replace(".", "", $biaya_hasil_);
			$biaya_hasil_round = str_replace(",", ".", $biaya_hasil_ex);
			$biaya_hasil = round($biaya_hasil_round, 2);

			$persen_hasil_ = $_POST['persen_hasil_'.$count];
			$persen_hasil_ex = str_replace(".", "", $persen_hasil_);
			$persen_hasil_round = str_replace(",", ".", $persen_hasil_ex);
			$persen_hasil = round($persen_hasil_round, 2);

				$sql_detail2 = $this->db->query("INSERT INTO public.beone_transfer_stock_detail(
																					transfer_stock_detail_id, transfer_stock_header_id, tipe_transfer_stock, item_id, qty, gudang_id, biaya, update_by, update_date, flag, persen_produksi)
																					VALUES (DEFAULT, $transfer_id, 'WIP', $item_hasil, $qty_hasil, 1, $biaya_hasil, 1, '$update_date', $session_id, $persen_hasil)");

					//SALDO AKHIR UNIT PRICE SAMPAI TGL SEKARANG DARI AWAL TAHUN
					//SALDO AWAL ITEM
					$sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =".intval($item_hasil));
					$saldo_awal_item = $sai->row_array();

					//cek saldo awal
					$inv = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = $item_hasil ORDER BY intvent_trans_id DESC LIMIT 1");
					$hasil_inv = $inv->row_array();

					if ($kode_biaya == 1){// PEMAKAIAN BAHAN BAKU
						//persen amount
						$persen_amount_round = ($amount_unit_price * $persen_hasil) / 100;
						$persen_amount = round($persen_amount_round, 2);

						//INSERT DI KARTO STOCK
						//$amount_hasil_round = ($persen_amount + $biaya_hasil) / $qty_hasil;
						//$amount_hasil = round($amount_hasil_round, 2);

						$sa_akhir_qty_round = $qty_hasil + $hasil_inv['sa_qty'];
						$sa_akhir_qty = round($sa_akhir_qty_round, 2);

						//$sa_akhir_amount_round = $sa_akhir_qty * $unit_price;
						$sa_akhir_amount_round = $persen_amount + $biaya_hasil;
						$sa_akhir_amount = round($sa_akhir_amount_round, 2);

						$unit_price_round = $sa_akhir_amount / $qty_hasil;
						$unit_price = round($unit_price_round, 2);

						$total_biaya_round = $total_biaya + $biaya_hasil;
						$total_biaya = round($total_biaya_round, 2);

						$sql_inventory2 = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, '$transfer_no', $item_hasil, '$tanggal', '$keterangan', $qty_hasil, $unit_price, 0, 0, $sa_akhir_qty, $unit_price, $sa_akhir_amount, 1, $session_id, '$update_date')");


						$amount_wip_round = $persen_amount + $biaya_hasil;
						$amount_wip = round($amount_wip_round, 2);

						$total_amount_wip_round = $total_amount_wip + $amount_wip;
						$total_amount_wip = round($total_amount_wip_round, 2);

					}else{//PEMAKAIAN WIP

						//INSERT DI KARTO STOCK
						$persen_amount_round = ($amount_unit_price * $persen_hasil) / 100;
						$persen_amount = round($persen_amount_round, 2);

						$amount_hasil_round = ($persen_amount + $biaya_hasil) / $qty_hasil;
						$amount_hasil = round($amount_hasil_round, 2);

						$amount_wip_round = $persen_amount + $biaya_hasil;
						$amount_wip = round($amount_wip_round, 2);

						$total_biaya_round = $total_biaya + $biaya_hasil;
						$total_biaya = round($total_biaya_round, 2);

						$sql_inventory2 = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, '$transfer_no', $item_hasil, '$tanggal', '$keterangan', $qty_hasil, $amount_hasil, 0, 0, $sa_akhir_qty, $unit_price, $sa_akhir_amount, 1, $session_id, '$update_date')");

						$total_amount_wip_round = $total_amount_wip + $amount_wip;
						$total_amount_wip = round($total_amount_wip_round, 2);
					}

		}

					if ($kode_biaya == 1){// PEMAKAIAN BAHAN BAKU
						$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																							gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																							VALUES (DEFAULT, '$tanggal', 13, '116-07', 61, '510-00', '$keterangan', $total_amount_wip, 0, '$transfer_no', '$transfer_no', $session_id, '$update_date')");

						$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																							gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																							VALUES (DEFAULT, '$tanggal', 61, '510-00', 13, '116-07', '$keterangan', 0, $total_amount_wip, '$transfer_no', '$transfer_no', $session_id, '$update_date')");

					}else{//PEMAKAIAN WIP
						$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																							gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																							VALUES (DEFAULT, '$tanggal', 14, '116-08', 61, '510-00', '$keterangan', $total_biaya, 0, '$transfer_no', '$transfer_no', $session_id, '$update_date')");

						$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																							gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																							VALUES (DEFAULT, '$tanggal', 61, '510-00', 14, '116-08', '$keterangan', 0, $total_biaya, '$transfer_no', '$transfer_no', $session_id, '$update_date')");
					}
		/******************************* END MENJADI ITEM ********************************************/

		if($sql_detail2)
			return true;
		return false;
	}

	public function load_header_stock_transfer(){
		$sql = $this->db->query("SELECT *
															FROM public.beone_transfer_stock
															WHERE flag=1");
		return $sql->result_array();
	}

	public function load_stock_transfer(){
		$sql = $this->db->query("SELECT h.transfer_stock_id, h.transfer_no, h.transfer_date, h.coa_kode_biaya, h.keterangan, h.update_by, h.update_date, h.flag, d.tipe_transfer_stock, d.item_id, d.qty, d.gudang_id, d.biaya
															FROM public.beone_transfer_stock h INNER JOIN public.beone_transfer_stock_detail d ON h.transfer_stock_id = d.transfer_stock_header_id
															WHERE h.flag=1");
		return $sql->result_array();
	}

	public function load_stock_transfer_print($transfer_id){
		$sql = $this->db->query("SELECT h.transfer_stock_id, h.transfer_no, h.transfer_date, h.coa_kode_biaya, h.keterangan, h.update_by, h.update_date, h.flag, d.tipe_transfer_stock, d.item_id, i.nama as namaitem, i.item_code, d.qty, d.gudang_id, d.biaya, d.persen_produksi
															FROM public.beone_transfer_stock h INNER JOIN public.beone_transfer_stock_detail d ON h.transfer_stock_id = d.transfer_stock_header_id INNER JOIN public.beone_item i ON d.item_id = i.item_id
															WHERE h.flag=1 AND h.transfer_stock_id =".intval($transfer_id));
		return $sql->result_array();
	}

	public function get_default_header($transfer_id){
		$sql = $this->db->query("SELECT * FROM public.beone_transfer_stock WHERE transfer_stock_id = ".intval($transfer_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function get_default_detail($transfer_id){
		$sql = $this->db->query("SELECT * FROM public.beone_transfer_stock_detail WHERE tipe_transfer_stock = 'BB' AND  transfer_stock_header_id = ".intval($transfer_id));
		return $sql->result_array();
	}

	public function get_default_detail_hasil($transfer_id){
		$sql = $this->db->query("SELECT * FROM public.beone_transfer_stock_detail WHERE tipe_transfer_stock = 'WIP' AND  transfer_stock_header_id = ".intval($transfer_id));
		return $sql->result_array();
	}


	public function delete($transfer_id, $transfer_no){
		$sql = $this->db->query("DELETE FROM public.beone_transfer_stock WHERE transfer_stock_id = ".intval($transfer_id));
		$sql_detail = $this->db->query("DELETE FROM public.beone_transfer_stock_detail WHERE transfer_stock_header_id = ".intval($transfer_id));

		//$tnumber = str_replace("-", "/", $transfer_no);
		$sql_gl = $this->db->query("DELETE FROM public.beone_gl WHERE pasangan_no = '$transfer_no'");
		$sql_inv = $this->db->query("DELETE FROM public.beone_inventory WHERE intvent_trans_no = '$transfer_no'");
	}


}
?>
