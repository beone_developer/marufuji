<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Sales_model extends CI_Model{


	public function simpan($post){
		$session_id = $this->session->userdata('user_id');
		$tanggal_awal = $this->db->escape($post['tanggal']);
		$customer = $this->db->escape($post['customer']);
		$keterangan = $this->db->escape($post['keterangan_header']);
		$sales_no = $this->db->escape($post['nomor_so']);
		$ppn = $this->db->escape($post['ppn']);
		$ppn_value_ = $this->db->escape($post['ppn_value']);
		$subtotal_ = $this->db->escape($post['subtotal']);
		$grandtotal_ = $this->db->escape($post['grandtotal']);

		$ppn_value = str_replace(".", "", $ppn_value_);
		$subtotal = str_replace(".", "", $subtotal_);
		$grandtotal = str_replace(".", "", $grandtotal_);
		$update_date = date('Y-m-d');

		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		$sql_header = $this->db->query("INSERT INTO public.beone_sales_header(sales_header_id, sales_no, trans_date, customer_id, keterangan, update_by, update_date, flag, ppn, subtotal, ppn_value, grandtotal)
																		VALUES (DEFAULT, $sales_no, '$tanggal', $customer, $keterangan, $session_id, '$tanggal', 1, $ppn, $subtotal, $ppn_value, $grandtotal)");

		$header_id = $this->db->query("SELECT * FROM public.beone_sales_header WHERE flag = 1 ORDER BY sales_header_id DESC LIMIT 1");
		$hasil_header_id = $header_id->row_array();
		$hid = $hasil_header_id['sales_header_id'];

		$total_persediaan_barang_jadi = 0;
		foreach ($_POST['rows'] as $key => $count ){
							 $item_id = $_POST['item_id_'.$count];
							 $qty_ = $_POST['qty_'.$count];
							 $price_ = $_POST['price_'.$count];
							 $amount_ = $_POST['amount_'.$count];

							 $qty = str_replace(".", "", $qty_);
							 $price = str_replace(".", "", $price_);
							 $amount = str_replace(".", "", $amount_);

							 $doc_ = explode(":", $post['nodoc_'.$count]);
					 			$doc = $doc_[1];

								//insert mutasi gudang detail
								$sql_gudang_detail = $this->db->query("INSERT INTO public.beone_gudang_detail(
																											gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan)
																											VALUES (DEFAULT, 2, '$tanggal', $item_id, 0, $qty, '$doc', $session_id, '$update_date', 1, $sales_no)");



							 $sql_detail = $this->db->query("INSERT INTO public.beone_sales_detail(sales_header_id, item_id, qty, price, amount, flag)	VALUES ($hid, $item_id, $qty, $price, $amount, 1)");

							 //cek transaksi di inventory
							 $count_item_inventory = $this->db->query("SELECT COUNT(item_id) as jml_transaksi FROM public.beone_inventory WHERE item_id = $item_id");
							 $hasil_count = $count_item_inventory->row_array();
		 		 			 $count_item = $hasil_count['jml_transaksi'];

							 if ($count_item == 0){//ambil dari saldo awal item
										 $saldo_awal_item = $this->db->query("SELECT * FROM public.beone_item WHERE item_id = $item_id");
										 $hasil_saldo_awal_item = $saldo_awal_item->row_array();
										 $saldo_awal_qty = $hasil_saldo_awal_item['saldo_qty'];
										 $saldo_awal_value = $hasil_saldo_awal_item['saldo_idr'];

										 if ($saldo_awal_value == 0 OR $saldo_awal_qty == 0){
											 		$unit_price_awal = 0;
										 }else{
											 		$unit_price_awal = $saldo_awal_value / $saldo_awal_qty;
													//$unit_price_awal = round($unit_price_awal_round, 2);
										 }
							 }else{//ada transaksi, jadi ambil unit price dari transaksi terakhir
										$saldo_awal_item = $this->db->query("SELECT * FROM public.beone_inventory WHERE item_id = $item_id ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
										$hasil_saldo_awal_item = $saldo_awal_item->row_array();
										$saldo_awal_qty = $hasil_saldo_awal_item['sa_qty'];
										$saldo_awal_value = $hasil_saldo_awal_item['sa_amount'];
										$unit_price_awal = $hasil_saldo_awal_item['sa_unit_price'];
							 }

							 $amount_penjualan = $unit_price_awal * $qty;
							 //$amount_penjualan = round($amount_penjualan_round, 2);

							 $sa_akhir_qty = $saldo_awal_qty - $qty;
							 $sa_akhir_amount = $saldo_awal_value - $amount_penjualan;

							 if ($sa_akhir_amount == 0 OR $sa_akhir_qty ==0){
								 	$unit_price_akhir = 0;
							 }else{
								 	$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;
							 }

							 //$unit_price_akhir = round($unit_price_akhir_round, 2);

							 $sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $sales_no, $item_id, '$tanggal', $keterangan, 0, 0, $qty, $unit_price_awal, $sa_akhir_qty, $unit_price_akhir, $sa_akhir_amount, 1, $session_id, '$update_date')");
							 $total_persediaan_barang_jadi = $total_persediaan_barang_jadi + $amount_penjualan;
					 }

					 $coa_jurnal_hpp = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 4"); //coa HPP
 		 			 $coa_hpp = $coa_jurnal_hpp->row_array();
 		 			 $chpp_id = $coa_hpp['coa_id'];
 		 			 $chpp_no = $coa_hpp['coa_no'];

					 $coa_jurnal_persediaan_barang_jadi = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 5"); //coa Persediaan Barang Jadi
 		 			 $coa_persediaan_barang_jadi = $coa_jurnal_persediaan_barang_jadi->row_array();
 		 			 $cpbj_id = $coa_persediaan_barang_jadi['coa_id'];
 		 			 $cpbj_no = $coa_persediaan_barang_jadi['coa_no'];

					 $sql_ledger_hpp_debet = $this->db->query("INSERT INTO public.beone_gl(
																						gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																						VALUES (DEFAULT, '$tanggal', $chpp_id, '$chpp_no', $cpbj_id, '$cpbj_no', $keterangan, $total_persediaan_barang_jadi, 0, $sales_no, $sales_no, $session_id, '$update_date')");

					$sql_ledger_hpp_kredit = $this->db->query("INSERT INTO public.beone_gl(
																					 gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																					 VALUES (DEFAULT, '$tanggal', $cpbj_id, '$cpbj_no', $chpp_id, '$chpp_no', $keterangan, 0, $total_persediaan_barang_jadi, $sales_no, $sales_no, $session_id, '$update_date')");




					 $coa_jurnal_piutang_usaha = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 6"); //coa Piutang Usaha
 		 			 $coa_piutang_usaha = $coa_jurnal_piutang_usaha->row_array();
 		 			 $cpu_id = $coa_piutang_usaha['coa_id'];
 		 			 $cpu_no = $coa_piutang_usaha['coa_no'];

					 $coa_jurnal_penjualan_lokal = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 7"); //coa Penjualan Lokal
 		 			 $coa_penjualan_lokal = $coa_jurnal_penjualan_lokal->row_array();
 		 			 $cpl_id = $coa_penjualan_lokal['coa_id'];
 		 			 $cpl_no = $coa_penjualan_lokal['coa_no'];

					 $coa_jurnal_ppn = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 2"); //coa jurnal ppn
 		 			 $coa_ppn = $coa_jurnal_ppn->row_array();
 		 			 $cppn_id = $coa_ppn['coa_id'];
 		 			 $cppn_no = $coa_ppn['coa_no'];

					 $sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																						gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																						VALUES (DEFAULT, '$tanggal', $cpu_id, '$cpu_no', $cpl_id, '$cpl_no', $keterangan, $subtotal, 0, $sales_no, $sales_no, $session_id, '$update_date')");

					$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																					 gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																					 VALUES (DEFAULT, '$tanggal', $cpl_id, '$cpl_no', $cpu_id, '$cpu_no', $keterangan, 0, $subtotal, $sales_no, $sales_no, $session_id, '$update_date')");


				  $sql_ledger_debet2 = $this->db->query("INSERT INTO public.beone_gl(
																					 gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																					 VALUES (DEFAULT, '$tanggal', $cpu_id, '$cpu_no', $cppn_id, '$cppn_no', $keterangan, $ppn_value, 0, $sales_no, $sales_no, $session_id, '$update_date')");

				 $sql_ledger_kredit2 = $this->db->query("INSERT INTO public.beone_gl(
 																				 gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
 																				 VALUES (DEFAULT, '$tanggal', $cppn_id, '$cppn_no', $cpu_id, '$cpu_no', $keterangan, 0, $ppn_value, $sales_no, $sales_no, $session_id, '$update_date')");

			 $sql_piutang = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																				hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																				VALUES (DEFAULT, $customer, '$tanggal', $sales_no, $keterangan, 0, $grandtotal, 0, 0, 0, $session_id, '$update_date', 1, 0)");

		if($sql_header && $sql_detail)
			return true;
		return false;
	}

	public function update($post, $sales_id){
		$session_id = $this->session->userdata('user_id');
		$tanggal_awal = $this->db->escape($post['tanggal']);
		$customer = $this->db->escape($post['customer']);
		$keterangan = $this->db->escape($post['keterangan_header']);
		$sales_no = $this->db->escape($post['nomor_so']);
		$ppn = $this->db->escape($post['ppn']);
		$ppn_value_ = $this->db->escape($post['ppn_value']);
		$subtotal_ = $this->db->escape($post['subtotal']);
		$grandtotal_ = $this->db->escape($post['grandtotal']);
		$update_date = date('Y-m-d');

		$ppn_value = str_replace(".", "", $ppn_value_);
		$subtotal = str_replace(".", "", $subtotal_);
		$grandtotal = str_replace(".", "", $grandtotal_);

		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;
		$tanggal_awal_tahun = $tgl_tahun."-01-01";

		$sql_header = $this->db->query("UPDATE public.beone_sales_header
																		SET update_by=$session_id, update_date='$update_date', ppn=$ppn, subtotal=$subtotal, ppn_value=$ppn_value, grandtotal=$grandtotal
																		WHERE sales_header_id = ".intval($sales_id));

		$total_persediaan_barang_jadi = 0;
		foreach ($_POST['rows'] as $key => $count ){
							 $item_id = $_POST['item_id_'.$count];
							 $qty_ = $_POST['qty_'.$count];
							 $price_ = $_POST['price_'.$count];
							 $amount_ = $_POST['amount_'.$count];

					 		 $qty_ex = str_replace(".", "", $qty_);
							 $price_ex = str_replace(".", "", $price_);
							 $amount_ex = str_replace(".", "", $amount_);

							 $qty = str_replace(",", ".", $qty_ex);
							 $price = str_replace(",", ".", $price_ex);
							 $amount = str_replace(",", ".", $amount_ex);

							 $sql_detail_del = $this->db->query("DELETE FROM public.beone_sales_detail WHERE sales_header_id =".intval($sales_id));

							 $sql_detail = $this->db->query("INSERT INTO public.beone_sales_detail(sales_detail_id, sales_header_id, item_id, qty, price, amount, flag)	VALUES (DEFAULT, $sales_id, $item_id, $qty, $price, $amount, 1)");

							 $sql_del_inventory = $this->db->query("DELETE FROM public.beone_inventory WHERE intvent_trans_no = $sales_no");

							 //cek transaksi di inventory
							 $count_item_inventory = $this->db->query("SELECT COUNT(item_id) as jml_transaksi FROM public.beone_inventory WHERE item_id = $item_id");
							 $hasil_count = $count_item_inventory->row_array();
		 		 			 $count_item = $hasil_count['jml_transaksi'];

							 if ($count_item == 0){//ambil dari saldo awal item
										 $saldo_awal_item = $this->db->query("SELECT * FROM public.beone_item WHERE item_id = $item_id");
										 $hasil_saldo_awal_item = $saldo_awal_item->row_array();
										 $saldo_awal_qty = $hasil_saldo_awal_item['saldo_qty'];
										 $saldo_awal_value = $hasil_saldo_awal_item['saldo_idr'];

										 if ($saldo_awal_value == 0 OR $saldo_awal_qty == 0){
											 		$unit_price_awal = 0;
										 }else{
											 		$unit_price_awal = $saldo_awal_value / $saldo_awal_qty;
													//$unit_price_awal = round($unit_price_awal_round, 2);
										 }
							 }else{//ada transaksi, jadi ambil unit price dari transaksi terakhir
										$saldo_awal_item = $this->db->query("SELECT * FROM public.beone_inventory WHERE item_id = $item_id ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
										$hasil_saldo_awal_item = $saldo_awal_item->row_array();
										$saldo_awal_qty = $hasil_saldo_awal_item['sa_qty'];
										$saldo_awal_value = $hasil_saldo_awal_item['sa_amount'];
										$unit_price_awal = $hasil_saldo_awal_item['sa_unit_price'];
							 }

							 $amount_penjualan = $unit_price_awal * $qty;

							 $sa_akhir_qty = $saldo_awal_qty - $qty;
							 $sa_akhir_amount = $saldo_awal_value - $amount_penjualan;

							 if ($sa_akhir_amount == 0 OR $sa_akhir_qty ==0){
							 	 $unit_price_akhir = 0;
							 }else{
							 	 $unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;
							 }

							 $sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $sales_no, $item_id, '$tanggal', $keterangan, 0, 0, $qty, $unit_price_awal, $sa_akhir_qty, $unit_price_akhir, $sa_akhir_amount, 1, $session_id, '$update_date')");
							 $total_persediaan_barang_jadi = $total_persediaan_barang_jadi + $amount_penjualan;
					 }

					 $sql_del_ledger = $this->db->query("DELETE FROM public.beone_gl WHERE gl_number = $sales_no");

					 $coa_jurnal_hpp = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 4"); //coa HPP
 		 			 $coa_hpp = $coa_jurnal_hpp->row_array();
 		 			 $chpp_id = $coa_hpp['coa_id'];
 		 			 $chpp_no = $coa_hpp['coa_no'];

					 $coa_jurnal_persediaan_barang_jadi = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 5"); //coa Persediaan Barang Jadi
 		 			 $coa_persediaan_barang_jadi = $coa_jurnal_persediaan_barang_jadi->row_array();
 		 			 $cpbj_id = $coa_persediaan_barang_jadi['coa_id'];
 		 			 $cpbj_no = $coa_persediaan_barang_jadi['coa_no'];

					 $sql_ledger_hpp_debet = $this->db->query("INSERT INTO public.beone_gl(
																						gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																						VALUES (DEFAULT, '$tanggal', $chpp_id, '$chpp_no', $cpbj_id, '$cpbj_no', $keterangan, $total_persediaan_barang_jadi, 0, $sales_no, $sales_no, $session_id, '$update_date')");

					$sql_ledger_hpp_kredit = $this->db->query("INSERT INTO public.beone_gl(
																					 gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																					 VALUES (DEFAULT, '$tanggal', $cpbj_id, '$cpbj_no', $chpp_id, '$chpp_no', $keterangan, 0, $total_persediaan_barang_jadi, $sales_no, $sales_no, $session_id, '$update_date')");




					 $coa_jurnal_piutang_usaha = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 6"); //coa Piutang Usaha
 		 			 $coa_piutang_usaha = $coa_jurnal_piutang_usaha->row_array();
 		 			 $cpu_id = $coa_piutang_usaha['coa_id'];
 		 			 $cpu_no = $coa_piutang_usaha['coa_no'];

					 $coa_jurnal_penjualan_lokal = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 7"); //coa Penjualan Lokal
 		 			 $coa_penjualan_lokal = $coa_jurnal_penjualan_lokal->row_array();
 		 			 $cpl_id = $coa_penjualan_lokal['coa_id'];
 		 			 $cpl_no = $coa_penjualan_lokal['coa_no'];

					 $coa_jurnal_ppn = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 2"); //coa jurnal ppn
 		 			 $coa_ppn = $coa_jurnal_ppn->row_array();
 		 			 $cppn_id = $coa_ppn['coa_id'];
 		 			 $cppn_no = $coa_ppn['coa_no'];

					 $sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																						gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																						VALUES (DEFAULT, '$tanggal', $cpu_id, '$cpu_no', $cpl_id, '$cpl_no', $keterangan, $subtotal, 0, $sales_no, $sales_no, $session_id, '$update_date')");

					$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																					 gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																					 VALUES (DEFAULT, '$tanggal', $cpl_id, '$cpl_no', $cpu_id, '$cpu_no', $keterangan, 0, $subtotal, $sales_no, $sales_no, $session_id, '$update_date')");


				  $sql_ledger_debet2 = $this->db->query("INSERT INTO public.beone_gl(
																					 gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																					 VALUES (DEFAULT, '$tanggal', $cpu_id, '$cpu_no', $cppn_id, '$cppn_no', $keterangan, $ppn_value, 0, $sales_no, $sales_no, $session_id, '$update_date')");

				 $sql_ledger_kredit2 = $this->db->query("INSERT INTO public.beone_gl(
 																				 gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
 																				 VALUES (DEFAULT, '$tanggal', $cppn_id, '$cppn_no', $cpu_id, '$cpu_no', $keterangan, 0, $ppn_value, $sales_no, $sales_no, $session_id, '$update_date')");

			 $sql_piutang = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																				hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																				VALUES (DEFAULT, $customer, '$tanggal', $sales_no, $keterangan, 0, $grandtotal, 0, 0, 0, $session_id, '$update_date', 1, 0)");

		if($sql_header && $sql_detail)
			return true;
		return false;
	}

	public function load_sales_header(){
		$sql = $this->db->query("SELECT h.sales_header_id, h.sales_no, h.trans_date, h.keterangan, h.customer_id, c.nama as ncustomer
															FROM public.beone_sales_header h INNER JOIN public.beone_custsup c ON h.customer_id = c.custsup_id WHERE h.flag = 1");
		return $sql->result_array();
	}

	public function get_default_header($sales_id){
		$sql = $this->db->query("SELECT h.sales_header_id, h.sales_no, h.trans_date, h.keterangan, h.customer_id, h.ppn, h.ppn_value, h.subtotal, h.grandtotal, c.nama as ncustomer
															FROM public.beone_sales_header h INNER JOIN public.beone_custsup c ON h.customer_id = c.custsup_id WHERE h.flag = 1 AND h.sales_header_id = ".intval($sales_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function get_default_detail($sales_id){
		$sql = $this->db->query("SELECT h.sales_header_id, h.sales_no, h.trans_date, h.keterangan, h.customer_id, d.item_id, d.qty, d.price, d.amount
															FROM public.beone_sales_header h INNER JOIN public.beone_sales_detail d ON h.sales_header_id = d.sales_header_id WHERE h.flag = 1 AND d.sales_header_id = ".intval($sales_id));
		return $sql->result_array();
	}

	public function delete($sales_id, $sales_no){
		$sql = $this->db->query("DELETE FROM public.beone_sales_header WHERE sales_header_id = ".intval($sales_id));
		$sql_detail = $this->db->query("DELETE FROM public.beone_sales_detail WHERE sales_header_id = ".intval($sales_id));

		$sls_no = str_replace("-", "/", $sales_no);
		$sql_gl = $this->db->query("DELETE FROM public.beone_gl WHERE gl_number = '$sls_no'");
		$sql_hp = $this->db->query("DELETE FROM public.beone_hutang_piutang WHERE nomor = '$sls_no'");
		$sql_inventory = $this->db->query("DELETE FROM public.beone_inventory WHERE intvent_trans_no = '$sls_no'");
	}


}
?>
