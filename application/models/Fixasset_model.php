<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Fixasset_model extends CI_Model{


	public function simpan_periode($post){
		$nama = $this->db->escape($post['periode']);

		$sql_fix_asset = $this->db->query("SELECT * FROM public.beone_fix_asset");

			$sql = $this->db->query("INSERT INTO public.beone_fix_asset_periode(
																periode_id, nama_periode)
																VALUES (DEFAULT, $nama)");

			$sql_periode_id = $this->db->query("SELECT periode_id FROM public.beone_fix_asset_periode ORDER BY periode_id DESC LIMIT 1");
			$hasil_period_id = $sql_periode_id->row_array();
			$pid = $hasil_period_id['periode_id'];

			foreach($sql_fix_asset->result_array() as $row){
						$sql_akm = $this->db->query("INSERT INTO public.beone_fix_asset_akm(
																				akm_id, periode_id, fix_asset_id, akm_penyusutan)
																				VALUES (DEFAULT, $pid, $row[fix_asset_id], $row[harga_perolehan])");
			}

			helper_log($tipe = "add", $str = "Periode Fix Asset ".$post['periode']);

		if($sql)
			return true;
		return false;
	}

	public function update_periode($post, $periode_id){
		$nama = $this->db->escape($post['periode']);

		$sql = $this->db->query("UPDATE public.beone_fix_asset_periode SET nama_periode=$nama WHERE periode_id = ".intval($periode_id));
		helper_log($tipe = "edit", $str = "Ubah Periode Fix Asset ".$post['periode']);

		if($sql)
			return true;
		return false;
	}

	public function delete_periode($periode_id){

		helper_log($tipe = "delete", $str = "Hapus Periode Fix Asset ");
		$sql = $this->db->query("DELETE FROM public.beone_fix_asset_periode WHERE periode_id =".intval($periode_id));
	}


	public function get_default_periode($periode_id){
		$sql = $this->db->query("SELECT * FROM public.beone_fix_asset_periode WHERE periode_id = ".intval($periode_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function load_periode(){
		$sql = $this->db->query("SELECT * FROM public.beone_fix_asset_periode");
		return $sql->result_array();
	}

	public function load_fixasset(){
		$sql = $this->db->query("SELECT * FROM public.beone_fix_asset");
		return $sql->result_array();
	}

	public function proses_fixasset($post){
		$akm_id = $this->db->escape($post['akm_id']);
		$nilai_perolehan_ = $this->db->escape($post['akm_penyusutan']);
		$jan_ = $this->db->escape($post['jan']);
		$feb_ = $this->db->escape($post['feb']);
		$mar_ = $this->db->escape($post['mar']);
		$apr_ = $this->db->escape($post['apr']);
		$mei_ = $this->db->escape($post['mei']);
		$jun_ = $this->db->escape($post['jun']);
		$jul_ = $this->db->escape($post['jul']);
		$ags_ = $this->db->escape($post['ags']);
		$sep_ = $this->db->escape($post['sep']);
		$okt_ = $this->db->escape($post['okt']);
		$nov_ = $this->db->escape($post['nov']);
		$des_ = $this->db->escape($post['des']);

		//$nilai_perolehan_ex = str_replace(".", "", $nilai_perolehan_);
		//$nilai_perolehan = str_replace(",", ".", $nilai_perolehan_ex);

		$jan_ex = str_replace(".", "", $jan_);
		$jan = str_replace(",", ".", $jan_ex);

		$feb_ex = str_replace(".", "", $feb_);
		$feb = str_replace(",", ".", $feb_ex);

		$mar_ex = str_replace(".", "", $mar_);
		$mar = str_replace(",", ".", $mar_ex);

		$apr_ex = str_replace(".", "", $apr_);
		$apr = str_replace(",", ".", $apr_ex);

		$mei_ex = str_replace(".", "", $mei_);
		$mei = str_replace(",", ".", $mei_ex);

		$jun_ex = str_replace(".", "", $jun_);
		$jun = str_replace(",", ".", $jun_ex);

		$jul_ex = str_replace(".", "", $jul_);
		$jul = str_replace(",", ".", $jul_ex);

		$ags_ex = str_replace(".", "", $ags_);
		$ags = str_replace(",", ".", $ags_ex);

		$sep_ex = str_replace(".", "", $sep_);
		$sep = str_replace(",", ".", $sep_ex);

		$okt_ex = str_replace(".", "", $okt_);
		$okt = str_replace(",", ".", $okt_ex);

		$nov_ex = str_replace(".", "", $nov_);
		$nov = str_replace(",", ".", $nov_ex);

		$des_ex = str_replace(".", "", $des_);
		$des = str_replace(",", ".", $des_ex);

		$total = $post['jan'] + $post['feb'] + $post['mar'] + $post['apr'] + $post['mei'] + $post['jun'] + $post['jul'] + $post['ags'] + $post['sep'] + $post['okt'] + $post['nov'] + $post['des'];
		$nilai_buku = $post['akm_penyusutan'] - ($post['jan'] + $post['feb'] + $post['mar'] + $post['apr'] + $post['mei'] + $post['jun'] + $post['jul'] + $post['ags'] + $post['sep'] + $post['okt'] + $post['nov'] + $post['des']);

		$sql = $this->db->query("UPDATE public.beone_fix_asset_akm
														SET jan = $jan, feb = $feb, mar = $mar, apr = $apr, mei = $mei, jun = $jun, jul = $jul, ags = $ags, sep = $sep, okt = $okt, nov = $nov, des = $des, nilai_buku = $nilai_buku  WHERE akm_id = $akm_id");

		if($sql)
			return true;
		return false;
	}

	public function simpan_fixasset($post){
		$nama_asset = $this->db->escape($post['nama_asset']);
		$tgl = $this->db->escape($post['tanggal_perolehan']);
		$harga_ = $this->db->escape($post['harga_perolehan']);

		$tgl_hari = substr($tgl, 1, 2);
		$tgl_bulan = substr($tgl, 4, 2);
		$tgl_tahun = substr($tgl, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		$harga_ex = str_replace(".", "", $harga_);
		$harga = str_replace(",", ".", $harga_ex);

		$sql = $this->db->query("INSERT INTO public.beone_fix_asset (fix_asset_id, nama_asset, tanggal_perolehan, harga_perolehan) VALUES (DEFAULT, $nama_asset, '$tanggal', $harga)");

		if($sql)
			return true;
		return false;
	}

	public function update_fixasset($post, $fix_asset_id){
		$nama_asset = $this->db->escape($post['nama_asset']);
		$tgl = $this->db->escape($post['tanggal_perolehan']);
		$harga_ = $this->db->escape($post['harga_perolehan']);

		$tgl_hari = substr($tgl, 1, 2);
		$tgl_bulan = substr($tgl, 4, 2);
		$tgl_tahun = substr($tgl, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		$harga_ex = str_replace(".", "", $harga_);
		$harga = str_replace(",", ".", $harga_ex);

		$sql = $this->db->query("UPDATE public.beone_fix_asset SET nama_asset=$nama_asset, tanggal_perolehan='$tanggal', harga_perolehan=$harga WHERE fix_asset_id = ".intval($fix_asset_id));

		if($sql)
			return true;
		return false;
	}

	public function get_default_fixasset($fix_asset_id){//untuk default fixasset master
		$sql = $this->db->query("SELECT * FROM public.beone_fix_asset WHERE fix_asset_id = ".intval($fix_asset_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function delete_fixasset($fix_asset_id){
		$sql = $this->db->query("DELETE FROM public.beone_fix_asset WHERE fix_asset_id =".intval($fix_asset_id));
	}

	public function get_default_fix_asset($periode_id, $fix_asset_id){
		$sql = $this->db->query("SELECT a.akm_id, f.nama_asset, a.periode_id, a.fix_asset_id, a.akm_penyusutan, a.jan, a.feb, a.mar, a.apr, a.mei, a.jun, a.jul, a.ags, a.sep, a.okt, a.nov, a.des, a.nilai_buku
														FROM public.beone_fix_asset f INNER JOIN public.beone_fix_asset_akm a ON f.fix_asset_id = a.fix_asset_id
														WHERE a.fix_asset_id = $fix_asset_id AND a.periode_id = ".intval($periode_id));
	if($sql->num_rows() > 0)
		return $sql->row_array();
	return false;
	}

	public function load_fixed_asset_akm($periode_id){
		$sql = $this->db->query("SELECT a.akm_id, f.nama_asset, a.periode_id, a.fix_asset_id, a.akm_penyusutan, a.jan, a.feb, a.mar, a.apr, a.mei, a.jun, a.jul, a.ags, a.sep, a.okt, a.nov, a.des, a.nilai_buku
														FROM public.beone_fix_asset f INNER JOIN public.beone_fix_asset_akm a ON f.fix_asset_id = a.fix_asset_id
														WHERE a.periode_id = ".intval($periode_id));
		return $sql->result_array();
	}

}
?>
