<?php

class preview_bc40_model extends CI_Model {

//    var $tabel = "usr";
    //   private $another;
    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
        $this->mysql = $this->load->database('mysql', TRUE);
    }

    public function select($aju, $id) {

        $select = "select DISTINCT th.id, rs.URAIAN_STATUS, th.nomor_aju, th.nomor_daftar, th.tanggal_daftar, 
        th.KODE_KANTOR as kode_kantor_pabean, rkpbean.URAIAN_KANTOR as kantor_pabean, rjt.URAIAN_JENIS_TPB,
rtp.URAIAN_TUJUAN_PENGIRIMAN, tps.KODE_TPS, th.NAMA_PENGANGKUT, th.NOMOR_POLISI,th.HARGA_PENYERAHAN, th.VOLUME, 
rkiusaha.uraian_kode_id, th.id_pengusaha, th.NAMA_PENGUSAHA, th.ALAMAT_PENGUSAHA, th.NOMOR_IJIN_TPB,
th.TANGGAL_IJIN_TPB, rja.KODE_JENIS_API, rja.URAIAN_JENIS_API,rkikirim.uraian_kode_id, th.ID_PENGIRIM, 
th.NAMA_PENGIRIM, th.ALAMAT_PENGIRIM, th.bruto, th.NETTO, th.JUMLAH_BARANG, th.KODE_VALUTA, th.NDPBM, 
th.CIF, th.CIF_RUPIAH,  th.KOTA_TTD, th.TANGGAL_TTD, th.NAMA_TTD, th.JABATAN_TTD
from tpb_header th
left join referensi_status rs on th.KODE_STATUS = rs.KODE_STATUS and th.KODE_DOKUMEN_PABEAN = rs.KODE_DOKUMEN
left join referensi_kantor_pabean rkpbean on th.KODE_KANTOR = rkpbean.KODE_KANTOR
left join referensi_tujuan_pengiriman rtp on th.KODE_TUJUAN_PENGIRIMAN = rtp.KODE_TUJUAN_PENGIRIMAN and th.KODE_DOKUMEN_PABEAN = rtp.KODE_DOKUMEN
left join referensi_tps tps on th.KODE_GUDANG_ASAL = tps.KODE_TPS
left join referensi_jenis_api rja on th.API_PENGUSAHA = rja.KODE_JENIS_API
left join referensi_jenis_tpb rjt on th.KODE_JENIS_TPB = rjt.KODE_JENIS_TPB
left join referensi_kode_id rkiusaha on th.kode_id_pengusaha = rkiusaha.kode_id
left join referensi_kode_id rkikirim on th.KODE_ID_PENGIRIM = rkikirim.kode_id
left join referensi_cara_angkut rca on th.KODE_CARA_ANGKUT = rca.ID
where th.nomor_aju = '" . $aju . "'";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_tujuan($aju, $id) {
        $select = " select distinct th.NOMOR_AJU,rtp.id,  rtp.URAIAN_TUJUAN_PENGIRIMAN from tpb_header th
left join referensi_tujuan_pengiriman rtp on th.KODE_TUJUAN_PENGIRIMAN = rtp.KODE_TUJUAN_PENGIRIMAN
 where th.nomor_aju = '" . $aju . "' and th.kode_dokumen_pabean = 40 and th.id = '" . $id . "'";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_packing($aju, $id) {
        $select = "  select nomor_dokumen, tanggal_dokumen from tpb_dokumen 
 left join referensi_dokumen on kode_jenis_dokumen = kode_dokumen 
 where id_header ='" . $id . "' and kode_dokumen = 217";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_another($aju, $id) {
        $select = " select uraian_dokumen, nomor_dokumen, tanggal_dokumen from tpb_dokumen 
 left join referensi_dokumen on kode_jenis_dokumen = kode_dokumen 
 where id_header ='" . $id . "' and kode_dokumen <> 217";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_lc($aju) {
        $select = "select th.NOMOR_AJU, td.NOMOR_DOKUMEN, td.TANGGAL_DOKUMEN as TANGGAL_DOKUMEN,rd.URAIAN_DOKUMEN from tpb_header th
        left join tpb_dokumen td on th.id = td.ID_HEADER
        left join referensi_dokumen rd on td.KODE_JENIS_DOKUMEN = rd.KODE_DOKUMEN
        where th.NOMOR_AJU = '" . $aju . "' and rd.kode_dokumen = 465 and th.kode_dokumen_pabean = 40";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_kontainer($aju) {
        $select = "select th.NOMOR_AJU, tk.NOMOR_KONTAINER, ruk.URAIAN_UKURAN_KONTAINER, rtk.URAIAN_TIPE_KONTAINER from tpb_header th
                    left join tpb_kontainer tk on th.id = tk.ID_HEADER
                    left join referensi_ukuran_kontainer ruk on tk.KODE_UKURAN_KONTAINER = ruk.KODE_UKURAN_KONTAINER
                    left join referensi_tipe_kontainer rtk on tk.KODE_TIPE_KONTAINER = rtk.KODE_TIPE_KONTAINER
                    where NOMOR_AJU = '" . $aju . "' and th.kode_dokumen_pabean = 40";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_kemasan($aju) {
        $select = "select th.NOMOR_AJU,tk.JUMLAH_KEMASAN, 
                tk.KODE_JENIS_KEMASAN, rk.URAIAN_KEMASAN from tpb_header th
                left join tpb_kemasan tk on th.id = tk.ID_HEADER 
                left join referensi_kemasan rk on tk.KODE_JENIS_KEMASAN = rk.KODE_KEMASAN 
                where NOMOR_AJU = '" . $aju . "' and th.kode_dokumen_pabean = 40";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function simpan($POST){

        $KODE_JENIS_DOKUMEN = $this->mysql->escape($POST['dokumen1']);
        $NOMOR_DOKUMEN = $this->mysql->escape($POST['nomor1']);
        $TANGGAL_DOKUMEN = $this->mysql->escape($POST['tgl1']);

        $sql = $this->mysql->query("INSERT INTO tpb_dokumen(KODE_JENIS_DOKUMEN, NOMOR_DOKUMEN, TANGGAL_DOKUMEN) 
                                    VALUES ($KODE_JENIS_DOKUMEN, $NOMOR_DOKUMEN, $TANGGAL_DOKUMEN)");

        if($sql)
            return true;
        return false; 

    }

    public function update(
        $id,
        $nomor_pendaftaran,
        $tanggal_pendaftaran,
        $kppbc_pengawas,
        $kode_gudang_plb,
        $kode_tujuan_pengiriman,
        $kode_id_pengusaha,
        $id_pengusaha,
        $nama_pengusaha,
        $alamat_pengusaha,
        // $ptb_pengusaha,
        // $jenis_npwp_pengirim,
        // $nomor_npwp_pengirim,
        $nama_pengirim,
        $alamat_pengirim
        // $nomor_dokumen,
        // $tanggal_dokumen
    ){
        // $k1,
        // $k2,
        // $fp1,
        // $fp2,
        // $fp1,
        // $fp2,
        // $volume,
        // $sarana_angkut,
        // $nomor_polisi,
        // $harga_penyerahan,
        // $bruto,
        // $netto,
        // $jml_barang,
        // $kota_ttd,
        // $tgl_ttd,
        // $pemberitahu,
        // $jabatan) {

        $select = "update tpb_header set "
                . "nomor_daftar = '" . $nomor_pendaftaran . "',"
                . "tanggal_daftar =  '" . $tanggal_pendaftaran . "',"
                . "kode_kantor = '" . $kppbc_pengawas . "',"
                . "kode_tujuan_pengiriman = '" . $kode_tujuan_pengiriman . "',"
                . "kode_id_pengusaha = '" . $kode_id_pengusaha . "',"
                . "id_pengusaha = '" . $id_pengusaha . "',"
                . "nama_pengusaha = '" . $nama_pengusaha . "',"
                . "alamat_pengusaha = '" . $alamat_pengusaha . "',"
                // . "ptb_pengusaha = '" . $ptb_pengusaha . "',"
                // . "jenis_npwp_pengirim = '" . $jenis_npwp_pengirim . "',"
                // . "nomor_npwp_pengirim = '" . $nomor_npwp_pengirim . "',"
                . "nama_pengirim = '" . $nama_pengirim . "',"
                . "alamat_pengirim = '" . $alamat_pengirim . "'"
                // . "nomor_dokumen = '" . $nomor_dokumen . "',"
                // . "tanggal_dokumen = '" . $tanggal_dokumen . "'"
                . "where id = '" . $id . "'";
                // . "k1 = '" . $k1 . "',"
                // . "nama_pemilik = '" . $nama_pemilik . "',"
                // . "alamat_pemilik = '" . $alamat_pemilik . "',"
                // . "NPPPJK = '" . $nomor_npppjk . "',"
                // . "TANGGAL_NPPPJK = '" . $tanggal_npppjk . "',"
                // . "KODE_CARA_ANGKUT = '" . $cara_angkut . "',"
                // . "NAMA_PENGANGKUT = '" . $nama_sarana_angkut . "',"
                // . "Nomor_voy_flight= '" . $nomor_vf . "',"
                // . "kode_bendera= '" . $kode_bendera . "',"
                // . "kode_pel_muat= '" . $kode_pel_muat . "',"
                // . "kode_pel_transit= '" . $kode_pel_transit . "',"
                // . "kode_pel_bongkar= '" . $kode_pel_bongkar . "',"
                // . "kode_tps= '" . $kode_tps . "',"
                // . "kode_valuta = '" . $kode_valuta . "',"
                // . "ndpbm = '" . $ndpbm . "',"
                // . "fob = '" . $fob . "',"
                // . "freight = '" . $freight . "',"
                // . "asuransi = '" . $asuransi . "',"
                // . "cif = '" . $cif . "',"
                // . "cif_rupiah = '" . $cif_rp . "',"
                // . "nama_pengangkut = '" . $sarana_angkut . "',"
                // . "bruto = '" . $bruto . "',"
                // . "netto = '" . $netto . "',"
                // . "kota_ttd = '" . $kota_ttd . "',"
                // . "tanggal_ttd = '" . $tgl_ttd . "',"
                // . "nama_ttd = '" . $nama_ttd . "',"
                // . "jabatan_ttd = '" . $jabatan_ttd . "'"
            
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;
    }

    public function delete($id){
        $sql = $this->mysql->query("DELETE FROM tpb_dokumen where ID = ".intval($id));

    }
    
   // ================== MODAL =====================
   // MODAL DAFTAR RESPON
   public function get_daftar_respon($id){

       $sql = $this->mysql->query("SELECT tr.*, rp.URAIAN_RESPON
                                   FROM tpb_respon tr
                                   LEFT JOIN referensi_respon rp on tr.KODE_RESPON = rp.KODE_RESPON
                                   WHERE ID_HEADER = ".intval($id));

       return $sql->result_array();
   }

   // MODAL DOKUMEN
   public function get_dokumen_modal($id){
       $sql = $this->mysql->query("SELECT td.*, rd.KODE_DOKUMEN, rd.URAIAN_DOKUMEN FROM tpb_dokumen td
           LEFT JOIN referensi_dokumen rd on rd.kode_dokumen = td.KODE_JENIS_DOKUMEN
           WHERE id_header = ".intval($id));
       
       return $sql->result_array();
   }

   public function get_default_modal_dokumen($id){

       $sql = $this->mysql->query("SELECT td.*, rd.KODE_DOKUMEN, rd.URAIAN_DOKUMEN FROM tpb_dokumen td
           LEFT JOIN referensi_dokumen rd on rd.kode_dokumen = td.KODE_JENIS_DOKUMEN
           WHERE td.id = ".intval($id));

       if($sql->num_rows() > 0)
           return $sql->row_array();
       return false;

   }

   public function update_dokumen(
               $KODE_JENIS_DOKUMEN, $NOMOR_DOKUMEN, $TANGGAL_DOKUMEN){

       $select = "update tpb_dokumen set "
               . "KODE_JENIS_DOKUMEN = '" . $KODE_JENIS_DOKUMEN . "',"
               . "NOMOR_DOKUMEN =  '" . $NOMOR_DOKUMEN . "',"
               . "TANGGAL_DOKUMEN = '" . $TANGGAL_DOKUMEN . "'";
               
       $data = $this->mysql->query($select);
       if ($data)
           return true;
       return false;
   }

   public function delete_dokumen($id){

        $select = "delete from tpb_dokumen where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        
        if ($data)
            return true;
        return false;
   }

   // MODAL KEMASAN
   public function get_modal_kemasan($id){

       $sql = $this->mysql->query("SELECT * FROM tpb_kemasan tk
                                   LEFT JOIN referensi_kemasan rk on tk.KODE_JENIS_KEMASAN = rk.KODE_KEMASAN
                                   WHERE tk.ID_HEADER = ".intval($id));

       return $sql->result_array();

   }

   public function get_default_modal_kemasan($id){
        
        $sql = $this->mysql->query("SELECT * FROM tpb_kemasan tk
                                    LEFT JOIN referensi_kemasan rk on tk.KODE_JENIS_KEMASAN = rk.KODE_KEMASAN
                                    WHERE tk.ID = ".intval($id));

        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
   }

   public function update_kemasan($JUMLAH_KEMASAN, $KODE_JENIS_KEMASAN, $MERK_KEMASAN){

        $select = "update tpb_kemasan set "
                . "JUMLAH_KEMASAN = '" . $JUMLAH_KEMASAN . "',"
                . "KODE_JENIS_KEMASAN =  '" . $KODE_JENIS_KEMASAN . "',"
                . "MERK_KEMASAN = '" . $MERK_KEMASAN . "'";
                
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;
   }

   public function delete_kemasan($id){

        $select = "delete from tpb_kemasan where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        
        if ($data)
            return true;
        return false;
   }

}

?>
