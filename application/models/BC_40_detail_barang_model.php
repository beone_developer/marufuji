<?php 

/**
 * 
 */
class BC_40_detail_barang_model extends CI_Model
{
	
	function __construct(){
	    parent::__construct();
	    $this->db = $this->load->database('default', TRUE);
	    $this->mysql = $this->load->database('mysql', TRUE);
	}

	public function get_default_barang($id){
        $sql = $this->mysql->query("
        	SELECT tb.*, rs.URAIAN_STATUS, rsatuan.URAIAN_SATUAN 
        	FROM tpb_barang tb
        	LEFT JOIN tpb_header th on tb.ID_HEADER = th.id
			LEFT JOIN referensi_status rs on th.KODE_STATUS = rs.KODE_STATUS and th.KODE_DOKUMEN_PABEAN = rs.KODE_DOKUMEN
			LEFT JOIN referensi_satuan rsatuan on tb.KODE_SATUAN = rsatuan.KODE_SATUAN
        	WHERE id_header = ".intval($id)." order by tb.id asc limit 1"
        );
        
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barangnext($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tb.*, rs.URAIAN_SATUAN, rk.URAIAN_KEMASAN, rkg.URAIAN_GUNA, rkb.URAIAN_KONDISI, rkb25.URAIAN_KATEGORI
            from tpb_barang tb
            left join referensi_satuan rs on tb.KODE_SATUAN = rs.KODE_SATUAN 
            left join referensi_kemasan rk on tb.KODE_KEMASAN = rk.KODE_KEMASAN
            left join referensi_kode_guna rkg on tb.KODE_GUNA = rkg.KODE_GUNA
            left join referensi_kondisi_barang rkb on rkb.KODE_KONDISI = tb.KONDISI_BARANG
            left join referensi_kategori_barangbc25 rkb25 on rkb25.KODE_KATEGORI = tb.KATEGORI_BARANG
            where (tb.ID > ".intval($ID)." OR tb.ID = (SELECT MAX(tb.ID) FROM tpb_barang tb where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID asc limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barangprev($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tb.*, rs.URAIAN_SATUAN, rk.URAIAN_KEMASAN, rkg.URAIAN_GUNA, rkb.URAIAN_KONDISI, rkb25.URAIAN_KATEGORI
            from tpb_barang tb
            left join referensi_satuan rs on tb.KODE_SATUAN = rs.KODE_SATUAN 
            left join referensi_kemasan rk on tb.KODE_KEMASAN = rk.KODE_KEMASAN
            left join referensi_kode_guna rkg on tb.KODE_GUNA = rkg.KODE_GUNA
            left join referensi_kondisi_barang rkb on rkb.KODE_KONDISI = tb.KONDISI_BARANG
            left join referensi_kategori_barangbc25 rkb25 on rkb25.KODE_KATEGORI = tb.KATEGORI_BARANG
            where (tb.ID < ".intval($ID)." OR tb.ID = (SELECT MIN(tb.ID) FROM tpb_barang tb where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID desc limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifcukai($ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = ".intval($ID)." order by ID asc limit 1) and JENIS_TARIF = 'CUKAI'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    
        public function get_default_barang_tarifBM($ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = ".intval($ID)." order by ID asc limit 1) and JENIS_TARIF = 'BM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPN($ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = ".intval($ID)." order by ID asc limit 1) and JENIS_TARIF = 'PPN'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPH($ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = ".intval($ID)." order by ID asc limit 1) and JENIS_TARIF = 'PPH'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPNBM($ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = ".intval($ID)." order by ID asc limit 1) and JENIS_TARIF = 'PPNBM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }


    public function get_default_barang_tarifcukainext($ID_HEADER, $ID){
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID asc limit 1) and JENIS_TARIF = 'CUKAI'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    
        public function get_default_barang_tarifBMnext($ID_HEADER, $ID){
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID asc limit 1) and JENIS_TARIF = 'BM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPNnext($ID_HEADER, $ID){
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID asc limit 1) and JENIS_TARIF = 'PPN'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPHnext($ID_HEADER, $ID){
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID asc limit 1) and JENIS_TARIF = 'PPH'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPNBMnext($ID_HEADER, $ID){
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID asc limit 1) and JENIS_TARIF = 'PPNBM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }




    public function get_default_barang_tarifcukaiprev($ID_HEADER, $ID){
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID desc limit 1) and JENIS_TARIF = 'CUKAI'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    
        public function get_default_barang_tarifBMprev($ID_HEADER, $ID){
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID desc limit 1) and JENIS_TARIF = 'BM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPNprev($ID_HEADER, $ID){
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID desc limit 1) and JENIS_TARIF = 'PPN'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPHprev($ID_HEADER, $ID){
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID desc limit 1) and JENIS_TARIF = 'PPH'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPNBMprev($ID_HEADER, $ID){
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID desc limit 1) and JENIS_TARIF = 'PPNBM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function update_detail_barang($post){

        $ID = $this->mysql->escape($post['ID']);
        $KODE_BARANG = $this->mysql->escape($post['kode_barang']);
        $URAIAN = $this->mysql->escape($post['uraian_barang']);
        $TIPE = $this->mysql->escape($post['tipe']);
        $UKURAN = $this->mysql->escape($post['ukuran']);
        $SPESIFIKASI_LAIN = $this->mysql->escape($post['spf_lain']);
        $MERK = $this->mysql->escape($post['merk']);
        $JUMLAH_SATUAN = $this->mysql->escape($post['jumlah_satuan']);
        $KODE_SATUAN = $this->mysql->escape($post['kode_satuan']);
        $NETTO = $this->mysql->escape($post['netto']);
        $VOLUME = $this->mysql->escape($post['volume']);
        $HARGA_PENYERAHAN = $this->mysql->escape($post['harga_penyerahan']);

        $sql = $this->mysql->query(
            "UPDATE tpb_barang SET 
            KODE_BARANG = $KODE_BARANG, 
            URAIAN = $URAIAN, 
            TIPE = $TIPE, 
            UKURAN = $UKURAN, 
            SPESIFIKASI_LAIN = $SPESIFIKASI_LAIN, 
            MERK = $MERK, 
            JUMLAH_SATUAN = $JUMLAH_SATUAN, 
            KODE_SATUAN = $KODE_SATUAN,   
            VOLUME = $VOLUME, 
            HARGA_PENYERAHAN = $HARGA_PENYERAHAN, 
            NETTO = $NETTO 
            WHERE ID = $ID"
        );

        if($sql)
            return true;
        return false;

    }
}

 ?>