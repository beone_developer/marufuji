<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Log_model extends CI_Model{

	 public function save_log($param)
    {
        $sql        = $this->db->insert_string('public.beone_log',$param);
        $ex         = $this->db->query($sql);
        return $this->db->affected_rows($sql);
    }


		public function load_log(){
			$sql = $this->db->query("SELECT * FROM public.beone_log");
			return $sql->result_array();
		}

}
?>
