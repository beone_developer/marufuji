<?php

class preview_bc23_model extends CI_Model {

//    var $tabel = "usr";
    //   private $another;
    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
        $this->mysql = $this->load->database('mysql', TRUE);
    }

    public function select($aju) {

        $select = "select th.id,th.diskon, th.asuransi,th.kode_harga, th.biaya_tambahan, th.nomor_aju as nomor_aju, th.nama_pemasok as nama_pemasok, "
                . "th.alamat_pemasok as alamat_pemasok, th.kode_negara_pemasok as kode_negara_pemasok, "
                . "th.nomor_daftar as nomor_daftar, "
                . "th.tanggal_daftar as tanggal_daftar,rkpbongkar.uraian_kantor as kppbc_bongkar, "
                . "rkpawas.uraian_kantor as kppbc_awas, rkpbongkar.kode_kantor as kode_kppbc_bongkar, "
                . "rkpawas.kode_kantor as kode_kppbc_awas,"
                . " rkiusaha.uraian_kode_id as jenis_identitas_importir,th.id_pengusaha as kode_identitas_importir, "
                . "th.nama_pengusaha as nama_importir,"
                . " th.alamat_pengusaha as alamat_importir,"
                . "th.nomor_ijin_tpb as nomor_ijin_tpb,"
                . "rjausaha.uraian_jenis_api,"
                . "rkimilik.uraian_kode_id as jenis_identitas_pemilik,"
                . "th.id_pemilik, th.nama_pemilik as nama_pemilik,th.ALAMAT_PEMILIK as alamat_pemilik, "
                . "th.api_pengusaha,"
                . "rjamilik.uraian_jenis_api,"
                . "th.API_PEMILIK,"
                . "rp.npwp as rp_npwp,"
                . "rp.nama as rp_nama, "
                . "rp.alamat as rp_alamat,"
                . "rp.nomor_npppjk, "
                . "rp.tanggal_npppjk, th.NOMOR_VOY_FLIGHT, th.KODE_BENDERA,"
                . "rca.URAIAN_CARA_ANGKUT,th.NAMA_PENGANGKUT,th.NAMA_PENGANGKUT,"
                . "th.KODE_PEL_MUAT,rpel_muat.uraian_pelabuhan as pel_muat,"
                . "th.KODE_PEL_TRANSIT,rpel_transit.uraian_pelabuhan as pel_transit,"
                . "th.KODE_PEL_BONGKAR,rpel_bongkar.uraian_pelabuhan as pel_bongkar,"
                . "th.kode_tps, rt.uraian_tps, th.kode_valuta, rv.uraian_valuta, th.NDPBM, th.FOB,"
                . "th.freight, th.asuransi, th.cif, th.cif_rupiah, th.bruto, th.netto, th.KOTA_TTD, th.TANGGAL_TTD, th.NAMA_TTD, th.JABATAN_TTD"
                . " from tpb_header th "
                . "left join referensi_kantor_pabean rkpbongkar on th.kode_kantor_bongkar = rkpbongkar.kode_kantor "
                . "left join referensi_kantor_pabean rkpawas on th.kode_kantor = rkpawas.kode_kantor "
                . "left join referensi_kode_id rkiusaha on th.kode_id_pengusaha = rkiusaha.kode_id "
                . "left join referensi_jenis_api rjausaha on th.kode_jenis_api_pengusaha  = rjausaha.kode_jenis_api "
                . "left join referensi_kode_id rkimilik on th.kode_id_pemilik = rkimilik.kode_id "
                . "left join referensi_jenis_api rjamilik on th.KODE_JENIS_API_PEMILIK  = rjamilik.kode_jenis_api "
                . "left join referensi_ppjk rp on th.id_ppjk = rp.id "
                . "left join referensi_pelabuhan rpel_muat on th.kode_pel_muat = rpel_muat.kode_pelabuhan "
                . "left join referensi_pelabuhan rpel_transit on th.kode_pel_transit = rpel_transit.kode_pelabuhan "
                . "left join referensi_pelabuhan rpel_bongkar on th.KODE_PEL_BONGKAR = rpel_bongkar.kode_pelabuhan "
                . "left join referensi_cara_angkut rca on th.KODE_CARA_ANGKUT = rca.ID "
                . "left join referensi_tps rt on th.kode_tps = rt.kode_tps "
                . "left join referensi_valuta rv on th.kode_valuta = rv.kode_valuta"
                . " where th.nomor_aju = '" . $aju . "' and th.kode_dokumen_pabean = 23";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_inv($aju) {
        $select = "select th.NOMOR_AJU, td.NOMOR_DOKUMEN, td.TANGGAL_DOKUMEN as TANGGAL_DOKUMEN,rd.URAIAN_DOKUMEN from tpb_header th
		left join tpb_dokumen td on th.id = td.ID_HEADER
		left join referensi_dokumen rd on td.KODE_JENIS_DOKUMEN = rd.KODE_DOKUMEN
		where th.NOMOR_AJU = '" . $aju . "' and rd.kode_dokumen = 380 and th.kode_dokumen_pabean = 23";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_another($aju) {
        $select = "select td.id as id_dokumen, th.NOMOR_AJU, td.NOMOR_DOKUMEN, td.TANGGAL_DOKUMEN as TANGGAL_DOKUMEN,rd.URAIAN_DOKUMEN from tpb_header th
		left join tpb_dokumen td on th.id = td.ID_HEADER
		left join referensi_dokumen rd on td.KODE_JENIS_DOKUMEN = rd.KODE_DOKUMEN
		where th.NOMOR_AJU = '" . $aju . "' and rd.kode_dokumen not in (380) and th.kode_dokumen_pabean = 23";
        $data = $this->mysql->query($select);
        return $data->result();
    }
    public function get_another_id($ID) {
        $select = "select td.id as id_dokumen, th.NOMOR_AJU, td.NOMOR_DOKUMEN, td.TANGGAL_DOKUMEN as TANGGAL_DOKUMEN,rd.URAIAN_DOKUMEN from tpb_header th
		left join tpb_dokumen td on th.id = td.ID_HEADER
		left join referensi_dokumen rd on td.KODE_JENIS_DOKUMEN = rd.KODE_DOKUMEN
		where th.ID = '" . $ID . "' and rd.kode_dokumen not in (380) and th.kode_dokumen_pabean = 23";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_lc($aju) {
        $select = "select th.NOMOR_AJU, td.NOMOR_DOKUMEN, td.TANGGAL_DOKUMEN as TANGGAL_DOKUMEN,rd.URAIAN_DOKUMEN from tpb_header th
		left join tpb_dokumen td on th.id = td.ID_HEADER
		left join referensi_dokumen rd on td.KODE_JENIS_DOKUMEN = rd.KODE_DOKUMEN
		where th.NOMOR_AJU = '" . $aju . "' and rd.kode_dokumen = 465 and th.kode_dokumen_pabean = 23";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_kontainer($aju) {
        $select = "select tk.id as id_kontainer, th.NOMOR_AJU, tk.NOMOR_KONTAINER, ruk.URAIAN_UKURAN_KONTAINER, rtk.URAIAN_TIPE_KONTAINER from tpb_header th
                    left join tpb_kontainer tk on th.id = tk.ID_HEADER
                    left join referensi_ukuran_kontainer ruk on tk.KODE_UKURAN_KONTAINER = ruk.KODE_UKURAN_KONTAINER
                    left join referensi_tipe_kontainer rtk on tk.KODE_TIPE_KONTAINER = rtk.KODE_TIPE_KONTAINER
                    where NOMOR_AJU = '" . $aju . "' and th.kode_dokumen_pabean = 23";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_kemasan($aju) {
        $select = "select tk.id as id_kemasan, th.NOMOR_AJU,tk.JUMLAH_KEMASAN,tk.MERK_KEMASAN,
                tk.KODE_JENIS_KEMASAN, rk.URAIAN_KEMASAN from tpb_header th
                left join tpb_kemasan tk on th.id = tk.ID_HEADER 
                left join referensi_kemasan rk on tk.KODE_JENIS_KEMASAN = rk.KODE_KEMASAN 
                where NOMOR_AJU = '" . $aju . "' and th.kode_dokumen_pabean = 23";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function update(
    $id, $nomor_pendaftaran, $tanggal_pendaftaran, $kppbc_bongkar, $kppbc_pengawas, $tujuan, $nama_pemasok, $alamat_pemasok, $negara_pemasok, $jenis_identitas_importir, $nomor_identitas_importir, $nama_importir, $nomor_ijin_tpb, $alamat_importir, $jenis_api_importir, $nomor_api_importir, $jenis_identitas_pemilik, $nomor_identitas_pemilik, $nama_pemilik, $alamat_pemilik, $jenis_api_pemilik, $nomor_api_pemilik, $nomor_npppjk, $tanggal_npppjk, $cara_angkut, $nama_sarana_angkut, $nomor_vf, $kode_bendera, $kode_pel_muat, $pel_muat, $kode_pel_transit, $kode_pel_bongkar, $pel_bongkar, $nomor_dokumen, $tgl_dokumen, $fasim_1, $fasim_2, $fasim_3, $LC, $tgl_lc, $BL, $BL_1, $BL_2, $bc11_1, $bc11_2, $kode_tps, $uraian_tps, $kode_valuta, $ndpbm, $fob, $freight, $asuransi, $cif, $cif_rp, $bruto, $netto, $kota_ttd, $tgl_ttd, $nama_ttd, $jabatan_ttd) {

        $select = "update tpb_header set "
                . "nomor_daftar = '" . $nomor_pendaftaran . "',"
                . "tanggal_daftar =  '" . $tanggal_pendaftaran . "',"
                . "kode_kantor_bongkar = '" . $kppbc_bongkar . "',"
                . "kode_kantor = '" . $kppbc_pengawas . "',"
                . "kode_kantor_tujuan = '" . $tujuan . "',"
                . "nama_pemasok = '" . $nama_pemasok . "',"
                . "alamat_pemasok = '" . $alamat_pemasok . "',"
                . "kode_bendera = '" . $negara_pemasok . "',"
                . "kode_id_pengusaha = '" . $jenis_identitas_importir . "',"
                . "id_pengusaha = '" . $nomor_identitas_importir . "',"
                . "nama_pengusaha = '" . $nama_importir . "',"
                . "nomor_ijin_tpb = '" . $nomor_ijin_tpb . "',"
                . "alamat_pengusaha = '" . $alamat_importir . "',"
                . "kode_jenis_api_pengusaha = '" . $jenis_api_importir . "',"
                . "api_pengusaha = '" . $nomor_api_importir . "',"
                . "kode_id_pemilik = '" . $jenis_identitas_pemilik . "',"
                . "id_pemilik = '" . $nomor_identitas_pemilik . "',"
                . "nama_pemilik = '" . $nama_pemilik . "',"
                . "alamat_pemilik = '" . $alamat_pemilik . "',"
                . "NPPPJK = '" . $nomor_npppjk . "',"
                . "TANGGAL_NPPPJK = '" . $tanggal_npppjk . "',"
                . "KODE_CARA_ANGKUT = '" . $cara_angkut . "',"
                . "NAMA_PENGANGKUT = '" . $nama_sarana_angkut . "',"
                . "Nomor_voy_flight= '" . $nomor_vf . "',"
                . "kode_bendera= '" . $kode_bendera . "',"
                . "kode_pel_muat= '" . $kode_pel_muat . "',"
                . "kode_pel_transit= '" . $kode_pel_transit . "',"
                . "kode_pel_bongkar= '" . $kode_pel_bongkar . "',"
                . "kode_tps= '" . $kode_tps . "',"
                . "kode_valuta = '" . $kode_valuta . "',"
                . "ndpbm = '" . $ndpbm . "',"
                . "fob = '" . $fob . "',"
                . "freight = '" . $freight . "',"
                . "asuransi = '" . $asuransi . "',"
                . "cif = '" . $cif . "',"
                . "cif_rupiah = '" . $cif_rp . "',"
                . "nama_pengangkut = '" . $sarana_angkut . "',"
                . "bruto = '" . $bruto . "',"
                . "netto = '" . $netto . "',"
                . "kota_ttd = '" . $kota_ttd . "',"
                . "tanggal_ttd = '" . $tgl_ttd . "',"
                . "nama_ttd = '" . $nama_ttd . "',"
                . "jabatan_ttd = '" . $jabatan_ttd . "'"
                . "where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;
    }

    public function get_respon($id) {
        $select = "select tr.KODE_RESPON, tr.NOMOR_RESPON, tr.TANGGAL_RESPON, tr.ID, tr.ID_HEADER, tr.WAKTU_RESPON from tpb_header th "
                . "left join tpb_respon tr on th.id = tr.id_header "
                . "where th.id = '" . $id . "'";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_default_barang($ID) {
        $sql = $this->mysql->query("select * from tpb_barang where id_header = " . intval($ID) . " order by ID asc limit 1");
        if ($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifcukai($ID) {
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = " . intval($ID) . " order by ID asc limit 1) and JENIS_TARIF = 'CUKAI'");
        if ($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifBM($ID) {
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = " . intval($ID) . " order by ID asc limit 1) and JENIS_TARIF = 'BM'");
        if ($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPNBM($ID) {
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = " . intval($ID) . " order by ID asc limit 1) and JENIS_TARIF = 'PPNBM'");
        if ($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPN($ID) {
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = " . intval($ID) . " order by ID asc limit 1) and JENIS_TARIF = 'PPN'");
        if ($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPH($ID) {
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = " . intval($ID) . " order by ID asc limit 1) and JENIS_TARIF = 'PPH'");
        if ($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function delete_dokumen($id) {
        $select = "delete from tpb_dokumen where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;
    }

    public function delete_kemasan($id) {
        $select = "delete from tpb_kemasan where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;
    }

    public function delete_kontainer($id) {
        $select = "delete from tpb_kontainer where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;
    }
    public function update_modal_dokumen($id, $jenis_dokumen, $nomor_dokumen, $tgl_dokumen){
       $select = "update tpb_dokumen "
               . "set kode_jenis_dokumen = (select KODE_DOKUMEN from referensi_dokumen where uraian_dokumen = '" . $jenis_dokumen . "'),"
               . "nomor_dokumen =  '" . $nomor_dokumen . "', tanggal_dokumen =  '" . $tgl_dokumen . "'"
               . "where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;

    }
    
    public function update_popup_kemasan($id, $jml_kemasan, $kode_kemasan,$merk_kemasan, $uraian_kemasan){
       $select = "update tpb_kemasan set kode_jenis_kemasan = (select KODE_KEMASAN from referensi_kemasan where uraian_kemasan = '" . $uraian_kemasan . "'), "
               . "jumlah_kemasan =  '" . $jml_kemasan . "', "
               . "merk_kemasan =  '" . $merk_kemasan . "' "
               . "where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;

    }
    public function update_popup_kontainer($id, $nomor_kontainer, $ukuran_kontainer,$tipe_kontainer){
       $select = "update tpb_kontainer set kode_ukuran_kontainer = (select kode_ukuran_kontainer from referensi_ukuran_kontainer where uraian_ukuran_kontainer = '" . $ukuran_kontainer . "'), "
               . "nomor_kontainer =  '" . $nomor_kontainer . "', "
               . "kode_tipe_kontainer =  (select kode_tipe_kontainer from referensi_tipe_kontainer where uraian_tipe_kontainer = '" . $tipe_kontainer . "') "
               . "where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;

    }
    public function get_default_modal_kemasan($ID){
        $sql = $this->mysql->query("SELECT tk.*, tk.ID,  rk.URAIAN_KEMASAN
            FROM tpb_kemasan tk
            left join referensi_kemasan rk on tk.KODE_JENIS_KEMASAN = rk.KODE_KEMASAN
            WHERE tk.id = ".intval($ID));

        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;

    }
    public function get_default_modal_dokumen($ID){
        $sql = $this->mysql->query("SELECT td.*, rd.KODE_DOKUMEN, rd.URAIAN_DOKUMEN FROM tpb_dokumen td
            LEFT JOIN referensi_dokumen rd on rd.kode_dokumen = td.KODE_JENIS_DOKUMEN
            WHERE td.id = ".intval($ID));

        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;

    }
    public function get_default_modal_kontainer($ID){
        $sql = $this->mysql->query("SELECT td.*, td.ID, rd.URAIAN_UKURAN_KONTAINER, rt.URAIAN_TIPE_KONTAINER FROM tpb_kontainer td
            LEFT JOIN referensi_ukuran_kontainer rd on rd.kode_ukuran_kontainer = td.kode_ukuran_kontainer
            left join referensi_tipe_kontainer rt on td.kode_tipe_kontainer = rt.kode_tipe_kontainer
            WHERE td.id = ".intval($ID));

        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;

    }
    public function update_popup_harga($id, $harga, $valuta, $modal_ndpbm,$harga_fob,
                $biaya_tambahan,$diskon,$asuransi_bayar_di,$nilai_asuransi,$freight,$fob,$cif,$cif_rp){
       $select = "update tpb_header set KODE_HARGA = '".$harga."', kode_valuta = '".$valuta."', "
               . "NDPBM =  '".$modal_ndpbm."', fob = '".$harga_fob."', biaya_tambahan = '".$biaya_tambahan."', "
               . "diskon = '".$diskon."',asuransi = '".$asuransi_bayar_di."',freight = '".$freight."', "
               . "fob = '".$fob."',cif = '".$cif."',cif_rupiah = '".$cif_rp."' where id = '".$id."'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;

    }

}

?>
