<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Stockopname_model extends CI_Model{

	public function load_stock_opname(){
		$sql = $this->db->query("SELECT *
															FROM public.beone_opname_header WHERE flag = 1");
		return $sql->result_array();
	}

	public function load_stock_detail_opname($opname_header_id){
		$sql = $this->db->query("SELECT d.opname_detail_id, d.opname_header_id, d.item_id, i.nama as nitem, d.qty_existing, d.qty_opname, d.qty_selisih, d.status_opname, d.flag
															FROM public.beone_opname_detail d INNER JOIN public.beone_item i ON d.item_id = i.item_id WHERE d.flag = 1 AND d.opname_header_id = ".intval($opname_header_id));
		return $sql->result_array();
	}

	public function get_default_header($opname_header_id){
		$sql = $this->db->query("SELECT *
															FROM public.beone_opname_header WHERE flag = 1 AND opname_header_id = ".intval($opname_header_id));
	if($sql->num_rows() > 0)
		return $sql->row_array();
	return false;
	}

	public function get_default($opname_detail_id){
		$sql = $this->db->query("SELECT d.opname_detail_id, d.opname_header_id, d.item_id, i.nama as nitem, d.qty_existing, d.qty_opname, d.qty_selisih, d.status_opname, d.flag
															FROM public.beone_opname_detail d INNER JOIN public.beone_item i ON d.item_id = i.item_id WHERE d.flag = 1 AND d.opname_detail_id = ".intval($opname_detail_id));
	if($sql->num_rows() > 0)
		return $sql->row_array();
	return false;
	}

	public function proses_opname($post, $opname_detail_id){
		$qty_opname_ = $this->db->escape($post['qty_opname']);

		$qty_opname_ex = str_replace(".", "", $qty_opname_);
		$qty_opname = str_replace(",", ".", $qty_opname_ex);

		$sql_detail = $this->db->query("SELECT * FROM public.beone_opname_detail WHERE opname_detail_id = $opname_detail_id");
		$hasil_sql_detail = $sql_detail->row_array();
		$header_id = $hasil_sql_detail['opname_header_id'];
		$qty_existing = $hasil_sql_detail['qty_existing'];

		$sql = $this->db->query("UPDATE public.beone_opname_detail
														SET qty_opname = $qty_opname, qty_existing = $qty_existing WHERE opname_detail_id = ".intval($opname_detail_id));

		$sql_setelah_insert = $this->db->query("SELECT * FROM public.beone_opname_detail WHERE opname_detail_id = $opname_detail_id");
		$hasil_setelah_insert = $sql_setelah_insert->row_array();
		$qty_opname_integer = $hasil_setelah_insert['qty_opname'];
		$qty_existing_integer = $hasil_setelah_insert['qty_existing'];

		$qty_selisih = $qty_existing_integer - $qty_opname_integer;

		if ($qty_selisih == 0){//qty cocok
				$status = 1;
		}else if($qty_selisih <= 0){//qty lebih dari existing
				$status = 0;
		}else{//qty kurang dari existing
				$status = 2;
		}

		$sql2 = $this->db->query("UPDATE public.beone_opname_detail
														SET qty_selisih = $qty_selisih, status_opname = $status WHERE opname_detail_id = ".intval($opname_detail_id));

		$sql_count_lebih = $this->db->query("SELECT COUNT(opname_detail_id) as lebih FROM public.beone_opname_detail WHERE status_opname = 0 AND opname_header_id = $header_id");
		$status_opname_lebih = $sql_count_lebih->row_array();
		$opname_lebih = $status_opname_lebih['lebih'];

		$sql_count_kurang = $this->db->query("SELECT COUNT(opname_detail_id) as kurang FROM public.beone_opname_detail WHERE status_opname = 2 AND opname_header_id = $header_id");
		$status_opname_kurang = $sql_count_kurang->row_array();
		$opname_kurang = $status_opname_kurang['kurang'];

		$sql_count_pas = $this->db->query("SELECT COUNT(opname_detail_id) as pas FROM public.beone_opname_detail WHERE status_opname = 1 AND opname_header_id = $header_id");
		$status_opname_pas = $sql_count_pas->row_array();
		$opname_pas = $status_opname_pas['pas'];

		$sql_header = $this->db->query("UPDATE public.beone_opname_header
														SET total_item_opname_match = $opname_pas, total_item_opname_min = $opname_kurang, total_item_opname_plus = $opname_lebih WHERE opname_header_id = ".intval($header_id));


		if($sql)
			return true;
		return false;
	}

	public function simpan($post){
		$session_id = $this->session->userdata('user_id');
		$no_opname = $this->db->escape($post['no_opname']);
		$keterangan = $this->db->escape($post['keterangan']);
		$update_date = date('Y-m-d');

		$tanggal_awal = $this->db->escape($post['tanggal']);
		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		$sql_count_item = $this->db->query("SELECT COUNT(item_id) as jml FROM public.beone_item WHERE flag = 1");
		$hasil_count_item = $sql_count_item->row_array();
		$total_item = $hasil_count_item['jml'];

		$sql = $this->db->query("INSERT INTO public.beone_opname_header(
														opname_header_id, document_no, opname_date, total_item_opname, total_item_opname_match, total_item_opname_plus, total_item_opname_min, update_by, flag, keterangan, update_date)
														VALUES (DEFAULT, $no_opname, '$tanggal', $total_item, $total_item, 0, 0, $session_id, 1, $keterangan, '$update_date')");

		$sql_hid = $this->db->query("SELECT opname_header_id FROM public.beone_opname_header WHERE flag = 1 ORDER BY opname_header_id DESC LIMIT 1");
		$hasil_hid = $sql_hid->row_array();
		$hid = $hasil_hid['opname_header_id'];


		$sql_list_item = $this->db->query("SELECT * FROM public.beone_item WHERE flag = 1");
		foreach($sql_list_item->result_array() as $row){
	//*******************************************************************
					$sql_mutasi_item = $this->db->query("SELECT *  FROM public.beone_inventory WHERE flag = 1 AND item_id = ".intval($row['item_id']));

					$count_inventory = $this->db->query("SELECT COUNT(item_id) as jml_transaksi  FROM public.beone_inventory WHERE flag = 1 AND item_id = ".intval($row['item_id']));
					$hasil_jml_transaksi = $count_inventory->row_array();

					$total_qty_in = 0;
					$total_qty_out = 0;
					if ($hasil_jml_transaksi['jml_transaksi'] > 0){
						foreach($sql_mutasi_item->result_array() as $mutasi){
									$qty_in = $mutasi['qty_in'];
									$qty_out = $mutasi['qty_out'];

									$total_qty_in = $total_qty_in + $qty_in;
									$total_qty_out = $total_qty_out + $qty_out;
						}
					}


					$qty_akhir= ($row['saldo_qty'] + $total_qty_in) - $total_qty_out;

		//*******************************************************************
					$sql2 = $this->db->query("INSERT INTO public.beone_opname_detail(
																		opname_detail_id, opname_header_id, item_id, qty_existing, qty_opname, qty_selisih, status_opname, flag)
																		VALUES (DEFAULT, $hid, $row[item_id], $qty_akhir, $qty_akhir, 0, 1, 1)");

		}


		if($sql)
			return true;
		return false;
	}

	public function update($post, $opname_header_id){
		$session_id = $this->session->userdata('user_id');
		$no_opname = $this->db->escape($post['no_opname']);
		$keterangan = $this->db->escape($post['keterangan']);
		$update_date = date('Y-m-d');

		$tanggal_awal = $this->db->escape($post['tanggal']);
		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		$sql = $this->db->query("UPDATE public.beone_opname_header
															SET document_no=$no_opname, opname_date='$tanggal',  update_by=$session_id, keterangan=$keterangan, update_date='$update_date'
															WHERE opname_header_id = ".intval($opname_header_id));

		if($sql)
			return true;
		return false;
	}

	public function delete($opname_header_id){
		$sql_header = $this->db->query("DELETE FROM public.beone_opname_header WHERE opname_header_id = ".intval($opname_header_id));
		$sql_detail = $this->db->query("DELETE FROM public.beone_opname_detail WHERE opname_header_id = ".intval($opname_header_id));
	}

}
?>
