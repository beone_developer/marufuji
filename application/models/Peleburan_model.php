<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Peleburan_model extends CI_Model{

	public function load_item(){
		$sql_list_item = $this->db->query("SELECT * FROM public.beone_item WHERE flag =1");
		return $sql_list_item->result_array();
	}

	public function load_peleburan(){
		$sql = $this->db->query("SELECT p.peleburan_id, p.peleburan_no, p.peleburan_date, p.keterangan, p.item_id, i.nama as nitem, p.qty_peleburan, p.update_by, p.update_date, p.flag
															FROM public.beone_peleburan p INNER JOIN public.beone_item i ON p.item_id = i.item_id
															WHERE p.flag = 1");

		return $sql->result_array();
	}

	public function simpan($post){
		$session_id = $this->session->userdata('user_id');
		$item = $this->db->escape($post['item']);
		$peleburan_no = $this->db->escape($post['nomor_peleburan']);
		//$qty_peleburan = $this->db->escape($post['qty_peleburan']);
		$keterangan = $this->db->escape($post['keterangan_peleburan']);
		$update_date = date('Y-m-d');

		$qty_peleburan_ = str_replace(".", "", $post['qty_peleburan']);
		$qty_peleburan = str_replace(",", ".", $qty_peleburan_);

		$tanggal_awal = $this->db->escape($post['tanggal']);
		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		$sql = $this->db->query("INSERT INTO public.beone_peleburan(
														peleburan_id, peleburan_no, peleburan_date, keterangan, item_id, qty_peleburan, update_by, update_date, flag)
														VALUES (DEFAULT, $peleburan_no, '$tanggal', $keterangan, $item, $qty_peleburan, $session_id, '$update_date', 1)");


//************************* INVENTORY ******************************************************
		$count_transksi_inventory = $this->db->query("SELECT COUNT(intvent_trans_id) as jml FROM public.beone_inventory WHERE flag = 1 AND item_id = $item");
		$hasil_count_inventory = $count_transksi_inventory->row_array();

		$sql_saldo_awal_item = $this->db->query("SELECT * FROM public.beone_item WHERE flag = 1 AND item_id = $item");
		$hasil_saldo_awal_item = $sql_saldo_awal_item->row_array();

		$sql_saldo_awal = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = $item ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
		$hasil_saldo_awal = $sql_saldo_awal->row_array();

		if ($hasil_count_inventory['jml'] == 0){// menggunakan saldo awal item
				$saldo_awal_qty = $hasil_saldo_awal_item['saldo_qty'];
				$saldo_awal_amount = $hasil_saldo_awal_item['saldo_idr'];

				if ($saldo_awal_qty == 0 OR $saldo_awal_amount == 0){
						$saldo_awal_unit_price = 0;
				}else{
						$saldo_awal_unit_price = $saldo_awal_amount / $saldo_awal_qty;
				}
		}else{// menggunakan saldo dari transaksi akhir inventory
				$saldo_awal_qty = $hasil_saldo_awal['sa_qty'];
				$saldo_awal_unit_price = $hasil_saldo_awal['sa_unit_price'];
				$saldo_awal_amount = $hasil_saldo_awal['sa_amount'];
		}


				$saldo_akhir_qty = $saldo_awal_qty - $qty_peleburan;
				$saldo_akhir_amount = $saldo_awal_amount - ($qty_peleburan * $saldo_awal_unit_price);
				$saldo_akhir_unit_price = $saldo_akhir_amount / $saldo_akhir_qty;

				$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(
																				intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date)
																				VALUES (DEFAULT, $peleburan_no, $item, '$tanggal', $keterangan, 0, 0, $qty_peleburan, $saldo_awal_unit_price, $saldo_akhir_qty, $saldo_akhir_unit_price, $saldo_akhir_amount, 1, $session_id, '$update_date')");


//************************* END INVENTORY ******************************************************

	if($sql AND $sql_inventory)
			return true;
		return false;
	}


	public function delete($peleburan_id, $peleburan_no){
		$sql = $this->db->query("DELETE FROM public.beone_peleburan WHERE peleburan_id = ".intval($peleburan_id));
		$sql_inv = $this->db->query("DELETE FROM public.beone_inventory WHERE intvent_trans_no = '$peleburan_no'");
		//$sql_gl = $this->db->query("DELETE FROM public.beone_gl WHERE pasangan_no = '$transfer_no'");
	}

}
?>
