<?php

class bc25_detail_model extends CI_Model {

//    var $tabel = "usr";
 //   private $another;
    function __construct(){
        parent::__construct();
        $this->db = $this ->load -> database('default', TRUE);
        $this->mysql = $this ->load -> database('mysql', TRUE);
    }

    public function get_default_barang($ID){
        $sql = $this->mysql->query("select tb.*, rs.URAIAN_SATUAN, rk.URAIAN_KEMASAN, rkg.URAIAN_GUNA, rkb.URAIAN_KONDISI, rkb25.URAIAN_KATEGORI
            from tpb_barang tb 
            left join referensi_satuan rs on tb.KODE_SATUAN = rs.KODE_SATUAN 
            left join referensi_kemasan rk on tb.KODE_KEMASAN = rk.KODE_KEMASAN
            left join referensi_kode_guna rkg on tb.KODE_GUNA = rkg.KODE_GUNA
            left join referensi_kondisi_barang rkb on rkb.KODE_KONDISI = tb.KONDISI_BARANG
            left join referensi_kategori_barangbc25 rkb25 on rkb25.KODE_KATEGORI = tb.KATEGORI_BARANG
            where id_header = ".intval($ID)." order by ID asc limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    
    public function get_default_barangprev($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tb.*, rs.URAIAN_SATUAN, rk.URAIAN_KEMASAN, rkg.URAIAN_GUNA, rkb.URAIAN_KONDISI, rkb25.URAIAN_KATEGORI
            from tpb_barang tb
            left join referensi_satuan rs on tb.KODE_SATUAN = rs.KODE_SATUAN 
            left join referensi_kemasan rk on tb.KODE_KEMASAN = rk.KODE_KEMASAN
            left join referensi_kode_guna rkg on tb.KODE_GUNA = rkg.KODE_GUNA
            left join referensi_kondisi_barang rkb on rkb.KODE_KONDISI = tb.KONDISI_BARANG
            left join referensi_kategori_barangbc25 rkb25 on rkb25.KODE_KATEGORI = tb.KATEGORI_BARANG
            where (tb.ID < ".intval($ID)." OR tb.ID = (SELECT MIN(tb.ID) FROM tpb_barang tb where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID desc limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barangnext($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tb.*, rs.URAIAN_SATUAN, rk.URAIAN_KEMASAN, rkg.URAIAN_GUNA, rkb.URAIAN_KONDISI, rkb25.URAIAN_KATEGORI
            from tpb_barang tb
            left join referensi_satuan rs on tb.KODE_SATUAN = rs.KODE_SATUAN 
            left join referensi_kemasan rk on tb.KODE_KEMASAN = rk.KODE_KEMASAN
            left join referensi_kode_guna rkg on tb.KODE_GUNA = rkg.KODE_GUNA
            left join referensi_kondisi_barang rkb on rkb.KODE_KONDISI = tb.KONDISI_BARANG
            left join referensi_kategori_barangbc25 rkb25 on rkb25.KODE_KATEGORI = tb.KATEGORI_BARANG
            where (tb.ID > ".intval($ID)." OR tb.ID = (SELECT MAX(tb.ID) FROM tpb_barang tb where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID asc limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifcukai($ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = ".intval($ID)." order by ID asc limit 1) and JENIS_TARIF = 'CUKAI'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    
        public function get_default_barang_tarifBM($ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = ".intval($ID)." order by ID asc limit 1) and JENIS_TARIF = 'BM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPN($ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = ".intval($ID)." order by ID asc limit 1) and JENIS_TARIF = 'PPN'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPH($ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = ".intval($ID)." order by ID asc limit 1) and JENIS_TARIF = 'PPH'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPNBM($ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = ".intval($ID)." order by ID asc limit 1) and JENIS_TARIF = 'PPNBM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }


    public function get_default_barang_tarifcukainext($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID asc limit 1) and JENIS_TARIF = 'CUKAI'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    
        public function get_default_barang_tarifBMnext($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID asc limit 1) and JENIS_TARIF = 'BM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPNnext($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID asc limit 1) and JENIS_TARIF = 'PPN'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPHnext($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID asc limit 1) and JENIS_TARIF = 'PPH'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPNBMnext($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID asc limit 1) and JENIS_TARIF = 'PPNBM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }




    public function get_default_barang_tarifcukaiprev($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID desc limit 1) and JENIS_TARIF = 'CUKAI'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    
        public function get_default_barang_tarifBMprev($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID desc limit 1) and JENIS_TARIF = 'BM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPNprev($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID desc limit 1) and JENIS_TARIF = 'PPN'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPHprev($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID desc limit 1) and JENIS_TARIF = 'PPH'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPNBMprev($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tbt.* from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID desc limit 1) and JENIS_TARIF = 'PPNBM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function updateBM($post){
        $ID = $this->mysql->escape($post['id_bm']);
        $KODE_TARIF = $this->mysql->escape($post['kode_tarif_bm']);
        $TARIF = $this->mysql->escape($post['tarif_bm']);
        $KODE_FASILITAS = $this->mysql->escape($post['kode_fasilitas_bm']);
        $TARIF_FASILITAS = $this->mysql->escape($post['tarif_fasilitas_bm']);

        $sql = $this->mysql->query("UPDATE tpb_barang_tarif SET KODE_TARIF = $KODE_TARIF, TARIF = $TARIF, KODE_FASILITAS = $KODE_FASILITAS, TARIF_FASILITAS = $TARIF_FASILITAS WHERE ID = $ID and JENIS_TARIF = 'BM'");

        if($sql)
            return true;
        return false;
    }

    public function updatePPN($post){
        $ID = $this->mysql->escape($post['id_ppn']);
        $TARIF = $this->mysql->escape($post['tarif_ppn']);
        $KODE_FASILITAS = $this->mysql->escape($post['kode_fasilitas_ppn']);
        $TARIF_FASILITAS = $this->mysql->escape($post['tarif_fasilitas_ppn']);

        $sql = $this->mysql->query("UPDATE tpb_barang_tarif SET TARIF = $TARIF, KODE_FASILITAS = $KODE_FASILITAS, TARIF_FASILITAS = $TARIF_FASILITAS WHERE ID = $ID and JENIS_TARIF = 'PPN'");

        if($sql)
            return true;
        return false;
    }

    public function updatePPnBM($post){
        $ID = $this->mysql->escape($post['id_ppnbm']);
        $TARIF = $this->mysql->escape($post['tarif_ppnbm']);
        $KODE_FASILITAS = $this->mysql->escape($post['kode_fasilitas_ppnbm']);
        $TARIF_FASILITAS = $this->mysql->escape($post['tarif_fasilitas_ppnbm']);

        $sql = $this->mysql->query("UPDATE tpb_barang_tarif SET TARIF = $TARIF, KODE_FASILITAS = $KODE_FASILITAS, TARIF_FASILITAS = $TARIF_FASILITAS WHERE ID = $ID and JENIS_TARIF = 'PPNBM'");

        if($sql)
            return true;
        return false;
    }

    public function updatePPH($post){
        $ID = $this->mysql->escape($post['id_pph']);
        $TARIF = $this->mysql->escape($post['tarif_pph']);
        $KODE_FASILITAS = $this->mysql->escape($post['kode_fasilitas_pph']);
        $TARIF_FASILITAS = $this->mysql->escape($post['tarif_fasilitas_pph']);

        $sql = $this->mysql->query("UPDATE tpb_barang_tarif SET TARIF = $TARIF, KODE_FASILITAS = $KODE_FASILITAS, TARIF_FASILITAS = $TARIF_FASILITAS WHERE ID = $ID and JENIS_TARIF = 'PPH'");

        if($sql)
            return true;
        return false;
    }

    public function updateCukai($post){
        $ID = $this->mysql->escape($post['id_cukai']);
        $KODE_KOMODITI_CUKAI = $this->mysql->escape($post['kode_komoditi_cukai']);
        $KODE_TARIF = $this->mysql->escape($post['kode_tarif_cukai']);
        $TARIF = $this->mysql->escape($post['tarif_cukai']);
        $KODE_SATUAN = $this->mysql->escape($post['kode_satuan_cukai']);
        $JUMLAH_SATUAN = $this->mysql->escape($post['jumlah_satuan_cukai']);
        $KODE_FASILITAS = $this->mysql->escape($post['kode_fasilitas_cukai']);
        $TARIF_FASILITAS = $this->mysql->escape($post['tarif_fasilitas_cukai']);

        $sql = $this->mysql->query("UPDATE tpb_barang_tarif SET KODE_KOMODITI_CUKAI = $KODE_KOMODITI_CUKAI, KODE_TARIF = $KODE_TARIF, TARIF = $TARIF, KODE_SATUAN = $KODE_SATUAN, JUMLAH_SATUAN = $JUMLAH_SATUAN, KODE_FASILITAS = $KODE_FASILITAS, TARIF_FASILITAS = $TARIF_FASILITAS WHERE ID = $ID and JENIS_TARIF = 'CUKAI'");

        if($sql)
            return true;
        return false;
    }


    public function update25($post){
        $ID = $this->mysql->escape($post['ID']);
        $KATEGORI_BARANG = $this->mysql->escape($post['kategori_barang']);
        $KONDISI_BARANG = $this->mysql->escape($post['kondisi_barang']);
        $KODE_BARANG = $this->mysql->escape($post['kode']);
        $POS_TARIF = $this->mysql->escape($post['nomor_hs']);
        $KODE_NEGARA_ASAL = $this->mysql->escape($post['negara_asal']);
        $URAIAN = $this->mysql->escape($post['uraian_barang']);
        $TIPE = $this->mysql->escape($post['tipe']);
        $UKURAN = $this->mysql->escape($post['ukuran']);
        $SPESIFIKASI_LAIN = $this->mysql->escape($post['spf_lain']);
        $MERK = $this->mysql->escape($post['merk']);
        $JUMLAH_SATUAN = $this->mysql->escape($post['jumlah_satuan']);
        $JUMLAH_KEMASAN = $this->mysql->escape($post['jumlah_kemasan']);
        $CIF = $this->mysql->escape($post['nilai_cif']);
        $KODE_SATUAN = $this->mysql->escape($post['jenis_satuan']);
        $KODE_KEMASAN = $this->mysql->escape($post['jenis_kemasan']);
        $CIF_RUPIAH = $this->mysql->escape($post['cif_rupiah']);
        $NETTO = $this->mysql->escape($post['netto']);
        $VOLUME = $this->mysql->escape($post['volume']);
        $HARGA_PENYERAHAN = $this->mysql->escape($post['harga_penyerahan']);
        $JUMLAH_BAHAN_BAKU = $this->mysql->escape($post['jumlah_bahan_baku']);

        $sql = $this->mysql->query("UPDATE tpb_barang SET KATEGORI_BARANG = $KATEGORI_BARANG, KONDISI_BARANG = $KONDISI_BARANG, KODE_BARANG = $KODE_BARANG, POS_TARIF = $POS_TARIF, KODE_NEGARA_ASAL = $KODE_NEGARA_ASAL, URAIAN = $URAIAN, MERK = $MERK, TIPE = $TIPE, UKURAN = $UKURAN, SPESIFIKASI_LAIN = $SPESIFIKASI_LAIN, JUMLAH_KEMASAN = $JUMLAH_KEMASAN, CIF = $CIF, JUMLAH_SATUAN = $JUMLAH_SATUAN, KODE_SATUAN = $KODE_SATUAN, KODE_KEMASAN = $KODE_KEMASAN, VOLUME = $VOLUME, HARGA_PENYERAHAN = $HARGA_PENYERAHAN, NETTO = $NETTO, CIF_RUPIAH = $CIF_RUPIAH, JUMLAH_BAHAN_BAKU = $JUMLAH_BAHAN_BAKU WHERE ID = $ID");

        if($sql)
            return true;
        return false;
    }

    public function update27($post){
        $ID = $this->mysql->escape($post['ID']);
        $KATEGORI_BARANG = $this->mysql->escape($post['kategori_barang']);
        $KONDISI_BARANG = $this->mysql->escape($post['kondisi_barang']);
        $KODE_BARANG = $this->mysql->escape($post['kode']);
        $POS_TARIF = $this->mysql->escape($post['nomor_hs']);
        $KODE_NEGARA_ASAL = $this->mysql->escape($post['negara_asal']);
        $URAIAN = $this->mysql->escape($post['uraian_barang']);
        $TIPE = $this->mysql->escape($post['tipe']);
        $UKURAN = $this->mysql->escape($post['ukuran']);
        $SPESIFIKASI_LAIN = $this->mysql->escape($post['spf_lain']);
        $MERK = $this->mysql->escape($post['merk']);
        $JUMLAH_SATUAN = $this->mysql->escape($post['jumlah_satuan']);
        $JUMLAH_KEMASAN = $this->mysql->escape($post['jumlah_kemasan']);
        $CIF = $this->mysql->escape($post['nilai_cif']);
        $KODE_SATUAN = $this->mysql->escape($post['jenis_satuan']);
        $KODE_KEMASAN = $this->mysql->escape($post['jenis_kemasan']);
        $CIF_RUPIAH = $this->mysql->escape($post['cif_rupiah']);
        $NETTO = $this->mysql->escape($post['netto']);
        $VOLUME = $this->mysql->escape($post['volume']);
        $HARGA_PENYERAHAN = $this->mysql->escape($post['harga_penyerahan']);
        $JUMLAH_BAHAN_BAKU = $this->mysql->escape($post['jumlah_bahan_baku']);

        $sql = $this->mysql->query("UPDATE tpb_barang SET KATEGORI_BARANG = $KATEGORI_BARANG, KONDISI_BARANG = $KONDISI_BARANG, KODE_BARANG = $KODE_BARANG, POS_TARIF = $POS_TARIF, KODE_NEGARA_ASAL = $KODE_NEGARA_ASAL, URAIAN = $URAIAN, MERK = $MERK, TIPE = $TIPE, UKURAN = $UKURAN, SPESIFIKASI_LAIN = $SPESIFIKASI_LAIN, JUMLAH_KEMASAN = $JUMLAH_KEMASAN, CIF = $CIF, JUMLAH_SATUAN = $JUMLAH_SATUAN, KODE_SATUAN = $KODE_SATUAN, KODE_KEMASAN = $KODE_KEMASAN, VOLUME = $VOLUME, HARGA_PENYERAHAN = $HARGA_PENYERAHAN, NETTO = $NETTO, CIF_RUPIAH = $CIF_RUPIAH, JUMLAH_BAHAN_BAKU = $JUMLAH_BAHAN_BAKU WHERE ID = $ID");

        if($sql)
            return true;
        return false;
    }


    public function update41($post){
        $ID = $this->mysql->escape($post['ID']);
        // $KATEGORI_BARANG = $this->mysql->escape($post['kategori_barang']);
        // $KONDISI_BARANG = $this->mysql->escape($post['kondisi_barang']);
        $KODE_BARANG = $this->mysql->escape($post['kode']);
        // $POS_TARIF = $this->mysql->escape($post['nomor_hs']);
        // $KODE_NEGARA_ASAL = $this->mysql->escape($post['negara_asal']);
        $URAIAN = $this->mysql->escape($post['uraian_barang']);
        $TIPE = $this->mysql->escape($post['tipe']);
        $UKURAN = $this->mysql->escape($post['ukuran']);
        $SPESIFIKASI_LAIN = $this->mysql->escape($post['spf_lain']);
        $MERK = $this->mysql->escape($post['merk']);
        $JUMLAH_SATUAN = $this->mysql->escape($post['jumlah_satuan']);
        // $JUMLAH_KEMASAN = $this->mysql->escape($post['jumlah_kemasan']);
        // $CIF = $this->mysql->escape($post['nilai_cif']);
        $KODE_SATUAN = $this->mysql->escape($post['jenis_satuan']);
        // $KODE_KEMASAN = $this->mysql->escape($post['jenis_kemasan']);
        // $CIF_RUPIAH = $this->mysql->escape($post['cif_rupiah']);
        $NETTO = $this->mysql->escape($post['netto']);
        $VOLUME = $this->mysql->escape($post['volume']);
        $HARGA_PENYERAHAN = $this->mysql->escape($post['harga_penyerahan']);
        // $JUMLAH_BAHAN_BAKU = $this->mysql->escape($post['jumlah_bahan_baku']);

        $sql = $this->mysql->query("UPDATE tpb_barang SET KODE_BARANG = $KODE_BARANG, URAIAN = $URAIAN, MERK = $MERK, TIPE = $TIPE, UKURAN = $UKURAN, SPESIFIKASI_LAIN = $SPESIFIKASI_LAIN, JUMLAH_SATUAN = $JUMLAH_SATUAN, KODE_SATUAN = $KODE_SATUAN, VOLUME = $VOLUME, HARGA_PENYERAHAN = $HARGA_PENYERAHAN, NETTO = $NETTO WHERE ID = $ID");

        if($sql)
            return true;
        return false;
    }


    }
?>
