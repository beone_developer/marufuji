<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Pm_model extends CI_Model{

	public function load_pm(){
		$sql = $this->db->query("SELECT * FROM public.beone_pm_header WHERE status = 1");
		return $sql->result_array();
	}

	public function simpan($post){
		$session_id = $this->session->userdata('user_id');
		$nomor_pm = $this->db->escape($post['nomor_pm']);
		$tanggal_pm = $this->db->escape($post['tanggal']);
		$customer = $this->db->escape($post['customer']);
		$qty_ = $this->db->escape($post['qty']);
		$keterangan = $this->db->escape($post['keterangan']);

		$tgl_bulan = substr($tanggal_pm, 1, 2);
		$tgl_hari = substr($tanggal_pm, 4, 2);
		$tgl_tahun = substr($tanggal_pm, 7, 4);
		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		$qty_ex = str_replace(".", "", $qty_);
		$qty = str_replace(",", ".", $qty_ex);

		$sql = $this->db->query("INSERT INTO public.beone_pm_header(
															pm_header_id, no_pm, tgl_pm, customer_id, qty, keterangan_artikel, status)
															VALUES (DEFAULT, $nomor_pm, '$tanggal', $customer, $qty, $keterangan, 1)");
		
		$header_id = $this->db->query("SELECT * FROM public.beone_pm_header ORDER BY pm_header_id DESC LIMIT 1");
		$hasil_header_id = $header_id->row_array();
		$hid = $hasil_header_id['pm_header_id'];

			foreach ($_POST['rows'] as $key => $count ){
					$item_id = $_POST['item_id_'.$count];
					$qty_ = $_POST['qty_'.$count];
					$satuan_id = $_POST['satuan_'.$count];

					$qty_ex = str_replace(".", "", $qty_);
					$qty = str_replace(",", ".", $qty_ex);
					

					$sql_detail = $this->db->query("INSERT INTO public.beone_pm_detail(item_id, qty, satuan_id,pm_header_id)	VALUES ($item_id, $qty, $satuan_id,$hid)");
				}
			if($sql && $sql_detail)
				return true;
			return false;
		}

		
	public function update($post, $pm_id){
		$session_id = $this->session->userdata('user_id');
		$nomor_pm = $this->db->escape($post['no_pm']);
		$tanggal_pm = $this->db->escape($post['tanggal']);
		$customer = $this->db->escape($post['customer']);
		$qty_ = $this->db->escape($post['qty']);
		$keterangan = $this->db->escape($post['keterangan']);

		$tgl_bulan = substr($tanggal_pm, 1, 2);
		$tgl_hari = substr($tanggal_pm, 4, 2);
		$tgl_tahun = substr($tanggal_pm, 7, 4);
		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		$qty_ex = str_replace(".", "", $qty_);
		$qty = str_replace(",", ".", $qty_ex);

			foreach ($_POST['rows'] as $key => $count ){
					$item_id = $_POST['item_id_'.$count];
					$qty_ = $_POST['qty_'.$count];
					$satuan_id = $_POST['satuan_id_'.$count];

					$qty_ex = str_replace(".", "", $qty_);
					$qty = str_replace(",", ".", $qty_ex);
					
					$sql_detail_del = $this->db->query("DELETE FROM public.beone_pm_detail WHERE pm_header_id =".intval($pm_id));

					$sql_detail = $this->db->query("INSERT INTO public.beone_pm_detail(item_id, qty, satuan_id,pm_header_id)	VALUES ($item_id, $qty, $satuan_id,$pm_id)");
				}
			if($sql && $sql_detail)
				return true;
			return false;
		}

		public function delete($pm_id){
			$sql = $this->db->query("DELETE FROM public.beone_pm_header WHERE pm_header_id = ".intval($pm_id));
			if($sql)
				return true;
			return false;
		}

		public function get_default_header($pm_id){
			$sql = $this->db->query("SELECT h.pm_header_id, h.no_pm, h.tgl_pm, h.keterangan_artikel, h.customer_id, h.qty, h.status,c.nama as ncustomer
																FROM public.beone_pm_header h INNER JOIN public.beone_custsup c ON h.customer_id = c.custsup_id WHERE h.status = 1 AND h.pm_header_id = ".intval($pm_id));
			if($sql->num_rows() > 0)
				return $sql->row_array();
			return false;
		}
	
		public function get_default_detail($pm_id){
			$sql = $this->db->query("SELECT d.pm_detail_id,d.item_id,d.qty,d.satuan_id,d.pm_header_id
																FROM public.beone_pm_header h INNER JOIN public.beone_pm_detail d ON h.pm_header_id = d.pm_header_id WHERE h.status = 1 AND d.pm_header_id = ".intval($pm_id));
			return $sql->result_array();
		}

}
?>
