<?php

class preview_bc261_model extends CI_Model {

//    var $tabel = "usr";
    //   private $another;
    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
        $this->mysql = $this->load->database('mysql', TRUE);
    }

    public function select($aju, $id) {

        $select = "select DISTINCT th.id, rs.URAIAN_STATUS, th.nomor_aju, th.nomor_daftar, 
        th.tanggal_daftar, th.KODE_KANTOR as kode_kantor_pabean, rkpbean.URAIAN_KANTOR as kantor_pabean,
        th.id_pengusaha, th.NAMA_PENGUSAHA, th.ALAMAT_PENGUSAHA,th.nomor_ijin_tpb, th.api_pengusaha,th.id_penerima_barang, 
        rkiusaha.uraian_kode_id, th.id_pengusaha, th.NAMA_PENGUSAHA, th.ALAMAT_PENGUSAHA, 
 th.ID_PENERIMA_BARANG, th.NAMA_PENERIMA_BARANG, th.ALAMAT_PENERIMA_BARANG,th.bruto, th.NETTO,
th.nama_penerima_barang, th.alamat_penerima_barang, rca.URAIAN_CARA_ANGKUT,
 th.JUMLAH_BARANG, th.KODE_VALUTA, th.NDPBM, th.CIF, th.CIF_RUPIAH, th.tanggal_ijin_tpb as tanggal_ijin, 
 th.KOTA_TTD, th.TANGGAL_TTD, th.NAMA_TTD, th.JABATAN_TTD
from tpb_header th
left join referensi_status rs on th.KODE_STATUS = rs.KODE_STATUS  and th.KODE_DOKUMEN_PABEAN = rs.KODE_DOKUMEN
left join referensi_kantor_pabean rkpbean on th.KODE_KANTOR = rkpbean.KODE_KANTOR
left join referensi_kode_id rkiusaha on th.kode_id_pengusaha = rkiusaha.kode_id
left join referensi_cara_angkut rca on th.KODE_CARA_ANGKUT = rca.ID
 where th.nomor_aju = '" . $aju . "' and th.kode_dokumen_pabean = 261 and th.id = '" . $id . "'";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_tujuan($aju, $id) {
        $select = " select distinct th.NOMOR_AJU,rtp.id,  rtp.URAIAN_TUJUAN_PENGIRIMAN from tpb_header th
left join referensi_tujuan_pengiriman rtp on th.KODE_TUJUAN_PENGIRIMAN = rtp.KODE_TUJUAN_PENGIRIMAN
 where th.nomor_aju = '" . $aju . "' and th.kode_dokumen_pabean = 261 and th.id = '" . $id . "'";
        $data = $this->mysql->query($select);
        return $data->result();
    }
     public function update_modal_kemasan($id_kemasan, $jumlah_kemasan) {
        $select = "update tpb_kemasan set jumlah_kemasan = '" . $jumlah_kemasan . "' where id = '" . $id_kemasan . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;
    }
     public function get_another_id($ID) {
        $select = "select td.id as id_dokumen, th.NOMOR_AJU, td.NOMOR_DOKUMEN, td.TANGGAL_DOKUMEN as TANGGAL_DOKUMEN,rd.URAIAN_DOKUMEN from tpb_header th
		left join tpb_dokumen td on th.id = td.ID_HEADER
		left join referensi_dokumen rd on td.KODE_JENIS_DOKUMEN = rd.KODE_DOKUMEN
		where th.ID = '" . $ID . "' and rd.kode_dokumen not in (380) and th.kode_dokumen_pabean = 23";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_packing($aju, $id) {
        $select = "  select nomor_dokumen, tanggal_dokumen from tpb_dokumen 
 left join referensi_dokumen on kode_jenis_dokumen = kode_dokumen 
 where id_header ='" . $id . "' and kode_dokumen = 217";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_another($aju, $id) {
        $select = " select tpb_dokumen.id, uraian_dokumen, kode_jenis_dokumen,nomor_dokumen, tanggal_dokumen 
            from tpb_dokumen 
 left join referensi_dokumen on kode_jenis_dokumen = kode_dokumen 
 where id_header ='" . $id . "' and kode_dokumen <> 217";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_jaminan($aju) {
        $select = " select tj.id as id_jaminan, th.NOMOR_AJU,rjj.URAIAN_JENIS_JAMINAN,
tj.NOMOR_JAMINAN, tj.TANGGAL_JAMINAN, tj.NILAI_JAMINAN, tj.TANGGAL_JATUH_TEMPO, tj.PENJAMIN, tj.NOMOR_BPJ from tpb_header th
                left join tpb_jaminan tj on th.id = tj.ID_HEADER 
		left join referensi_jenis_jaminan rjj on tj.KODE_JENIS_JAMINAN = rjj.KODE_JENIS_JAMINAN
                where NOMOR_AJU = '" . $aju . "' and th.kode_dokumen_pabean = 261";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_kontainer($aju) {
        $select = "select tk.id as id_kontainer, th.NOMOR_AJU, tk.NOMOR_KONTAINER, ruk.URAIAN_UKURAN_KONTAINER, rtk.URAIAN_TIPE_KONTAINER from tpb_header th
                    left join tpb_kontainer tk on th.id = tk.ID_HEADER
                    left join referensi_ukuran_kontainer ruk on tk.KODE_UKURAN_KONTAINER = ruk.KODE_UKURAN_KONTAINER
                    left join referensi_tipe_kontainer rtk on tk.KODE_TIPE_KONTAINER = rtk.KODE_TIPE_KONTAINER
                    where NOMOR_AJU = '" . $aju . "' and th.kode_dokumen_pabean = 261";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function get_kemasan($aju) {
        $select = "select tk.ID as id_kemasan,th.NOMOR_AJU,tk.JUMLAH_KEMASAN, tk.MERK_KEMASAN,
                tk.KODE_JENIS_KEMASAN, rk.URAIAN_KEMASAN from tpb_header th
                left join tpb_kemasan tk on th.id = tk.ID_HEADER 
                left join referensi_kemasan rk on tk.KODE_JENIS_KEMASAN = rk.KODE_KEMASAN 
                where NOMOR_AJU = '" . $aju . "' and th.kode_dokumen_pabean = 261";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function update(
    $id, $nomor_pendaftaran, $tanggal_pendaftaran, $kppbc_pengawas, $tujuan_kirim, $nama_pengusaha, $alamat_pengusaha, $ptb_pengusaha, $tgl_ijinptb_pengusaha, $jenis_api_pengusaha, $nomor_api_pengusaha, $jenis_npwp_penerima, $nomor_npwp_penerima, $nama_penerima, $nama_penerima, $alamat_penerima, $nomor_dokumen, $tanggal_dokumen, $fasim_1, $fasim_2, $fasim_3, $kode_valuta, $ndpbm, $cif, $cif_rp, $sarana_angkut, $bruto, $netto, $jml_barang, $kota_ttd, $tgl_ttd, $pemberitahu, $jabatan) {

        $select = "update tpb_header set "
                . "nomor_daftar = '" . $nomor_pendaftaran . "',"
                . "tanggal_daftar =  '" . $tanggal_pendaftaran . "',"
                . "kode_kantor = '" . $kppbc_pengawas . "',"
                . "kode_kantor_tujuan = '" . $tujuan_kirim . "',"
                . "nama_pengusaha = '" . $nama_pengusaha . "',"
                . "alamat_pengusaha = '" . $alamat_pengusaha . "',"
                . "nomor_ijin_tpb = '" . $ptb_pengusaha . "',"
                . "tanggal_ijin_tpb = '" . $tgl_ijinptb_pengusaha . "',"
                . "kode_jenis_api_pengusaha = '" . $jenis_api_pengusaha . "',"
                . "api_pengusaha = '" . $nomor_api_pengusaha . "',"
                . "kode_id_penerima_barang = '" . $jenis_npwp_penerima . "',"
                . "id_penerima_barang = '" . $nomor_npwp_penerima . "',"
                . "nama_penerima_barang = '" . $nama_penerima . "',"
                . "alamat_penerima_barang = '" . $alamat_penerima . "',"
                . "kode_valuta = '" . $kode_valuta . "',"
                . "ndpbm = '" . $ndpbm . "',"
                . "cif = '" . $cif . "',"
                . "cif_rupiah = '" . $cif_rp . "',"
                . "nama_pengangkut = '" . $sarana_angkut . "',"
                . "bruto = '" . $bruto . "',"
                . "netto = '" . $netto . "',"
                . "jumlah_barang = '" . $jml_barang . "',"
                . "kota_ttd = '" . $kota_ttd . "',"
                . "tanggal_ttd = '" . $tgl_ttd . "',"
                . "nama_ttd = '" . $pemberitahu . "',"
                . "jabatan_ttd = '" . $jabatan . "'"
                . "where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;
    }

    public function get_respon($id) {
        $select = "select tr.KODE_RESPON, tr.NOMOR_RESPON, tr.TANGGAL_RESPON, tr.ID, tr.ID_HEADER, tr.WAKTU_RESPON from tpb_header th "
                . "left join tpb_respon tr on th.id = tr.id_header "
                . "where th.id = '" . $id . "'";
        $data = $this->mysql->query($select);
        return $data->result();
    }

    public function delete_respon($id) {
        $select = "delete from tpb_respon where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;
    }

    public function get_default_barang($ID) {
        $sql = $this->mysql->query("select * from tpb_barang where id_header = " . intval($ID) . " order by ID asc limit 1");
        if ($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifcukai($ID) {
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = " . intval($ID) . " order by ID asc limit 1) and JENIS_TARIF = 'CUKAI'");
        if ($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifBM($ID) {
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = " . intval($ID) . " order by ID asc limit 1) and JENIS_TARIF = 'BM'");
        if ($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPNBM($ID) {
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = " . intval($ID) . " order by ID asc limit 1) and JENIS_TARIF = 'PPNBM'");
        if ($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPN($ID) {
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = " . intval($ID) . " order by ID asc limit 1) and JENIS_TARIF = 'PPN'");
        if ($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPH($ID) {
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = " . intval($ID) . " order by ID asc limit 1) and JENIS_TARIF = 'PPH'");
        if ($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function delete_dokumen($id) {
        $select = "delete from tpb_dokumen where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;
    }

    public function delete_kemasan($id) {
        $select = "delete from tpb_kemasan where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;
    }

    public function delete_kontainer($id) {
        $select = "delete from tpb_kontainer where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;
    }
    public function delete_jaminan($id) {
        $select = "delete from tpb_jaminan where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;
    }
    public function get_default_modal_dokumen($ID){
        $sql = $this->mysql->query("SELECT td.*, rd.KODE_DOKUMEN, rd.URAIAN_DOKUMEN FROM tpb_dokumen td
            LEFT JOIN referensi_dokumen rd on rd.kode_dokumen = td.KODE_JENIS_DOKUMEN
            WHERE td.id = ".intval($ID));

        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;

    }
    public function get_default_modal_kemasan($ID){
        $sql = $this->mysql->query("SELECT tk.*, tk.ID,  rk.URAIAN_KEMASAN
            FROM tpb_kemasan tk
            left join referensi_kemasan rk on tk.KODE_JENIS_KEMASAN = rk.KODE_KEMASAN
            WHERE tk.id = ".intval($ID));

        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;

    }
    public function update_modal_dokumen($id, $jenis_dokumen, $nomor_dokumen, $tgl_dokumen){
       $select = "update tpb_dokumen "
               . "set kode_jenis_dokumen = (select KODE_DOKUMEN from referensi_dokumen where uraian_dokumen = '" . $jenis_dokumen . "'),"
               . "nomor_dokumen =  '" . $nomor_dokumen . "', tanggal_dokumen =  '" . $tgl_dokumen . "'"
               . "where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;

    }
    public function update_popup_kemasan($id, $jml_kemasan, $kode_kemasan,$merk_kemasan, $uraian_kemasan){
       $select = "update tpb_kemasan set kode_jenis_kemasan = (select KODE_KEMASAN from referensi_kemasan where uraian_kemasan = '" . $uraian_kemasan . "'), "
               . "jumlah_kemasan =  '" . $jml_kemasan . "', "
               . "merk_kemasan =  '" . $merk_kemasan . "' "
               . "where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;

    }
public function update_popup_kontainer($id, $nomor_kontainer, $ukuran_kontainer,$tipe_kontainer){
       $select = "update tpb_kontainer set kode_ukuran_kontainer = (select kode_ukuran_kontainer from referensi_ukuran_kontainer where uraian_ukuran_kontainer = '" . $ukuran_kontainer . "'), "
               . "nomor_kontainer =  '" . $nomor_kontainer . "', "
               . "kode_tipe_kontainer =  (select kode_tipe_kontainer from referensi_tipe_kontainer where uraian_tipe_kontainer = '" . $tipe_kontainer . "') "
               . "where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;

    }
    public function get_default_modal_kontainer($ID){
        $sql = $this->mysql->query("SELECT td.*, td.ID, rd.URAIAN_UKURAN_KONTAINER, rt.URAIAN_TIPE_KONTAINER FROM tpb_kontainer td
            LEFT JOIN referensi_ukuran_kontainer rd on rd.kode_ukuran_kontainer = td.kode_ukuran_kontainer
            left join referensi_tipe_kontainer rt on td.kode_tipe_kontainer = rt.kode_tipe_kontainer
            WHERE td.id = ".intval($ID));

        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;

    }
    public function get_default_modal_jaminan($ID){
        $sql = $this->mysql->query("SELECT tj.*, tj.ID, rj.URAIAN_JENIS_JAMINAN
            FROM tpb_jaminan tj
            LEFT JOIN referensi_jenis_jaminan rj on tj.kode_jenis_jaminan = rj.kode_jenis_jaminan
            WHERE tj.id = ".intval($ID));

        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;

    }
    public function update_popup_jaminan($id, $jenis_jaminan, $nomor_jaminan, $tgl_jaminan,$nilai_jaminan,$jatuh_tempo,$penjamin,$nomor_bpj){
       $select = "update tpb_jaminan set kode_jenis_jaminan = (select kode_jenis_jaminan from referensi_jenis_jaminan where uraian_jenis_jaminan = '" . $jenis_jaminan . "'), "
               . "nomor_jaminan =  '" . $nomor_jaminan . "', "
               . "tanggal_jaminan =  '" . $tgl_jaminan . "', "
               . "nilai_jaminan =  '" . $nilai_jaminan . "', "
               . "tanggal_jatuh_tempo =  '" . $jatuh_tempo . "', "
               . "penjamin =  '" . $penjamin . "', "
               . "nomor_bpj =  '" . $nomor_bpj . "' "
               . "where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;

    }

}

?>
