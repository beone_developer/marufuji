<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Custsup_model extends CI_Model{

	public function simpan($post){
		$nama = $this->db->escape($post['nama']);
		$tipe = $post['tipe_custsup'];
		$alamat = $this->db->escape($post['alamat']);
		$negara = $this->db->escape($post['negara']);
		$saldo_idr_ = $this->db->escape($post['saldo_idr']);
		$saldo_valas_ = $this->db->escape($post['saldo_valas']);
		$update_date = date('Y-m-d');

		$saldo_idr_ex = str_replace(".", "", $saldo_idr_);
		$saldo_idr = str_replace(",", ".", $saldo_idr_ex);

		$saldo_valas_ex = str_replace(".", "", $saldo_valas_);
		$saldo_valas = str_replace(",", ".", $saldo_valas_ex);

		if ($tipe == 1){
				$sql = $this->db->query("INSERT INTO public.beone_custsup(custsup_id, nama, alamat, tipe_custsup, negara, saldo_hutang_idr, saldo_hutang_valas, saldo_piutang_idr, saldo_piutang_valas, flag)	VALUES (DEFAULT, $nama, $alamat, $tipe, $negara, $saldo_idr, $saldo_valas, 0, 0, 1)");
				helper_log($tipe = "add", $str = "Tambah Supplier ".$post['nama']);
		}else{
				$sql = $this->db->query("INSERT INTO public.beone_custsup(custsup_id, nama, alamat, tipe_custsup, negara, saldo_hutang_idr, saldo_hutang_valas, saldo_piutang_idr, saldo_piutang_valas, flag)	VALUES (DEFAULT, $nama, $alamat, $tipe, $negara, 0, 0, $saldo_idr, $saldo_valas, 1)");
				helper_log($tipe = "add", $str = "Tambah Customer ".$post['nama']);
		}

		if($sql)
			return true;
		return false;
	}


	public function update($post, $custsup_id){
		$nama = $this->db->escape($post['nama']);
		$tipe = $post['tipe_custsup'];
		$alamat = $this->db->escape($post['alamat']);
		$negara = $this->db->escape($post['negara']);
		$saldo_idr_ = $this->db->escape($post['saldo_idr']);
		$saldo_valas_ = $this->db->escape($post['saldo_valas']);
		$update_date = date('Y-m-d');

		$saldo_idr_ex = str_replace(".", "", $saldo_idr_);
		$saldo_idr = str_replace(",", ".", $saldo_idr_ex);

		$saldo_valas_ex = str_replace(".", "", $saldo_valas_);
		$saldo_valas = str_replace(",", ".", $saldo_valas_ex);


		if ($tipe == 1){
				$sql = $this->db->query("UPDATE public.beone_custsup SET nama=$nama, alamat=$alamat, negara=$negara, saldo_hutang_idr=$saldo_idr, saldo_hutang_valas=$saldo_valas WHERE custsup_id = ".intval($custsup_id));
				helper_log($tipe = "edit", $str = "Ubah Supplier ".$post['nama']);
		}else{
				$sql = $this->db->query("UPDATE public.beone_custsup SET nama=$nama, alamat=$alamat, negara=$negara, saldo_piutang_idr=$saldo_idr, saldo_piutang_valas=$saldo_valas WHERE custsup_id = ".intval($custsup_id));
				helper_log($tipe = "edit", $str = "Ubah Customer ".$post['nama']);
		}

		if($sql)
			return true;
		return false;
	}

	public function delete($custsup_id){
		$sql_custsup = $this->db->query("SELECT nama, tipe_custsup FROM public.beone_custsup WHERE custsup_id = ".intval($custsup_id));
		$hasil_custsup = $sql_custsup->row_array();

		if ($hasil_custsup['tipe_custsup'] == 1){
			helper_log($tipe = "delete", $str = "Hapus Supplier ".$hasil_custsup['nama']);
		}else{
			helper_log($tipe = "delete", $str = "Hapus Customer ".$hasil_custsup['nama']);
		}

		$sql = $this->db->query("UPDATE public.beone_custsup SET flag=0 WHERE custsup_id =".intval($custsup_id));
	}

	public function get_default($custsup_id){
		$sql = $this->db->query("SELECT * FROM public.beone_custsup WHERE custsup_id = ".intval($custsup_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function load_custsup(){
		$sql = $this->db->query("SELECT * FROM public.beone_custsup");
		return $sql->result_array();
	}

	public function load_supplier(){
		$sql = $this->db->query("SELECT * FROM public.beone_custsup WHERE tipe_custsup = 1 AND flag = 1 ORDER BY nama ASC");
		return $sql->result_array();
	}

	public function load_receiver(){
		$sql = $this->db->query("SELECT * FROM public.beone_custsup WHERE tipe_custsup = 2 AND flag = 1 ORDER BY nama ASC");
		return $sql->result_array();
	}

	public function load_country(){
		$sql = $this->db->query("SELECT * FROM public.beone_country WHERE flag = 1");
		return $sql->result_array();
	}

	/****************************************************************************/
	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename){
		$this->load->library('upload'); // Load librari upload

		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']	= '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;

		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		}else{
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}

	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	/*public function insert_multiple($data){
		$this->db->insert_batch('beone_coa', $data);
	}*/

	public function insert_multiple($data){
		//$this->db->insert_batch('beone_coa', $data);
		foreach($data as $row){
			$sql = $this->db->query("INSERT INTO public.beone_custsup VALUES ($row[custsup_id], '$row[nama]', '$row[alamat]', $row[tipe_custsup], $row[negara], $row[saldo_hutang_idr], $row[saldo_hutang_valas], $row[saldo_piutang_idr], $row[saldo_piutang_valas], $row[flag], $row[pelunasan_hutang_idr], $row[pelunasan_hutang_valas], $row[pelunasan_piutang_idr], $row[pelunasan_piutang_valas], $row[status_lunas_piutang], $row[status_lunas_hutang])");
		}}
	/****************************************************************************/


}
?>
