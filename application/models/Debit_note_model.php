<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Debit_note_model extends CI_Model{

	public function simpan_debit_note($post){
		$session_id = $this->session->userdata('user_id');
		$nomor_transaksi = $this->db->escape($post['nomor_kd_note']);
		$tanggal_awal = $this->db->escape($post['tanggal_dk_note']);
		$coa_id = $this->db->escape($post['coa_id']);
		$customer = $this->db->escape($post['customer']);
		$keterangan = $this->db->escape($post['keterangan_debit_note']);
		$saldo_idr_ = $this->db->escape($post['jumlah_idr']);
		$saldo_valas_ = $this->db->escape($post['jumlah_valas']);
		$posisi = $this->db->escape($post['posisi']);//0=minus, 1=plus

		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		$update_date = date('Y-m-d');

		$saldo_idr_ex = str_replace(".", "", $saldo_idr_);
		$saldo_idr = str_replace(",", ".", $saldo_idr_ex);

		$saldo_valas_ex = str_replace(".", "", $saldo_valas_);
		$saldo_valas = str_replace(",", ".", $saldo_valas_ex);

		$coa_jurnal_piutang_usaha = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 6"); //coa jurnal piutang usaha
		$coa_piutang_usaha = $coa_jurnal_piutang_usaha->row_array();
		$cpu_id = $coa_piutang_usaha['coa_id'];
		$cpu_no = $coa_piutang_usaha['coa_no'];

		$coa_debit_note = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = $coa_id");
		$hasil_coa_debit_note = $coa_debit_note->row_array();
		$coa_no = $hasil_coa_debit_note['nomor'];

		if ($post['posisi'] == 1){//posisi plus (beli)
			$sql = $this->db->query("INSERT INTO public.beone_hutang_piutang(
															hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
															VALUES (DEFAULT, $customer, '$tanggal', $nomor_transaksi, $keterangan, $saldo_valas, $saldo_idr, 0, 0, 0, $session_id, '$update_date', 1, 1)");

			$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																			 gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																			 VALUES (DEFAULT, '$tanggal', $coa_id, '$coa_no', $cpu_id, '$cpu_no', $keterangan, 0, $saldo_idr, $nomor_transaksi, $nomor_transaksi, $session_id, '$update_date')");

			$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																			gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																			VALUES (DEFAULT, '$tanggal', $cpu_id, '$cpu_no', $coa_id, '$coa_no', $keterangan, $saldo_idr, 0, $nomor_transaksi, $nomor_transaksi, $session_id, '$update_date')");

		}else{//posisi minus (pelunasan)
			$sql = $this->db->query("INSERT INTO public.beone_hutang_piutang(
															hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
															VALUES (DEFAULT, $customer, '$tanggal', $nomor_transaksi, $keterangan, 0, 0, $saldo_valas, $saldo_idr, 0, $session_id, '$update_date', 1, 1)");

			$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																			 gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																			 VALUES (DEFAULT, '$tanggal', $cpu_id, '$cpu_no', $coa_id, '$coa_no', $keterangan, $saldo_idr, 0, $nomor_transaksi, $nomor_transaksi, $session_id, '$update_date')");

			$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																			gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																			VALUES (DEFAULT, '$tanggal', $coa_id, '$coa_no', $cpu_id, '$cpu_no', $keterangan, 0, $saldo_idr, $nomor_transaksi, $nomor_transaksi, $session_id, '$update_date')");
		}

		if($sql)
			return true;
		return false;
	}

	public function load_debit_note(){
		$sql = $this->db->query("SELECT * FROM public.beone_hutang_piutang WHERE nomor LIKE 'DBN%' ORDER BY nomor ASC");
		return $sql->result_array();
	}

	public function get_default($hp_id){
		$sql = $this->db->query("SELECT * FROM public.beone_hutang_piutang WHERE hutang_piutang_id = ".intval($hp_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}


	public function update($post, $kdn_no){
		$session_id = $this->session->userdata('user_id');
		$id = $this->db->escape($post['hp_id']);
		$coa_id = $this->db->escape($post['coa_id']);
		$customer = $this->db->escape($post['customer']);
		$keterangan = $this->db->escape($post['keterangan_kredit_note']);
		$saldo_idr_ = $this->db->escape($post['jumlah_idr']);
		$saldo_valas_ = $this->db->escape($post['jumlah_valas']);
		$posisi = $this->db->escape($post['posisi']);//0=minus, 1=plus

		$update_date = date('Y-m-d');

		$saldo_idr_ex = str_replace(".", "", $saldo_idr_);
		$saldo_idr = str_replace(",", ".", $saldo_idr_ex);

		$saldo_valas_ex = str_replace(".", "", $saldo_valas_);
		$saldo_valas = str_replace(",", ".", $saldo_valas_ex);


			$cek_customer = $this->db->query("SELECT * FROM public.beone_custsup WHERE custsup_id = $customer");
			$hasil_cek_customer = $cek_customer->row_array();

			$coa_jurnal_piutang_usaha = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 6"); //coa jurnal piutang usaha
			$coa_piutang_usaha = $coa_jurnal_piutang_usaha->row_array();
			$cpu_id = $coa_piutang_usaha['coa_id'];
			$cpu_no = $coa_piutang_usaha['coa_no'];

			$coa_debit_note = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = $coa_id");
			$hasil_coa_debit_note = $coa_debit_note->row_array();
			$coa_no = $hasil_coa_debit_note['nomor'];

		$sql_dk = $this->db->query("SELECT * FROM public.beone_gl WHERE gl_number = '$kdn_no'");


		if ($post['posisi'] == 1){//posisi plus (beli)
						$sql = $this->db->query("UPDATE public.beone_hutang_piutang
																			SET  custsup_id=$customer, keterangan=$keterangan, valas_trans=$saldo_valas, idr_trans=$saldo_idr, valas_pelunasan=0, idr_pelunasan=0, update_by=$session_id, update_date='$update_date'
																			WHERE hutang_piutang_id = $id");

						foreach($sql_dk->result_array() as $row){
									$id_gl = $row['gl_id'];
									if ($row['kredit'] == 0){ //posisi debit
													$sql_ledger_debet = $this->db->query("UPDATE public.beone_gl
																																SET coa_id=$coa_id, coa_no='$coa_no', coa_id_lawan=$cpu_id, coa_no_lawan='$cpu_no', keterangan=$keterangan, debet=$saldo_idr, update_by=$session_id, update_date='$update_date'
																																WHERE gl_id = $id_gl");

									}else{
													$sql_ledger_kredit = $this->db->query("UPDATE public.beone_gl
																																SET coa_id=$cpu_id, coa_no='$cpu_no', coa_id_lawan=$coa_id, coa_no_lawan='$coa_no', keterangan=$keterangan, kredit=$saldo_idr, update_by=$session_id, update_date='$update_date'
																																WHERE gl_id = $id_gl");
									}
						}

		}else{//posisi minus (pelunasan)
						$sql = $this->db->query("UPDATE public.beone_hutang_piutang
																			SET  custsup_id=$customer, keterangan=$keterangan, valas_trans=0, idr_trans=0, valas_pelunasan=$saldo_valas, idr_pelunasan=$saldo_idr, update_by=$session_id, update_date='$update_date'
																			WHERE hutang_piutang_id = $id");

							foreach($sql_dk->result_array() as $row){
										$id_gl = $row['gl_id'];
										if ($row['kredit'] == 0){ //posisi debit
														$sql_ledger_debet = $this->db->query("UPDATE public.beone_gl
																																	SET coa_id=$cpu_id, coa_no='$cpu_no', coa_id_lawan=$coa_id, coa_no_lawan='$coa_no', keterangan=$keterangan, debet=$saldo_idr, update_by=$session_id, update_date='$update_date'
																																	WHERE gl_id = $id_gl");

										}else{
														$sql_ledger_kredit = $this->db->query("UPDATE public.beone_gl
																																	SET coa_id=$coa_id, coa_no='$coa_no', coa_id_lawan=$cpu_id, coa_no_lawan='$cpu_no', keterangan=$keterangan, kredit=$saldo_idr, update_by=$session_id, update_date='$update_date'
																																	WHERE gl_id = $id_gl");
										}
							}
		}


		if($sql AND $sql_ledger_debet AND $sql_ledger_kredit)
			return true;
		return false;
	}


	public function delete($hp_id, $dbn_no){
		$sql_hp = $this->db->query("DELETE FROM public.beone_hutang_piutang WHERE hutang_piutang_id = $hp_id");
		$sql_gl = $this->db->query("DELETE FROM public.beone_gl WHERE gl_number = '$dbn_no'");

		if($sql_hp AND $sql_gl)
			return true;
		return false;
	}


}
?>
