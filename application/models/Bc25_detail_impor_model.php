<?php

class bc25_detail_impor_model extends CI_Model {

//    var $tabel = "usr";
 //   private $another;
    function __construct(){
        parent::__construct();
        $this->db = $this ->load -> database('default', TRUE);
        $this->mysql = $this ->load -> database('mysql', TRUE);
    }

    public function get_default_bahan($ID_HEADER, $ID){
        $sql = $this->mysql->query("select * from tpb_bahan_baku where id_barang = ".intval($ID)." and id_header = ".intval($ID_HEADER)." order by ID asc limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    public function get_default_bahanprev($ID_HEADER, $ID, $ID_BARANG){
        $sql = $this->mysql->query("select * from tpb_bahan_baku 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_bahan_baku where id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER).")) and id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER)." order by ID desc limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_bahannext($ID_HEADER, $ID, $ID_BARANG){
        $sql = $this->mysql->query("select * from tpb_bahan_baku 
            where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_bahan_baku where id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER).")) and id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER)." order by ID asc limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_bahan_tarifcukai($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tbt.* from tpb_bahan_baku_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_bahan_baku = (select ID from tpb_bahan_baku where id_header = ".intval($ID_HEADER)." and id_barang = ".intval($ID)." order by id asc limit 1)
                and JENIS_TARIF = 'CUKAI'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    
        public function get_default_bahan_tarifBM($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tbt.* from tpb_bahan_baku_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_bahan_baku = (select ID from tpb_bahan_baku where id_header = ".intval($ID_HEADER)." and id_barang = ".intval($ID)." order by id asc limit 1)
                and JENIS_TARIF = 'BM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_bahan_tarifPPN($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tbt.* from tpb_bahan_baku_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_bahan_baku = (select ID from tpb_bahan_baku where id_header = ".intval($ID_HEADER)." and id_barang = ".intval($ID)." order by id asc limit 1)
                and JENIS_TARIF = 'PPN'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_bahan_tarifPPH($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tbt.* from tpb_bahan_baku_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_bahan_baku = (select ID from tpb_bahan_baku where id_header = ".intval($ID_HEADER)." and id_barang = ".intval($ID)." order by id asc limit 1)
                and JENIS_TARIF = 'PPH'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_bahan_tarifPPNBM($ID_HEADER, $ID){
        $sql = $this->mysql->query("select tbt.* from tpb_bahan_baku_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_bahan_baku = (select ID from tpb_bahan_baku where id_header = ".intval($ID_HEADER)." and id_barang = ".intval($ID)." order by id asc limit 1)
                and JENIS_TARIF = 'PPNBM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }





    public function get_default_bahan_tarifcukainext($ID_HEADER, $ID, $ID_BARANG){
        $sql = $this->mysql->query("select tbt.* from tpb_bahan_baku_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_bahan_baku = (select ID from tpb_bahan_baku where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_bahan_baku where id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER).")) and id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER)." order by ID asc limit 1)
                and JENIS_TARIF = 'CUKAI'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    
        public function get_default_bahan_tarifBMnext($ID_HEADER, $ID, $ID_BARANG){
        $sql = $this->mysql->query("select tbt.* from tpb_bahan_baku_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_bahan_baku = (select ID from tpb_bahan_baku where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_bahan_baku where id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER).")) and id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER)." order by ID asc limit 1)
                and JENIS_TARIF = 'BM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_bahan_tarifPPNnext($ID_HEADER, $ID, $ID_BARANG){
        $sql = $this->mysql->query("select tbt.* from tpb_bahan_baku_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_bahan_baku = (select ID from tpb_bahan_baku where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_bahan_baku where id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER).")) and id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER)." order by ID asc limit 1)
                and JENIS_TARIF = 'PPN'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_bahan_tarifPPHnext($ID_HEADER, $ID, $ID_BARANG){
        $sql = $this->mysql->query("select tbt.* from tpb_bahan_baku_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_bahan_baku = (select ID from tpb_bahan_baku where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_bahan_baku where id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER).")) and id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER)." order by ID asc limit 1)
                and JENIS_TARIF = 'PPH'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_bahan_tarifPPNBMnext($ID_HEADER, $ID, $ID_BARANG){
        $sql = $this->mysql->query("select tbt.* from tpb_bahan_baku_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_bahan_baku = (select ID from tpb_bahan_baku where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_bahan_baku where id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER).")) and id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER)." order by ID asc limit 1)
                and JENIS_TARIF = 'PPNBM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }



    public function get_default_bahan_tarifcukaiprev($ID_HEADER, $ID, $ID_BARANG){
        $sql = $this->mysql->query("select tbt.* from tpb_bahan_baku_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_bahan_baku = (select ID from tpb_bahan_baku 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_bahan_baku where id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER).")) and id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER)." order by ID desc limit 1)
                and JENIS_TARIF = 'CUKAI'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    
        public function get_default_bahan_tarifBMprev($ID_HEADER, $ID, $ID_BARANG){
        $sql = $this->mysql->query("select tbt.* from tpb_bahan_baku_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_bahan_baku = (select ID from tpb_bahan_baku 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_bahan_baku where id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER).")) and id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER)." order by ID desc limit 1)
                and JENIS_TARIF = 'BM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_bahan_tarifPPNprev($ID_HEADER, $ID, $ID_BARANG){
        $sql = $this->mysql->query("select tbt.* from tpb_bahan_baku_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_bahan_baku = (select ID from tpb_bahan_baku 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_bahan_baku where id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER).")) and id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER)." order by ID desc limit 1)
                and JENIS_TARIF = 'PPN'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_bahan_tarifPPHprev($ID_HEADER, $ID, $ID_BARANG){
        $sql = $this->mysql->query("select tbt.* from tpb_bahan_baku_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_bahan_baku = (select ID from tpb_bahan_baku 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_bahan_baku where id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER).")) and id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER)." order by ID desc limit 1)
                and JENIS_TARIF = 'PPH'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_bahan_tarifPPNBMprev($ID_HEADER, $ID, $ID_BARANG){
        $sql = $this->mysql->query("select tbt.* from tpb_bahan_baku_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_bahan_baku = (select ID from tpb_bahan_baku 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_bahan_baku where id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER).")) and id_barang = ".intval($ID_BARANG)." and id_header = ".intval($ID_HEADER)." order by ID desc limit 1)
                and JENIS_TARIF = 'PPNBM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }


    public function update25($post){
        $ID = $this->mysql->escape($post['ID']);
        $KODE_JENIS_DOK_ASAL = $this->mysql->escape($post['dok_asal']);
        $KODE_KANTOR = $this->mysql->escape($post['kppbc_dok']);
        $NOMOR_DAFTAR_DOK_ASAL = $this->mysql->escape($post['nomor_daftar']);
        $TANGGAL_DAFTAR_DOK_ASAL = $this->mysql->escape($post['tgl_daftar']);
        $NOMOR_AJU_DOK_ASAL = $this->mysql->escape($post['nomor_aju']);
        $SERI_BARANG_DOK_ASAL = $this->mysql->escape($post['seri_barang']);
        $KODE_BARANG = $this->mysql->escape($post['kode']);
        $POS_TARIF = $this->mysql->escape($post['nomor_hs']);
        $URAIAN = $this->mysql->escape($post['uraian_barang']);
        $MERK = $this->mysql->escape($post['merk']);
        $TIPE = $this->mysql->escape($post['tipe']);
        $UKURAN = $this->mysql->escape($post['ukuran']);
        $SPESIFIKASI_LAIN = $this->mysql->escape($post['spf_lain']);
        $CIF = $this->mysql->escape($post['cif_usd']);
        $NDPBM = $this->mysql->escape($post['ndpbm']);
        $CIF_RUPIAH = $this->mysql->escape($post['cif_rupiah']);
        $JUMLAH_SATUAN = $this->mysql->escape($post['jumlah_satuan']);
        $JENIS_SATUAN = $this->mysql->escape($post['satuan']);
        $NETTO = $this->mysql->escape($post['netto']);
        $HARGA_PENYERAHAN = $this->mysql->escape($post['harga_penyerahan']);
        
        
        $sql = $this->mysql->query("UPDATE tpb_bahan_baku SET KODE_JENIS_DOK_ASAL = $KODE_JENIS_DOK_ASAL, KODE_KANTOR = $KODE_KANTOR, NOMOR_DAFTAR_DOK_ASAL = $NOMOR_DAFTAR_DOK_ASAL, TANGGAL_DAFTAR_DOK_ASAL = $TANGGAL_DAFTAR_DOK_ASAL, NOMOR_AJU_DOK_ASAL = $NOMOR_AJU_DOK_ASAL, SERI_BARANG_DOK_ASAL = $SERI_BARANG_DOK_ASAL, KODE_BARANG = $KODE_BARANG, POS_TARIF = $POS_TARIF, URAIAN = $URAIAN, MERK = $MERK, TIPE = $TIPE, UKURAN = $UKURAN, SPESIFIKASI_LAIN = $SPESIFIKASI_LAIN, CIF = $CIF, JUMLAH_SATUAN = $JUMLAH_SATUAN, JENIS_SATUAN = $JENIS_SATUAN, HARGA_PENYERAHAN = $HARGA_PENYERAHAN, NETTO = $NETTO, CIF_RUPIAH = $CIF_RUPIAH, NDPBM = $NDPBM WHERE ID = $ID");

        if($sql)
            return true;
        return false;
    }


    public function update27($post){
        $ID = $this->mysql->escape($post['ID']);
        $KODE_JENIS_DOK_ASAL = $this->mysql->escape($post['dok_asal']);
        $KODE_KANTOR = $this->mysql->escape($post['kppbc_dok']);
        $NOMOR_DAFTAR_DOK_ASAL = $this->mysql->escape($post['nomor_daftar']);
        $TANGGAL_DAFTAR_DOK_ASAL = $this->mysql->escape($post['tgl_daftar']);
        $NOMOR_AJU_DOK_ASAL = $this->mysql->escape($post['nomor_aju']);
        $SERI_BARANG_DOK_ASAL = $this->mysql->escape($post['seri_barang']);
        $KODE_BARANG = $this->mysql->escape($post['kode']);
        $POS_TARIF = $this->mysql->escape($post['nomor_hs']);
        $URAIAN = $this->mysql->escape($post['uraian_barang']);
        $MERK = $this->mysql->escape($post['merk']);
        $TIPE = $this->mysql->escape($post['tipe']);
        $UKURAN = $this->mysql->escape($post['ukuran']);
        $SPESIFIKASI_LAIN = $this->mysql->escape($post['spf_lain']);
        $CIF = $this->mysql->escape($post['cif_usd']);
        $JUMLAH_SATUAN = $this->mysql->escape($post['jumlah_satuan']);
        $JENIS_SATUAN = $this->mysql->escape($post['satuan']);
        $HARGA_PENYERAHAN = $this->mysql->escape($post['harga_penyerahan']);
        
        
        $sql = $this->mysql->query("UPDATE tpb_bahan_baku SET KODE_JENIS_DOK_ASAL = $KODE_JENIS_DOK_ASAL, KODE_KANTOR = $KODE_KANTOR, NOMOR_DAFTAR_DOK_ASAL = $NOMOR_DAFTAR_DOK_ASAL, TANGGAL_DAFTAR_DOK_ASAL = $TANGGAL_DAFTAR_DOK_ASAL, NOMOR_AJU_DOK_ASAL = $NOMOR_AJU_DOK_ASAL, SERI_BARANG_DOK_ASAL = $SERI_BARANG_DOK_ASAL, KODE_BARANG = $KODE_BARANG, POS_TARIF = $POS_TARIF, URAIAN = $URAIAN, MERK = $MERK, TIPE = $TIPE, UKURAN = $UKURAN, SPESIFIKASI_LAIN = $SPESIFIKASI_LAIN, CIF = $CIF, JUMLAH_SATUAN = $JUMLAH_SATUAN, JENIS_SATUAN = $JENIS_SATUAN, HARGA_PENYERAHAN = $HARGA_PENYERAHAN WHERE ID = $ID");

        if($sql)
            return true;
        return false;
    }

    }

?>
