<h3>List Pembelian</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>
	<table class="table table-striped table-bordered table-hover" id="sample_1">
		<thead>
		<tr>
			<th>
				<center>Nomor PM</center>
			</th>
			<th>
				<center>Tanggal</center>
			</th>
			<th>
				<center>Customer</center>
			</th>
			<th>
				<center>Qty</center>
			</th>
			<th>
				<center>Keterangan</center>
			</th>
			<th>
				<center>Action</center>
			</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($list_pm as $row) { ?>
			<tr>
				<td><?php echo $row['no_pm']; ?></td>
				<td><?php echo $row['tgl_pm']; ?></td>
				<td><?php echo $row['customer_id']; ?></td>
				<td><?php echo $row['qty']; ?></td>
				<td><?php echo $row['keterangan_artikel']; ?></td>
				<td>
					<!-- <?php //if(helper_security("pm_edit") == 1){?> -->
						<a href='<?php echo base_url('Pm_controller/edit/'.$row['pm_header_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i> </a>
					<!-- <?php //}?> -->
					<a href="javascript:dialogHapus('<?php echo base_url('Pm_controller/delete_pm/' . $row['pm_header_id'] . ''); ?>')"
					   class='btn red'><i class="fa fa-trash-o"></i></a>
					<a href='<?php echo base_url('Pm_controller/pm_print/'.$row['pm_header_id'].'');?>' class='btn yellow'><i class="fa fa-print"></i> </a></center></td>
				</td>
			</tr>
			<?php
		}
		?>
		</tbody>
	</table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
