<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<?php $list_item = $this->db->query("SELECT * FROM public.beone_item");?>

<?php
/******************* GENERATE NOMOR BOM ******************************/
$tgl = date('m/d/Y');
$thn = substr($tgl,8,2);
$bln = substr($tgl,0,2);


$kode_awal = "BOM"."-".$thn."-".$bln."-";

$sql = $this->db->query("SELECT * FROM public.beone_transfer_stock WHERE transfer_no LIKE '$kode_awal%' ORDER BY transfer_no DESC LIMIT 1");
$hasil = $sql->row_array();

$urutan = substr($hasil['transfer_no'],10,4);
$no_lanjutan = $urutan+1;

$digit = strlen($no_lanjutan);
$jml_nol = 4-$digit;

$cetak_nol = "";

for ($i = 1; $i <= $jml_nol; $i++) {
    $cetak_nol = $cetak_nol."0";
}

$nomor_bom = $kode_awal.$cetak_nol.$no_lanjutan;

/*************************************************/
?>

<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Bill Of Material</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">




          <form role="form" method="get">
              <div class="form-body">

                <div class="row">
                  <div class="col-sm-4"><h3><b><?php echo $nomor_bom;?></b></h3></div>
                  <div class="col-sm-8">
                    <div class="form-group">
                          <label>Tanggal</label>
                          <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input class="form-control form-control-inline input-medium date-picker input-circle-right" size="16" type="text" name="tanggal" value="<?php echo date('m/d/Y');?>" readonly required/>
                            <span class="help-block"></span>
                          </div>
                    </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                            <label>Kegiatan Produksi</label>
                            <div class="input-group">
                               <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="kode_biaya" required>
                                 <option value=""></option>
                                 <option value=1>PRODUKSI WIP</option>
                                 <option value=2>PRODUKSI BARANG JADI</option>
                               </select>
                         </div>
                       </div>
                    </div>

                      <div class="col-sm-8">
                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" class="form-control" placeholder="Keterangan" id="keterangan" name="keterangan" required>
                            <input type="hidden" class="form-control" name="transfer_no" value='<?php echo $nomor_bom;?>'>
                          </div>
                      </div>
                    </div>

              </div>
              <div class="form-actions">
                  <button type="submit" class="btn blue" name="submit_transfer" id="submit_transfer">Lanjut</button>
              </div>
          </form>
  </div>
</div>
