<a href="<?php echo base_url('BC40_detail_barang_controller/prev/'.$default['ID_HEADER'].'/'.$default['ID'].'');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Prev</a>

<a href="<?php echo base_url('BC40_detail_barang_controller/next/'.$default['ID_HEADER'].'/'.$default['ID'].'');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Next</a>


<div class="portlet light bordered">
	<div class="portlet-title">
		
		<form method="POST">
			<input type="hidden" class="form-control" id="ID" 
						name="ID" value="<?=isset($default['ID'])? $default['ID'] : ""?>">

			<div class="form-body">
				<div class="row">
					<label for="status" class="col-sm-2 col-form-label">Status</label>
					<div class="col-sm-2">
						<input style="border: none; margin-top: -5px;" type="text" readonly class="form-control" id="status" name="status" value="<?=isset($default['URAIAN_STATUS'])? $default['URAIAN_STATUS'] : ""?>">
					</div>
				</div>
			</div>
			<br>
			<div class="form-body">
				<div class="row">
					<div class="col-sm-6">
						<label><b>DATA BARANG BC 4.0</b></label>
					</div>
				</div>
			</div>
			<br>
			<div class="form-body">
				<div class="row">
					<div class="col-sm-1">
						<label style="margin-top: 5px;">Detail Ke</label>
					</div>

					<div class="col-sm-1">
						<input type="text" readonly class="form-control" id="status" 
						name="status" value="">
					</div>

					<div class="col-sm-1">
						<label style="margin-top: 5px;">Dari</label>
					</div>

					<div class="col-sm-1">
						<input type="text" readonly class="form-control" id="status" 
						name="status" value="">
					</div>
				</div>
			</div>
			<br>
			<div class="form-body">
				<div class="row">
					<div class="col-sm-6">
						<label>Kode Barang</label>
						<input type="text" readonly class="form-control" id="kode_barang" 
						name="kode_barang" value="<?=isset($default['KODE_BARANG'])? $default['KODE_BARANG'] : ""?>">
					</div>
					<div class="col-sm-6">
						<label>Uraian Barang</label>
						<input type="text" readonly class="form-control" id="uraian_barang" 
						name="uraian_barang" value="<?=isset($default['URAIAN'])? $default['URAIAN'] : ""?>">
					</div>
				</div>
			</div>
			<br>
			<div class="form-body">
				<div class="row">
					<div class="col-sm-3">
						<label>Merk</label>
						<input type="text" class="form-control" id="merk" 
						name="merk" value="<?=isset($default['MERK'])? $default['MERK'] : ""?>">
					</div>
					<div class="col-sm-3">
						<label>Tipe</label>
						<input type="text" class="form-control" id="tipe" 
						name="tipe" value="<?=isset($default['TIPE'])? $default['TIPE'] : ""?>">
					</div>
					<div class="col-sm-3">
						<label>Ukuran</label>
						<input type="text" class="form-control" id="ukuran" 
						name="ukuran" value="<?=isset($default['UKURAN'])? $default['UKURAN'] : ""?>">
					</div>
					<div class="col-sm-3">
						<label>Spf Lain</label>
						<input type="text" class="form-control" id="spf_lain" 
						name="spf_lain" value="<?=isset($default['SPESIFIKASI_LAIN'])? $default['SPESIFIKASI_LAIN'] : ""?>">
					</div>
				</div>
			</div>
			<br>
			<div class="form-body">
				<div class="row">
					<div class="col-sm-6">
						<label><b>SATUAN, BERAT DAN HARGA</b></label>
					</div>
				</div>
			</div>
			<br>
			<div class="form-body">
				<div class="row">
					<div class="col-sm-4">
						<label>Jumlah Satuan</label>
						<input type="text" class="form-control" name="jumlah_satuan" value="<?=isset($default['JUMLAH_SATUAN'])? $default['JUMLAH_SATUAN'] : ""?>">
					</div>
					<div class="col-sm-4">
						<label>Netto (Kgm)</label>
						<input type="text" class="form-control" name="netto" value="<?=isset($default['NETTO'])? $default['NETTO'] : ""?>">
					</div>
					<div class="col-sm-4">
						<label>Harga Penyerahan (Rp)</label>
						<input type="text" class="form-control" name="harga_penyerahan" value="<?=isset($default['HARGA_PENYERAHAN'])? $default['HARGA_PENYERAHAN'] : ""?>">
					</div>
				</div>
			</div>
			<br>
			<div class="form-body">
				<div class="row">
					<div class="col-sm-4">
						<label>Jenis Satuan</label>
						<input type="text" class="form-control" name="uraian_satuan" value="<?=isset($default['URAIAN_SATUAN'])? $default['URAIAN_SATUAN'] : ""?>">
					</div>
					<div class="col-sm-4">
						<input style="border: none; margin-top: 20px;" type="text" readonly class="form-control" name="kode_satuan" value="<?=isset($default['KODE_SATUAN'])? $default['KODE_SATUAN'] : ""?>">
					</div>
					<div class="col-sm-4">
						<label>Volume (m3)</label>
						<input type="text" class="form-control" name="volume" value="<?=isset($default['VOLUME'])? $default['VOLUME'] : ""?>">
					</div>
					
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<br />
					<div class="form-actions">
					    <button type="submit" class="btn blue" name="submit_detail">Simpan</button>
					</div>
				</div>
			</div>
		</form>

	</div>
</div>