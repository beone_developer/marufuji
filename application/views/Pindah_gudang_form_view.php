<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Form Pindah Gudang</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">
          <form role="form" method="get">
              <div class="form-body">
                <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                            <label>Tanggal</label>
                            <div class="input-group">
                              <span class="input-group-addon input-circle-left">
                                  <i class="fa fa-calendar"></i>
                              </span>
                              <input class="form-control form-control-inline input-medium date-picker input-circle-right" name="tanggal" size="16" type="text" value="<?php echo date('m/d/Y');?>" readonly/>
                              <span class="help-block"></span>
                            </div>
                      </div>
                  </div>

                  <div class="col-sm-8">
                    <div class="form-group">
                        <label>Gudang Asal</label>
                        <div class="input-group">
                           <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="gudang_asal" required>
                              <option value=""></option>
                             <?php 	foreach($list_gudang as $row){ ?>
                               <option value="<?php echo $row['gudang_id'];?>"><?php echo $row['nama'];?></option>
                             <?php } ?>
                           </select>
                     </div>
                   </div>
                  </div>
              </div>
            </div>

            <div class="form-actions">
                <?php if(helper_security("pindah_gudang") == 1){?>
                <button type="submit" class="btn blue" name="submit_pindah">Lanjut</button>
                <?php }?>
            </div>
          </form>
      </div>
    </div>
