<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Form Pindah Gudang</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">
          <form role="form" method="post">
              <div class="form-body">
                <div class="row">

                  <div class="col-sm-3">
                    <h3><?php echo $_GET['tgl'];?></h3>
                    <input type="text" name="tanggal" value='<?php echo $_GET['tgl'];?>' hidden>
                    <input type="text" name="gudang_asal" value='<?php echo $_GET['gudang'];?>' hidden>

                    <?php
                        $sql_gudang_asal = $this->db->query("SELECT * FROM public.beone_gudang WHERE gudang_id = $_GET[gudang]");
                        $hasil_gudang_asal = $sql_gudang_asal->row_array();
                    ?>
                    <h4><b><?php echo $hasil_gudang_asal['nama'];?></b></h4>
                  </div>


                  <div class="col-sm-9" id="detail_cash_bank">
                      <div class="portlet light bordered">

                        <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                  <label>Gudang Tujuan</label>
                                  <div class="input-group">
                                     <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="gudang_tujuan" required>
                                        <option value=""></option>
                                       <?php 	foreach($list_gudang as $row){
                                          if ($row['gudang_id'] <> $hasil_gudang_asal['gudang_id']){
                                        ?>
                                         <option value="<?php echo $row['gudang_id'];?>"><?php echo $row['nama'];?></option>
                                       <?php }} ?>
                                     </select>
                               </div>
                             </div>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-sm-12">
                            <div class="form-group">
                                <label>Item</label>
                                <input type="hidden" class="form-control" name="nodoc" id="nodoc" required>
                                <select id="item" class="form-control input-sm select2-multiple" name="item" onChange="copydoc();" required>
                                  <option value=""></option>
                                <?php
                                $sql_item_gudang = $this->db->query("SELECT d.gudang_id, d.item_id, i.nama as nitem, SUM(qty_in) - SUM(qty_out) as jml_qty, d.nomor_transaksi
                                                                    FROM public.beone_gudang_detail d INNER JOIN public.beone_item i ON d.item_id = i.item_id
                                                                    WHERE d.flag = 1 AND d.gudang_id = $_GET[gudang] GROUP BY i.nama, d.item_id, d.gudang_id, d.nomor_transaksi");

                                foreach($sql_item_gudang->result_array() as $row){
                                    if ($row['jml_qty'] <> 0){
                                ?>
                                    <option value="<?php echo $row['item_id'];?>"><?php echo $row['nitem']." | Ready Stock = ".number_format($row['jml_qty'],2)." | Doc :".$row['nomor_transaksi'];?></option>
                                <?php
                                    }
                                }
                                ?>
                              </select>
                           </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label>Qty</label>
                              <input type="text" class="form-control" placeholder="Qty Pindah" name="qty" id="qty" required>
                            </div>
                          </div>
                          <div class="col-sm-9">
                            <div class="form-group">
                              <label>Keterangan</label>
                              <input type="text" class="form-control" placeholder="Keterangan" name="keterangan" required>
                            </div>
                          </div>
                        </div>

                      </div>
                  </div>
              </div>
            </div>

            <div class="form-actions">
                <button type="button" class="btn default">Cancel</button>
                <button type="submit" class="btn red" name="submit_pindah">Submit</button>
            </div>
          </form>
      </div>
    </div>

    <script>
    function copydoc(){
      var namaItem = $('#item option:selected').text();
      var nodoc = document.getElementById('nodoc').value = namaItem;
    }
    </script>

        <script type="text/javascript">

    		var qty = document.getElementById('qty');
    		  qty.addEventListener('keyup', function(e){
    			qty.value = formatRupiah(this.value, 'Rp. ');
    		});

    		/* Fungsi formatRupiah */
    		function formatRupiah(angka, prefix){
    			var number_string = angka.replace(/[^,\d]/g, '').toString(),
    			split   		= number_string.split(','),
    			sisa     		= split[0].length % 3,
    			rupiah     		= split[0].substr(0, sisa),
    			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    			// tambahkan titik jika yang di input sudah menjadi angka ribuan
    			if(ribuan){
    				separator = sisa ? '.' : '';
    				rupiah += separator + ribuan.join('.');
    			}

    			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    		}
    	</script>
