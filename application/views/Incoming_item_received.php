<h3>Incoming Item Received</h3>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="tools"></div>
	</div>
	<?php $list_item2 = $this->db->query("SELECT * FROM public.beone_item"); ?>


	<form role="form" method="post">
		<div class="form-body">

			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">

						<?php
						/******************* GENERATE NOMOR BOM ******************************/

						$tanggal_aju['tanggal_aju'];
						$tgl = substr($tanggal_aju['tanggal_aju'], 0, 10);
						$thn = substr($tgl, 2, 2);
						$bln = substr($tgl, 5, 2);


						$kode_awal = "RCV" . "-" . $thn . "-" . $bln . "-";

						$sql = $this->db->query("SELECT * FROM public.beone_received_import WHERE nomor_received LIKE '$kode_awal%' ORDER BY nomor_received DESC LIMIT 1");
						$hasil = $sql->row_array();

						$urutan = 0;
								if (isset($hasil)){
									$urutan = substr($hasil['nomor_received'], 10, 4);
								}	
						
						//$urutan = substr($hasil['nomor_received'], 10, 4);
						$no_lanjutan = $urutan + 1;

						$digit = strlen($no_lanjutan);
						$jml_nol = 4 - $digit;

						$cetak_nol = "";

						for ($i = 1; $i <= $jml_nol; $i++) {
							$cetak_nol = $cetak_nol . "0";
						}

						$nomor_received = $kode_awal . $cetak_nol . $no_lanjutan;

						/*************************************************/
						?>


						<div name="txtnorc" id="txtnorc"><h3><b><?php echo $nomor_received; ?></b></h3></div>
						<input type="hidden" id="received_no" name="received_no" value='<?php echo $nomor_received; ?>'>
						<!--<label>No Received</label>
						  <div class="input-group">
							  <span class="input-group-addon input-circle-left">
								  <i class="fa fa-car"></i>
							  </span>
							  <input type="text" class="form-control input-circle-right" placeholder="No Penerimaan Barang" name="received_no" required>
						  </div>-->
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label>Received Date</label>
						<div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-calendar"></i>
                        </span>
							<input class="form-control form-control-inline input-medium date-picker input-circle-right"
								   size="16" type="text" name="received_date" value="<?php echo date('m/d/Y'); ?>"
								   readonly required/>
							<span class="help-block"></span>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label>Kurs</label>
						<div class="input-group">
                              <span class="input-group-addon input-circle-left">
                                  <i class="fa fa-dollar"></i>
                              </span>
							<input type="text" class="form-control input-circle-right" placeholder="Kurs saat ini"
								   name="kurs" id="kurs" value="<?php if ($kurs['ndpbm'] == NULL) {
								echo 1;
							} else {
								echo $kurs['ndpbm'] * 1;
							} ?>" required readonly>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<!--<div class="col-sm-4">
                    <div class="form-group">
                    <label>Supplier TPB</label>
                      <input type='text' class='form-control' name='supplier_tpb' id='supplier_tpb' value="<?php //if($supplier['nama_pemasok'] == ""){echo $pengirim['nama_pengirim'];}else{echo $supplier['nama_pemasok'];}?>" required readonly>
                    </div>
                </div>-->
				<div class="col-sm-4">
					<div class="form-group">
						<label>Nomor PO</label>
						<div class="input-group">
							<select id="select2-single-input-sm" class="form-control input-sm select2-multiple"
									name="nomor_po" required>
								<option value=""></option>
								<?php foreach ($list_nomor_po as $row) { ?>
									<option
										value="<?php echo $row['purchase_header_id']; ?>"><?php echo $row['purchase_no']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label>Supplier</label>
						<div class="input-group">
							<select id="select2-single-input-sm" class="form-control input-sm select2-multiple"
									name="supplier" required>
								<option value=""></option>
								<?php foreach ($list_supplier

								as $row){ ?>
								<option value='<?php echo $row['custsup_id']; ?>'><?php echo $row['nama']; ?></opiton>
									<?php } ?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<hr/>

			<h4>List Barang</h4>
			<?php
			$no = 0;
			$harga_invoice = 0;
			$list_item = $this->db->query("SELECT * FROM public.beone_item");
			foreach ($item as $row) {
				$no = $no + 1;
				$harga_invoice = $row['harga_invoice'];
				?>

				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label>Item TPB</label>
							<input type='text' class='form-control' name='item_tpb' id='item_tpb'
								   value="<?php echo $row['uraian']; ?>" required readonly>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="form-group">
							<label>Qty</label>
							<input type='text' class='form-control' name='<?php echo "qty_" . $no; ?>' id='qty'
								   value="<?php echo $row['jumlah_satuan']; ?>" required readonly>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="form-group">
							<label>Price <?php if ($row['harga_satuan'] == NULL) {
									echo "Rp";
								} else {
									echo "$";
								} ?></label>
							<input type='text' class='form-control' name='<?php echo "harga_satuan_" . $no; ?>'
								   id='harga_satuan' value="<?php if ($row['harga_satuan'] == NULL) {
								echo number_format($row['harga_penyerahan'], 2);
							} else {
								echo number_format($row['harga_satuan'], 2);
							} ?>" required readonly>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label>Item</label>
							<div class="input-group">
								<select id='item' class='form-control input-sm select2-multiple'
										name='<?php echo "item_" . $no; ?>'>
									<option value=0><?php echo " - Pilih Item - "; ?></option>
									<?php foreach ($list_item->result_array() as $row) {
										echo '<option value=' . $row['item_id'] . '>' . $row['nama'] . '</option>';
									} ?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
			?>
			<input type='hidden' class='form-control' name='jml_item' id='jml_item' value="<?php echo $no; ?>" required
				   readonly>
			<input type='hidden' class='form-control' name='nomor_aju' id='nomor_aju'
				   value="<?php echo $nomor_aju['nomor_aju']; ?>" required readonly>
			<input type='hidden' class='form-control' name='harga_invoice' id='harga_invoice'
				   value="<?php if ($nomor_aju['harga_invoice'] == NULL) {
					   echo $nomor_aju['harga_penyerahan'];
				   } else {
					   echo $nomor_aju['harga_invoice'];
				   }; ?>" required readonly>
		</div>
		<div class="form-actions">
			<a href='<?php echo base_url('Inventin_controller'); ?>' class='btn default'><i
					class="fa fa-arrow-circle-o-left"></i> Back</a>
			<button type="submit" class="btn red" name="submit_in">Submit</button>
		</div>
	</form>


	<script type="text/javascript">
		var kurs = document.getElementById('kurs');
		kurs.addEventListener('keyup', function (e) {
			kurs.value = formatRupiah(this.value, 'Rp. ');
		});

		/* Fungsi formatRupiah */
		function formatRupiah(angka, prefix) {
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
				split = number_string.split(','),
				sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if (ribuan) {
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}

			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		}
	</script>
