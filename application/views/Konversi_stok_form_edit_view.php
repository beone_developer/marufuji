<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<?php $list_item = $this->db->query("SELECT * FROM public.beone_item"); ?>

<div class="portlet box blue ">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i> Form Konversi Stock
		</div>
		<div class="tools">
			<a href="" class="collapse"> </a>
			<a href="#portlet-config" data-toggle="modal" class="config"> </a>
			<a href="" class="reload"> </a>
			<a href="" class="remove"> </a>
		</div>
	</div>
	<div class="portlet-body form">


		<form role="form" method="post">
			<div class="form-body">

			<div class="row">
                  <div class="col-sm-5"><h3><b><?php echo $default['konversi_stok_no'];?></b></h3></div>
                  <input type"text" name="konversi_stok_no" value='<?php echo $default['konversi_stok_no'];?>' hidden>
                  <div class="col-sm-7"><h3><?php echo $default['konversi_stok_date'];?></h3></div>
                  <input type"text" name="konversi_stok_date" value='<?php echo helper_tanggalupdate($default['konversi_stok_date']);?>' hidden>
                </div>

				<div class="row">
					<div class="col-sm-4"><h4><?php echo $default['item_name'];?></h4></div>
                    <input type"text" name="customer" value='<?php echo $default['item_name'];?>' hidden>

					<div class="col-md-4">
						<div class="form-group">
							<label>Quantity</label>
							<input type='text' class='form-control' placeholder="Quantity" name='qty' id='qty' value='<?php echo $default['qty'];?>'
								required>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>UoM</label>
							<select id='satuan_qty' class='form-control input-sm select2-multiple' name='satuan_qty'  onchange="changeSatuan()" required>
								<option
									value="<?= isset($default['satuan_id']) ? $default['satuan_id'] : "" ?>"><?= isset($default['satuan_code']) ? $default['satuan_code'] : "" ?></option>
								<?php foreach ($list_satuan as $satuan) { ?>
									<option
										value="<?php echo $satuan['satuan_id']; ?>"><?php echo $satuan['satuan_code']; ?></option>
								<?php } ?>
								<
							</select>
						</div>
					</div>
				</div>

				<hr
				/ style="border-color: #3598DC;">
				<table id="datatable" class="table striped hovered cell-hovered">
					<thead>
					<tr>
						<td width="5%">ID</td>
						<td width="20%">Item</td>
						<td width="15%">Base Qty</td>
						<td width="15%">Allocation Qty</td>
						<td width="15%">UoM</td>
						<td width="5%"></td>
					</tr>
					</thead>
					<tbody id="container">
						<?php
                            $ctr = 0;
                            foreach($default_detail as $row){
                            $ctr = $ctr + 1;
                            $nama_ctr = "".$ctr;
                            $nama_item = "item_".$nama_ctr;
                            $nama_item_id = "item_id_".$nama_ctr;
                            $nama_qty = "qty_".$nama_ctr;
                            $nama_qty_real = "qty_real_".$nama_ctr;
                            $nama_satuan = "satuan_qty_".$nama_ctr;
                            $nama_satuan_id = "satuan_id_".$nama_ctr;
                            $rowss = "rows_".$nama_ctr;
                            $detail_id = "detail_id_".$nama_ctr;
                          ?>

							<tr class="records" id="'+count+'">
								<td><input class="form-control" id='<?php echo $nama_item;?>' name='<?php echo $nama_item;?>' type="text" value='<?php echo $row['item_id'];?>' readonly></td>
								<td><input class="form-control" id='<?php echo $nama_item;?>' name='<?php echo $nama_item;?>' type="text" value='<?php echo $row['item_name'];?>' readonly></td>
								<td><input class="form-control" id='<?php echo $nama_qty;?>' name='<?php echo $nama_qty;?>' type="text" value='<?php echo number_format($row['qty'],2);?>' readonly></td>
								<td><input class="form-control" id='<?php echo $nama_qty_real;?>' name='<?php echo $nama_qty;?>' type="text" value='<?php echo number_format($row['qty'],2);?>' readonly></td>
								<td><input class="form-control" id='<?php echo $nama_satuan;?>' name='<?php echo $nama_satuan;?>' type="text" value='<?php echo $row['satuan'];?>' readonly></td>
								<td><button type="button" class="btn red" onclick="hapus('<?php echo $nama_ctr;?>')">X</button></td>
								<td><input class="form-control" id='<?php echo $nama_satuan_id;?>' name='<?php echo $nama_satuan_id;?>' type="hidden" value='<?php echo $row['satuan_id'];?>' readonly></td>
								<td><input id='<?php echo $rowss;?>' name="rows[]" value='<?php echo $nama_ctr;?>' type="hidden"></td></td>
								<td><input id='<?php echo $detail_id;?>' name='<?php echo $detail_id;?>' value='<?php echo $row['konversi_stok_detail_id'];?>' type="hidden"></td></tr>
							</tr>
						<?php
                            }
                           ?>
					</tbody>
				</table>

			</div>
			<div class="form-actions">
                <a class="btn blue" data-toggle="modal" href="#responsive"> Tambah Data </a>
				<?php //if (helper_security("pembelian_add") == 1) { ?>
					<button type="submit" class="btn red" name="submit_konversi">Submit</button>
				<?php //} ?>
			</div>
		</form>
	</div>
</div>


<!--------------------------- MODAL ADD ITEM--------------------------------------------->
<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Detail Item</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
            <div class="form-group">
                <label>Item</label>
                <select id='item_modal' class='form-control input-sm select2-multiple' name='item_modal' required>
                  <option value=0><?php echo " - Pilih Item - ";?></option>
                  <?php  foreach($list_item->result_array() as $row){ echo '<option value='.$row['item_id'].'>'.$row['nama'].'</option>';} ?>
                </select>
            </div>
            </div>
            <div class="col-md-2"></div>
        </div>

        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-2">
                <div class="form-group">
                <label>Quantity</label>
                <input type='text' class='form-control' placeholder="Quantity" name='qty_modal' id='qty_modal' required onchange="onchangeQty(this.value)">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                <label>Quantity Real</label>
                <input type='text' class='form-control' placeholder="Quantity Real" name='qty_real_modal' id='qty_real_modal' required>
                </div>
            </div>
            <div class="col-md-3">
				<div class="form-group">
					<label>UoM</label>
						<select id='satuan_qty_modal' class='form-control input-sm select2-multiple' name='satuan_qty_modal' required>
								<option
									value="<?= isset($default['satuan_id']) ? $default['satuan_id'] : "" ?>"><?= isset($default['satuan_code']) ? $default['satuan_code'] : "" ?></option>
								<?php foreach ($list_satuan as $satuan) { ?>
									<option
										value="<?php echo $satuan['satuan_id']; ?>"><?php echo $satuan['satuan_code']; ?></option>
								<?php } ?>
								<
							</select>
						</div>
            </div>
            <div class="col-md-2"></div>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" class="btn green" name="add_btn" id="add_btn">Insert</button>
    </div>
</div>


<script>
	$(document).ready(function() {
    		var count = 0;

    		$("#add_btn").click(function(){
					count += 1;
		
          var item = document.getElementById('item_modal');
          var qty = document.getElementById('qty_modal');
          var qty_real = document.getElementById('qty_real_modal');
          var satuan_qty = $('#satuan_qty_modal option:selected').text();
		  var namaItem = $('#item_modal option:selected').text();

					$('#container').append('<tr class="records" id="'+count+'">'
							+ '<td><input class="form-control" id="item_id_' + count + '" name="item_id_'+count+'" type="text" value="'+item.value+'" readonly></td>'
							+ '<td><input class="form-control" id="item_' + count + '" name="item_'+count+'" type="text" value="'+namaItem+'" readonly></td>'
							+ '<td><input class="form-control" id="qty_' + count + '" name="qty_'+count+'" type="text" value="'+qty.value+'" readonly></td>'
							+ '<td><input class="form-control" id="qty_real_' + count + '" name="qty_real_'+count+'" type="text" value="'+qty_real.value+'" readonly></td>'
							+ '<td><input class="form-control" id="satuan_qty_' + count + '" name="satuan_qty_'+count+'" type="text" value="'+satuan_qty+'" readonly></td>'
							+ '<td><input id="rows_' + count + '" name="rows[]" value="'+ count +'" type="hidden"></td>'
							+ '<td><button type="button" class="btn red" onclick="hapus('+count+')">X</button></td></tr>'
					);

          eraseText();
          $('#responsive').modal('hide');
				});

				$(".remove_item").live('click', function (ev) {
    			if (ev.type == 'click') {
	        	$(this).parents(".records").fadeOut();
	        	$(this).parents(".records").remove();
        	}
     		});
		});
	function changeSatuan() {
		var satuan_qty = $('#satuan_qty option:selected').text();
		$('tr').each(function(i){
			$('#satuan_qty_'+i).val(satuan_qty);
		});
	}

    function eraseText() {
     document.getElementById("item_modal").value = "";
     document.getElementById("qty_modal").value = "";
     document.getElementById("qty_real_modal").value = "";
     document.getElementById("satuan_qty_modal").value = "";
	}

	function onchangeQty(val_qty)
    {
		var qty_header = document.getElementById('qty').value;
		$real = val_qty * qty_header;
		$('#qty_real_modal').val($real);
    }

	function hapus(rowid)
    {
        var row = document.getElementById(rowid);
        row.parentNode.removeChild(row);
    }
</script>