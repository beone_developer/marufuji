        <!-- BEGIN PAGE TITLE-->
        <?php
          $data_perusahaan = $this->db->query("SELECT * FROM public.beone_konfigurasi_perusahaan");
          $hasil_data_perusahaan = $data_perusahaan -> row_array();
        ?>
        <h4><b><?php //echo $hasil_data_perusahaan['nama_perusahaan'];?></b></h4>
        <h5><?php //echo $hasil_data_perusahaan['alamat_perusahaan'];?></h5>
        <h5><?php //echo $hasil_data_perusahaan['kota_perusahaan'];?> <?php //echo $hasil_data_perusahaan['provinsi_perusahaan'];?> <?php //echo $hasil_data_perusahaan['kode_pos_perusahaan'];?></h5>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <?php
          $tgl = "";
          $nvoucher = "";
          $tp = "";
          foreach($print_voucher as $row){
            $tgl = $row['voucher_date'];
            $nvoucher = $row['voucher_number'];
            $tp = $row['tipe'];
            $bankkas = $row['coa_id_cash_bank'];
          }
        ?>

        <div class="invoice">
            <?php
                $mk = "";
                if ($tp == 0){ $mk = "KELUAR";}else{ $mk = "MASUK";}
                if ($bankkas <= 3){$txtBankkas = "KAS";}else{$txtBankkas = "BANK";}
            ?>
            <center><h2><b>BUKTI <?php echo $txtBankkas." ".$mk;?><b></h2></center>
            <hr/>
            <div class="row">
                <div class="col-xs-2">
                    <ul class="list-unstyled">
                        <li> <h5> Tanggal </h5> </li>
                        <li> <h5> No Voucher </h5> </li>
                    </ul>
                </div>
                <div class="col-xs-4">
                    <ul class="list-unstyled">
                        <li><h5>: <?php echo helper_tanggalupdate($tgl);?> </h5> </li>
                        <li><h5>: <?php echo $nvoucher;?> </h5> </li>
                    </ul>
                </div>
                <div class="col-xs-2">
                    <ul class="list-unstyled">
                        <li><h5> </h5></li>
                        <li><h5> </h5></li>
                    </ul>
                </div>
                <div class="col-xs-4"></div>
            </div>
            <hr />
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th> Acc Number </th>
                                <th class="hidden-xs"> Description </th>
                                <th class="hidden-xs"> Amount </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                          $subtotal = 0;
                          foreach($print_voucher as $row){ ?>
                                  <tr>
                                    <td> <?php echo $row['coa_no_lawan'];?> </td>
                                    <td class="hidden-xs"> <?php echo $row['keterangan_detail'];?> </td>
                                    <td class="hidden-xs"> <?php echo number_format($row['jumlah_idr'],2);?> </td>
                                  <tr>
                          <?php
                              $subtotal = $subtotal + $row['jumlah_idr'];
                              } ?>
                            <tr>
                                <td>  </td>
                                <td class="hidden-xs"> <b>TOTAL</b> </td>
                                <td class="hidden-xs"> <b><?php echo number_format($subtotal,2);?></b> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>


            <?php
            function konversi($x){

              $x = abs($x);
              $angka = array ("","satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
              $temp = "";

              if($x < 12){
               $temp = " ".$angka[$x];
              }else if($x<20){
               $temp = konversi($x - 10)." belas";
              }else if ($x<100){
               $temp = konversi($x/10)." puluh". konversi($x%10);
              }else if($x<200){
               $temp = " seratus".konversi($x-100);
              }else if($x<1000){
               $temp = konversi($x/100)." ratus".konversi($x%100);
              }else if($x<2000){
               $temp = " seribu".konversi($x-1000);
              }else if($x<1000000){
               $temp = konversi($x/1000)." ribu".konversi($x%1000);
              }else if($x<1000000000){
               $temp = konversi($x/1000000)." juta".konversi($x%1000000);
              }else if($x<1000000000000){
               $temp = konversi($x/1000000000)." milyar".konversi($x%1000000000);
              }

              return $temp;
             }

             function tkoma($x){
              $str = stristr($x,".");
              $ex = explode('.',$x);

              if(($ex[1]/10) >= 1){
               $a = abs($ex[1]);
             }else{$a = 0;}//custom lukman
              $string = array("nol", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan",   "sembilan","sepuluh", "sebelas");
              $temp = "";

              $a2 = $ex[1]/10;
              $pjg = strlen($str);
              $i =1;


              if($a>=1 && $a< 12){
               $temp .= " ".$string[$a];
              }else if($a>12 && $a < 20){
               $temp .= konversi($a - 10)." belas";
              }else if ($a>20 && $a<100){
               $temp .= konversi($a / 10)." puluh". konversi($a % 10);
              }else{
               if($a2<1){

                while ($i<$pjg){
                 $char = substr($str,$i,1);
                 $i++;
                 $temp .= " ".$string[$char];
                }
               }
              }
              return $temp;
             }

             function terbilang($x){
              if($x<0){
               $hasil = "minus ".trim(konversi(x));
              }else{
               $poin = trim(tkoma($x));
               $hasil = trim(konversi($x));
              }

            if($poin){
               $hasil = $hasil." koma ".$poin;
              }else{
               $hasil = $hasil;
              }
              return $hasil;
             }

             $panjang_subtotal = strlen($subtotal);
             $digit_mulai = $panjang_subtotal - 2;
             $digit_koma = substr($subtotal, $digit_mulai ,2);

            if ($digit_koma == "00"){
                  echo "<h4><i>".ucwords(terbilang($subtotal.".00"))." Rupiah</h4></i>";
            }else{
                  echo "<h4><i>".ucwords(terbilang($subtotal))." Rupiah</h4></i>";
            }

            ?>
            <br />
            <br />
            <br />
            <div class="row">
                <div class="col-xs-4">
                    <div class="well">
                      <center>
                      <strong>Mengetahui</strong>
                      <br/>
                      <br/>
                      <br/>
                      <strong>( .................... )</strong>
                      </center>
                    </div>
                </div>
                <div class="col-xs-4 invoice-block">
                  <div class="well">
                  <center>
                  <strong>Kasir</strong>
                  <br/>
                  <br/>
                  <br/>
                  <strong>( .................... )</strong>
                  </center>
                  </div>
                </div>
                <div class="col-xs-4">
                    <div class="well">
                      <center>
                      <strong>Penerima</strong>
                      <br/>
                      <br/>
                      <br/>
                      <strong>( .................... )</strong>
                      </center>
                    </div>
                </div>
            </div>
            <div class="row">
              <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();"> Print
                  <i class="fa fa-print"></i>
              </a>
              <!--<a class="btn btn-lg green hidden-print margin-bottom-5"> Submit Your Invoice
                  <i class="fa fa-check"></i>
              </a>-->
            </div>
        </div>
