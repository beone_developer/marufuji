<?php
$tgl_akhir_formated = date('Y-m-d');

$thn_awal = substr($tgl_akhir_formated,0,4);

$tgl_awal_formated = $thn_awal."-01-01";


?>
    <!-- BEGIN PAGE TITLE-->
          <h2>
              <center><b>Assets Bahan Penolong</b></center>
          </h2>
          <hr />

              <div class="row">
                  <div class="col-xs-12">
                      <table class="table table-striped table-hover">
                          <thead>
                              <tr>
                                  <th><small> Kode </small></th>
                                  <th><small> Item </small></th>
                                  <th><small> Satuan </small></th>
                                  <th><small> Q Awal </small></th>
                                  <th><small> Harga Awal </small></th>
                                  <th><small> Jml Awal </small></th>
                                  <th><small> Q Masuk </small></th>
                                  <th><small> Jml Masuk </small></th>
                                  <th><small> Q Keluar </small></th>
                                  <th><small> Jml Keluar </small></th>
                                  <th><small> Q Akhir </small></th>
                                  <th><small> Harga Akhir </small></th>
                                  <th><small> Jml Akhir </small></th>
                              </tr>
                          </thead>

                          <tbody>
                            <?php
                            $subtotal_qty_awal = 0;
                            $subtotal_saldo_awal = 0;
                            $subtotal_qty_in = 0;
                            $subtotal_jumlah_in = 0;
                            $subtotal_qty_out = 0;
                            $subtotal_jumlah_out = 0;
                            $subtotal_qty_akhir = 0;
                            $subtotal_jumlah_akhir = 0;

                            foreach($list_assets_bahan_penolong as $row){

                              //query mutasi item
                              $sql_mutasi_item = $this->db->query("SELECT *  FROM public.beone_inventory WHERE flag = 1 AND trans_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND item_id = ".intval($row['item_id']));

                              $count_inventory = $this->db->query("SELECT COUNT(item_id) as jml_transaksi  FROM public.beone_inventory WHERE flag = 1 AND trans_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND item_id = ".intval($row['item_id']));
                              $hasil_jml_transaksi = $count_inventory->row_array();

                              if ($row['saldo_idr'] == 0 OR $row['saldo_qty'] == 0){
                                  $unit_price_awal = 0;
                              }else{
                                  $unit_price_awal_round = $row['saldo_idr'] / $row['saldo_qty'];
                                  $unit_price_awal = round($unit_price_awal_round, 2);
                              }

                              $total_qty_in = 0;
                              $total_jumlah_in = 0;
                              $total_qty_out = 0;
                              $total_jumlah_out = 0;

                              if ($hasil_jml_transaksi['jml_transaksi'] > 0){
                                foreach($sql_mutasi_item->result_array() as $mutasi){
                                      $qty_in = $mutasi['qty_in'];
                                      $jumlah_in = $qty_in * $mutasi['value_in'];

                                      $qty_out = $mutasi['qty_out'];
                                      $jumlah_out = $qty_out * $mutasi['value_out'];

                                      $total_qty_in = $total_qty_in + $qty_in;
                                      $total_jumlah_in = $total_jumlah_in + $jumlah_in;
                                      $total_qty_out = $total_qty_out + $qty_out;
                                      $total_jumlah_out = $total_jumlah_out + $jumlah_out;
                                }
                              }


                              $qty_akhir_round = ($row['saldo_qty'] + $total_qty_in) - $total_qty_out;
                              $qty_akhir = round($qty_akhir_round, 2);
                              $jumlah_akhir_round = ($row['saldo_idr'] + $total_jumlah_in) - $total_jumlah_out;
                              $jumlah_akhir = round($jumlah_akhir_round,2);

                              if ($qty_akhir == 0 OR $jumlah_akhir == 0){
                                    $unit_price_akhir = 0;
                              }else{
                                    $unit_price_akhir_round = $jumlah_akhir / $qty_akhir;
                                    $unit_price_akhir = round($unit_price_akhir_round, 2);
                              }

                              if ($row['item_type_id'] == 1){
                                  $tipe_item = "BB";
                              }else if($row['item_type_id'] == 2){
                                  $tipe_item = "WIP";
                              }else{
                                  $tipe_item = "BJ";
                              }

                            ?>

                              <tr>
                                  <td><small> <?php echo $row['item_code'];?> </small></td>
                                  <td><small> <?php echo $row['nama'];?> </small></td>
                                  <td><small>  <?php echo $row['nsatuan'];?></small></td>
                                  <td><small>  <?php echo number_format($row['saldo_qty'], 2);?></small></td>
                                  <td><small>  <?php echo number_format($unit_price_awal, 2);?></small></td>
                                  <td><small>  <?php echo number_format($row['saldo_idr'], 2);?></small></td>
                                  <td><small>  <?php echo number_format($total_qty_in, 2);?> </small></td>
                                  <td><small>  <?php echo number_format($total_jumlah_in, 2);?> </small></td>
                                  <td><small>  <?php echo number_format($total_qty_out, 2);?> </small></td>
                                  <td><small>  <?php echo number_format($total_jumlah_out, 2);?> </small></td>
                                  <td><small>  <?php echo number_format($qty_akhir, 2);?> </small></td>
                                  <td><small>  <?php echo number_format($unit_price_akhir, 2);?> </small></td>
                                  <td><small>  <?php echo number_format($jumlah_akhir, 2);?> </small></td>
                              </tr>
                            <?php
                                $subtotal_qty_awal = $subtotal_qty_awal + $row['saldo_qty'];
                                $subtotal_saldo_awal = $subtotal_saldo_awal + $row['saldo_idr'];
                                $subtotal_qty_in = $subtotal_qty_in + $total_qty_in;
                                $subtotal_jumlah_in = $subtotal_jumlah_in + $total_jumlah_in;
                                $subtotal_qty_out = $subtotal_qty_out + $total_qty_out;
                                $subtotal_jumlah_out = $subtotal_jumlah_out + $total_jumlah_out;
                                $subtotal_qty_akhir = $subtotal_qty_akhir + $qty_akhir;
                                $subtotal_jumlah_akhir = $subtotal_jumlah_akhir + $jumlah_akhir;
                              }
                            ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b><small><?php echo number_format($subtotal_qty_awal, 2);?></small></b></td>
                                <td><b><small>  </small></b></td>
                                <td><b><small><?php echo number_format($subtotal_saldo_awal, 2);?></small></b></td>
                                <td><b><small><?php echo number_format($subtotal_qty_in, 2);?></small></b></td>
                                <td><b><small><?php echo number_format($subtotal_jumlah_in, 2);?></small></b></td>
                                <td><b><small><?php echo number_format($subtotal_qty_out, 2);?></small></b></td>
                                <td><b><small><?php echo number_format($subtotal_jumlah_out, 2);?></small></b></td>
                                <td><b><small><?php echo number_format($subtotal_qty_akhir, 2);?></small></b></td>
                                <td><b><small>  </small></b></td>
                                <td><b><small><?php echo number_format($subtotal_jumlah_akhir, 2);?></small></b></td>
                            </tr>

                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
