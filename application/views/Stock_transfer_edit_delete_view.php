<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<?php $list_item = $this->db->query("SELECT * FROM public.beone_item");?>

<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Bill Of Material</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">




          <form role="form" method="post">
              <div class="form-body">

                <div class="row">
                  <div class="col-sm-4"><h3><b><?php echo $default['transfer_no'];?></b></h3></div>
                  <div class="col-sm-8"><h3><?php echo $default['transfer_date'];?></h3></div>
                  </div>

                  <div class="row">
                      <div class="col-sm-4"><h3><i><?php if($default['coa_kode_biaya'] == 1){echo "PRODUKSI WIP";}else{echo "PRODUKSI BARANG JADI";}?></i></h3></div>

                      <div class="col-sm-8"><h3><?php echo $default['keterangan'];?></h3></div>
                    </div>

                    <div class="row">
                      <div class="col-sm-4"><input type="hidden" class="form-control" id="cocokan_dari_item" name="cocokan_dari_item" value=0 required></div>
                      <div class="col-sm-4"><input type="hidden" class="form-control" id="cocokan_hasil_item" name="cocokan_hasil_item" value=0 required></div>
                      <div class="col-sm-4"></div>
                    </div>

                    <hr / style="border-color: #3598DC;">
                    <table  id="datatable" class="table striped hovered cell-hovered">
                        <h4><b>DARI ITEM</b></h4>
            						<thead>
            							<tr>
                            <td width="40%">Item</td>
            								<td width="20%">Qty</td>
            								<td width="30%">Gudang</td>
                            <td width="5%"></td>
                            <td width="5%"></td>
            							</tr>

                          </thead>
                          <tbody id="container">

                            <?php
                            $ctr = 0;
                             foreach($default_detail as $row){
                             $ctr = $ctr + 1;
                             $nama_ctr = "x".$ctr;
                             $nama_item = "item_".$nama_ctr;
                             $nama_qty = "qty_".$nama_ctr;
                             $nama_gudang = "gudang_".$nama_ctr;
                             $nama_item_id = "item_id_".$nama_ctr;
                             $nama_gudang_id = "gudang_id_".$nama_ctr;
                             $rowss = "rows_".$nama_ctr;

                             $sql_cari_nama_item = $this->db->query("SELECT * FROM public.beone_item WHERE item_id = ".intval($row['item_id']));
                 					 	 $hasil_cari_nama_item = $sql_cari_nama_item->row_array();
                 					 	 $nitem = $hasil_cari_nama_item['nama'];

                             $sql_cari_nama_gudang = $this->db->query("SELECT * FROM public.beone_gudang WHERE gudang_id = ".intval($row['gudang_id']));
                 					 	 $hasil_cari_nama_gudang = $sql_cari_nama_gudang->row_array();
                 					 	 $ngudang = $hasil_cari_nama_gudang['nama'];

                           ?>

                           <tr class='records' id='<?php echo $nama_ctr;?>'>
              						 <td><input class="form-control" id='<?php echo $nama_item;?>' name='<?php echo $nama_item;?>' type="text" value='<?php echo $nitem;?>' readonly></td>
              						 <td><input class="form-control" id='<?php echo $nama_qty;?>' name='<?php echo $nama_qty;?>' type="text" value='<?php echo $row['qty'];?>' readonly></td>
                           <td><input class="form-control" id='<?php echo $nama_gudang;?>' name='<?php echo $nama_gudang;?>' type="text" value='<?php echo $ngudang;?>' readonly></td>
                           <td><input class="form-control" id='<?php echo $nama_item_id;?>' name='<?php echo $nama_item_id;?>' type="hidden" value='<?php echo $row['item_id'];?>' readonly></td>
                           <td><input class="form-control" id='<?php echo $nama_gudang_id;?>' name='<?php echo $nama_gudang_id;?>' type="hidden" value='<?php echo $row['gudang_id'];?>' readonly></td>
                           <td><input id='<?php echo $rowss;?>' name="rows[]" value='<?php echo $nama_ctr;?>' type="hidden"></td></td></tr>

                           <?php
                             }
                            ?>

             						  </tbody>


            				</table>


                    <hr / style="border-color: #3598DC;">
                    <table  id="datatable_hasil" class="table striped hovered cell-hovered">
                        <h4><b>MENJADI ITEM</b></h4>
            						<thead>
            							<tr>
                            <td width="20%">Item</td>
            								<td width="15%">Qty</td>
            								<td width="15%">Biaya</td>
                            <td width="20%">Gudang</td>
            								<td width="20%">% Hasil</td>
                            <td width="5%"></td>
                            <td width="5%"></td>

            							</tr>
                         <tbody id="container_hasil">

                           <?php
                           $ctr = 0;
                            foreach($default_detail_hasil as $row){
                            $ctr = $ctr + 1;
                            $nama_ctr = "xh".$ctr;
                            $nama_item = "item_hasil_".$nama_ctr;
                            $nama_qty = "qty_hasil_".$nama_ctr;
                            $nama_biaya = "biaya_hasil_".$nama_ctr;
                            $nama_persen = "persen_hasil_".$nama_ctr;
                            $nama_gudang = "gudang_hasil_".$nama_ctr;
                            $nama_item_id = "item_hasil_id_".$nama_ctr;
                            $nama_gudang_id = "gudang_hasil_id_".$nama_ctr;
                            $rowss = "rows_hasil".$nama_ctr;

                            $sql_cari_nama_item = $this->db->query("SELECT * FROM public.beone_item WHERE item_id = ".intval($row['item_id']));
                            $hasil_cari_nama_item = $sql_cari_nama_item->row_array();
                            $nitem = $hasil_cari_nama_item['nama'];

                            $sql_cari_nama_gudang = $this->db->query("SELECT * FROM public.beone_gudang WHERE gudang_id = ".intval($row['gudang_id']));
                            $hasil_cari_nama_gudang = $sql_cari_nama_gudang->row_array();
                            $ngudang = $hasil_cari_nama_gudang['nama'];

                          ?>

                          <tr class='records' id='<?php echo $nama_ctr;?>'>
                          <td><input class="form-control" id='<?php echo $nama_item;?>' name='<?php echo $nama_item;?>' type="text" value='<?php echo $nitem;?>' readonly></td>
                          <td><input class="form-control" id='<?php echo $nama_qty;?>' name='<?php echo $nama_qty;?>' type="text" value='<?php echo $row['qty'];?>' readonly></td>
                          <td><input class="form-control" id='<?php echo $nama_biaya;?>' name='<?php echo $nama_biaya;?>' type="text" value='<?php echo $row['biaya'];?>' readonly></td>
                          <td><input class="form-control" id='<?php echo $nama_gudang;?>' name='<?php echo $nama_gudang;?>' type="text" value='<?php echo $ngudang;?>' readonly></td>
                          <td><input class="form-control" id='<?php echo $nama_persen;?>' name='<?php echo $nama_persen;?>' type="text" value='<?php echo $row['persen_produksi'];?>' readonly></td>
                          <td><input class="form-control" id='<?php echo $nama_item_id;?>' name='<?php echo $nama_item_id;?>' type="hidden" value='<?php echo $row['item_id'];?>' readonly></td>
                          <td><input class="form-control" id='<?php echo $nama_gudang_id;?>' name='<?php echo $nama_gudang_id;?>' type="hidden" value='<?php echo $row['gudang_id'];?>' readonly></td>
                          <td><input id='<?php echo $rowss;?>' name="rows_hasil[]" value='<?php echo $nama_ctr;?>' type="hidden"></td></td></tr>

                          <?php
                            }
                           ?>

                         </tbody>
            				</table>
              </div>
              <div class="form-actions">
              </div>
          </form>
  </div>
</div>
