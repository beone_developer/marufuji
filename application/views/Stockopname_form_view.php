<h3>Stock Opname Detail <b><?=$tipe?></b></h3>
<div class="portlet light bordered">
  <div class="portlet-title">

    <?php if($tipe == "Ubah"){?>
    <form role="form" method="post">
      <div class="form-body">
        <div class="row">
          <div class="col-sm-4">
              <label>Item</label>
              <input type="text" class="form-control" value="<?=isset($default['nitem'])? $default['nitem'] : ""?>" name="nama_item" readonly required>
          </div>
          <div class="col-sm-4">
              <label>Qty Opname</label>
              <input type="hidden" class="form-control" value="<?=isset($default['opname_detail_id'])? $default['opname_detail_id'] : ""?>" name="opname_detail_id" readonly required>
              <input type="text" class="form-control" id='qty_opname' value="<?=isset($default['qty_opname'])? $default['qty_opname'] : ""?>" name="qty_opname" required>
          </div>
          </div>
      </div>
      <br />
      <div class="form-actions">
          <a href='<?php echo base_url('Stockopname_controller');?>' class='btn default'> Kembali</a>
          <button type="submit" class="btn blue" name="submit_opname">Update</button>
      </div>
    </form>
  <?php }?>
      <br />

      <table class="table table-striped table-bordered table-hover" id="sample_1">
              <thead>
                <tr>
                    <th width="30%"><center>Item</center></th>
                    <th width="15%"><center>Qty Ext</center></th>
                    <th width="15%"><center>Qty Opname</center></th>
                    <th width="10%"><center>Selisih</center></th>
                    <th width="10%"><center>Status</center></th>
                    <th width="10%"><center>Action</center></th>
                </tr>
              </thead>
              <tbody>
              <?php
                  foreach($list_detail_stockopname as $row){
              ?>
                <tr>
                    <td><?php echo $row['nitem'];?></td>
                    <td><?php echo number_format($row['qty_existing'],2);?></td>
                    <td><?php echo number_format($row['qty_opname'],2);?></td>
                    <td><?php echo number_format($row['qty_selisih'],2);?></td>
                    <td><?php
                          if ($row['status_opname'] == 1){
                             echo "Sesuai";
                          }else if ($row['status_opname'] <=2){
                             echo "Lebih";
                          }else if ($row['status_opname'] >= 0){
                             echo "Kurang";
                          }
                    ?></td>
                    <td><?php if(helper_security("stockopname_add") == 1){?><a href='<?php echo base_url('Stockopname_controller/Edit/'.$row['opname_header_id'].'/'.$row['opname_detail_id'].'');?>' class='btn blue'> Opname </a><?php }?></td>
                </tr>
              <?php } ?>
        </tbody>
    </table>

</div>

<script type="text/javascript">

var qty_opname = document.getElementById('qty_opname');
  qty_opname.addEventListener('keyup', function(e){
  qty_opname.value = formatRupiah(this.value, 'Rp. ');
});

/* Fungsi formatRupiah */
function formatRupiah(angka, prefix){
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
  split   		= number_string.split(','),
  sisa     		= split[0].length % 3,
  rupiah     		= split[0].substr(0, sisa),
  ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
}
</script>
