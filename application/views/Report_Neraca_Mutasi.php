<?php
  $tgl_awal = $_GET['tglawal'];
  $tgl_akhir = $_GET['tglakhir'];

  $thn_awal = substr($tgl_awal,6,4);
  $bln_awal = substr($tgl_awal,0,2);
  $day_awal = substr($tgl_awal,3,2);
  $tgl_awal_formated = $thn_awal."-".$bln_awal."-".$day_awal;

  $thn_akhir = substr($tgl_akhir,6,4);
  $bln_akhir = substr($tgl_akhir,0,2);
  $day_akhir = substr($tgl_akhir,3,2);
  $tgl_akhir_formated = $thn_akhir."-".$bln_akhir."-".$day_akhir;
?>
    <!-- BEGIN PAGE TITLE-->
          <h2>
              <center><b>Report Neraca Mutasi</b></center>
          </h2>
          <h4>
              <center>Periode <?php echo $day_awal."/".$bln_awal."/".$thn_awal;?> sampai <?php echo $day_akhir."/".$bln_akhir."/".$thn_akhir;?></center>
          </h4>
          <hr />

              <div class="row">
                  <div class="col-xs-12">
                      <table class="table table-striped table-hover">
                          <thead>
                              <tr>
                                  <th> COA </th>
                                  <th> Akun </th>
                                  <th> SA Debit </th>
                                  <th class="hidden-xs"> SA Kredit </th>
                                  <th class="hidden-xs"> M Debit </th>
                                  <th class="hidden-xs"> M Kredit </th>
                                  <th> S Debit </th>
                                  <th> S Kredit </th>
                              </tr>
                          </thead>

                          <tbody>
                            <?php

                            $ctr_debet_saldo_awal = 0;
                            $ctr_kredit_saldo_awal = 0;
                            $ctr_debet_saldo_mutasi = 0;
                            $ctr_kredit_saldo_mutasi = 0;
                            $ctr_debet_saldo_akhir = 0;
                            $ctr_kredit_saldo_akhir = 0;

                            foreach($list_coa as $row){

                              //query list total mutasi debet kredit 01 Januari sampai tanggal awal filter
                              $sql_saldo_mutasi = $this->db->query("SELECT sum(debet) as totald, sum(kredit) as totalk FROM public.beone_gl WHERE gl_date BETWEEN '2019-01-01' AND '$tgl_awal_formated' AND coa_id =".intval($row['coa_id']));
                              $hasil_saldo_mutasi = $sql_saldo_mutasi->row_array();


                              //mencari rumus posisi debet kredit dari tipe coa
                              $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id =".intval($row['coa_id']));
                              $hasil_posisi_dk = $sql_posisi_dk->row_array();

                              if ($hasil_posisi_dk['dk'] == "D"){
                                  $total_debet_mutasi = $hasil_saldo_mutasi['totald'] - $hasil_saldo_mutasi['totalk'];
                                  $total_kredit_mutasi = 0;
                              }elseif($hasil_posisi_dk['dk'] == "K"){
                                  $total_debet_mutasi = 0;
                                  $total_kredit_mutasi = $hasil_saldo_mutasi['totalk'] - $hasil_saldo_mutasi['totald'];
                              }


                            ?>
                              <tr>
                                  <td> <?php echo $row['nomor'];?> </td>
                                  <td> <?php echo $row['nama'];?> </td>
                                  <td> <?php echo number_format($row['debet_idr']+$total_debet_mutasi,2);?> </td>
                                  <td class="hidden-xs"> <?php echo number_format($row['kredit_idr']+$total_kredit_mutasi,2);?> </td>

                                  <?php
                                    //mengambil nilai total debit kredit akun
                                    $sql_total_mutasi_tgl = $this->db->query("SELECT SUM(debet) as debett, SUM(kredit) as kreditt FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND coa_id =".intval($row['coa_id']));
                                    $hasil_total_mutasi_tgl = $sql_total_mutasi_tgl->row_array();
                                  ?>

                                  <td class="hidden-xs"> <?php echo number_format($hasil_total_mutasi_tgl['debett'],2);?> </td>
                                  <td class="hidden-xs"> <?php echo number_format($hasil_total_mutasi_tgl['kreditt'],2);?> </td>

                                  <?php
                                      $saldo_akhir_akun_debet = (($row['debet_idr']+$total_debet_mutasi) - ($row['kredit_idr']+$total_kredit_mutasi)) + ($hasil_total_mutasi_tgl['debett'] - $hasil_total_mutasi_tgl['kreditt']);
                                      $saldo_akhir_akun_kredit = (($row['kredit_idr']+$total_kredit_mutasi) - ($row['debet_idr']+$total_debet_mutasi)) + ($hasil_total_mutasi_tgl['kreditt'] - $hasil_total_mutasi_tgl['debett']);

                                      if ($hasil_posisi_dk['dk'] == "D"){
                                          $s_akhir_debet = $saldo_akhir_akun_debet;
                                          $s_akhir_kredit = 0;
                                      }elseif($hasil_posisi_dk['dk'] == "K"){
                                          $s_akhir_debet = 0;
                                          $s_akhir_kredit = $saldo_akhir_akun_kredit;
                                      }
                                  ?>

                                  <td> <?php echo number_format($s_akhir_debet,2); ?> </td>
                                  <td> <?php echo number_format($s_akhir_kredit,2); ?></td>
                              </tr>
                            <?php

                                $ctr_debet_saldo_awal = $ctr_debet_saldo_awal + ($row['debet_idr']+$total_debet_mutasi);
                                $ctr_kredit_saldo_awal = $ctr_kredit_saldo_awal + ($row['kredit_idr']+$total_kredit_mutasi);
                                $ctr_debet_saldo_mutasi = $ctr_debet_saldo_mutasi + $hasil_total_mutasi_tgl['debett'];
                                $ctr_kredit_saldo_mutasi = $ctr_kredit_saldo_mutasi + $hasil_total_mutasi_tgl['kreditt'];
                                $ctr_debet_saldo_akhir = $ctr_debet_saldo_akhir + $s_akhir_debet;
                                $ctr_kredit_saldo_akhir = $ctr_kredit_saldo_akhir + $s_akhir_kredit;
                              }
                            ?>

                              <tr>
                                  <td></td>
                                  <td></td>
                                  <td><b><?php echo number_format($ctr_debet_saldo_awal);?></b></td>
                                  <td><b><?php echo number_format($ctr_kredit_saldo_awal);?></b></td>
                                  <td><b><?php echo number_format($ctr_debet_saldo_mutasi);?></b></td>
                                  <td><b><?php echo number_format($ctr_kredit_saldo_mutasi);?></b></td>
                                  <td><b><?php echo number_format($ctr_debet_saldo_akhir);?></b></td>
                                  <td><b><?php echo number_format($ctr_kredit_saldo_akhir);?></b></td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
