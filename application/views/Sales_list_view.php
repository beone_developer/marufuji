<h3>List Penjualan</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th width='15%'><center>No Penjualan</center></th>
              <th width='25%'><center>Customer</center></th>
              <th width='15%'><center>Tanggal</center></th>
              <th width='25%'><center>Keterangan</center></th>
              <th width='20%'><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_sales_header as $row){ ?>
            <tr>
                <td><small><center><?php echo $row['sales_no'];?></center></small></td>
                <td><small><?php echo $row['ncustomer'];?></small></td>
                <td><small><center><?php echo $row['trans_date'];?></center></small></td>
                <td><small><?php echo $row['keterangan'];?></small></td>
                <?php
                  $sales_no = str_replace("/", "-", $row['sales_no']); //konfersi karena akan dianggap parameter
                ?>
                <td><center>
                    <?php if(helper_security("penjualan_edit") == 1){?>
                    <a href='<?php echo base_url('Sales_controller/edit/'.$row['sales_header_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i> </a>
                    <?php }?>
                    <?php if(helper_security("penjualan_delete") == 1){?>
                    <a href="javascript:dialogHapus('<?php echo base_url('Sales_controller/delete/'.$row['sales_header_id'].'/'.$sales_no.'');?>')" class='btn red'><i class="fa fa-trash-o"></i> </a>
                    <?php }?>
                    <a href='<?php echo base_url('Sales_controller/sales_print/'.$row['sales_header_id'].'');?>' class='btn yellow'><i class="fa fa-print"></i> </a>
                </center></td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
