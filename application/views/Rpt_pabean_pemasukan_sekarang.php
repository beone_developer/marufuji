<?php
//$awal = helper_tanggalinsert($_GET['tglawal']);
//$akhir = helper_tanggalinsert($_GET['tglakhir']);

$hari_ini = date("Y-m-d");
$kurang30 = date('Y-m-d', strtotime('-30 days', strtotime($hari_ini)));

$awal = $kurang30;
$akhir = $hari_ini;
?>

<h3>Report Pabean Pemasukan</h3>
<h4>Periode <?php echo $awal;?> Sampai <?php echo $akhir;?></h4>
<div class="portlet light bordered">
  <!--<div class="portlet-title">
      <div class="tools"> </div>
  </div>-->
  <a href="<?php echo base_url('Bc_controller/Print_pabean_pemasukan?tglawal='.$awal.'&tglakhir='.$akhir.''); ?>" class="btn btn-lg blue hidden-print margin-bottom-5"> Print
      <i class="fa fa-print"></i>
  </a>

<table class="table table-striped table-bordered table-hover" id="sample_2">
        <thead>
          <tr>
              <th colspan="1"></th>
              <th colspan="1"></th>
              <th colspan="2"><center>Doc</center></th>
              <th colspan="2"><center>Received</center></th>
              <th colspan="1"></th>
              <th colspan="2"><center>Item</center></th>
              <th colspan="4"></th>
          </tr>
          <tr>
              <th width="5%">No</th>
							<th width="5%">BC</th>
              <th width="10%">No</th>
              <th width="10%">Date</th>
              <th width="10%">No</th>
              <th width="10%">Date</th>
              <th width="15%">Supplier</th>
              <th width="5%">Kode</th>
              <th width="10%">Nama</th>
              <th width="5%">Sat</th>
              <th width="5%">Qty</th>
              <th width="5%">Val</th>
							<th width="10%">Jml</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $sql_received = $this->db->query("SELECT tpb_header_id, nomor_aju, nomor_received, tanggal_received, kurs FROM public.beone_received_import WHERE tanggal_received between '$awal' and '$akhir' ORDER BY tanggal_received ASC");
          $no = 0;
          foreach($sql_received->result_array() as $row_received){
            $no_received = $row_received['nomor_received'];
            $tgl_received = $row_received['tanggal_received'];
            $kurs = $row_received['kurs'];

                $this->mysql = $this ->load -> database('mysql', TRUE);
                $sql = $this->db->query("SELECT v.intvent_trans_no, v.item_id, i.nama, i.item_code, v.qty_in, v.value_in, s.satuan_code FROM public.beone_inventory v INNER JOIN public.beone_item i ON v.item_id = i.item_id INNER JOIN public.beone_satuan_item s ON s.satuan_id = i.satuan_id WHERE v.intvent_trans_no = '$row_received[nomor_aju]'");


                $tpb_header = $this->mysql->query("select kode_dokumen_pabean, nomor_daftar, tanggal_daftar, harga_penyerahan, harga_invoice from tpb_header where id = ".intval($row_received['tpb_header_id']));
        				$hasil_tpb_header = $tpb_header->row_array();

                $supplier_import = $this->db->query("SELECT c.nama FROM public.beone_hutang_piutang p INNER JOIN public.beone_custsup c ON p.custsup_id = c.custsup_id WHERE p.keterangan = 'IMPOR' AND p.nomor = '$row_received[nomor_aju]'");
        				$hasil_supplier_import = $supplier_import->row_array();

                  		foreach($sql->result_array() as $row){
                        $no = $no + 1;
                        $kode01 = substr($hasil_tpb_header['kode_dokumen_pabean'],0,1);
                        $kode02 = substr($hasil_tpb_header['kode_dokumen_pabean'],1,1);

                        if ($hasil_tpb_header['kode_dokumen_pabean'] == '23'){
                            $jml_value = ($row['value_in'] / $kurs) * $row['qty_in'];
                        }else if($hasil_tpb_header['kode_dokumen_pabean'] == '40'){
                            $jml_value = $row['value_in'];
                        }

                        /*$detail_item = $this->db->query("SELECT * FROM public.beone_item WHERE item_code = '$row[kode_barang]'");
                        $hasil_item = $detail_item->row_array();
                    		$hasil_item_id = $hasil_item['item_id'];*/

                        /*$detail_qty = $this->db->query("SELECT * FROM public.beone_inventory WHERE intvent_trans_no = '$row_received[nomor_aju]' AND keterangan = 'IMPOR'");
                        $hasil_qty = $detail_qty->row_array();
                    		$hasil_qty_terima = $hasil_qty['qty_in'];*/
					?>
          <tr>
              <td><center><small><?php echo $no;?></small></center></td>
              <td><small><?php echo "BC ".$kode01.'. '.$kode02;?></small></td>
              <td><small><?php echo $hasil_tpb_header['nomor_daftar'];?></small></td>
              <td><small><?php echo date('d-m-Y',strtotime($hasil_tpb_header['tanggal_daftar']));?></small></td>
              <td><small><?php echo $no_received?></small></td>
              <td><small><?php echo $tgl_received;?></small></td>

              <td><small><?php echo $hasil_supplier_import['nama'];?></small></td>

              <td><small><?php echo $row['item_code'];?></small></td>
              <td><small><?php echo $row['nama'];?></small></td>
              <td><small><?php echo $row['satuan_code'];?></small></td>
              <td><small><?php echo $row['qty_in'];?></small></td>

              <?php if ($hasil_tpb_header['kode_dokumen_pabean'] == '40'){?>
              <td><small><?php echo 'IDR';?></small></td>
              <?php }else{?>
              <td><small><?php echo 'USD';?></small></td>
              <?php }?>

              <td><small><?php echo number_format($jml_value,2);?></small></td>

          </tr>
            <?php
              }
            }
            ?>
        </tbody>
    </table>
</div>
