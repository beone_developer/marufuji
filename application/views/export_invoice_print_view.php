        <!-- END PAGE HEADER-->
        <?php
          foreach($data_export as $row){
            $invoice_no = $row['invoice_no'];
            $tgl_invoice = $row['invoice_date'];
            $nama_customer = $row['nama'];
            $alamat = $row['alamat'];
            $sailing_date = $row['surat_jalan_date'];
            $vessel = $row['vessel'];
            $port_loading = $row['port_loading'];
            $port_destination = $row['port_destination'];
            $container = $row['container'];
            $no_container = $row['no_container'];
            $no_seal = $row['no_seal'];
          }
        ?>

        <div class="invoice">
            <center>
            <table>
                <td>
                  <img src="<?php echo base_url();?>assets/global/img/Logo PO Tarindo.png">&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
                <td>
                  <center><h2 style="color:#191970;"><b>PT. TARINDO UNIMETAL UTAMA<b></h2></center>
                  <center style="color:#191970;">Dismantling & Smelter of non-ferrous metals</center>
                  <center style="color:#FF0000;">Kawasan Berikat Ngoro Industri Persada Kav F-8,</center>
                  <center style="color:#FF0000;">Ds./kec, Ngoro, Mojokerto 61385, Jawa Timur - Indonesia</center>
                </td>
            </table>
          </center>

            <hr/ style="border-color:#FF0000;">
            <br />

            <center><h3><b>PACKING LIST</b></h3></center>

            <br />
            <br />
            <div class="row">
                <div class="col-xs-2">
                    <ul class="list-unstyled">
                        <li> <h4> Invoice no </h4> </li>
                        <br />
                        <li> <h4> Messrs </h4> </li>
                        <li> <h4>  </h4> </li>
                        <br />
                        <br />
                        <li> <h4> Sailling Date </h4> </li>
                        <br />
                        <li> <h4> Name of Vessel </h4> </li>
                        <br />
                        <li> <h4> Port of Loading </h4> </li>
                        <br />
                        <li> <h4> Port of Destination </h4> </li>
                    </ul>
                </div>
                <div class="col-xs-6">
                    <ul class="list-unstyled">

                        <li><h4>: <?php echo $invoice_no;?> </h4> </li>
                        <br />
                        <li><h4>: <?php echo $nama_customer;?> </h4> </li>
                        <li><h4>: <?php echo $alamat;?> </h4> </li>
                        <br />
                        <li><h4>: <?php echo $sailing_date;?> </h4> </li>
                        <br />
                        <li><h4>: <?php echo $vessel;?> </h4> </li>
                        <br />
                        <li><h4>: <?php echo $port_loading;?> </h4> </li>
                        <br />
                        <li><h4>: <?php echo $port_destination;?> </h4> </li>
                    </ul>
                </div>

                <div class="col-xs-2">
                  <ul class="list-unstyled">
                    <li> <h4> Date </h4> </li>
                  </ul>
                </div>

                <div class="col-xs-2">
                  <ul class="list-unstyled">
                    <li><h4>: <?php echo $tgl_invoice;?> </h4> </li>
                  </ul>
                </div>


            </div>
            <div class="row">
                <div class="col-xs-12">
                </div>
            </div>
            <br />
            <br />
            <br />
<!------------------------------------------------------------------------->
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th class="hidden-xs"> Marks / Container .no </th>
                                <th class="hidden-xs"> Description of Goods </th>
                                <th class="hidden-xs"> Quantity </th>
                                <th class="hidden-xs"> <center> Gross Weight <br /> KG </center></th>
                                <th class="hidden-xs"> <center> Netto Weight <br /> KG </center></th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                          $total_packing = 0;
                          $total_brutto = 0;
                          $total_netto = 0;
                          foreach($data_export as $row){
                            $total_packing = $total_packing + $row['satuan_pack'];
                            $total_brutto = $total_brutto + $row['brutto'];
                            $total_netto = $total_netto + $row['netto'];
                          ?>

                                  <tr>
                                    <td></td>
                                    <td class="hidden-xs"> <?php echo $row['nitem'];?> </td>
                                    <td class="hidden-xs"> <?php echo $row['satuan_pack']." Bags";?> </td>
                                    <td class="hidden-xs"> <center><?php echo number_format($row['brutto'], 2);?> </center></td>
                                    <td class="hidden-xs"> <center><?php echo number_format($row['netto'], 2);?> </center></td>
                                  <tr>
                          <?php
                              }
                          ?>
                                  <tr>
                                    <td></td>
                                    <td class="hidden-xs"><b>TOTAL</b></td>
                                    <td class="hidden-xs"><b><?php echo number_format($total_packing, 2)." Bags";?></b></td>
                                    <td class="hidden-xs"><center><b> <?php echo number_format($total_brutto, 2);?> <b/></center></td>
                                    <td class="hidden-xs"><center><b> <?php echo number_format($total_netto, 2);?></b></center></td>
                                  <tr>
                        </tbody>
                    </table>
                </div>
            </div>
<!------------------------------------------------------------------------->

            <br />
            <br />
            <br />

            <div class="row">
                <div class="col-xs-2">
                    <ul class="list-unstyled">
                        <li> <h4> Packing List </h4> </li>
                        <br />
                        <li> <h4> Origin </h4> </li>
                        <br />
                        <li> <h4> Term </h4> </li>
                    </ul>
                </div>
                <div class="col-xs-6">
                    <ul class="list-unstyled">

                        <li><h4>: <?php echo "In Bag";?> </h4> </li>
                        <br />
                        <li><h4>: <?php echo "Indonesia";?> </h4> </li>
                        <br />
                        <li><h4>: <?php echo "T/T";?> </h4> </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                  <ul class="list-unstyled">
                  <li> <h4> Loaded Into Closed (<?php echo $container;?>') Container :  </h4> </li>
                  </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-2">
                  <ul class="list-unstyled">
                  <li> <h4> Cont. No.</h4> </li>
                  <li> <h4> <?php echo $no_container;?></h4></li>
                  </ul>
                </div>
                <div class="col-xs-2">
                  <ul class="list-unstyled">
                  <li> <h4> Seal. No.</h4> </li>
                  <li> <h4> <?php echo $no_seal;?> </h4></li>
                  </ul>
                </div>
            </div>


            <br />
            <br />
            <div class="row">
              <div class="col-xs-6"></div>
                <div class="col-xs-3">
                      <h4>PT. Tarindo Unimetal Utama,</h4>
                      <br/>
                      <br/>
                      <br />
                      <hr/ style="border-color:black;">
                </div>
            </div>
            <div class="row">
              <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();"> Print
                  <i class="fa fa-print"></i>
              </a>
            </div>
        </div>
