<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Form Kas & Bank</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">
          <form role="form" method="get">
              <div class="form-body">
                <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                            <label>Tanggal Voucher</label>
                            <div class="input-group">
                              <span class="input-group-addon input-circle-left">
                                  <i class="fa fa-calendar"></i>
                              </span>
                              <input class="form-control form-control-inline input-medium date-picker input-circle-right" name="tanggal_voucher" size="16" type="text" value="<?php echo date('m/d/Y');?>" readonly>
                              <span class="help-block"></span>
                            </div>
                      </div>

                      <div class="form-group">
                          <label>Akun Kas Bank</label>
                          <div class="input-group">
                             <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="coa_id_cash_bank" required>
                               <?php 	foreach($list_coa_kas_bank as $row){ ?>
                                 <option value="<?php echo $row['coa_id'];?>"><?php echo $row['nomor']." | ".$row['nama'];?></option>
                               <?php } ?>
                             </select>
                       </div>
                     </div>

                     <div class="form-group">
                     <label>Keterangan</label>
                     <input type="text" class="form-control" placeholder="Keterangan Voucher" name="keterangan_header" id="keterangan_header" required>
                     </div>

                      <div class="form-group">
                            <label>Tipe Voucher</label>
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="tipe_voucher" id="optionsRadios4" value="1" checked> Debit
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="tipe_voucher" id="optionsRadios5" value="0"> Credit
                                    <span></span>
                                </label>
                            </div>
                        </div>

                          <!--<div name="txtSatuan" id="txtSatuan"><b>Nomor Voucher</b></div>-->

                  </div>


                  <!--<div class="col-sm-8" id="detail_cash_bank"></div>-->

              </div>
            </div>

            <div class="form-actions">
                <button type="button" class="btn default">Cancel</button>
                <?php if(helper_security("kas_bank_add") == 1){?>
                <button type="submit" class="btn red" name="submit_header">Next</button>
                <?php }?>
            </div>
          </form>
      </div>
    </div>
