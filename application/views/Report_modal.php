<?php
  $tgl_awal = $_GET['tglawal'];
  $tgl_akhir = $_GET['tglakhir'];

  $thn_awal = substr($tgl_awal,6,4);
  $bln_awal = substr($tgl_awal,0,2);
  $day_awal = substr($tgl_awal,3,2);
  $tgl_awal_formated = $thn_awal."-".$bln_awal."-".$day_awal;

  $thn_akhir = substr($tgl_akhir,6,4);
  $bln_akhir = substr($tgl_akhir,0,2);
  $day_akhir = substr($tgl_akhir,3,2);
  $tgl_akhir_formated = $thn_akhir."-".$bln_akhir."-".$day_akhir;

  $tgl_awal_tahun = $thn_awal."-01-"."01";

  $cetak_bln_akhir = "";
  if ($bln_akhir == "01"){
      $cetak_bln_akhir = "JANUARI";
  }elseif($bln_akhir == "02"){
      $cetak_bln_akhir = "FEBRUARI";
  }elseif($bln_akhir == "03"){
      $cetak_bln_akhir = "MARET";
  }elseif($bln_akhir == "04"){
      $cetak_bln_akhir = "APRIL";
  }elseif($bln_akhir == "05"){
      $cetak_bln_akhir = "MEI";
  }elseif($bln_akhir == "06"){
      $cetak_bln_akhir = "JUNI";
  }elseif($bln_akhir == "07"){
      $cetak_bln_akhir = "JULI";
  }elseif($bln_akhir == "08"){
      $cetak_bln_akhir = "AGUSTUS";
  }elseif($bln_akhir == "09"){
      $cetak_bln_akhir = "SEPTEMBER";
  }elseif($bln_akhir == "10"){
      $cetak_bln_akhir = "OKTOBER";
  }elseif($bln_akhir == "11"){
      $cetak_bln_akhir = "NOVEMBER";
  }elseif($bln_akhir == "12"){
      $cetak_bln_akhir = "DESEMBER";
  }


?>

          <h1>
              <center><b>Report Perubahan Modal</b></center>
          </h1>
          <hr />

          <div class="row">
              <div class="col-xs-12">
                  <table class="table table-striped table-hover">
                      <thead>
                          <tr>
                              <th><b>KETERANGAN</b></th>
                              <th><b>MODAL SAHAM</b></th>
                              <th><b>SALDO LABA</b></th>
                              <th><b>TOTAL EKUITAS</b></th>
                          </tr>
                      </thead>
                      <tbody>

                      <?php
                          /************ Saldo Akhir Akun MODAL DISETOR (COA + TOTAL MUTASI) **************/

                          //SALDO COA MODAL DISETOR
                          $coa_modal_disetor = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_transaksi = 3");
                          $hasil_coa_modal_disetor = $coa_modal_disetor->row_array();

                          $no_coa_modal_disetor = $hasil_coa_modal_disetor['nomor'];

                          //TOTAL MUTASI MODAL DISETOR
                          $modal_disetor = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_akhir_formated' AND coa_no = '$no_coa_modal_disetor'");
                          $sa_modal_disetor = $modal_disetor->row_array();

                          /************ END Saldo Akhir Akun MODAL DISETOR (COA + TOTAL MUTASI) **************/

                          //SALDO COA (LABA) / RUGI TAHUN LALU
                          $coa_laba_rugi_lalu = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_transaksi = 4");
                          $laba_rugi_thn_lalu = $coa_laba_rugi_lalu->row_array();

                          $total_equitas_sa = $hasil_coa_modal_disetor['kredit_idr'] + $laba_rugi_thn_lalu['kredit_idr'];

                          //TOTAL MUTASI DEFIDEN
                          $mutasi_labar_rugi = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_akhir_formated' AND coa_no = '$no_coa_modal_disetor'");
                          $hasil_mutasi_laba_rugi = $mutasi_labar_rugi->row_array();

/*********************************************** FUNCTION LABA RUGI******************************************************************************/
                                    // RUMUS LABA RUGI KOTOR = PENJUALAN - HPP
                                    // RUMUS LABA RUGI USAHA = LABA RUGI KOTOR - BIAYA ADMIN DAN UMUM
                                    // RUMUS LABA RUGI BERSIH = LABA RUGI USAHA + PENDAPATAN LAIN

                                    /************************************* PENJUALAN ********************************************************/
                                    $total_penjualan = 0; //TOTAL PENJUALAN = SALDO AWAL AKUN PENJALAN COA + MUTASI AKUN PENJUALAN DI GENERAL LEDGER

                                    foreach($saldo_awal_coa_penjualan as $row){
                                        //nilai Saldo Awal dari COA Akun PENJUALAN
                                        $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id =".intval($row['coa_id']));
                                        $hasil_posisi_dk = $sql_posisi_dk->row_array();

                                        //nilai Mutasi Akun Penjualan di General Ledger
                                        $sql_total_mutasi_dk = $this->db->query("SELECT SUM(debet) as totmdebet, SUM(kredit) as totmkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND coa_id =".intval($row['coa_id']));
                                        $hasil_total_mutasi_dk = $sql_total_mutasi_dk->row_array();

                                        if ($hasil_posisi_dk['dk'] == "D"){
                                            $saldo = $hasil_total_mutasi_dk['totmdebet'] - $hasil_total_mutasi_dk['totmkredit'];
                                        }elseif($hasil_posisi_dk['dk'] == "K"){
                                            $saldo = $hasil_total_mutasi_dk['totmkredit'] - $hasil_total_mutasi_dk['totmdebet'];
                                        }

                                        //HASIL TOTAL PENJUALAN
                                        $total_penjualan = $total_penjualan + $saldo;
                                      }
                                    /************************************* END PENJUALAN ********************************************************/

                                    /***************************************************** TOTAL DEBET KREDIT AKUN HPP *********************************************************/
                                          //Tidak menggunakan foreach karena hanya menyorot 1 Akun (Akun HPP hanya ada 1)
                                          $total_hpp = 0;

                                          //posisi Debet Kredit Akun HPP
                                          $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE nomor = '500-00'");
                                          $hasil_posisi_dk = $sql_posisi_dk->row_array();

                                          //nilai Mutasi Akun HPP di General Ledger
                                          $sql_total_mutasi_dk = $this->db->query("SELECT SUM(debet) as totmdebet, SUM(kredit) as totmkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND coa_id =".intval($hasil_posisi_dk['coa_id']));
                                          $hasil_total_mutasi_dk = $sql_total_mutasi_dk->row_array();

                                          if ($hasil_posisi_dk['dk'] == "D"){
                                              $saldo_HPP = $hasil_total_mutasi_dk['totmdebet']-$hasil_total_mutasi_dk['totmkredit'];
                                          }elseif($hasil_posisi_dk['dk'] == "K"){
                                              $saldo_HPP = $hasil_total_mutasi_dk['totmkredit']-$hasil_total_mutasi_dk['totmdebet'];
                                          }
                                    /***************************************************** END TOTAL DEBET KREDIT AKUN HPP *********************************************************/

                                          $rugi_laba_kotor = $total_penjualan - $saldo_HPP;

                                    /****************************************** ADMIN BIAYA UMUM ************************************/
                                          $total_biaya_admin_umum = 0;

                                          foreach($list_coa_biaya_admin_umum as $row){
                                            //posisi Debet Kredit Akun HPP
                                            $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id =".intval($row['coa_id']));
                                            $hasil_posisi_dk = $sql_posisi_dk->row_array();

                                            //nilai Mutasi Akun BIAYA ADMIN DAN UMUM di General Ledger
                                            $sql_dk_admin_umum = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND coa_id =".intval($row['coa_id']));
                                            $hasil_dk_admin_umum = $sql_dk_admin_umum->row_array();

                                            if ($hasil_posisi_dk['dk'] == "D"){
                                                $saldo_admin = $hasil_dk_admin_umum['totdebet'] - $hasil_dk_admin_umum['totkredit'];
                                            }elseif($hasil_posisi_dk['dk'] == "K"){
                                                $saldo_admin = $hasil_dk_admin_umum['totkredit'] - $hasil_dk_admin_umum['totdebet'];
                                            }
                                          $total_biaya_admin_umum = $total_biaya_admin_umum + $saldo_admin;
                                          }
                                    /****************************************** ADMIN BIAYA UMUM ************************************/

                                          $rugi_laba_usaha = $rugi_laba_kotor - $total_biaya_admin_umum;

                                    //**************************************** PENDAPATAN BIAYA LAIN ***************************
                                    $total_pendapatan_biaya_lain = 0;


                                    foreach($list_coa_pendapatan_lain as $row){
                                      //posisi Debet Kredit Akun PENDAPATAN LAIN LAIN
                                      $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id =".intval($row['coa_id']));
                                      $hasil_posisi_dk = $sql_posisi_dk->row_array();

                                      //nilai Mutasi Akun PENDAPATAN LAIN LAIN di General Ledger
                                      $sql_dk_pendapatan = $this->db->query("SELECT SUM(debet) as totpdebet, SUM(kredit) as totpkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND coa_id =".intval($row['coa_id']));
                                      $hasil_dk_pendapatan = $sql_dk_pendapatan->row_array();

                                      if ($hasil_posisi_dk['dk'] == "D"){
                                            $saldo_pendapatan = $hasil_dk_pendapatan['totpdebet'] - $hasil_dk_pendapatan['totpkredit'];
                                      }elseif($hasil_posisi_dk['dk'] == "K"){
                                          $saldo_pendapatan = $hasil_dk_pendapatan['totpkredit'] - $hasil_dk_pendapatan['totpdebet'];
                                      }

                                      $total_pendapatan_biaya_lain = $total_pendapatan_biaya_lain + $saldo_pendapatan;
                                    }
                                    //******************************************* END PENDAPATAN BIAYA LAIN ***************************

                                      $laba_rugi_bersih_usaha = $rugi_laba_usaha + $total_pendapatan_biaya_lain;
/*********************************************** END FUNCTION LABA RUGI******************************************************************************/

                    $total_modal_saham = $hasil_coa_modal_disetor['kredit_idr']+$sa_modal_disetor['totkredit'];
                    $total_saldo_laba_rugi = ($laba_rugi_thn_lalu['kredit_idr']-$hasil_mutasi_laba_rugi['totdebet'])+$laba_rugi_bersih_usaha;
                    $total_equitas = (($total_equitas_sa + $sa_modal_disetor['totkredit'])-$hasil_mutasi_laba_rugi['totdebet'])+$laba_rugi_bersih_usaha;

              ?>
                              <tr>
                                  <td><?php echo "SALDO AWAL, 01 JANUARI ".$thn_awal; ?></td>
                                  <td><?php echo number_format($hasil_coa_modal_disetor['kredit_idr'],2);?></td>
                                  <td><?php echo number_format($laba_rugi_thn_lalu['kredit_idr'],2);?></td>
                                  <td><?php echo number_format($total_equitas_sa,2);?></td>
                              </tr>
                              <tr>
                                  <td><?php echo "PENAMBAHAN MODAL ".$thn_awal; ?></td>
                                  <td><?php echo number_format($sa_modal_disetor['totkredit'],2);?></td> <!--selalu posisi kredit--->
                                  <td><?php echo number_format(0,2);?></td>
                                  <td><?php echo number_format($sa_modal_disetor['totkredit'],2);?></td>
                              </tr>
                              <tr>
                                  <td><?php echo "PEMBAGIAN DEVIDEN ".$thn_awal; ?></td>
                                  <td><?php echo number_format(0,2);?></td> <!--selalu posisi kredit--->
                                  <td><?php echo number_format($hasil_mutasi_laba_rugi['totdebet'],2);?></td>
                                  <td><?php echo number_format($hasil_mutasi_laba_rugi['totdebet'],2);?></td>
                              </tr>
                              <tr>
                                  <td><?php echo "LABA BERSIH ".$thn_awal; ?></td>
                                  <td><?php echo number_format(0,2);?></td> <!--selalu posisi kredit--->
                                  <td><?php echo number_format($laba_rugi_bersih_usaha,2);?></td>
                                  <td><?php echo number_format($laba_rugi_bersih_usaha,2);?></td>
                              </tr>
                              <tr>
                                  <td><?php echo "SALDO AKHIR ,".$day_akhir." ".$cetak_bln_akhir." ".$thn_akhir; ?></td>
                                  <td><?php echo number_format($total_modal_saham,2);?></td> <!--selalu posisi kredit--->
                                  <td><?php echo number_format($total_saldo_laba_rugi,2);?></td>
                                  <td><?php echo number_format($total_equitas,2);?></td>
                              </tr>
                      </tbody>
                  </table>

              </div>
          </div>
