<?php
  $tgl = $_GET['tgl'];

  $thn_awal = substr($tgl,6,4);
  $bln_awal = substr($tgl,0,2);
  $day_awal = substr($tgl,3,2);
  $tgl_formated = $thn_awal."-".$bln_awal."-".$day_awal;

  if (intval($bln_awal) == 1){
    $txtBulan = "JANUARI";
  }elseif (intval($bln_awal) == 2) {
    $txtBulan = "FEBRUARI";
  }elseif (intval($bln_awal) == 3) {
    $txtBulan = "MARET";
  }elseif (intval($bln_awal) == 4) {
    $txtBulan = "APRIL";
  }elseif (intval($bln_awal) == 5) {
    $txtBulan = "MEI";
  }elseif (intval($bln_awal) == 6) {
    $txtBulan = "JUNI";
  }elseif (intval($bln_awal) == 7) {
    $txtBulan = "JULI";
  }elseif (intval($bln_awal) == 8) {
    $txtBulan = "AGUSTUS";
  }elseif (intval($bln_awal) == 9) {
    $txtBulan = "SEPTEMBER";
  }elseif (intval($bln_awal) == 10) {
    $txtBulan = "OKTOBER";
  }elseif (intval($bln_awal) == 11) {
    $txtBulan = "NOVEMBER";
  }elseif (intval($bln_awal) == 12) {
    $txtBulan = "DESEMBER";
  }

  $tgl_awal_tahun = $thn_awal."-01-"."01";
?>

  <h1>
      <center><b>Report HPP</b></center>
  </h1>
  <hr />

      <div class="row">
          <div class="col-xs-12">
              <table class="table table-striped table-hover">
                  <thead>
                      <tr>
                          <th><b></b></th>
                          <th><b></b></th>
                          <th><b></b></th>
                      </tr>
                  </thead>

                  <tbody>
<!------------------------------------- PERSEDIAAN BAHAN BAKU AWAL -------------------------------------------------------------->
                    <?php
                          $sql_sawal = $this->db->query("SELECT * FROM public.beone_coa WHERE nomor = '116-01'");
                          $hasil_sawal = $sql_sawal->row_array();

                          $hasil_saldo_bahan_baku = $hasil_sawal['debet_idr'] - $hasil_sawal['kredit_idr'];
                    ?>
                    <tr>
                          <td>PERSEDIAAN BAHAN BAKU AWAL</td>
                          <td><?php echo number_format($hasil_saldo_bahan_baku, 2);?></td>
                          <td></td>
                    </tr>
<!----------------------------------------- PERSEDIAAN BAHAN BAKU AWAL -------------------------------------------------------------->

<!------------------------------------- PEMBELIAN BAHAN BAKU -------------------------------------------------------------->
                    <?php
                          $sql = $this->db->query("SELECT SUM(debet) as pembelian_bahan_baku FROM public.beone_gl WHERE coa_no = '116-01' AND gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated'");
                          $hasil_pembelian_bahan_baku = $sql->row_array();
                    ?>
                    <tr>
                          <td>PEMBELIAN BAHAN BAKU</td>
                          <td><?php echo number_format($hasil_pembelian_bahan_baku['pembelian_bahan_baku'], 2);?></td>
                          <td></td>
                    </tr>
<!----------------------------------------- PEMBELIAN BAHAN BAKU -------------------------------------------------------------->
                    <tr>
                          <?php
                              $barang_tersedia = $hasil_saldo_bahan_baku + $hasil_pembelian_bahan_baku['pembelian_bahan_baku'];
                          ?>
                          <td><b>BARANG TERSEDIA</b></td>
                          <td><b><?php echo number_format($barang_tersedia, 2);?></b></td>
                    </tr>

<!------------------------------------- PERSEDIAAN BAHAN BAKU AKHIR -------------------------------------------------------------->
                    <?php
                          $sql = $this->db->query("SELECT SUM(debet) as saldo_debet FROM public.beone_gl WHERE coa_no = '116-01' AND gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated'");
                          $hasil_debet = $sql->row_array();

                          $sql2 = $this->db->query("SELECT SUM(kredit) as saldo_kredit FROM public.beone_gl WHERE coa_no = '116-01' AND gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated'");
                          $hasil_kredit = $sql2->row_array();

                          $persediaan_bahan_baku_akhir = $hasil_debet['saldo_debet'] -  $hasil_kredit['saldo_kredit'];
                    ?>
                    <tr>
                          <td>PERSEDIAAN BAHAN BAKU AKHIR</td>
                          <td><?php echo number_format($persediaan_bahan_baku_akhir, 2);?></td>
                          <td></td>
                    </tr>
<!----------------------------------------- PERSEDIAAN BAHAN BAKU AKHIR -------------------------------------------------------------->

                    <tr>
                          <?php
                              $pemakaian_bahan_baku = $barang_tersedia - $persediaan_bahan_baku_akhir;
                          ?>
                          <td><b>PEMAKAIAN BAHAN BAKU</b></td>
                          <td><b><?php echo number_format($pemakaian_bahan_baku, 2);?></b></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>

<!------------------------------------- RINCIAN BIAYA -------------------------------------------------------------->
<tr>
    <td><b>RINCIAN BIAYA : </b></td>
    <td></td>
    <td></td>
</tr>
<?php
    $coa_biaya = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id BETWEEN 64 AND 77");
    $jml_total_rincian = 0 ;
    foreach($coa_biaya->result_array() as $row){
          $sql = $this->db->query("SELECT SUM(debet) as saldo_debet, SUM(kredit) as saldo_kredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id =".intval($row['coa_id']));
          $rincian_biaya = $sql->row_array();

          $total_rincian = $rincian_biaya['saldo_debet'] - $rincian_biaya['saldo_kredit'];

          echo "<tr>";
          echo "<td>".$row['nama']."</td>";
          echo "<td>".number_format($total_rincian, 2)."</td>";
          echo "<td></td>";
          echo "</tr>";

          $jml_total_rincian = $jml_total_rincian + $total_rincian;
    }
?>
<tr style:""></tr>
<tr>
    <td><b>TOTAL RINCIAN BIAYA : </b></td>
    <td><b><?php echo number_format($jml_total_rincian,2);?></b></td>
    <td></td>
</tr>
<!----------------------------------------- END RINCIAN BIAYA -------------------------------------------------------------->
<tr>
    <td></td>
    <td></td>
    <td></td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
      <?php
          $hp_produksi = $pemakaian_bahan_baku + $jml_total_rincian;
      ?>
      <td>HP PRODUKSI</td>
      <td><?php echo number_format($hp_produksi, 2);?></td>
</tr>

<!------------------------------------- PERSEDIAAN AWAL BARANG JADI -------------------------------------------------------------->
                    <?php
                          $sql_sawal = $this->db->query("SELECT * FROM public.beone_coa WHERE nomor = '116-08'");
                          $hasil_sawal = $sql_sawal->row_array();

                          $sawal_final = $hasil_sawal['debet_idr'] - $hasil_sawal['kredit_idr'];
                    ?>
                    <tr>
                          <td>PERSEDIAAN AWAL BARANG JADI</td>
                          <td><?php echo number_format($sawal_final, 2);?></td>
                          <td></td>
                    </tr>
<!----------------------------------------- END PERSEDIAAN AWAL BARANG JADI -------------------------------------------------------------->
<tr>
      <?php
          $barang_tersedia_untuk_dijual = $hp_produksi + $sawal_final;
      ?>
      <td><b>BARANG TERSEDIA UNTUK DIJUAL</b></td>
      <td><b><?php echo number_format($barang_tersedia_untuk_dijual, 2);?></b></td>
</tr>
<!------------------------------------- BARANG JADI AKHIR -------------------------------------------------------------->
                    <?php
                          $sql_sawal = $this->db->query("SELECT * FROM public.beone_coa WHERE nomor = '116-08'");
                          $hasil_sawal = $sql_sawal->row_array();

                          $sql = $this->db->query("SELECT SUM(debet) as saldo_debet FROM public.beone_gl WHERE coa_no = '116-08' AND gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated'");
                          $hasil_debet = $sql->row_array();

                          $sql2 = $this->db->query("SELECT SUM(kredit) as saldo_kredit FROM public.beone_gl WHERE coa_no = '116-08' AND gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated'");
                          $hasil_kredit = $sql2->row_array();

                          $sawal_final = $hasil_sawal['debet_idr'] - $hasil_sawal['kredit_idr'];
                          $barang_jadi_akhir = ($sawal_final + $hasil_debet['saldo_debet']) -  $hasil_kredit['saldo_kredit'];
                    ?>
                    <tr>
                          <td>BARANG JADI AKHIR</td>
                          <td><?php echo number_format($barang_jadi_akhir, 2);?></td>
                          <td></td>
                    </tr>
<!----------------------------------------- END BARANG JADI AKHIR -------------------------------------------------------------->
<tr>
      <?php
          $hpp = $barang_tersedia_untuk_dijual - $barang_jadi_akhir;
      ?>
      <td><b>HPP</b></td>
      <td><b><?php echo number_format($hpp, 2);?></b></td>
</tr>
                  </tbody>
              </table>

          </div>
      </div>
