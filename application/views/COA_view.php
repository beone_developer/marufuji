<form method="post" enctype="multipart/form-data">
		<input type="file" name="file">
		<input type="submit" name="preview" value="Import">
	</form>
<h3>Chart Of Account</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="caption font-dark">
          <a href='<?php echo base_url('COA_controller/add'); ?>' class='btn green'><i class="glyphicon glyphicon-plus"></i> ADD DATA</a>
      </div>
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th><center>Nomor</center></th>
              <th><center>Nama</center></th>
              <th><center>Debet Valas</center></th>
              <th><center>Debet IDR</center></th>
              <th><center>Kredit Valas</center></th>
              <th><center>Debet IDR</center></th>
              <th><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_coa as $row){ ?>
            <tr>
                <td><?php echo $row['nomor'];?></td>
                <td><?php echo $row['nama'];?></td>
                <td><?php echo number_format($row['debet_valas']);?></td>
                <td><?php echo number_format($row['debet_idr']);?></td>
                <td><?php echo number_format($row['kredit_valas']);?></td>
                <td><?php echo number_format($row['kredit_idr']);?></td>
                <td><a href='<?php echo base_url('COA_controller/edit/'.$row['coa_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i></a> <a href="javascript:dialogHapus('<?php echo base_url('COA_controller/delete/'.$row['coa_id'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i></a></td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
