<h3>Stock Opname <b><?=$tipe?></b></h3>
<div class="portlet light bordered">
  <div class="portlet-title">
    <form role="form" method="post">
            <div class="form-body">
              <div class="row">
                <div class="col-sm-4">
                    <label>No Opname</label>
                    <input type="text" class="form-control" placeholder="No Opname" id="no_opname" value="<?=isset($default['document_no'])? $default['document_no'] : ""?>" name="no_opname" required>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                          <label>Tanggal</label>
                          <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input class="form-control form-control-inline input-medium date-picker input-circle-right" name="tanggal" size="16" type="text" value="<?=isset($default['opname_date'])? helper_tanggalupdate($default['opname_date']) : date('m/d/Y')?>" readonly/>
                            <span class="help-block"></span>
                          </div>
                    </div>
                </div>
                <div class="col-sm-4"></div>
                </div>
                <div class="row">
                  <div class="col-sm-8">
                      <label>Keterangan</label>
                      <input type="text" class="form-control" placeholder="Keterangan" id="keterangan" value="<?=isset($default['keterangan'])? $default['keterangan'] : ""?>" name="keterangan" required>
                  </div>
                  <div class="col-sm-4"></div>
                </div>
            </div>
            <br />
            <div class="form-actions">
                <a href='<?php echo base_url('Item_controller');?>' class='btn default'> Cancel</a>
                <?php if(helper_security("stockopname_add") == 1){?>
                <button type="submit" class="btn blue" name="submit_opname">Submit</button>
                <?php }?>
            </div>
          </form>
      <hr />
      <br />

      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th><center>No Opname</center></th>
              <th><center>Tanggal</center></th>
              <th><center>Keterangan</center></th>
              <th><center>Total Item</center></th>
              <th><center>Sesuai</center></th>
              <th><center>Lebih</center></th>
              <th><center>Kurang</center></th>
              <th><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_stockopname as $row){ ?>
            <tr>
                <td><?php echo $row['document_no'];?></td>
                <td><?php echo $row['opname_date'];?></td>
                <td><?php echo $row['keterangan'];?></td>
                <td><?php echo number_format($row['total_item_opname'],2);?></td>
                <td><?php echo number_format($row['total_item_opname_match'],2);?></td>
                <td><?php echo number_format($row['total_item_opname_plus'],2);?></td>
                <td><?php echo number_format($row['total_item_opname_min'],2);?></td>
                <td>
                    <a href='<?php echo base_url('Stockopname_controller/Stockopname_detail/'.$row['opname_header_id'].'');?>' class='btn yellow'><i class='fa fa-eye'></i><small> </small></a>
                    <?php if(helper_security("stockopname_edit") == 1){?>
                    <a href='<?php echo base_url('Stockopname_controller/Edit_header/'.$row['opname_header_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i></a>
                    <?php }?>
                    <?php if(helper_security("stockopname_delete") == 1){?>
                    <a href="javascript:dialogHapus('<?php echo base_url('Stockopname_controller/delete/'.$row['opname_header_id'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i></a></td>
                    <?php }?>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
