<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>

<div class="portlet light bordered">
  <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Role User</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">

          <form role="form" method="post">
              <div class="form-body">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                          <label>Nama Role</label>
                          <input type="text" class="form-control" placeholder="Nama Role" id="nama_role" name="nama_role" value="<?=isset($default['nama_role'])? $default['nama_role'] : ""?>">
                        </div>
                    </div>

                      <div class="col-sm-8">
                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" class="form-control" placeholder="Keterangan" id="keterangan" name="keterangan" value="<?=isset($default['keterangan'])? $default['keterangan'] : ""?>">
                          </div>
                      </div>
                    </div>

                      <button type="submit" class="btn red" name="update_role">Update</button>

                    <hr / style="border-color: #3598DC;">

                    <div class="row">
                        <div class="col-sm-3">
                          <!------------------- MASTER MENU ---------------------------------->
                          <?php if ($default['master_menu'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'master_menu'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">MASTER MENU<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'master_menu'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">MASTER MENU<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END MASTER_MENU---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- PEMBELIAN MENU ---------------------------------->
                          <?php if ($default['pembelian_menu'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'pembelian_menu'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">PEMBELIAN<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'pembelian_menu'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">PEMBELIAN<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END PEMBELIAN_MENU---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- PENJUALAN MENU ---------------------------------->
                          <?php if ($default['penjualan_menu'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'penjualan_menu'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">PENJUALAN<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'penjualan_menu'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">PENJUALAN<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END PENJUALAN_MENU---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- INVENTORY MENU ---------------------------------->
                          <?php if ($default['inventory_menu'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'inventory_menu'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">INVENTORY<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'inventory_menu'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">INVENTORY<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END INVENTORY MENU ---------------------------------->
                        </div>
                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3">
                          <!------------------- PRODUKSI MENU ---------------------------------->
                          <?php if ($default['produksi_menu'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'produksi_menu'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">PRODUKSI<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'produksi_menu'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">PRODUKSI<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END PRODUKSI MENU ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- ASSET MENU ---------------------------------->
                          <?php if ($default['asset_menu'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'asset_menu'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">ASSET<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'asset_menu'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">ASSET<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END ASSET MENU ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- JURNAL UMUM MENU ---------------------------------->
                          <?php if ($default['jurnal_umum_menu'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'jurnal_umum_menu'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">JURNAL UMUM<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'jurnal_umum_menu'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">JURNAL UMUM<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END JURNAL UMUM MENU ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- KAS BANK MENU ---------------------------------->
                          <?php if ($default['kasbank_menu'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'kasbank_menu'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">KAS & BANK<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'kasbank_menu'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">KAS & BANK<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END KAS BANK MENU ---------------------------------->
                        </div>
                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3">
                          <!------------------- LAPORAN INVENTORY ---------------------------------->
                          <?php if ($default['laporan_inventory'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'laporan_inventory'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">LAPORAN INVENTORY<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'laporan_inventory'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">LAPORAN INVENTORY<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END LAPORAN INVENTORY ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- LAPORAN KEUANGAN ---------------------------------->
                          <?php if ($default['laporan_keuangan'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'laporan_keuangan'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">LAPORAN KEUANGAN<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'laporan_keuangan'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">LAPORAN KEUANGAN<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END LAPORAN KEUANGAN ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- KONFIGURASI ---------------------------------->
                          <?php if ($default['konfigurasi'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'konfigurasi'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">KONFIGURASI<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'konfigurasi'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">KONFIGURASI<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END KONFIGURASI ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- IMPORT ---------------------------------->
                          <?php if ($default['import_menu'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'import_menu'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">IMPORT<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'import_menu'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">IMPORT<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END IMPORT ---------------------------------->
                        </div>
                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3">
                          <!------------------- EKSPORT ---------------------------------->
                          <?php if ($default['eksport_menu'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'eksport_menu'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">EKSPORT<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'eksport_menu'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">EKSPORT<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END EKSPORT ---------------------------------->
                        </div>
                    </div>

              </div>
          </form>
  </div>
</div>
