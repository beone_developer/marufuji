<?php
$q = $_GET['q'];

if ($q == 1){//produksi wip
  $list_item = $this->db->query("SELECT * FROM public.beone_item WHERE item_type_id = 1 AND flag = 1");
  $gudang = 1;
}else{//produksi barang jadi
  $list_item = $this->db->query("SELECT * FROM public.beone_item WHERE item_type_id = 2 AND flag = 1");
  $gudang = 3;
}

$gudang_asal = $this->db->query("SELECT * FROM public.beone_gudang WHERE gudang_id = ".intval($gudang));
$hasil_gudang_asal = $gudang_asal->row_array();
$gudang_id = $hasil_gudang_asal['gudang_id'];
$nama = $hasil_gudang_asal['nama'];

?>
<div class="col-sm-4">
<div class="form-group">
    <label>Item</label>
      <select id='item_modal' class='form-control input-sm select2-multiple' name='item_modal'>
        <option value=""></option>
        <?php foreach($list_item->result_array() as $row){ echo '<option value='.$row['item_id'].'>'.$row['nama'].'</option>';} ?>
      </select>
</div>
</div>

<div class="col-sm-4">
<label>Gudang</label>
<select id='gudang_modal' class='form-control input-sm select2-multiple' name='gudang_modal' disabled>
  <option value="<?php echo $gudang_id;?>"><?php echo $nama;?></option>
</select>
</div>
