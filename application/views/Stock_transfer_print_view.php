        <!-- BEGIN PAGE TITLE-->
        <?php
          $data_perusahaan = $this->db->query("SELECT * FROM public.beone_konfigurasi_perusahaan");
          $hasil_data_perusahaan = $data_perusahaan -> row_array();
        ?>
        <h4><b><?php //echo $hasil_data_perusahaan['nama_perusahaan'];?></b></h4>
        <h5><?php //echo $hasil_data_perusahaan['alamat_perusahaan'];?></h5>
        <h5><?php //echo $hasil_data_perusahaan['kota_perusahaan'];?> <?php //echo $hasil_data_perusahaan['provinsi_perusahaan'];?> <?php //echo $hasil_data_perusahaan['kode_pos_perusahaan'];?></h5>

        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <?php
          $tgl = "";
          $ntransfer = "";
          $tipe_transaksi = 0;
          $produksi_id = 0;
          foreach($print_transfer as $row){
            $tgl = $row['transfer_date'];
            $ntransfer = $row['transfer_no'];
            $tipe_transaksi = $row['coa_kode_biaya'];
            $produksi_id = $row['transfer_stock_id'];

            $sql_BB = $this->db->query("SELECT SUM(d.qty) as haqty
            FROM public.beone_transfer_stock h INNER JOIN public.beone_transfer_stock_detail d ON h.transfer_stock_id = d.transfer_stock_header_id INNER JOIN public.beone_item i ON d.item_id = i.item_id
            WHERE h.flag=1 AND h.transfer_stock_id = $produksi_id AND d.tipe_transfer_stock = 'BB'");
            $hasil_sql_BB = $sql_BB -> row_array();

            $sql_WIP = $this->db->query("SELECT SUM(d.qty) as haqty
            FROM public.beone_transfer_stock h INNER JOIN public.beone_transfer_stock_detail d ON h.transfer_stock_id = d.transfer_stock_header_id INNER JOIN public.beone_item i ON d.item_id = i.item_id
            WHERE h.flag=1 AND h.transfer_stock_id = $produksi_id AND d.tipe_transfer_stock = 'WIP'");
            $hasil_sql_WIP = $sql_WIP -> row_array();

            $susut = $hasil_sql_BB['haqty'] -  $hasil_sql_WIP['haqty'];

            if($susut == 0){
                $susut_persen = 0;
            }else{
                $susut_persen = ($susut * 100) / $hasil_sql_BB['haqty'];
            }

          }
        ?>

        <div class="invoice">
            <center><h2><b>BUKTI PENYERAHAN BARANG<b></h2></center>
            <hr/>
            <div class="row">
                <div class="col-xs-2">
                    <ul class="list-unstyled">
                        <li> <h5> Dari </h5> </li>
                        <li> <h5> Kepada </h5> </li>
                    </ul>
                </div>
                <div class="col-xs-4">
                    <ul class="list-unstyled">
                      <?php
                        if ($tipe_transaksi == 1){
                           $dari = "PRODUKSI I";
                           $ke = "PRODUKSI II";
                        }else{
                          $dari = "PRODUKSI II";
                          $ke = "PRODUKSI BARANG JADI";
                        }
                      ?>
                        <li><h5>: <?php echo $dari;?> </h5> </li>
                        <li><h5>: <?php echo $ke;?> </h5> </li>
                    </ul>
                </div>
                <div class="col-xs-2">
                    <ul class="list-unstyled">
                      <li> <h5> Tanggal </h5> </li>
                      <li> <h5> Nomor </h5> </li>
                    </ul>
                </div>
                <div class="col-xs-4">
                  <ul class="list-unstyled">
                      <li><h5>: <?php echo date('d-m-Y', strtotime($tgl));?> </h5> </li>
                      <li><h5>: <?php echo $ntransfer;?> </h5> </li>
                  </ul>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th> No </th>
                                <th class="hidden-xs"> No KIK </th>
                                <th class="hidden-xs"> Nama Barang </th>
                                <th class="hidden-xs"> Ukuran </th>
                                <th class="hidden-xs"> Satuan </th>
                                <th class="hidden-xs"> Jumlah </th>

                                <?php if ($tipe_transaksi == 2){?>
                                <th class="hidden-xs"> Susut </th>
                                <?php }?>

                                <th class="hidden-xs"> Keterangan </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                          $subtotal = 0;
                          $no = 0;
                          foreach($print_transfer as $row){
                                  $no = $no + 1;
                                  if ($row['tipe_transfer_stock'] == "WIP"){
                          ?>

                                  <tr>
                                    <td> <?php echo $no;?> </td>
                                    <td> <?php echo $row['item_code'];?> </td>
                                    <td class="hidden-xs"> <?php echo $row['namaitem'];?> </td>
                                    <td class="hidden-xs"> <?php echo "";?> </td>
                                    <td class="hidden-xs"> <?php echo "KG";?> </td>
                                    <td class="hidden-xs"> <?php echo number_format($row['qty'], 2);?> </td>

                                    <?php if ($tipe_transaksi == 2){?>
                                    <td class="hidden-xs"> <?php echo $susut." (".$susut_persen." %)";?> </td>
                                    <?php }?>

                                    <td class="hidden-xs"> <?php echo $row['keterangan'];?> </td>
                                  <tr>
                          <?php
                                  $subtotal = $subtotal + $row['qty'];
                                }
                              } ?>

                                  <tr>
                                    <td> </td>
                                    <td> </td>
                                    <td class="hidden-xs"><b>TOTAL</b></td>
                                    <td class="hidden-xs"> </td>
                                    <td class="hidden-xs"> </td>
                                    <td class="hidden-xs"><b> <?php echo number_format($subtotal, 2);?> <b/></td>
                                    <td class="hidden-xs"> </td>
                                  <tr>
                        </tbody>
                    </table>
                </div>
            </div>


            <br />
            <br />
            <br />
            <div class="row">
                <div class="col-xs-4">
                    <div class="well">
                      <center>
                      <strong>Mengetahui,</strong>
                      <br/>
                      <br/>
                      <br/>
                      <strong>( .................... )</strong>
                      </center>
                    </div>
                </div>
                <div class="col-xs-4 invoice-block">
                  <div class="well">
                  <center>
                  <strong>Menerima,</strong>
                  <br/>
                  <br/>
                  <br/>
                  <strong>( .................... )</strong>
                  </center>
                  </div>
                </div>
                <div class="col-xs-4">
                    <div class="well">
                      <center>
                      <strong>Menyerahkan,</strong>
                      <br/>
                      <br/>
                      <br/>
                      <strong>( .................... )</strong>
                      </center>
                    </div>
                </div>
            </div>
            <div class="row">
              <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();"> Print
                  <i class="fa fa-print"></i>
              </a>
              <!--<a class="btn btn-lg green hidden-print margin-bottom-5"> Submit Your Invoice
                  <i class="fa fa-check"></i>
              </a>-->
            </div>
        </div>
