<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>

<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Form Kas Bank Ubah</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">
          <form role="form" method="post">
              <div class="form-body">
                <div class="row">

                  <div class="col-sm-2">
                      <h3><?php echo $default['voucher_date'];?></h3>
                      <input type="text" name="tgl_voucher" value='<?php echo $default['voucher_date'];?>' hidden>

                      <h3><?php if ($default['tipe'] == 1){ echo "DEBIT";}else{ echo "KREDIT";}?></h3>
                      <input type="text" name="tipe_voucher" value='<?php echo $default['tipe'];?>' hidden>
                      <hr />

                      <h5><i><?php echo $default['keterangan'];?></i></h5>
                      <input type="text" name="keterangan_voucher" value='<?php echo $default['keterangan'];?>' hidden>

                      <h4><b><?php echo $default['voucher_number'];?></b></h4>
                      <input type="text" name="no_voucher" value='<?php echo $default['voucher_number'];?>' hidden>

                      <input type="text" name="coa_id_voucher" value='<?php echo $default['coa_id_cash_bank'];?>' hidden>
                      <input type="text" name="coa_no_voucher" value='<?php echo $default['coa_no'];?>' hidden>

                      <!--untuk cocokan bank dan pelunasan-->
                      <input type="hidden" class="form-control" placeholder="" name="cocokan" id="cocokan" value=0>
                      <input type="hidden" class="form-control" placeholder="" name="cocokan_pembayaran" id="cocokan_pembayaran" value=0>

                  </div>


                  <div class="col-sm-10" id="detail_cash_bank">
                      <div class="portlet light bordered">

                        <table  id="datatable" class="table striped hovered cell-hovered">
                						<thead>
                							<tr>
                								<td width="20%">Akun</td>
                                <td width="20%">Keterangan</td>
                								<td width="20%">Jml Valas</td>
                								<td width="15%">Kurs</td>
                								<td width="20%">Jml IDR</td>
                                <td width="5%"></td>
                							</tr>
                						 </thead>
                             <tbody id="container">

                               <?php
                               $ctr = 0;
                                foreach($default_detail as $row){
                                $ctr = $ctr + 1;
                                $nama_ctr = "x".$ctr;
                                $nama_coa_lawan = "coa_lawan_".$nama_ctr;
                                $nama_keterangan = "keterangan_voucher_".$nama_ctr;
                                $nama_valas = "valas_".$nama_ctr;
                                $nama_kurs = "kurs_".$nama_ctr;
                                $nama_saldo = "saldo_".$nama_ctr;
                                $nama_coa_lawan_id = "coa_id_lawan_".$nama_ctr;
                                $rowss = "rows_".$nama_ctr;
                                $detail_id = "detail_id_".$nama_ctr;
                              ?>

                              <tr class='records' id='<?php echo $nama_ctr;?>'>
                              <td><input class="form-control" id='<?php echo $nama_coa_lawan;?>' name='<?php echo $nama_coa_lawan;?>' type="text" value='<?php echo $row['coa_no_lawan'];?>' readonly></td>
                              <td><input class="form-control" id='<?php echo $nama_keterangan;?>' name='<?php echo $nama_keterangan;?>' type="text" value='<?php echo $row['keterangan_detail'];?>' readonly></td>
                              <td><input class="form-control" id='<?php echo $nama_valas;?>' name='<?php echo $nama_valas;?>' type="text" value='<?php echo $row['jumlah_valas'];?>' readonly></td>
                              <td><input class="form-control" id='<?php echo $nama_kurs;?>' name='<?php echo $nama_kurs;?>' type="text" value='<?php echo $row['kurs'];?>' readonly></td>
                              <td><input class="form-control" id='<?php echo $nama_saldo;?>' name='<?php echo $nama_saldo;?>' type="text" value='<?php echo $row['jumlah_idr'];?>' readonly></td>
                              <td><input id='<?php echo $nama_coa_lawan_id;?>' name='<?php echo $nama_coa_lawan_id;?>' type="hidden" value='<?php echo $row['coa_id_lawan'];?>'></td>
                              <td><input id='<?php echo $rowss;?>' name="rows[]" value='<?php echo $nama_ctr;?>' type="hidden"></td>
                              <td><button type="button" class="btn red" onclick="hapus('<?php echo $nama_ctr;?>')">X</button></td>
                              <td><input id='<?php echo $detail_id;?>' name='<?php echo $detail_id;?>' value='<?php echo $row['voucher_detail_id'];?>' type="hidden"></td></tr>

                              <?php
                                }
                               ?>
                						</tbody>
                				</table>
                        <div class="form-actions" id='pelunasan'>
                          <a class="btn blue" data-toggle="modal" id="tambahdata_pelunasan" href="#responsive_pelunasan"><i class="fa fa-plus"></i> Tambah Pelunasan </a>

                          <table  id="datatable" class="table striped hovered cell-hovered">
                  						<thead>
                  							<tr>
                  								<td width="65%">Customer</td>
                                  <td width="15%">Valas</td>
                  								<td width="15%">IDR</td>
                                  <td width="5%"></td>
                  							</tr>
                  						 </thead>
                               <tbody id="container_pelunasan">

                  						</tbody>
                  				</table>
                        </div>
                      </div>
                  </div>
              </div>
            </div>

            <div class="form-actions">
              <a class="btn blue" data-toggle="modal" href="#responsive" id="tambahdata"><i class="fa fa-plus"></i> Tambah Data </a>
              <button type="submit" class="btn red" name="submit_voucher" id="submit_voucher">Submit</button>
              <!--<a class="btn green" id="cekdata" onclick="validasi_voucher();"><i class="fa fa-plus"></i> Validasi </a>-->
            </div>
          </form>
      </div>
    </div>


    <!--------------------------- MODAL ADD VOUCHER--------------------------------------------->
    <div id="responsive" class="modal fade" tabindex="-1" data-width="760">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Voucher</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Akun Lawan Transaksi</label>
                    <div class="input-group">
                       <select class="form-control input-sm select2-multiple" name="coa_id_lawan" id="coa_id_lawan" required>
                          <option value=""></option>
                         <?php 	foreach($list_coa as $row){ ?>
                           <option value="<?php echo $row['coa_id'];?>"><?php echo $row['nomor']." | ".$row['nama'];?></option>
                         <?php } ?>
                       </select>
                     </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Keterangan Voucher</label>
                  <input type="text" class="form-control" placeholder="Keterangan" name="keterangan_voucher" id="keterangan_voucher" required>
                </div>
              </div>
            </div>
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                      <label>Jumlah Valas</label>
                      <input type="text" class="form-control" placeholder="Jumlah Valas" name="jumlah_valas" id="valas" onchange="autoTotal()" required>
                      </div>
                    </div>

                    <div class="col-sm-4">
                      <div class="form-group">
                      <label>Kurs</label>
                      <input type="text" class="form-control" placeholder="Kurs" name="kurs" id="kurs" onchange="autoTotal()" required>
                      </div>
                    </div>

                    <div class="col-sm-4">
                      <div class="form-group">
                      <label>Jumlah IDR</label>
                      <input type="text" class="form-control" placeholder="Jumlah IDR" name="jumlah_idr" id="saldo" required>
                      </div>
                    </div>
                  </div>

        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="button" class="btn green" name="add_btn" id="add_btn">Insert</button>
        </div>
    </div>
    <!------------------------------------------------------------------------>

    <!--------------------------- MODAL ADD PELUNASAN--------------------------------------------->
    <div id="responsive_pelunasan" class="modal fade" tabindex="-1" data-width="760">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">Pelunasan</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>No Invoice</label>
                <div class="input-group">
                   <select class="form-control input-sm select2-multiple" name="invoice" id="invoice" required>
                      <option value=""></option>
                     <?php
                        if ($tp == 1){//Pelunasan Piutang
                          $tipe_hutang_piutang = $list_piutang;
                          $saldo_awal_hutang_piutang = $list_saldo_awal_piutang;
                        }else{ //Pelunasan Hutang
                            $tipe_hutang_piutang = $list_hutang;
                            $saldo_awal_hutang_piutang = $list_saldo_awal_hutang;
                        }

                        foreach($tipe_hutang_piutang as $row){
                      ?>
                       <option value="<?php echo $row['hutang_piutang_id'];?>"><?php echo $row['ncustsup']." | ".$row['nomor']." | ".$row['trans_date']." | ".number_format($row['sisa'],2);?></option>
                     <?php }

                        foreach($saldo_awal_hutang_piutang as $row2){
                      ?>
                        <option value="<?php echo "c_".$row2['custsup_id'];?>"><?php echo $row2['nama']." | IDR ".number_format($row2['sisaidr'],2)." | $ ".number_format($row2['sisavalas'],2);?></option>
                      <?php
                        }

                     ?>
                   </select>
                 </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Nominal Valas" name="pembayaran_valas" id="pembayaran_valas" onchange="autoTotal_pelunasan();"required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Nominal Kurs" name="pembayaran_kurs" id="pembayaran_kurs" onchange="autoTotal_pelunasan();" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Nominal IDR" name="pembayaran" id="pembayaran" required>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
          <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
          <button type="button" class="btn green" name="add_btn_pelunasan" id="add_btn_pelunasan">Insert</button>
      </div>
    </div>
    <!------------------------------------------------------------------------>

    <script>
    var xx = document.getElementById("pelunasan");
    xx.style.display = 'none';

    /*var yy = document.getElementById("submit_voucher");
    yy.style.display = 'none';*/

    $(document).ready(function() {
        		var count = 0;

        		$("#add_btn").click(function(){
    					count += 1;

              var coa_id_lawan = document.getElementById('coa_id_lawan');
              var keterangan_voucher = document.getElementById('keterangan_voucher');
              var valas = document.getElementById('valas');
              var c = document.getElementById('kurs');
              var saldo = document.getElementById('saldo');
              var coa_lawan = $('#coa_id_lawan option:selected').text();


              if (coa_id_lawan.value == ""){
                alert("Akun lawan tidak boleh kosong...!!!");
                coa_id_lawan.focus();
              }else if(keterangan_voucher.value == ""){
                alert("Keterangan voucher tidak boleh kosong...!!!");
                keterangan_voucher.focus();
              }else if(valas.value == ""){
                alert("Isikan 0 jika tidak ada...!!!");
                valas.focus();
              }else if(kurs.value == ""){
                alert("Isikan 0 jika tidak ada...!!!");
                kurs.focus();
              }else if(saldo.value == ""){
                alert("Saldo tidak boleh kosong...!!!");
                saldo.focus();
              }else{
                        $('#container').append(
                             '<tr class="records" id="'+count+'">'
                           + '<td><input class="form-control" id="coa_lawan_' + count + '" name="coa_lawan_'+count+'" type="text" value="'+coa_lawan+'" readonly></td>'
                           + '<td><input class="form-control" id="keterangan_voucher_' + count + '" name="keterangan_voucher_'+count+'" type="text" value="'+keterangan_voucher.value+'" readonly></td>'
                           + '<td><input class="form-control" id="valas_' + count + '" name="valas_'+count+'" type="text" value="'+valas.value+'" readonly></td>'
                           + '<td><input class="form-control" id="kurs_' + count + '" name="kurs_'+count+'" type="text" value="'+kurs.value+'" readonly></td>'
                           + '<td><input class="form-control" id="saldo_' + count + '" name="saldo_'+count+'" type="text" value="'+saldo.value+'" readonly></td>'
                           + '<td><input id="rows_' + count + '" name="rows[]" value="'+ count +'" type="hidden"></td>'
                           + '<td><input id="coa_id_lawan_' + count + '" name="coa_id_lawan_'+count+'" type="hidden" value="'+coa_id_lawan.value+'"></td>'
                           + '<td><button type="button" class="btn red" onclick="hapus('+count+')">X</button></td></tr>'
                        );

                        //SETTING PELUNASAN HUTANG PIUTANG BERDASARKAN AKUN
                        if (coa_id_lawan.value == 9 || coa_id_lawan.value == 40 || coa_id_lawan.value == 41){
                          var x = document.getElementById("pelunasan");
                            x.style.display = 'block';
                        }
                        autoCocokan_tambah(saldo.value);
                        eraseText();
                        $('#responsive').modal('hide');
              }
    				});



            var count_pelunasan = 0;

        		$("#add_btn_pelunasan").click(function(){
    					count_pelunasan += 1;

              var invoice = document.getElementById('invoice');
              var nominal_bayar = document.getElementById('pembayaran');
              var nominal_bayar_valas = document.getElementById('pembayaran_valas');
              var detail_invoice = $('#invoice option:selected').text();


              if (invoice.value == ""){
                alert("Invoice tidak boleh kosong...!!!");
                invoice.focus();
              }else if(nominal_bayar.value == ""){
                alert("Nominal Pembayaran tidak boleh kosong...!!!");
                nominal_bayar.focus();
              }else{
                    $('#container_pelunasan').append(
                         '<tr class="records" id="pelunasan_'+count_pelunasan+'">'
                       + '<td><input class="form-control" id="invoice_' + count_pelunasan + '" name="invoice_'+count_pelunasan+'" type="text" value="'+detail_invoice+'" readonly></td>'
                       + '<td><input class="form-control" id="pembayaran_' + count_pelunasan + '" name="pembayaran_'+count_pelunasan+'" type="text" value="'+nominal_bayar.value+'" readonly></td>'
                       + '<td><input class="form-control" id="pembayaran_valas_' + count_pelunasan + '" name="pembayaran_valas_'+count_pelunasan+'" type="text" value="'+nominal_bayar_valas.value+'" readonly></td>'
                       + '<td><input id="rows_' + count_pelunasan + '" name="rows_pelunasan[]" value="'+ count_pelunasan +'" type="hidden"></td>'
                       + '<td><input id="hp_id_' + count_pelunasan + '" name="hp_id_'+count_pelunasan+'" type="hidden" value="'+invoice.value+'"></td>'
                       + '<td><button type="button" class="btn red" onclick="hapus_pelunasan('+count_pelunasan+')">X</button></td></tr>'
                    );

                    //menghilangkan tombol tambah data
                    var zz = document.getElementById("tambahdata");
                    zz.style.display = 'none';

                    autoCocokan_tambah_pelunasan(nominal_bayar.value);
                    eraseText_pembayaran();
                    $('#responsive_pelunasan').modal('hide');
              }
    				});
    		});

        function eraseText() {
         document.getElementById("coa_id_lawan").value = "";
         document.getElementById("keterangan_voucher").value = "";
         document.getElementById("valas").value = "";
         document.getElementById("kurs").value = "";
         document.getElementById("saldo").value = "";
        }


        function eraseText_pembayaran() {
         document.getElementById("pembayaran").value = "";
         document.getElementById("invoice").value = "";
        }

        function hapus(rowid)
        {
            autoCocokan_kurang("saldo_"+rowid);

            var row = document.getElementById(rowid);
            row.parentNode.removeChild(row);
        }

        function hapus_pelunasan(rowid)
        {
            autoCocokan_pelunasan_kurang("pembayaran_"+rowid);

            var row = document.getElementById("pelunasan_"+rowid);
            row.parentNode.removeChild(row);
        }
    </script>

    <script>
        function autoTotal(){
          var a = document.getElementById('valas').value;
          var b = document.getElementById('kurs').value;

          var c_ex = a.split('.').join('');
          var d_ex = b.split('.').join('');

          var c = c_ex.split(',').join('.');
          var d = d_ex.split(',').join('.');

          document.getElementById('saldo').value = c * d;
        }


        function autoTotal_pelunasan(){
          var a = document.getElementById('pembayaran_valas').value;
          var b = document.getElementById('pembayaran_kurs').value;

          var c_ex = a.split('.').join('');
          var d_ex = b.split('.').join('');

          var c = c_ex.split(',').join('.');
          var d = d_ex.split(',').join('.');

          document.getElementById('pembayaran').value = c * d;
        }


        function autoCocokan_tambah(xsaldo){
          var totalBank = document.getElementById('cocokan').value;
          var bank = xsaldo;

          var tb_ex = totalBank.split('.').join('');
          var b_ex = bank.split('.').join('');

          var tb = tb_ex.split(',').join('.');
          var b = b_ex.split(',').join('.');

          document.getElementById('cocokan').value = (tb*1) + (b*1);
          document.getElementById('pembayaran').value = (tb*1) + (b*1);
        }

        function autoCocokan_tambah_pelunasan(xsaldo){
          var totalBank = document.getElementById('cocokan_pembayaran').value;
          var bank = xsaldo;

          var tb_ex = totalBank.split('.').join('');
          var b_ex = bank.split('.').join('');

          var tb = tb_ex.split(',').join('.');
          var b = b_ex.split(',').join('.');

          document.getElementById('cocokan_pembayaran').value = (tb*1) + (b*1);
        }


        function autoCocokan_kurang(xsaldo){
          var totalBank = document.getElementById('cocokan').value;
          var bank = document.getElementById(xsaldo).value;

          var tb_ex = totalBank.split('.').join('');
          var b_ex = bank.split('.').join('');

          var tb = tb_ex.split(',').join('.');
          var b = b_ex.split(',').join('.');

          document.getElementById('cocokan').value = (tb*1) - (b*1);
          document.getElementById('pembayaran').value = (tb*1) - (b*1);
        }

        function autoCocokan_pelunasan_kurang(xsaldo){
          var totalBank = document.getElementById('cocokan_pembayaran').value;
          var bank = document.getElementById(xsaldo).value;

          var tb_ex = totalBank.split('.').join('');
          var b_ex = bank.split('.').join('');

          var tb = tb_ex.split(',').join('.');
          var b = b_ex.split(',').join('.');

          document.getElementById('cocokan_pembayaran').value = (tb*1) - (b*1);
        }

        function validasi_voucher(){
            var aa = document.getElementById('cocokan').value;
            var bb = document.getElementById('cocokan_pembayaran').value;

            if(aa == 0){
              alert("Isikan data pembayaran sebelum menyimpan...!!!");
            }else if(aa !=0 && bb != 0){
                  if (aa != bb){
                      alert("Nilai total pembayaran tidak sesuai...!!!");
                  }else{
                    var yy = document.getElementById("submit_voucher");
                    yy.style.display = 'block';

                    var zz = document.getElementById("cekdata");
                    zz.style.display = 'none';

                    var xx = document.getElementById("tambahdata");
                    xx.style.display = 'none';

                    var vv = document.getElementById("tambahdata_pelunasan");
                    vv.style.display = 'none';
                  }
            }else if(aa !=0 && bb == 0){
              var yy = document.getElementById("submit_voucher");
              yy.style.display = 'block';

              var zz = document.getElementById("cekdata");
              zz.style.display = 'none';

              var xx = document.getElementById("tambahdata");
              xx.style.display = 'none';

              var vv = document.getElementById("tambahdata_pelunasan");
              vv.style.display = 'none';
            }
        }
    </script>

    <script type="text/javascript">

		var valas = document.getElementById('valas');
		valas.addEventListener('keyup', function(e){
			// tambahkan 'Rp.' pada saat form di ketik
			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
			valas.value = formatRupiah(this.value, 'Rp. ');
		});

    var kurs = document.getElementById('kurs');
		kurs.addEventListener('keyup', function(e){
			kurs.value = formatRupiah(this.value, 'Rp. ');
		});

    var pembayaran_valas = document.getElementById('pembayaran_valas');
		  pembayaran_valas.addEventListener('keyup', function(e){
			pembayaran_valas.value = formatRupiah(this.value, 'Rp. ');
		});

    var pembayaran_kurs = document.getElementById('pembayaran_kurs');
		  pembayaran_kurs.addEventListener('keyup', function(e){
			pembayaran_kurs.value = formatRupiah(this.value, 'Rp. ');
		});


    var saldo = document.getElementById('saldo');
		saldo.addEventListener('keyup', function(e){
			saldo.value = formatRupiah(this.value, 'Rp. ');
		});

    var pembayaran = document.getElementById('pembayaran');
		pembayaran.addEventListener('keyup', function(e){
			pembayaran.value = formatRupiah(this.value, 'Rp. ');
		});


		/* Fungsi formatRupiah */
		function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}

			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		}
	</script>
