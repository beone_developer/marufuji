<h3>List General Journal</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th><center>Nomor</center></th>
              <th><center>Tanggal</center></th>
              <th><center>Keterangan</center></th>
              <th><center>COA</center></th>
              <th><center>COA Lawan</center></th>
              <th><center>Debet</center></th>
              <th><center>Kredit</center></th>
              <th><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_gj as $row){ ?>
            <tr>
                <td><?php echo $row['gl_number'];?></td>
                <td><?php echo $row['gl_date'];?></td>
                <td><?php echo $row['keterangan'];?></td>
                <td><?php echo $row['coa_no'];?></td>
                <td><?php echo $row['coa_no_lawan'];?></td>
                <td><?php echo number_format($row['debet'],2);?></td>
                <td><?php echo number_format($row['kredit'],2);?></td>
                <td>
                    <?php if(helper_security("jurnal_umum_edit") == 1){?>
                    <a href='<?php echo base_url('Gj_controller/edit/'.$row['gl_id'].'/'.$row['gl_number'].'');?>' class='btn blue'><i class="fa fa-pencil"></i></a>
                    <?php }?>
                    <?php if(helper_security("jurnal_umum_delete") == 1){?>
                    <a href="javascript:dialogHapus('<?php echo base_url('Gj_controller/delete/'.$row['gl_id'].'/'.$row['gl_number'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i></a>
                    <?php }?>
                </td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
