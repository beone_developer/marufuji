<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<?php
$list_item = $this->db->query("SELECT * FROM public.beone_item");
?>

<div class="portlet box blue ">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i> Form Penjualan
		</div>
		<div class="tools">
			<a href="" class="collapse"> </a>
			<a href="#portlet-config" data-toggle="modal" class="config"> </a>
			<a href="" class="reload"> </a>
			<a href="" class="remove"> </a>
		</div>
	</div>
	<div class="portlet-body form">


		<form role="form" method="post" onsubmit="return validasi_input(this)">
			<div class="form-body">

				<div class="row">
					<div class="col-sm-5"><h3><b><?php echo $default['sales_no']; ?></b></h3></div>
					<input type"text" name="nomor_so" value='<?php echo $default['sales_no']; ?>' hidden>
					<div class="col-sm-7"><h3><?php echo $default['trans_date']; ?></h3></div>
					<input type"text" name="tanggal" value='<?php echo helper_tanggalupdate($default['trans_date']); ?>'
					hidden>
				</div>

				<div class="row">
					<div class="col-sm-5"><h4><?php echo $default['ncustomer']; ?></h4></div>
					<input type"text" name="customer" value='<?php echo $default['customer_id']; ?>' hidden>
					<div class="col-sm-7"><h3><?php echo $default['keterangan']; ?></h3></div>
					<input type"text" name="keterangan_header" value='<?php echo $default['keterangan']; ?>' hidden>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label>PPN</label>
							<div class="mt-radio-inline">
								<label class="mt-radio">
									<?php if ($default['ppn'] == 0) { ?>
										<input type="radio" name="ppn" id="optionsRadios4" value="0"
											   onchange="cekppn(value)" required checked> Tanpa PPN
									<?php } else { ?>
										<input type="radio" name="ppn" id="optionsRadios4" value="0"
											   onchange="cekppn(value)" required> Tanpa PPN
									<?php } ?>
									<span></span>
								</label>
								<label class="mt-radio">
									<?php if ($default['ppn'] == 1) { ?>
										<input type="radio" name="ppn" id="optionsRadios5" value="1"
											   onchange="cekppn(value)" required checked> Belum PPN
									<?php } else { ?>
										<input type="radio" name="ppn" id="optionsRadios5" value="1"
											   onchange="cekppn(value)" required> Belum PPN
									<?php } ?>
									<span></span>
								</label>
								<label class="mt-radio">
									<?php if ($default['ppn'] == 2) { ?>
										<input type="radio" name="ppn" id="optionsRadios6" value="2"
											   onchange="cekppn(value)" required checked> Termasuk PPN
									<?php } else { ?>
										<input type="radio" name="ppn" id="optionsRadios6" value="2"
											   onchange="cekppn(value)" required> Termasuk PPN
									<?php } ?>
									<span></span>
								</label>
							</div>
						</div>
					</div>
				</div>

				<hr
				/ style="border-color: #3598DC;">
				<table id="datatable" class="table striped hovered cell-hovered">
					<thead>
					<tr>
						<td width="20%">Item</td>
						<td width="15%">Qty</td>
						<td width="15%">Price</td>
						<td width="15%">Amount</td>
						<td width="5%"></td>
					</tr>
					</thead>
					<tbody id="container">

					<?php
					$default_subtotal = 0;
					$ctr = 0;
					$ppn_value = 0;

					foreach ($default_detail as $row) {
						$ctr = $ctr + 1;
						$nama_ctr = "x" . $ctr;
						$nama_item = "item_" . $nama_ctr;
						$nama_item_id = "item_id_" . $nama_ctr;
						$nama_qty = "qty_" . $nama_ctr;
						$nama_price = "price_" . $nama_ctr;
						$nama_amount = "amount_" . $nama_ctr;
						$rowss = "rows_" . $nama_ctr;
						$detail_id = "detail_id_" . $nama_ctr;

						$default_subtotal = $default_subtotal + $row['amount'];

						$sql_cari_nama_item = $this->db->query("SELECT * FROM public.beone_item WHERE item_id = " . intval($row['item_id']));
						$hasil_cari_nama_item = $sql_cari_nama_item->row_array();
						$nitem = $hasil_cari_nama_item['nama'];
						?>

						<tr class='records' id='<?php echo $nama_ctr; ?>'>
							<td><input class="form-control" id='<?php echo $nama_item; ?>'
									   name='<?php echo $nama_item; ?>' type="text" value='<?php echo $nitem; ?>'
									   readonly></td>
							<td><input class="form-control" id='<?php echo $nama_qty; ?>'
									   name='<?php echo $nama_qty; ?>' type="text"
									   value='<?php echo number_format($row['qty'], 2); ?>' readonly></td>
							<td><input class="form-control" id='<?php echo $nama_price; ?>'
									   name='<?php echo $nama_price; ?>' type="text"
									   value='<?php echo number_format($row['price'], 2); ?>' readonly></td>
							<td><input class="form-control" id='<?php echo $nama_amount; ?>'
									   name='<?php echo $nama_amount; ?>' type="text"
									   value='<?php echo number_format($row['amount'], 2); ?>' readonly></td>
							<td>
								<button type="button" class="btn red" onclick="hapus('<?php echo $nama_ctr; ?>')">X
								</button>
							</td>
							<td><input class="form-control" id='<?php echo $nama_item_id; ?>'
									   name='<?php echo $nama_item_id; ?>' type="hidden"
									   value='<?php echo $row['item_id']; ?>' readonly></td>
							<td><input id='<?php echo $rowss; ?>' name="rows[]" value='<?php echo $nama_ctr; ?>'
									   type="hidden"></td>
							</td>
							<td><input id='<?php echo $detail_id; ?>' name='<?php echo $detail_id; ?>'
									   value='<?php echo $row['sales_detail_id']; ?>' type="hidden"></td>
						</tr>

						<?php
					}
					?>

					</tbody>
				</table>

				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4"></div>
					<div class="col-sm-4">
						<input type="hidden" class="form-control" placeholder="Sub Total" name="subtotal_backup"
							   id="subtotal_backup" value='<?php echo $default_subtotal; ?>' readonly>
						<input type="text" class="form-control" placeholder="Sub Total" name="subtotal" id="subtotal"
							   value='<?php echo $default['subtotal']; ?>' readonly>
						<br/>
						<input type="text" class="form-control" placeholder="PPN" name="ppn_value" id="ppn"
							   value='<?php echo $default['ppn_value']; ?>' readonly>
						<hr
						/ style="border-color: #3598DC;">
						<input type="text" class="form-control" placeholder="Grand Total" name="grandtotal"
							   id="grandtotal" value='<?php echo $default['grandtotal']; ?>' readonly>
					</div>
				</div>


			</div>
			<div class="form-actions">
				<a class="btn default" data-toggle="modal" href="#responsive"> Tambah Data </a>
				<!--<button type="button" class="btn default" onclick=hapus()>Hapus</button>-->
				<button type="submit" class="btn red" name="submit_sales">Submit</button>
			</div>
		</form>
	</div>
</div>


<!--------------------------- MODAL ADD ITEM--------------------------------------------->
<?php
$list_item2 = $this->db->query("SELECT d.gudang_id, d.item_id, i.nama as nitem, SUM(qty_in) - SUM(qty_out) as jml_qty, d.nomor_transaksi
                                          FROM public.beone_gudang_detail d INNER JOIN public.beone_item i ON d.item_id = i.item_id
                                          WHERE d.flag = 1 AND d.gudang_id = 2 GROUP BY i.nama, d.item_id, d.gudang_id, d.nomor_transaksi");

$gudang = 2;

$gudang_asal = $this->db->query("SELECT * FROM public.beone_gudang WHERE gudang_id = " . intval($gudang));
$hasil_gudang_asal = $gudang_asal->row_array();
$gudang_id = $hasil_gudang_asal['gudang_id'];
$nama = $hasil_gudang_asal['nama'];
?>

<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Detail Item</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-sm-12">
				<!--<div name="txtItemAsal" id="txtItemAsal"><b>Item</b></div>-->
				<div class="form-group">
					<label>Item</label>
					<input type="hidden" class="form-control" name="nodoc" id="nodoc" required>
					<select id='item_modal' class='form-control input-sm select2-multiple' name='item_modal'
							onchange="copydoc();">
						<option value=""></option>
						<?php foreach ($list_item2->result_array() as $row) {
							if ($row['jml_qty'] <> 0) {
								?>
								<option
									value="<?php echo $row['item_id']; ?>"><?php echo $row['nitem'] . " | Ready Stock = " . number_format($row['jml_qty'], 2) . " | Doc :" . $row['nomor_transaksi']; ?></option>
								<?php
							}
						}
						?>
					</select>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label>Quantity</label>
					<input type='text' class='form-control' placeholder="Quantity" name='qty_modal' id='qty_modal'
						   onchange="autoAmount()" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Price</label>
					<input type='text' class='form-control' placeholder="Price" name='price_modal' id='price_modal'
						   onchange="autoAmount()" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Amount</label>
					<input type='text' class='form-control' placeholder="Amount" name='amount_modal' id='amount_modal'
						   required>
				</div>
			</div>
		</div>

	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
		<button type="button" class="btn green" name="add_btn" id="add_btn">Insert</button>
	</div>
</div>

<script type="text/javascript">
	function validasi_input(form) {
		if (form.grandtotal.value == 0) {
			alert("Data detail penjualan belum ada...!!!");
			form.grandtotal.focus();
			return (false);
		}
		return (true);
	}
</script>

<script>
	$(document).ready(function () {
		var count = 0;

		$("#add_btn").click(function () {
			count += 1;

			var item = document.getElementById('item_modal');
			var qty = document.getElementById('qty_modal');
			var price = document.getElementById('price_modal');
			var amount = document.getElementById('amount_modal');
			var namaItem = $('#item_modal option:selected').text();

			$('#container').append(
				'<tr class="records" id="' + count + '">'
				+ '<td><input class="form-control" id="item_' + count + '" name="item_' + count + '" type="text" value="' + namaItem + '" readonly></td>'
				+ '<td><input class="form-control" id="qty_' + count + '" name="qty_' + count + '" type="text" value="' + qty.value + '" readonly></td>'
				+ '<td><input class="form-control" id="price_' + count + '" name="price_' + count + '" type="text" value="' + price.value + '" readonly></td>'
				+ '<td><input class="form-control" id="amount_' + count + '" name="amount_' + count + '" type="text" value="' + amount.value + '" readonly></td>'
				+ '<td><button type="button" class="btn red" onclick="hapus(' + count + ')">X</button></td>'
				+ '<td><input id="rows_' + count + '" name="rows[]" value="' + count + '" type="hidden"></td>'
				+ '<td><input class="form-control" id="item_id_' + count + '" name="item_id_' + count + '" type="hidden" value="' + item.value + '" readonly></td></tr>'
			);

			autoSubtotal();
			eraseText();
			cekppn1();
			cekGrandTotal();
			$('#responsive').modal('hide');
		});

		$(".remove_item").live('click', function (ev) {
			if (ev.type == 'click') {
				$(this).parents(".records").fadeOut();
				$(this).parents(".records").remove();
			}
		});
	});

	/*function hapus(x){
			alert(x);
			document.getElementById('datatable').deleteRow(x);
	}*/


	function eraseText() {
		document.getElementById("item_modal").value = "";
		document.getElementById("qty_modal").value = "";
		document.getElementById("price_modal").value = "";
		document.getElementById("amount_modal").value = "";
	}


	function hapus(rowid) {
		autominSubtotal("amount_" + rowid);
		var row = document.getElementById(rowid);
		row.parentNode.removeChild(row);

		cekppn1();
		cekGrandTotal();
		//var amnt = document.getElementById("amount_"+rowid).value;
	}
</script>

<script type="text/javascript">
	var qty_modal = document.getElementById('qty_modal');
	qty_modal.addEventListener('keyup', function (e) {
		qty_modal.value = formatRupiah(this.value, 'Rp. ');
	});

	var price_modal = document.getElementById('price_modal');
	price_modal.addEventListener('keyup', function (e) {
		price_modal.value = formatRupiah(this.value, 'Rp. ');
	});

	var amount_modal = document.getElementById('amount_modal');
	amount_modal.addEventListener('keyup', function (e) {
		amount_modal.value = formatRupiah(this.value, 'Rp. ');
	});

	var subtotal = document.getElementById('subtotal');
	subtotal.addEventListener('keyup', function (e) {
		subtotal.value = formatRupiah(this.value, 'Rp. ');
	});

	var grandtotal = document.getElementById('grandtotal');
	grandtotal.addEventListener('keyup', function (e) {
		grandtotal.value = formatRupiah(this.value, 'Rp. ');
	});


	/* Fungsi formatRupiah */
	function formatRupiah(angka, prefix) {
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split = number_string.split(','),
			sisa = split[0].length % 3,
			rupiah = split[0].substr(0, sisa),
			ribuan = split[0].substr(sisa).match(/\d{3}/gi);

		// tambahkan titik jika yang di input sudah menjadi angka ribuan
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}

		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
	}
</script>

<script>
	function autoAmount() {
		var a = document.getElementById('qty_modal').value;
		var b = document.getElementById('price_modal').value;

		var c = a.split('.').join('');
		var d = b.split('.').join('');
		document.getElementById('amount_modal').value = c * d;
	}
</script>


<script>
	function autoSubtotal() {
		var a = document.getElementById('subtotal').value;
		var b = document.getElementById('amount_modal').value;

		var c_ = a.split('.').join('');
		var c = c_.split(',').join('.');

		var d_ = b.split('.').join('');
		var d = d_.split(',').join('.');

		document.getElementById('subtotal').value = (c * 1) + (d * 1);
		document.getElementById('subtotal_backup').value = (c * 1) + (d * 1);
	}

	function autominSubtotal(amnt) {
		var a = document.getElementById('subtotal').value;
		var b = document.getElementById(amnt).value;

		var c = a.split('.').join('');
		var d = b.split('.').join('');
		document.getElementById('subtotal').value = (c * 1) - (d * 1);
		document.getElementById('subtotal_backup').value = (c * 1) - (d * 1);
	}

	function cekppn(ppn) {
		var b = document.getElementById('subtotal').value;
		var bx = document.getElementById('subtotal_backup').value;
		var d = b.split('.').join('');
		var bxx = bx.split('.').join('');

		if (ppn == 0) {//tanpa ppn
			document.getElementById('ppn').value = d * 0;

			var zx = document.getElementById('subtotal_backup').value;
			document.getElementById('subtotal').value = zx;
		} else if (ppn == 1) {//menggunakan ppn
			var ddc = bxx * 0.1;
			document.getElementById('ppn').value = ddc;

			var zx = document.getElementById('subtotal_backup').value;
			document.getElementById('subtotal').value = zx;
		} else if (ppn == 2) {//include ppn
			var dd = Math.round(d / 1.1);
			var zz = Math.round((dd * 10) / 100);
			document.getElementById('subtotal').value = dd;
			document.getElementById('ppn').value = zz;
		}

		cekGrandTotal();
	}

	function cekppn1() {
		var ppn1 = document.getElementById('optionsRadios4').checked;
		var ppn2 = document.getElementById('optionsRadios5').checked;
		var ppn3 = document.getElementById('optionsRadios6').checked;


		var b = document.getElementById('subtotal').value;
		var d = b.split('.').join('');

		if (ppn1 == true) {
			document.getElementById('ppn').value = d * 0;
		} else if (ppn2 == true) {
			var ddc = d * 0.1;
			document.getElementById('ppn').value = ddc;
		} else if (ppn3 == true) {
			var dd = Math.round(d / 1.1);
			var zz = Math.round((dd * 10) / 100);
			document.getElementById('subtotal').value = dd;
			document.getElementById('ppn').value = zz;
		}
	}


	function cekGrandTotal() {
		var subtotal_ = document.getElementById('subtotal').value;
		var ppn_ = document.getElementById('ppn').value;

		var subtotal = subtotal_.split('.').join('');
		var ppn = ppn_.split('.').join('');

		var gt = (subtotal * 1) + (ppn * 1);
		var grandtotal = document.getElementById('grandtotal').value = gt;
		//var grandtotal = document.getElementById('grandtotal').value = gt.toLocaleString(undefined, {maximumFractionDigits:2});

	}

</script>

<script>
	function copydoc() {
		var namaItem = $('#item_modal option:selected').text();
		var nodoc = document.getElementById('nodoc').value = namaItem;
	}
</script>
