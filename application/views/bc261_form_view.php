<div class="container">
    <div class="row">
        <p class="text-center" style="font-size: 17px"><b>PEMBERITAHUAN IMPOR BARANG UNTUK DITIMBUN <br>DI TEMPAT PENIMBUNAN BERIKAT DENGAN JAMINAN</b></p>
    </div>

    <form action="">
        <div class="form-group row">
            <label for="status" class="col-sm-2 col-form-label">Status</label>
            <div class="col-sm-2">
                <input style="border: none;" type="text" readonly class="form-control" id="status" value="EDIT">
            </div>
        </div>

        <div class="form-group row">
            <label for="status_perbaikan" class="col-sm-2 col-form-label">Status Perbaikan</label>
            <div class="col-sm-2">
                <input style="border: none;" type="text" readonly class="form-control" id="status_perbaikan" value="-">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-2">
                <a href="#modal" class="btn btn-primary btn-sm" role="button" data-target="#modal" data-toggle="modal">DAFTAR RESPON</a>
            </div>
        </div>

        <!-- FORM NOMOR PENGAJUAN -->
        <div style="margin-top: 30px;">
            <div class="form-group row">
                <label for="nomor_pengajuan" class="col-sm-2 col-form-label">Nomor Pengajuan</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="nomor_pengajuan" value = "">
                </div>
            </div>

            <div class="form-group row">
                <label for="nomor_pendaftaran" class="col-sm-2 col-form-label">Nomor Pendaftaran</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="nomor_pendaftaran" value="">
                </div>
            </div>

            <div class="form-group row">
                <label for="tanggal_pendaftaran" class="col-sm-2 col-form-label">Tanggal Pendaftaran</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="tanggal_pendaftaran" value="" placeholder="DDMMYY">
                </div>
            </div>		
        </div>
        <!-- TUTUP FORM NOMOR PENGAJUAN -->

        <!-- FORM KPPBC BONGKAR -->
        <div style="margin-top: 30px;">


            <div class="form-group row">
                <label for="kppbc_pengawas" class="col-sm-2 col-form-label">Kantor Pabean</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="kppbc_pengawas" value="">
                </div>
            </div>

            <div class="form-group row">
                <label for="tujuan" class="col-sm-2 col-form-label">Tujuan Pengiriman </label>
                <div class="col-sm-4">
                    <select id="inputState" class="form-control">
                        <option selected>Choose...</option>
                        <option>...</option>
                    </select>
                </div>
            </div>		
        </div>
        <!-- TUTUP FORM KPPBC BONGKAR -->


        <div class="row">
            <div class="col-sm-12">

                <!-- FORM LEFT SIDE -->
                <ol>
                    <div class="col-sm-6">
                        <p><b>PENGUSAHA</b></p>
                        <!-- FORM PENGUSAHA -->
                        <div>
                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Identitas</label>
                                    <div class="col-sm-4">
                                        <select id="inputState" class="form-control">
                                            <option selected>NPWP</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="nama" value = "">
                                    </div>
                                </li>
                            </div>
                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="nama" value = "">
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="alamat" value = "">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Negara" class="col-sm-3 col-form-label">IJIN PTB</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="negara" value = "">
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="negara" value = "">
                                </div>
                            </div>
                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">API</label>
                                    <div class="col-sm-4">
                                        <select id="inputState" class="form-control">
                                            <option selected>API</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="nama" value = "">
                                    </div>
                                </li>
                            </div>

                        </div>
                        <!-- TUTUP FORM PEMASOK -->		

                        <!-- FORM Penerima barang -->
                        <div>
                            <p style="margin-left: -38px"><b>PENERIMA BARANG</b></p>

                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Identitas</label>
                                    <div class="col-sm-4">
                                        <select id="inputState" class="form-control">
                                            <option selected>NPWP</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="nama" value = "">
                                    </div>
                                </li>
                            </div>	

                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="alamat" value ="" >
                                    </div>
                                </li>

                            </div>
                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="alamat" value ="" >
                                    </div>
                                </li>

                            </div>
                        </div>
                        <!-- TUTUP FORM IMPORTIR -->

                        <!-- FORM PEMILIK -->
                        <div>
                            <p style="margin-left: -38px"><b>DOKUMEN [F6]</b></p>

                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Packing List</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="nama" value ="" >
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="nama" value ="" >
                                    </div>
                                </li>
                            </div>	

                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Fasilitas Impor</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="nama" value ="" >
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="nama" value ="" >
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" id="nama" value ="" >
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <li>
                                    <div class="col-sm-10">
                                        <table class="table table-sm">
                                            <thead>
                                                <tr class="bg-success">
                                                    <th scope="col">Jenis Dokumen</th>
                                                    <th scope="col">Nomor Dokumen</th>
                                                    <th scope="col">Tanggal</th>
                                                </tr>
                                            </thead>
                                            <tbody>


                                            </tbody>
                                        </table>
                                    </div>
                                </li>
                            </div>
                        </div>


                        <!-- FORM HARGA -->
                        <div>
                            <p style="margin-left: -38px"><b>HARGA</b></p>


                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Valuta</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="alamat" value="" >
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <li>
                                    <div class="col-sm-3">
                                        <button type="submit" class="btn blue" name="submit_user">NDPBM</button>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="api" value ="" >
                                    </div>
                                </li>
                                <!--									<div class="col-sm-4">
                                                                                                                <input type="text" class="form-control" id="alamat" >
                                                                                                        </div>-->
                            </div>

                            <div class="form-group row">
                                <li>
                                    <label for="Negara" class="col-sm-3 col-form-label">Nilai CIF</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="alamat" value="">
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">

                                <label for="Negara" class="col-sm-3 col-form-label">Nilai CIF (Rp)</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="alamat" value="">
                                </div>

                            </div>


                        </div>
                        <!-- TUTUP FORM HARGA -->	
                    </div>
                    <!-- TUTUP FORM LEFT SIDE -->


                    <!-- FORM RIGHT SIDE -->
                    <div class="col-sm-6" style="padding-right: 50px;">
                        <p><b>PENGANGKUTAN</b></p>
                        <div class="form-group row">
                            <li>
                                <label for="nama" class="col-sm-4 col-form-label">Jenis Sarana Pengangkut</label>
                                <div class="col-sm-6">
                                    <select id="inputState" class="form-control">
                                        <option selected></option>
                                        <option>...</option>
                                    </select>
                                </div>
                            </li>
                        </div>


                        <div class="form-group row">
                            <li>
                                <label for="tujuan" class="col-sm-10 col-form-label">KONTAINER [F5]</label>
                            </li>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <table class="table table-sm">
                                    <thead>
                                        <tr class="bg-success">
                                            <th scope="col">No Cont</th>
                                            <th scope="col">Ukuran</th>
                                            <th scope="col">Tipe</th>
                                        </tr>
                                    </thead>
                                    <tbody>



                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group row">
                            <li>
                                <label for="tujuan" class="col-sm-10 col-form-label">KEMASAN [F7]</label>
                            </li>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <table class="table table-sm">
                                    <thead>
                                        <tr class="bg-success">
                                            <th scope="col">Jumlah</th>
                                            <th scope="col">Kode Jenis</th>
                                            <th scope="col">Jenis</th>
                                        </tr>
                                    </thead>
                                    <tbody>



                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- FORM BARANG -->
                        <div>
                            <p style="margin-left: -38px"><b>BARANG</b></p>

                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Bruto (Kg)</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="nama" value ="" >
                                    </div>
                                </li>
                            </div>
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Netto (Kg)</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="nama" value ="" >
                                    </div>

                                </li>

                            </div>	
                            <div class="form-group row">

                                <label class="col-sm-3 col-form-label">Jumlah Barang</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="nama" value ="" >
                                </div>



                            </div>	
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <table class="table table-sm">
                                    <thead>
                                        <tr class="bg-success">
                                            <th scope="col">Jenis Pungutan</th>
                                            <th scope="col">Jumlah</th>

                                        </tr>
                                    </thead>
                                    <tbody>



                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- FORM PUNGUTAN-->
                        <!--							<div class="form-group row">
                                                                                <li>
                                                                                        <label for="tujuan" class="col-sm-10 col-form-label">Pngutan</label>
                                                                                </li>
                                                                        </div>-->







                </ol>
                <div class="form-group row">
                    <li>
                        <label for="tujuan" class="col-sm-10 col-form-label">JAMINAN [F3]</label>
                    </li>
                </div>
                <table class="table table-striped table-bordered table-hover">

                    <thead>
                        <tr class="bg-success">
                            <th scope="col">Jenis</th>
                            <th scope="col">Nomor</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col">Nilai</th>
                            <th scope="col">Jatuh Tempo</th>
                            <th scope="col">Penjamin</th>
                            <th scope="col">Nomor BPJ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                        </tr>
                    </tbody>
                </table>
                <p>Dengan saya menyatakan bertanggung jawab atas kebenaran
                    <br></br>hal hal yang diberitahukan dalam pemberitahuan pabean ini. </p>
                <div class="form-group row">
                    <!--<li>-->
                    <!--<label for="Negara" class="col-sm-3 col-form-label">Pelabuhan Bongkar</label>-->
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="alamat" Placeholder="kota" >
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="alamat" Placeholder="Tanggal">
                    </div>
                    <!--</li>-->
                </div>
                <div class="form-group row">
                    <!--<li>-->
                    <label for="Negara" class="col-sm-2 col-form-label">Pemberitahu</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="alamat" Placeholder="nama" >
                    </div>
                    <!--</li>-->
                </div>
                <div class="form-group row">
                    <!--<li>-->
                    <label for="Negara" class="col-sm-2 col-form-label">Jabatan</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="alamat" Placeholder="jabatan" >
                    </div>
                    <!--</li>-->
                </div>
            </div>


        </div>		
        <!-- TUTUP FORM RIGHT SIDE -->

</div>
</div>
</form>
<!-- MODAL DOKUMEN -->

<div id="modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>RESPON</b></h3>
        </div>
        <div class="modal-body">
            
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-sm">
                        <thead>
                            <tr>
                                <th scope="col"><center><p style="font-size: 12px;">Kode</p></center></th>
                        <th scope="col"><center><p style="font-size: 12px;">Uraian</p></center></th>
                        <th scope="col"><center><p style="font-size: 12px;">Waktu</p></center></th>
                        </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                    </table>
                </div>
            </div>	
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-primary">Save changes</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>
</div>
<!-- TUTUP CONTAINER -->
