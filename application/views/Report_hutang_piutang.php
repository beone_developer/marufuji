<?php
  $tgl_awal = $_GET['tglawal'];
  $tgl_akhir = $_GET['tglakhir'];
  $tipe = $_GET['hp'];

  $thn_awal = substr($tgl_awal,6,4);
  $bln_awal = substr($tgl_awal,0,2);
  $day_awal = substr($tgl_awal,3,2);
  $tgl_awal_formated = $thn_awal."-".$bln_awal."-".$day_awal;

  $thn_akhir = substr($tgl_akhir,6,4);
  $bln_akhir = substr($tgl_akhir,0,2);
  $day_akhir = substr($tgl_akhir,3,2);
  $tgl_akhir_formated = $thn_akhir."-".$bln_akhir."-".$day_akhir;

  $tgl_awal_tahun = $thn_awal."-01-"."01";
?>
          <h1>
              <center><b>Report Kartu <?php if ($tipe == 1){echo "Hutang";}else{echo "Piutang";}?></b></center>
          </h1>
          <hr />

              <div class="row">
                  <div class="col-xs-12">
                    <?php
                		//$list_custsup = $this->db->query("SELECT DISTINCT h.custsup_id, c.nama, h.tipe_trans FROM public.beone_hutang_piutang h INNER JOIN public.beone_custsup c ON h.custsup_id = c.custsup_id WHERE tipe_trans = $tipe");

                    if ($tipe == 1){//hutang
                        $list_custsup = $this->db->query("SELECT custsup_id, nama, alamat, tipe_custsup, negara, saldo_hutang_idr, saldo_hutang_valas, saldo_piutang_idr, saldo_piutang_valas, flag
                        FROM public.beone_custsup where tipe_custsup =1");
                    }else{//piutang
                        $list_custsup = $this->db->query("SELECT custsup_id, nama, alamat, tipe_custsup, negara, saldo_hutang_idr, saldo_hutang_valas, saldo_piutang_idr, saldo_piutang_valas, flag
                        FROM public.beone_custsup where tipe_custsup =2");
                    }


                    foreach($list_custsup->result_array() as $row){

                      //SALDO AWAL HUTANG PIUTANG
                      $sacs = $this->db->query("SELECT * FROM public.beone_custsup WHERE custsup_id =".intval($row['custsup_id']));
                      $saldo_awal_cs = $sacs->row_array();

                      //SALDO MUTASI HUTANG PIUTANG DARI AWAL TAHUN SAMPAI SEKARANG
                      $sm = $this->db->query("SELECT SUM(idr_trans) as idr, SUM(valas_trans) as valas, SUM(idr_pelunasan) as idr_p, SUM(valas_pelunasan) as valas_p FROM public.beone_hutang_piutang WHERE trans_date BETWEEN '$tgl_awal_tahun' AND '$tgl_awal_formated' AND custsup_id =".intval($row['custsup_id']));
                      $saldo_mutasi_hp = $sm->row_array();

                      $saldo_awal_hp_valas = ($saldo_awal_cs['saldo_hutang_valas'] + $saldo_mutasi_hp['valas']) - $saldo_mutasi_hp['valas_p'];
                      $saldo_awal_hp_idr = ($saldo_awal_cs['saldo_hutang_idr'] + $saldo_mutasi_hp['idr']) - $saldo_mutasi_hp['idr_p'];

                      echo "<h4><b>".$row['nama']."</b></h4>";
                    ?>
                      <table class="table table-striped table-hover">
                          <thead>
                              <tr>
                                  <th><small> Tanggal </small></th>
                                  <th><small> Nomor </small></th>
                                  <th><small> Ket </small></th>
                                  <th><small> <?php if($tipe == 1){echo "Beli";}else{echo "Jual";}?> USD </small></th>
                                  <th><small> <?php if($tipe == 1){echo "Beli";}else{echo "Jual";}?> IDR </small></th>
                                  <th><small> Bayar USD </small></th>
                                  <th><small> Bayar IDR </small></th>
                                  <th><small> Saldo USD </small></th>
                                  <th><small> Saldo IDR </small></th>
                              </tr>
                          </thead>

                          <tbody>
                              <tr>
                                  <td><small></small></td>
                                  <td><small></small></td>
                                  <td><small></small></td>
                                  <td><small></small></td>
                                  <td><small></small></td>
                                  <td><small></small></td>
                                  <td><small></small></td>
                                  <td><small><?php echo number_format($saldo_awal_hp_valas,2);?></small></td>
                                  <td><small><?php echo number_format($saldo_awal_hp_idr,2);?></small></td>
                                </tr>

                            <?php

                                $total_hp_usd = 0;
                                $total_hp_idr = 0;
                                $total_lunas_usd = 0;
                                $total_lunas_idr = 0;
                                $total_sa_usd = $saldo_awal_hp_valas;
                                $total_sa_idr = $saldo_awal_hp_idr;

                                if ($tipe == 1){//hutang
                                      $custsup_saldo_awal = $this->db->query("SELECT * FROM public.beone_custsup WHERE flag = 1 AND saldo_piutang_idr != 0 OR saldo_piutang_valas != 0");
                                }else{//piutang

                                }


                                $mutasi_hp = $this->db->query("SELECT h.hutang_piutang_id, h.custsup_id, c.nama, h.trans_date, h.nomor, h.keterangan, h.valas_trans, h.idr_trans, h.valas_pelunasan, h.idr_pelunasan, h.tipe_trans
                                                              	FROM public.beone_hutang_piutang h INNER JOIN public.beone_custsup c ON h.custsup_id = c.custsup_id
                                                                WHERE h.tipe_trans=$tipe AND h.trans_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND h.custsup_id = $row[custsup_id] ORDER BY h.trans_date ASC");

                                foreach($mutasi_hp->result_array() as $row_mutasi){

                                $total_hp_usd = $total_hp_usd + $row_mutasi['valas_trans'];
                                $total_hp_idr = $total_hp_idr + $row_mutasi['idr_trans'];
                                $total_lunas_usd = $total_lunas_usd + $row_mutasi['valas_pelunasan'];
                                $total_lunas_idr = $total_lunas_idr + $row_mutasi['idr_pelunasan'];

                                $sa_usd = ($total_sa_usd + $row_mutasi['valas_trans'])- $row_mutasi['valas_pelunasan'];
                                $sa_idr = ($total_sa_idr + $row_mutasi['idr_trans']) - $row_mutasi['idr_pelunasan'];

                                $total_sa_usd = ($total_sa_usd + $row_mutasi['valas_trans'] - $row_mutasi['valas_pelunasan']);
                                $total_sa_idr = ($total_sa_idr + $row_mutasi['idr_trans']) - $row_mutasi['idr_pelunasan'];

                              ?><tr>
                                  <td><small><?php echo $row_mutasi['trans_date'];?></small></td>
                                  <td><small><?php echo $row_mutasi['nomor'];?></small></td>
                                  <td><small></small></td>
                                  <td><small><?php echo number_format($row_mutasi['valas_trans'],2);?></small></td>
                                  <td><small><?php echo number_format($row_mutasi['idr_trans'],2);?></small></td>
                                  <td><small><?php echo number_format($row_mutasi['valas_pelunasan'],2);?></small></td>
                                  <td><small><?php echo number_format($row_mutasi['idr_pelunasan'],2);?></small></td>
                                  <td><small><?php echo number_format($sa_usd,2);?></small></td>
                                  <td><small><?php echo number_format($sa_idr,2);?></small></td>
                                </tr>
                              <?php
                                }
                              ?>
                                <td><small></small></td>
                                <td><small></small></td>
                                <td><small></small></td>
                                <td><b><small><?php echo number_format($total_hp_usd,2);?></small></b></td>
                                <td><b><small><?php echo number_format($total_hp_idr,2);?></small></b></td>
                                <td><b><small><?php echo number_format($total_lunas_usd,2);?></small></b></td>
                                <td><b><small><?php echo number_format($total_lunas_idr,2);?></small></b></td>
                                <td><b><small><?php echo number_format($total_sa_usd,2);?></small></b></td>
                                <td><b><small><?php echo number_format($total_sa_idr,2);?></small></b></td>


                          </tbody>
                      </table>
                      <br />
                      <?php
                        }
                        ?>
                  </div>
              </div>


          </div>
