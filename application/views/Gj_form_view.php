<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Form General Journal</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">
          <form role="form" method="post">
              <div class="form-body">
                <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                            <label>Tanggal Journal</label>
                            <div class="input-group">
                              <span class="input-group-addon input-circle-left">
                                  <i class="fa fa-calendar"></i>
                              </span>
                              <input class="form-control form-control-inline input-medium date-picker input-circle-right" name="tanggal_journal" size="16" type="text" value="<?php echo date('m/d/Y');?>" />
                              <span class="help-block"></span>
                            </div>
                      </div>

                          <div name="txtSatuan" id="txtSatuan"><b>Nomor Journal</b></div>

                  </div>


                  <div class="col-sm-8" id="detail_cash_bank">
                      <div class="portlet light bordered">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Akun</label>
                                    <div class="input-group">
                                       <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="coa_id" onChange="tampilnomor(this.value);" required>
                                            <option value=""></option>
                                         <?php 	foreach($list_coa as $row){ ?>
                                           <option value="<?php echo $row['coa_id'];?>"><?php echo $row['nomor']." | ".$row['nama'];?></option>
                                         <?php } ?>
                                       </select>
                                 </div>
                               </div>
                            </div>

                            <div class="col-sm-6">
                              <div class="form-group">
                                  <label>Akun Lawan Transaksi</label>
                                  <div class="input-group">
                                     <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="coa_id_lawan" onChange="tampilnomor(this.value);" required>
                                        <option value=""></option>
                                       <?php 	foreach($list_coa as $row){ ?>
                                         <option value="<?php echo $row['coa_id'];?>"><?php echo $row['nomor']." | ".$row['nama'];?></option>
                                       <?php } ?>
                                     </select>
                               </div>
                             </div>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label>Keterangan Voucher</label>
                              <input type="text" class="form-control" placeholder="Keterangan" name="keterangan_journal" required>
                            </div>
                          </div>
                          <div class="col-sm-12">
                              <div class="row">
                                <div class="col-sm-4">
                                  <div class="form-group">
                                  <label>Jumlah IDR</label>
                                  <input type="text" class="form-control" placeholder="Jumlah IDR" name="jumlah_idr" id="saldo" required>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>

                      </div>
                  </div>
              </div>
            </div>

            <div class="form-actions">
                <button type="button" class="btn default">Cancel</button>
                <?php if(helper_security("jurnal_umum_add") == 1){?>
                <button type="submit" class="btn red" name="submit_journal">Submit</button>
                <?php }?>
            </div>
          </form>
      </div>
    </div>

    <script>
				function tampilnomor(str)
				{
				if (str=="")
				  {
				  document.getElementById("txtSatuan").innerHTML="";
				  return;
				  }
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
					document.getElementById("txtSatuan").innerHTML=xmlhttp.responseText;
					}
				  }
				xmlhttp.open("GET","Gj_controller/get_nomor_journal?q="+str,true);
				xmlhttp.send();
				}
				</script>


        <script type="text/javascript">

    		var saldo = document.getElementById('saldo');
    		saldo.addEventListener('keyup', function(e){
    			saldo.value = formatRupiah(this.value, 'Rp. ');
    		});

    		/* Fungsi formatRupiah */
    		function formatRupiah(angka, prefix){
    			var number_string = angka.replace(/[^,\d]/g, '').toString(),
    			split   		= number_string.split(','),
    			sisa     		= split[0].length % 3,
    			rupiah     		= split[0].substr(0, sisa),
    			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    			// tambahkan titik jika yang di input sudah menjadi angka ribuan
    			if(ribuan){
    				separator = sisa ? '.' : '';
    				rupiah += separator + ribuan.join('.');
    			}

    			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    		}
    	</script>
