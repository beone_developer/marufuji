<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Form Debit Note</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">
          <form role="form" method="post">
              <div class="form-body">
                <div class="row">

                  <div class="col-sm-4">
                      <h2><?php echo $default['trans_date'];?></h2>
                      <b><h2><?php echo $default['nomor'];?></b></h2>
                      <input type="hidden" name="hp_id" value='<?php echo $default['hutang_piutang_id'];?>'>
                      <input type="hidden" name="kdn_no" value='<?php echo $default['nomor'];?>'>
                  </div>



                  <div class="col-sm-8" id="detail_cash_bank">
                      <div class="portlet light bordered">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?php
                                          $coa_debit_note = $this->db->query("SELECT * FROM public.beone_gl WHERE gl_number = '$default[nomor]' AND debet = 0");
                                          $hasil_coa_debit_note = $coa_debit_note->row_array();
                                          $biaya_coa_id = $hasil_coa_debit_note['coa_id'];
                                          $biaya_coa_no = $hasil_coa_debit_note['coa_no'];
                                    ?>

                                    <label>Akun Biaya</label>
                                    <div class="input-group">
                                       <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="coa_id" required>
                                          <option value=<?php echo $biaya_coa_id;?>><?php echo $biaya_coa_no;?></option>
                                         <?php 	foreach($list_coa as $row){ ?>
                                           <option value="<?php echo $row['coa_id'];?>"><?php echo $row['nomor']." | ".$row['nama'];?></option>
                                         <?php } ?>
                                       </select>
                                 </div>
                               </div>
                            </div>

                            <div class="col-sm-6">
                              <div class="form-group">
                                <?php
                                      $sql_customer = $this->db->query("SELECT * FROM public.beone_custsup WHERE custsup_id = ".intval($default['custsup_id']));
                                      $hasil_customer = $sql_customer->row_array();
                                      $nama_customer = $hasil_customer['nama'];
                                ?>
                                  <label>Customer</label>
                                  <div class="input-group">
                                     <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="customer" onChange="tampilkode(this.value);" required>
                                        <option value=<?php echo $default['custsup_id'];?>><?php echo $nama_customer;?></option>
                                       <?php 	foreach($list_customer as $row){ ?>
                                         <option value="<?php echo $row['custsup_id'];?>"><?php echo $row['nama'];?></option>
                                       <?php } ?>
                                     </select>
                               </div>
                             </div>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label>Keterangan</label>
                              <input type="text" class="form-control" placeholder="Keterangan" name="keterangan_kredit_note" value='<?php echo $default['keterangan'];?>' required>
                            </div>
                          </div>
                          <div class="col-sm-12">
                              <div class="row">
                                <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Posisi</label>
                                      <div class="input-group">
                                         <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="posisi" required>
                                              <?php
                                                  if ($default['valas_pelunasan'] > 0 OR $default['idr_pelunasan'] > 0){
                                                     $nilai_plus_min = 0; //nilai minus
                                                     $plus_min = "MINUS (-)";
                                                     $idr = $default['idr_pelunasan'];
                                                     $valas = $default['valas_pelunasan'];
                                                  }else{
                                                    $nilai_plus_min = 1; //nilai plus
                                                    $plus_min = "PLUS (+)";
                                                    $idr = $default['idr_trans'];
                                                    $valas = $default['valas_trans'];
                                                  }
                                              ?>
                                              <option value='<?php echo $nilai_plus_min;?>'><?php echo $plus_min;?></option>
                                              <option value='0'>MINUS (-)</option>
                                              <option value='1'>PLUS (+)</option>
                                         </select>
                                   </div>
                                 </div>
                                </div>
                                <div class="col-sm-4">
                                  <div class="form-group">
                                  <label>Jumlah IDR</label>
                                  <?php
                                      $saldo_pelunasan = str_replace(".", ",", $idr);
                                  ?>
                                  <input type="text" class="form-control" placeholder="Jumlah IDR" name="jumlah_idr" id="saldo" value='<?php echo $saldo_pelunasan;?>' required>
                                  </div>
                                </div>
                                <div class="col-sm-4">
                                  <div class="form-group">
                                  <label>Jumlah Valas</label>
                                  <?php
                                      $saldo_pelunasan_valas = str_replace(".", ",", $valas);
                                  ?>
                                  <input type="text" class="form-control" placeholder="Jumlah Valas" name="jumlah_valas" id="saldo_valas" value='<?php echo $saldo_pelunasan_valas;?>' required>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>

                      </div>
                  </div>
              </div>
            </div>

            <div class="form-actions">
                <a href="<?php echo base_url();?>Debit_note_controller/List_debit_note" class="btn default"> Cancel</a>
                <button type="submit" class="btn red" name="submit_debit_note">Submit</button>
            </div>
          </form>
      </div>
    </div>

    <script>
				function tampilkode(str)
				{
				if (str=="")
				  {
				  document.getElementById("txtkdnote").innerHTML="";
				  return;
				  }
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
					document.getElementById("txtkdnote").innerHTML=xmlhttp.responseText;
					}
				  }
				xmlhttp.open("GET","Kredit_note_controller/get_nomor_kredit_note?q="+str,true);
				xmlhttp.send();
				}
				</script>


        <script type="text/javascript">

    		var saldo = document.getElementById('saldo');
    		saldo.addEventListener('keyup', function(e){
    			saldo.value = formatRupiah(this.value, 'Rp. ');
    		});

        var saldo_valas = document.getElementById('saldo_valas');
    		saldo_valas.addEventListener('keyup', function(e){
    			saldo_valas.value = formatRupiah(this.value, 'Rp. ');
    		});

    		/* Fungsi formatRupiah */
    		function formatRupiah(angka, prefix){
    			var number_string = angka.replace(/[^,\d]/g, '').toString(),
    			split   		= number_string.split(','),
    			sisa     		= split[0].length % 3,
    			rupiah     		= split[0].substr(0, sisa),
    			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    			// tambahkan titik jika yang di input sudah menjadi angka ribuan
    			if(ribuan){
    				separator = sisa ? '.' : '';
    				rupiah += separator + ribuan.join('.');
    			}

    			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    		}
    	</script>
