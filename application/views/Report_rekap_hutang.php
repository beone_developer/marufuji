<?php
  $tgl_awal = $_GET['tglawal'];
  $tgl_akhir = $_GET['tglakhir'];

  $thn_awal = substr($tgl_awal,6,4);
  $bln_awal = substr($tgl_awal,0,2);
  $day_awal = substr($tgl_awal,3,2);
  $tgl_awal_formated = $thn_awal."-".$bln_awal."-".$day_awal;

  $thn_akhir = substr($tgl_akhir,6,4);
  $bln_akhir = substr($tgl_akhir,0,2);
  $day_akhir = substr($tgl_akhir,3,2);
  $tgl_akhir_formated = $thn_akhir."-".$bln_akhir."-".$day_akhir;
?>
    <!-- BEGIN PAGE TITLE-->
          <h2>
              <center><b>Rekap Hutang</b></center>
          </h2>
          <h4>
              <center>Periode <?php echo $day_awal."/".$bln_awal."/".$thn_awal;?> sampai <?php echo $day_akhir."/".$bln_akhir."/".$thn_akhir;?></center>
          </h4>
          <hr />

              <div class="row">
                  <div class="col-xs-12">
                      <table class="table table-striped table-hover">
                          <thead>
                              <tr>
                                  <th> Supplier </th>
                                  <th> S Awal IDR </th>
                                  <th> S Awal Valas </th>
                                  <th> Beli IDR </th>
                                  <th> Beli Valas </th>
                                  <th> Pelunasan IDR </th>
                                  <th> Pelunasan Valas </th>
                                  <th> S Akhir IDR </th>
                                  <th> S Akhir Valas </th>
                              </tr>
                          </thead>

                          <tbody>
                            <?php
                            $total_saldo_awal_hutang_idr = 0;
                            $total_saldo_awal_hutang_valas = 0;
                            $total_pembelian_idr = 0;
                            $total_pembelian_valas = 0;
                            $total_pelunasan_idr = 0;
                            $total_pelunasan_valas = 0;
                            $total_saldo_akhir_hutang_idr = 0;
                            $total_saldo_akhir_hutang_valas = 0;

                            foreach($list_supplier as $row){

                              //query mutasi hutang
                              $sql_mutasi_hutang = $this->db->query("SELECT SUM(idr_trans) as mutasi_hutang_idr, SUM(valas_trans) as mutasi_hutang_valas, SUM(idr_pelunasan) as pelunasan_idr, SUM(valas_pelunasan) as pelunasan_valas FROM public.beone_hutang_piutang WHERE tipe_trans = 1 AND flag = 1 AND trans_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND custsup_id = ".intval($row['custsup_id']));
                              $hasil_mutasi_hutang = $sql_mutasi_hutang->row_array();

                              /*
                              $pelunasan_idr = $hasil_mutasi_hutang['pelunasan_idr'] + $row['pelunasan_hutang_idr'];
                              $pelunasan_valas = $hasil_mutasi_hutang['pelunasan_valas'] + $row['pelunasan_hutang_valas'];
                              */

                              $pelunasan_idr = $hasil_mutasi_hutang['pelunasan_idr'];
                              $pelunasan_valas = $hasil_mutasi_hutang['pelunasan_valas'];

                              $saldo_akhir_hutang_idr = ($row['saldo_hutang_idr'] + $hasil_mutasi_hutang['mutasi_hutang_idr']) - $pelunasan_idr;
                              $saldo_akhir_hutang_valas = ($row['saldo_hutang_valas'] + $hasil_mutasi_hutang['mutasi_hutang_valas']) - $pelunasan_valas;
                            ?>
                              <tr>
                                  <td> <?php echo $row['nama'];?> </td>
                                  <td> <?php echo number_format($row['saldo_hutang_idr'], 2);?> </td>
                                  <td> <?php echo number_format($row['saldo_hutang_valas'], 2);?> </td>
                                  <td> <?php echo number_format($hasil_mutasi_hutang['mutasi_hutang_idr'], 2);?> </td>
                                  <td> <?php echo number_format($hasil_mutasi_hutang['mutasi_hutang_valas'],2);?> </td>
                                  <td> <?php echo number_format($pelunasan_idr, 2);?> </td>
                                  <td> <?php echo number_format($pelunasan_valas, 2);?> </td>
                                  <td> <?php echo number_format($saldo_akhir_hutang_idr, 2);?> </td>
                                  <td> <?php echo number_format($saldo_akhir_hutang_valas, 2);?> </td>
                              </tr>
                            <?php
                                $total_saldo_awal_hutang_idr = $total_saldo_awal_hutang_idr + $row['saldo_hutang_idr'];
                                $total_saldo_awal_hutang_valas = $total_saldo_awal_hutang_valas + $row['saldo_hutang_valas'];
                                $total_pembelian_idr = $total_pembelian_idr + $hasil_mutasi_hutang['mutasi_hutang_idr'];
                                $total_pembelian_valas = $total_pembelian_valas + $hasil_mutasi_hutang['mutasi_hutang_valas'];
                                $total_pelunasan_idr = $total_pelunasan_idr + $pelunasan_idr;
                                $total_pelunasan_valas = $total_pelunasan_valas + $pelunasan_valas;
                                $total_saldo_akhir_hutang_idr = $total_saldo_akhir_hutang_idr + $saldo_akhir_hutang_idr;
                                $total_saldo_akhir_hutang_valas = $total_saldo_akhir_hutang_valas + $saldo_akhir_hutang_valas;
                              }
                            ?>

                              <tr>
                                  <td></td>
                                  <td><b><?php echo number_format($total_saldo_awal_hutang_idr);?></b></td>
                                  <td><b><?php echo number_format($total_saldo_awal_hutang_valas);?></b></td>
                                  <td><b><?php echo number_format($total_pembelian_idr);?></b></td>
                                  <td><b><?php echo number_format($total_pembelian_valas);?></b></td>
                                  <td><b><?php echo number_format($total_pelunasan_idr);?></b></td>
                                  <td><b><?php echo number_format($total_pelunasan_valas);?></b></td>
                                  <td><b><?php echo number_format($total_saldo_akhir_hutang_idr);?></b></td>
                                  <td><b><?php echo number_format($total_saldo_akhir_hutang_valas);?></b></td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
