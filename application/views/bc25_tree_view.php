<h3>BC 2.5<b>
  <!-- <?=$tipe?> -->
  </b></h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="form-actions">
            <a href='<?php echo base_url('Bc25_controller/form/');?>' class='btn default'> Create</a>

        </div>
      <div class="tools"> </div>
      <table class="table table-striped table-bordered table-hover" id="sample_2">
              <thead>
                <tr>
                    <th><center>Nomor Pengajuan</center></th>
                    <th><center>Pemilik</center></th>
                    <th><center>Penerima</center></th>
                    <th><center>Jml Brg</center></th>
                    <th><center>Jml Kon</center></th>
                    <th><center>Jml Kms</center></th>
                    <th><center>Status</center></th>
                    <th><center>Action</center></th>
                </tr>
              </thead>
              <tbody>
                <?php   foreach($ptb as $row){ ?>
                  <tr>
                      <td><?php echo $row['NOMOR_AJU'];?></td>
                      <td><?php echo $row['NAMA_PEMILIK'];?></td>
                      <td><?php echo $row['NAMA_PENERIMA_BARANG'];?></td>
                      <td><?php echo $row['JUMLAH_BARANG'];?></td>
                      <td><?php echo $row['JUMLAH_KONTAINER'];?></td>
                      <td><?php echo $row['JUMLAH_KEMASAN'];?></td>
                      <td><?php echo $row['URAIAN_STATUS'];?></td>
                      <td>
                              <a href='<?php echo base_url('Bc25_controller/edit/'.$row['ID'].'');?>' class='btn blue'><i class="fa fa-pencil"></i></a>
                              <a href="javascript:dialogHapus('<?php echo base_url('Bc25_controller/delete/'.$row['ID'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i></a>
                      </td>
                  </tr>
                  <?php
                    }
                  ?>
              </tbody>
          </table>
  </div>
</div>

<script>
  function dialogHapus(urlHapus) {
    if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
    document.location = urlHapus;
    }
  }
</script>
