<h3>Konfigurasi Perusahaan</h3>
<div class="portlet light bordered">
  <div class="portlet-title">


    <form role="form" method="post">
        <div class="form-body">

            <div class="row">
            <div class="col-sm-6">
                <label>Nama Perusahaan</label>
                <input type="text" class="form-control" placeholder="Nama Perusahaan" id="nama_perusahaan" value="<?php echo $data_perusahaan['nama_perusahaan'];?>" name="nama_perusahaan" required>
            </div>
            </div>
            <br />
            <div class="row">
              <div class="col-sm-8">
                  <label>Alamat Perusahaan</label>
                  <input type="text" class="form-control" placeholder="Alamat Perusahaan" id="alamat_perusahaan" value="<?php echo $data_perusahaan['alamat_perusahaan'];?>" name="alamat_perusahaan" required>
              </div>
            </div>
            <br />
            <div class="row">
              <div class="col-sm-4">
                  <label>Kota</label>
                  <input type="text" class="form-control" placeholder="Kota" id="kota" value="<?php echo $data_perusahaan['kota_perusahaan'];?>" name="kota" required>
              </div>
              <div class="col-sm-4">
                  <label>Provinsi</label>
                  <input type="text" class="form-control" placeholder="Provinsi" id="provinsi" value="<?php echo $data_perusahaan['provinsi_perusahaan'];?>" name="provinsi" required>
              </div>
              <div class="col-sm-4">
                  <label>Kode Pos</label>
                  <input type="text" class="form-control" placeholder="Kode Pos" id="kode_pos" value="<?php echo $data_perusahaan['kode_pos_perusahaan'];?>" name="kode_pos" required>
              </div>
            </div>

        </div>
        <br />
        <div class="form-actions">
            <button type="submit" class="btn blue" name="submit_perusahaan">Submit</button>
        </div>
      </form>

      <hr />
      <br />

      <div class="tools"> </div>
  </div>
</div>
