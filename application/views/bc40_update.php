<div class="portlet light bordered">
    <div class="portlet-title">
        
            <div class="row">
                <p class="text-center" style="font-size: 17px"><b>PEMBERITAHUAN PEMASUKAN BARANG ASAL TEMPAT LAIN DALAM DAERAH PABEAN <br> KE TEMPAT PENIMBUNAN BERIKAT</b></p>
            </div>

            <form action="preview_BC40/edit" method = "POST">
                <div class="form-group row">
                    <label for="status" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-2">
                        <input type="hidden" class="form-control" name="id" name ="id" Placeholder="id" value = "<?php
                        if ($ptb_edit) {
                            foreach ($ptb_edit as $data) {
                                echo $data->id;
                            }
                        }
                        ?>" >
                        <input style="border: none;" type="text" readonly class="form-control" name="status" value="<?php
                        $n = 0;
                        if ($ptb_edit) {
                            foreach ($ptb_edit as $data) {
                                $n = $n + 1;
                                echo $data->URAIAN_STATUS;
                            }
                        }
                        ?>">
                    </div>

                    <div style="margin-top: -10px;" class="col-sm-2 col-sm-offset-6">
                        <a href="#modal-daftar-respon" data-target="#modal-daftar-respon" data-toggle="modal">
                            <p><b>DAFTAR RESPON</b></p>
                        </a>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="status_perbaikan" class="col-sm-2 col-form-label">Status Perbaikan</label>
                    <div class="col-sm-2">
                        <input style="border: none;" type="text" readonly class="form-control" name="status_perbaikan" value="-">
                    </div>
                </div>

                <!-- FORM NOMOR PENGAJUAN -->
                <div style="margin-top: 30px;">
                    <div class="form-group row">
                        <label for="nomor_pengajuan" class="col-sm-2 col-form-label">Nomor Pengajuan</label>
                        <div class="col-sm-4">
                            <input type="text" readonly class="form-control" name="nomor_pengajuan" value = "<?php
                            $n = 0;
                            if ($ptb_edit) {
                                foreach ($ptb_edit as $data) {
                                    $n = $n + 1;
                                    echo $data->nomor_aju;
                                }
                            }
                            ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="nomor_pendaftaran" class="col-sm-2 col-form-label">Nomor Pendaftaran</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="nomor_pendaftaran" value="<?php
                            $n = 0;
                            if ($ptb_edit) {
                                foreach ($ptb_edit as $data) {
                                    $n = $n + 1;
                                    echo $data->nomor_daftar;
                                }
                            }
                            ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="tanggal_pendaftaran" class="col-sm-2 col-form-label">Tanggal Pendaftaran</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="tanggal_pendaftaran" value="<?php
                            $n = 0;
                            if ($ptb_edit) {
                                foreach ($ptb_edit as $data) {
                                    $n = $n + 1;
                                    echo $data->tanggal_daftar;
                                }
                            }
                            ?>" placeholder="DDMMYY">
                        </div>
                    </div>      
                </div>
                <!-- TUTUP FORM NOMOR PENGAJUAN -->

                <!-- FORM KPPBC BONGKAR -->
                <div style="margin-top: 30px;">


                    <div class="form-group row">
                        <label for="kppbc_pengawas" class="col-sm-2 col-form-label">Kantor Pabean</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="kppbc_pengawas" value="<?php
                            $n = 0;
                            if ($ptb_edit) {
                                foreach ($ptb_edit as $data) {
                                    $n = $n + 1;
                                    echo $data->kode_kantor_pabean . " - " . $data->kantor_pabean;
                                }
                            }
                            ?>">
                        </div>
                        <label for="tujuan" class="col-sm-2 col-form-label">Kode Gudang PLB </label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="kode_gudang_plb" value ="" >
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="tujuan" class="col-sm-2 col-form-label">Jenis TPB </label>
                        <div class="col-sm-4">
                            <select name="kode_jenis_tpb" class="form-control">
                                <option selected>Choose...</option>
                                <option>...</option>
                            </select>
                        </div>
                    </div>  

                    <div class="form-group row">
                        <label for="tujuan" class="col-sm-2 col-form-label">Tujuan Pengiriman </label>
                        <div class="col-sm-4">
                            <select name="kode_tujuan_pengiriman" class="form-control">
                                <option selected>Choose...</option>
                                <option>...</option>
                            </select>
                        </div>
                    </div>      
                </div>
                <!-- TUTUP FORM KPPBC BONGKAR -->


                <div class="row">
                    <div class="col-sm-12">

                        <!-- FORM LEFT SIDE -->
                        <ol>
                            <div class="col-sm-6">
                                <p style="margin-left: -38px"><b>PENGUSAHA TPB</b></p>
                                <!-- FORM PENGUSAHA -->
                                <div>
                                    <div class="form-group row">
                                        <li>
                                            <label for="nama" class="col-sm-3 col-form-label">NPWP</label>
                                            <div class="col-sm-4">
                                                <select name="kode_id_pengusaha" class="form-control">
                                                    <option selected>NPWP</option>
                                                    <option>...</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="id_pengusaha" value = "<?php
                                                if ($ptb_edit) {
                                                    foreach ($ptb_edit as $data) {
                                                        echo $data->id_pengusaha;
                                                    }
                                                }
                                                ?>">
                                            </div>
                                        </li>
                                    </div>  
                                    <div class="form-group row">
                                        <li>
                                            <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="nama_pengusaha" value = "<?php
                                                $n = 0;
                                                if ($ptb_edit) {
                                                    foreach ($ptb_edit as $data) {
                                                        $n = $n + 1;
                                                        echo $data->NAMA_PENGUSAHA;
                                                    }
                                                }
                                                ?>">
                                            </div>
                                        </li>
                                    </div>

                                    <div class="form-group row">
                                        <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="alamat_pengusaha" value = "<?php
                                            $n = 0;
                                            if ($ptb_edit) {
                                                foreach ($ptb_edit as $data) {
                                                    $n = $n + 1;
                                                    echo $data->ALAMAT_PENGUSAHA;
                                                }
                                            }
                                            ?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Negara" class="col-sm-3 col-form-label">IJIN PTB</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="ptb_pengusaha" value = "<?php
                                            $n = 0;
                                            if ($ptb_edit) {
                                                foreach ($ptb_edit as $data) {
                                                    $n = $n + 1;
                                                    echo $data->NOMOR_IJIN_TPB;
                                                }
                                            }
                                            ?>">
                                        </div>

                                    </div>


                                </div>
                                <!-- TUTUP FORM PEMASOK -->     

                                <!-- FORM Penerima barang -->
                                <div>
                                    <p style="margin-left: -38px"><b>PENGIRIM BARANG</b></p>

                                    <div class="form-group row">
                                        <li>
                                            <label for="nama" class="col-sm-3 col-form-label">NPWP</label>
                                            <div class="col-sm-4">
                                                <select name="jenis_npwp_pengirim" class="form-control">
                                                    <option selected>NPWP</option>
                                                    <option>...</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="nomor_npwp_pengirim" value = "<?php
                                                if ($ptb_edit) {
                                                    foreach ($ptb_edit as $data) {
                                                        echo $data->ID_PENGIRIM;
                                                    }
                                                }
                                                ?>">
                                            </div>
                                        </li>
                                    </div>  

                                    <div class="form-group row">
                                        <li>
                                            <label for="alamat" class="col-sm-3 col-form-label">Nama</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="nama_pengirim" value ="<?php
                                                if ($ptb_edit) {
                                                    foreach ($ptb_edit as $data) {
                                                        echo $data->NAMA_PENGIRIM;
                                                    }
                                                }
                                                ?>" >
                                            </div>
                                        </li>

                                    </div>
                                    <div class="form-group row">
                                        <li>
                                            <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="alamat_pengirim" value ="<?php
                                                if ($ptb_edit) {
                                                    foreach ($ptb_edit as $data) {
                                                        echo $data->ALAMAT_PENGIRIM;
                                                    }
                                                }
                                                ?>" >
                                            </div>
                                        </li>

                                    </div>
                                </div>
                                <!-- TUTUP FORM IMPORTIR -->

                                <!-- FORM PEMILIK -->
                                <div>
                                    <a href="#modal-dokumen" data-target="#modal-dokumen" data-toggle="modal">
                                        <p style="margin-left: -38px"><b>DOKUMEN</b></p>
                                    </a>

                                    <div class="form-group row">
                                        <li>
                                            <label for="nama" class="col-sm-3 col-form-label">Packing List</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="nomor_dokumen" value ="<?php
                                                if ($ptb_packing) {
                                                    foreach ($ptb_packing as $data) {
                                                        echo $data->nomor_dokumen;
                                                    }
                                                }
                                                ?>" >
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="tanggal_dokumen" value ="<?php
                                                if ($ptb_packing) {
                                                    foreach ($ptb_packing as $data) {
                                                        echo $data->tanggal_dokumen;
                                                    }
                                                }
                                                ?>" >
                                            </div>
                                        </li>
                                    </div>  

                                    <div class="form-group row">
                                        <li>
                                            <label for="nama" class="col-sm-3 col-form-label">Kontrak</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="k1" value ="" >
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="k2" value ="" >
                                            </div>
                                        </li>
                                    </div>  
                                    <div class="form-group row">
                                        <li>
                                            <label for="nama" class="col-sm-3 col-form-label">Faktur Pajak</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="fp1" value ="" >
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="fp2" value ="" >
                                            </div>
                                        </li>
                                    </div>  
                                    <div class="form-group row">
                                        <li>
                                            <label for="nama" class="col-sm-3 col-form-label">SKEP</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="skep1" value ="" >
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="skep2" value ="" >
                                            </div>
                                        </li>
                                    </div>

                                    <div class="form-group row">
                                        <li>
                                            <div class="col-sm-10">
                                                <table class="table table-sm">
                                                    <thead>
                                                        <tr class="bg-success">
                                                            <th scope="col">Jenis Dokumen</th>
                                                            <th scope="col">Nomor Dokumen</th>
                                                            <th scope="col">Tanggal</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php
                                                        $n = 0;
                                                        if ($ptb_other) {
                                                            foreach ($ptb_other as $data) {
                                                                $n = $n + 1;
                                                                ?>
                                                            <td width="30%"><?php echo $data->uraian_dokumen; ?></td>
                                                            <td width="30%"><?php echo $data->nomor_dokumen; ?></td>
                                                            <td width="30%"><?php echo $data->tanggal_dokumen; ?></td></tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>                          

                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>
                                    </div>
                                </div>


                                <!-- FORM HARGA -->

                                <!-- TUTUP FORM HARGA -->   
                            </div>
                            <!-- TUTUP FORM LEFT SIDE -->


                            <!-- FORM RIGHT SIDE -->
                            <div class="col-sm-6" style="padding-right: 50px;">
                                <p><b>PENGANGKUTAN</b></p>
                                <div class="form-group row">
                                    <li>
                                        <label for="nama" class="col-sm-4 col-form-label">Jenis Sarana Pengangkutan Darat</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="sarana_angkut" value ="<?php
                                            if ($ptb_edit) {
                                                foreach ($ptb_edit as $data) {
                                                    echo $data->NAMA_PENGANGKUT;
                                                }
                                            }
                                            ?>" >
                                        </div>
                                    </li>
                                </div>
                                <div class="form-group row">
                                    <li>
                                        <label for="nama" class="col-sm-4 col-form-label">Nomor Polisi</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="nomor_polisi" value ="<?php
                                            if ($ptb_edit) {
                                                foreach ($ptb_edit as $data) {
                                                    echo $data->NOMOR_POLISI;
                                                }
                                            }
                                            ?>" >
                                        </div>
                                    </li>
                                </div>

                                <div class="form-group row">

                                    <label for="tujuan" class="col-sm-10 col-form-label">HARGA</label>

                                </div>
                                <div class="form-group row">
                                    <li>
                                        <label for="nama" class="col-sm-4 col-form-label">Harga Penyerahan</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="harga_penyerahan" value ="<?php
                                            if ($ptb_edit) {
                                                foreach ($ptb_edit as $data) {
                                                    echo $data->HARGA_PENYERAHAN;
                                                }
                                            }
                                            ?>" >
                                        </div>
                                    </li>
                                </div>




                                <div class="form-group row">
                                    
                                    <a href="#modal-kemasan" data-target="#modal-kemasan" data-toggle="modal">
                                        <li><p><b>KEMASAN</b></p></li>
                                    </a>
                                
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <table class="table table-sm">
                                            <thead>
                                                <tr class="bg-success">
                                                    <th scope="col">Jumlah</th>
                                                    <th scope="col">Kode Jenis</th>
                                                    <th scope="col">Jenis</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $n = 0;
                                                if ($ptb_kemasan) {
                                                    foreach ($ptb_kemasan as $data) {
                                                        $n = $n + 1;
                                                        ?>
                                                    <td width="30%"><?php echo $data->JUMLAH_KEMASAN; ?></td>
                                                    <td width="30%"><?php echo $data->KODE_JENIS_KEMASAN; ?></td>
                                                    <td width="30%"><?php echo $data->URAIAN_KEMASAN; ?></td></tr>
                                                    <?php
                                                }
                                            }
                                            ?>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- FORM BARANG -->
                                <div>
                                    <?php  foreach ($ptb_edit as $data) { ?>
                                    <div>
                                        <a href="./BC40_detail_barang_controller?id=<?php echo $data->id; ?>">
                                            <p style="margin-left: -38px"><b>BARANG</b></p>
                                        </a>
                                    <?php } ?>

                                    <div class="form-group row">
                                        <li>
                                            <label class="col-sm-3 col-form-label">Volume (m3)</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="volume" value ="<?php
                                                if ($ptb_edit) {
                                                    foreach ($ptb_edit as $data) {
                                                        echo $data->VOLUME;
                                                    }
                                                }
                                                ?>" >
                                            </div>
                                        </li>
                                    </div>
                                    <div class="form-group row">
                                        <li>
                                            <label class="col-sm-3 col-form-label">Berat Kotor (Kg)</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="bruto" value ="<?php
                                                if ($ptb_edit) {
                                                    foreach ($ptb_edit as $data) {
                                                        echo $data->bruto;
                                                    }
                                                }
                                                ?>" >
                                            </div>

                                        </li>

                                    </div>  
                                    <div class="form-group row">
                                        <li>
                                            <label class="col-sm-3 col-form-label">Berat Bersih (Kg)</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="netto" value ="<?php
                                                if ($ptb_edit) {
                                                    foreach ($ptb_edit as $data) {
                                                        echo $data->NETTO;
                                                    }
                                                }
                                                ?>" >
                                            </div>

                                        </li>

                                    </div>  
                                    <div class="form-group row">

                                        <label class="col-sm-3 col-form-label">Jumlah Barang</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" name="jml_barang" value ="<?php
                                            if ($ptb_edit) {
                                                foreach ($ptb_edit as $data) {
                                                    echo $data->JUMLAH_BARANG;
                                                }
                                            }
                                            ?>" >
                                        </div>



                                    </div>  
                                </div>

                                <!-- FORM PUNGUTAN-->
                                <!--                            <div class="form-group row">
                                                                                        <li>
                                                                                                <label for="tujuan" class="col-sm-10 col-form-label">Pngutan</label>
                                                                                        </li>
                                                                                </div>-->







                        </ol>




                    </div>      
                    <!-- TUTUP FORM RIGHT SIDE -->
                    <p>Dengan saya menyatakan bertanggung jawab atas kebenaran
                        <br></br>hal hal yang diberitahukan dalam pemberitahuan pabean ini. </p>
                    <div class="form-group row">
                        <!--<li>-->
                        <!--<label for="Negara" class="col-sm-3 col-form-label">Pelabuhan Bongkar</label>-->
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="kota_ttd" Placeholder="kota" value = "<?php
                            if ($ptb_edit) {
                                foreach ($ptb_edit as $data) {
                                    echo $data->KOTA_TTD;
                                }
                            }
                            ?>">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="tgl_ttd" Placeholder="Tanggal" value = "<?php
                            if ($ptb_edit) {
                                foreach ($ptb_edit as $data) {
                                    echo $data->TANGGAL_TTD;
                                }
                            }
                            ?>">
                        </div>
                        <!--</li>-->
                    </div>
                    <div class="form-group row">
                        <!--<li>-->
                        <label for="Negara" class="col-sm-2 col-form-label">Pemberitahu</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="pemberitahu" Placeholder="nama" value = "<?php
                            if ($ptb_edit) {
                                foreach ($ptb_edit as $data) {
                                    echo $data->NAMA_TTD;
                                }
                            }
                            ?>">
                        </div>
                        <!--</li>-->
                    </div>
                    <div class="form-group row">
                        <!--<li>-->
                        <label for="Negara" class="col-sm-2 col-form-label">Jabatan</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="jabatan" Placeholder="jabatan" value = "<?php
                                   if ($ptb_edit) {
                                       foreach ($ptb_edit as $data) {
                                           echo $data->JABATAN_TTD;
                                       }
                                   }
                                   ?>" >
                        </div>
                        <!--</li>-->
                    </div>
                    
                </div>
                <div class="form-actions">
                    <a href='<?php echo base_url('BC_40'); ?>' class='btn default'> Cancel</a>
                    <button type="submit" class="btn blue" name="edit_bc40">Save Changes</button>
                </div>
        </div>
        </div>
        </form>

        <!-- MODAL DAFTAR RESPON -->
        
        <div id="modal-daftar-respon" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h3 class="modal-title"><b>Respon</b></h3>

                <div class="row">
                    <div class="col-sm-6">
                        <a href="#" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Respon</a>
                        <a href="#" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Status</a>
                    </div>
                    
                </div>
              </div>
              <div class="modal-body">
                
                <div class="row">
                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                        <table class="table table-striped table-bordered table-sm">
                            <thead>
                              <tr>
                                  <th scope="col"><center><p style="font-size: 12px;">Kode</p></center></th>
                                  <th scope="col"><center><p style="font-size: 12px;">Uraian</p></center></th>
                                  <th scope="col"><center><p style="font-size: 12px;">Waktu</p></center></th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($modal_daftar_respon as $data) { ?>
                                <tr>
                                    <td><p style="font-size: 12px"><?php echo $data['KODE_RESPON'];?></p></td>
                                    <td><p style="font-size: 12px"><?php echo $data['URAIAN_RESPON'];?></p></td>
                                    <td><p style="font-size: 12px"><?php echo $data['WAKTU_RESPON'];?></p></td>
                                </tr>
                               <?php } ?>
                            </tbody>
                        </table>
                    </div>
                  </div>    
                </div>

              <!-- <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div> -->
            </div>
        </div>
        <!-- TUTUP MODAL DAFTAR RESPON -->


        <!-- MODAL DOKUMEN -->
        
        <div id="modal-dokumen" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h3 class="modal-title"><b>Dokumen</b></h3>
              </div>
              <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form action="preview_BC40/update" method="POST">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Dokumen</label>
                                        <input type="text" class="form-control" id="dokumen1" name="dokumen1">
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Nomor</label>
                                        <input type="text" class="form-control" id="nomor1" name="nomor1" >
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Tanggal</label>
                                        <input type="text" class="form-control" id="tgl1" name="tgl1" placeholder="DDMMYY" >
                                    </div>
                                </div>
                            </div>
                        
                    </div>  
                </div>
                
                <div class="row">
                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                        <table class="table table-striped table-bordered table-sm">
                            <thead>
                              <tr>
                                  <th scope="col"><center><p style="font-size: 12px;">Seri</p></center></th>
                                  <th scope="col"><center><p style="font-size: 12px;">Kode Dokumen</p></center></th>
                                  <th scope="col"><center><p style="font-size: 12px;">Jenis Dokumen</p></center></th>
                                  <th scope="col"><center><p style="font-size: 12px;">Nomor</p></center></th>
                                  <th scope="col"><center><p style="font-size: 12px;">Tanggal</p></center></th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($modal_dokumen as $row) { ?>
                                <tr>
                                    <td><p style="font-size: 12px"><?php echo $row['SERI_DOKUMEN'];?></p></td>
                                    <td><p style="font-size: 12px"><?php echo $row['KODE_JENIS_DOKUMEN'];?></p></td>
                                    <td><p style="font-size: 12px"><?php echo $row['TIPE_DOKUMEN'];?></p></td>
                                    <td><p style="font-size: 12px"><?php echo $row['NOMOR_DOKUMEN'];?></p></td>
                                    <td><p style="font-size: 12px"><?php echo $row['TANGGAL_DOKUMEN'];?></p></td>
                                    
                                    <td>
                                        <button type="button" class="btn blue" onclick="tampilkanEditDokumen(<?php echo $row['ID'];?>)"><i class="fa fa-pencil"></i></button>
                                        
                                        <a href="javascript:dialogHapus('<?php echo base_url('preview_BC40/delete_dokumen/'.$row['ID'].''); ?>')" class='btn red'><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </td>
                                </tr>
                               <?php } ?>
                            </tbody>
                        </table>
                    </div>
                  </div>    
                </div>

              <div class="modal-footer">
                <button type="submit" class="btn btn-primary" name="submit_modal_dokumen">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </form>
        </div>
        <!-- TUTUP MODAL DOKUMEN -->

        <!-- MODAL KEMASAN -->
        <div id="modal-kemasan" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h3 class="modal-title"><b>Kemasan</b></h3>
              </div>

              <form action="preview_BC40/update_modal_kemasan" method="POST">
                  <div class="modal-body">
                      <div class="row">
                        <div class="col-sm-12">
                            
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Jumlah</label>
                                        <input type="text" class="form-control" id="jumlah" name="jumlah">
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Jenis</label>
                                        <input type="text" class="form-control" id="jenis" name="jenis" >
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Merk</label>
                                        <input type="text" class="form-control" id="merk" name="merk">
                                    </div>
                                </div>
                            </div>

                        </div>
                      </div>

                      <div class="row">
                          <div class="table-wrapper-scroll-y my-custom-scrollbar">
                              <table class="table table-striped table-bordered table-sm">
                                  <thead>
                                    <tr>
                                        <th scope="col"><center><p style="font-size: 12px;">No</p></center></th>
                                        <th scope="col"><center><p style="font-size: 12px;">Jumlah</p></center></th>
                                        <th scope="col"><center><p style="font-size: 12px;">Kode</p></center></th>
                                        <th scope="col"><center><p style="font-size: 12px;">Uraian</p></center></th>
                                        <th scope="col"><center><p style="font-size: 12px;">Merk Kemasan</p></center></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                      
                                      <?php 
                                        $i = 1;
                                        foreach ($modal_kemasan as $row) { 
                                      ?>
                                      <tr>
                                          <td><p style="font-size: 12px"><?php echo $i;?></p></td>
                                          <td><p style="font-size: 12px"><?php echo $row['JUMLAH_KEMASAN'];?></p></td>
                                          <td><p style="font-size: 12px"><?php echo $row['KODE_JENIS_KEMASAN'];?></p></td>
                                          <td><p style="font-size: 12px"><?php echo $row['URAIAN_KEMASAN'];?></p></td>
                                          <td><p style="font-size: 12px"><?php echo $row['MERK_KEMASAN'];?></p></td>
                                          
                                          <td>
                                              <button type="button" class="btn blue" onclick="tampilkanEditKemasan(<?php echo $row['ID'];?>)"><i class="fa fa-pencil"></i></button>
                                              
                                              <a href="javascript:dialogHapus('<?php echo base_url('preview_BC40/delete_kemasan/'.$row['ID'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i></a>
                                          </td>
                                      </tr>

                                      <?php $i++; } ?>
                                     
                                  </tbody>
                              </table>
                          </div>
                        </div>    
                  </div>

                  <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" name="submit_modal_dokumen">Save changes</button>
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
              </form>
            </div>
        </div>
        <!-- TUTUP MODAL KEMASAN -->

    </div>
</div>

<script type="text/javascript">

  function tampilkanEditDokumen(id){

    $.ajax({
        url : "<?php echo base_url('preview_BC40/editmodal/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $("#dokumen1").empty();
            $("#dokumen1").val(data.dokumenedit.KODE_JENIS_DOKUMEN);
            $("#nomor1").empty();
            $("#nomor1").val(data.dokumenedit.NOMOR_DOKUMEN);
            $("#tgl1").empty();
            $("#tgl1").val(data.dokumenedit.TANGGAL_DOKUMEN);
            $("#modal").modal('show');
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
  }

  function tampilkanEditKemasan(id){

    $.ajax({
        url : "<?php echo base_url('preview_BC40/editmodalkemasan/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $("#jumlah").empty();
            $("#jumlah").val(data.kemasanedit.JUMLAH_KEMASAN);
            $("#jenis").empty();
            $("#jenis").val(data.kemasanedit.KODE_JENIS_KEMASAN);
            $("#merk").empty();
            $("#merk").val(data.kemasanedit.MERK_KEMASAN);
            $("#modal").modal('show');
  
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
  }

  function dialogHapus(urlHapus) {
      if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
          document.location = urlHapus;
      }
  }

</script>


