<?php
$pdf = new FPDF('l', 'mm', 'A5');

$datas['header'] = [
	'title' => 'Judul',
	'code' => 'IX....',
	'nama' => 'Perusahaan',
];
$datas['detail'][] = ['item_code' => 'xaew1', 'item_name' => 'tes'];
$datas['detail'][] = ['item_code' => 'xaew2', 'item_name' => 'tes2'];
$datas['detail'][] = ['item_code' => 'xaew3', 'item_name' => 'tes3'];

$pdf->AddPage();
$pdf->SetFont('Arial', 'B', 16);
$pdf->Cell(190, 7, $datas['header']['title'], 0, 1, 'C');
$pdf->Ln(1);
foreach ($datas['detail'] as $row) {
	$pdf->Cell(20, 6, $row['item_code'], 1, 0);
	$pdf->Cell(85, 6, $row['item_name'], 1, 1); // param paling terakhir kalau 1 untuk next row
}

$pdf->Cell(190, 7, $datas['header']['title'], 0, 1, 'C');
$pdf->Output();
