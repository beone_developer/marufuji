<div class="container">
    <div class="row">
        <p class="text-center" style="font-size: 17px"><b>PEMBERITAHUAN IMPOR BARANG UNTUK DITIMBUN <br>DI TEMPAT PENIMBUNAN BERIKAT</b></p>
    </div>

    <form action="">
        <div class="form-group row">
            <label for="status" class="col-sm-2 col-form-label">Status</label>
            <div class="col-sm-2">
                <input readonly style="border: none;" type="text" readonly class="form-control" id="status" value="EDIT">
            </div>
        </div>

        <div class="form-group row">
            <label for="status_perbaikan" class="col-sm-2 col-form-label">Status Perbaikan</label>
            <div class="col-sm-2">
                <input readonly style="border: none;" type="text" readonly class="form-control" id="status_perbaikan" value="-">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-2">
                <a href="#modal" class="btn btn-primary btn-sm" role="button" data-target="#modal" data-toggle="modal">DAFTAR RESPON</a>
            </div>
        </div>

        <!-- FORM NOMOR PENGAJUAN -->
        <div style="margin-top: 30px;">
            <div class="form-group row">
                <label for="nomor_pengajuan" class="col-sm-2 col-form-label">Nomor Pengajuan</label>
                <div class="col-sm-4">
                    <input readonly type="text" readonly class="form-control" id="nomor_pengajuan" value = "<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->nomor_aju;
                        }
                    }
                    ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="nomor_pendaftaran" class="col-sm-2 col-form-label">Nomor Pendaftaran</label>
                <div class="col-sm-4">
                    <input readonly type="text" readonly class="form-control" id="nomor_pendaftaran" value="<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->nomor_daftar;
                        }
                    }
                    ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="tanggal_pendaftaran" class="col-sm-2 col-form-label">Tanggal Pendaftaran</label>
                <div class="col-sm-4">
                    <input readonly type="text" readonly class="form-control" id="tanggal_pendaftaran" value="<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->tanggal_daftar;
                        }
                    }
                    ?>" placeholder="DDMMYY">
                </div>
            </div>		
        </div>
        <!-- TUTUP FORM NOMOR PENGAJUAN -->

        <!-- FORM KPPBC BONGKAR -->
        <div style="margin-top: 30px;">
            <div class="form-group row">
                <label for="kppbc_bongkar" class="col-sm-2 col-form-label">KPPBC Bongkar</label>
                <div class="col-sm-4">
                    <input readonly type="text" readonly class="form-control" id="kppbc_bongkar" value="<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->kode_kppbc_bongkar . " - " . $data->kppbc_bongkar;
                        }
                    }
                    ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="kppbc_pengawas" class="col-sm-2 col-form-label">KPPBC Pengawas</label>
                <div class="col-sm-4">
                    <input readonly type="text" readonly class="form-control" id="kppbc_pengawas" value="<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->kode_kppbc_awas . " - " . $data->kppbc_awas;
                        }
                    }
                    ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="tujuan" class="col-sm-2 col-form-label">Tujuan</label>
                <div class="col-sm-4">
                    <select id="inputState" readonly class="form-control">
                        <option selected>Choose...</option>
                        <option>...</option>
                    </select>
                </div>
            </div>		
        </div>
        <!-- TUTUP FORM KPPBC BONGKAR -->


        <div class="row">
            <div class="col-sm-12">

                <!-- FORM LEFT SIDE -->
                <ol>
                    <div class="col-sm-6">
                        <p><b>PEMASOK</b></p>
                        <!-- FORM PEMASOK -->
                        <div>
                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input readonly type="text" class="form-control" id="nama" value = "<?php
                                        $n = 0;
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                $n = $n + 1;
                                                echo $data->nama_pemasok;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                <div class="col-sm-8">
                                    <input readonly type="text" class="form-control" id="alamat" value = "<?php
                                    $n = 0;
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            $n = $n + 1;
                                            echo $data->alamat_pemasok;
                                        }
                                    }
                                    ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Negara" class="col-sm-3 col-form-label">Negara</label>
                                <div class="col-sm-8">
                                    <input readonly type="text" class="form-control" id="negara" value = "<?php
                                    $n = 0;
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            $n = $n + 1;
                                            echo $data->kode_negara_pemasok;
                                        }
                                    }
                                    ?>">
                                </div>
                            </div>
                        </div>
                        <!-- TUTUP FORM PEMASOK -->		

                        <!-- FORM IMPORTIR -->
                        <div>
                            <p style="margin-left: -38px"><b>IMPORTIR</b></p>

                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Identitas</label>
                                    <div class="col-sm-4">
                                        <select id="inputState" readonly class="form-control">
                                            <option selected>NPWP</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="nama" value = "<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->kode_identitas_importir;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>	

                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control" id="alamat" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->nama_importir;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">No Izin</label>
                                <div class="col-sm-8">
                                    <input type="text" readonly class="form-control" id="alamat" value = "<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->nomor_ijin_tpb;
                                        }
                                    }
                                    ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                <div class="col-sm-8">
                                    <input type="text" readonly class="form-control" id="alamat" value ="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->alamat_importir;
                                        }
                                    }
                                    ?>" >
                                </div>
                            </div>


                            <div class="form-group row">
                                <li>
                                    <label for="Negara" class="col-sm-3 col-form-label">API</label>
                                    <div class="col-sm-4">
                                        <select id="inputState" readonly class="form-control">
                                            <option selected>APIP</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="api" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->api_pengusaha;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>
                        </div>
                        <!-- TUTUP FORM IMPORTIR -->

                        <!-- FORM PEMILIK -->
                        <div>
                            <p style="margin-left: -38px"><b>PEMILIK</b></p>

                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Identitas</label>
                                    <div class="col-sm-4">
                                        <select id="inputState" readonly class="form-control">
                                            <option selected>NPWP</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="nama" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->jenis_identitas_pemilik;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>	

                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control" id="nama_pemilik" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->nama_pemilik;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                <div class="col-sm-8">
                                    <input type="text" readonly class="form-control" id="alamat" value="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->alamat_pemilik;
                                        }
                                    }
                                    ?>" >
                                </div>
                            </div>


                            <div class="form-group row">	
                                <label for="Negara" class="col-sm-3 col-form-label">API</label>
                                <div class="col-sm-4">
                                    <select id="inputState" readonly class="form-control">
                                        <option selected>APIP</option>
                                        <option>...</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" readonly class="form-control" id="alamat" value="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->API_PEMILIK;
                                        }
                                    }
                                    ?>">
                                </div>

                            </div>
                        </div>

                        <!-- FORM PPJK -->
                        <div>
                            <p style="margin-left: -38px"><b>PPJK</b></p>

                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Identitas</label>
                                    <div class="col-sm-4">
                                        <select id="inputState" readonly class="form-control">
                                            <option selected>NPWP</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="nama" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->rp_npwp;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>	

                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control" id="alamat" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->rp_nama;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                <div class="col-sm-8">
                                    <input type="text" readonly class="form-control" id="alamat" value = "<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->rp_alamat;
                                        }
                                    }
                                    ?>">
                                </div>
                            </div>


                            <div class="form-group row">
                                <li>
                                    <label for="Negara" class="col-sm-3 col-form-label">NP-PPJK</label>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="alamat" placeholder="" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->nomor_npppjk;
                                            }
                                        }
                                        ?>">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="alamat" placeholder="DDMMYY" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->tanggal_npppjk;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>
                        </div>
                        <!-- TUTUP FORM PPJK -->

                        <!-- FORM PENGANGKUTAN -->
                        <div>
                            <p style="margin-left: -38px"><b>PENGANGKUTAN</b></p>

                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Cara Pengangkutan</label>
                                    <div class="col-sm-6">
                                        <select id="inputState" readonly class="form-control">
                                            <option selected>NPWP</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                </li>
                            </div>	

                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Nama Sarana Pengangkut</label>
                                    <div class="col-sm-8">
                                        <input readonly type="text" class="form-control" id="alamat" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->NAMA_PENGANGKUT;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">Voy/Flight & Negara</label>
                                <div class="col-sm-2">
                                    <input readonly type="text" class="form-control" id="alamat" value="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->NOMOR_VOY_FLIGHT;
                                        }
                                    }
                                    ?>" >
                                </div>
                                <div class="col-sm-2">
                                    <input readonly type="text" class="form-control" id="alamat" value="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->KODE_BENDERA;
                                        }
                                    }
                                    ?>" >
                                </div>
                                <!--									<div class="col-sm-4">
                                                                                                                <input type="text" class="form-control" id="alamat" >
                                                                                                        </div>-->
                            </div>

                            <div class="form-group row">
                                <li>
                                    <label for="Negara" class="col-sm-3 col-form-label">Pelabuhan Muat</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" class="form-control" id="alamat" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->KODE_PEL_MUAT;
                                            }
                                        }
                                        ?>">
                                    </div>
                                    <div class="col-sm-4">
                                        <input readonly type="text" class="form-control" id="alamat" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->pel_muat;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <li>
                                    <label for="Negara" class="col-sm-3 col-form-label">Pelabuhan Transit</label>
                                    <div class="col-sm-8">
                                        <input readonly type="text" class="form-control" id="alamat" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->KODE_PEL_TRANSIT . " - " . $data->pel_transit;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <li>
                                    <label for="Negara" class="col-sm-3 col-form-label">Pelabuhan Bongkar</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" class="form-control" id="alamat" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->KODE_PEL_BONGKAR;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                    <div class="col-sm-4">
                                        <input readonly type="text" class="form-control" id="alamat" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->pel_bongkar;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>
                        </div>
                        <!-- TUTUP FORM PENGANGKUTAN -->	
                    </div>
                    <!-- TUTUP FORM LEFT SIDE -->


                    <!-- FORM RIGHT SIDE -->
                    <div class="col-sm-6" style="padding-right: 50px;">
                        <li>
                                <a href="#modal4" class="btn btn-primary btn-sm" role="button" data-target="#modal4" data-toggle="modal">DOKUMEN</a>
                            </li>
                        <div class="form-group row">
                            <li>
                                <label for="nama" class="col-sm-3 col-form-label">Invoice</label>
                                <div class="col-sm-4">
                                    <input readonly type="text" class="form-control" id="nama" value="<?php
                                    if ($ptb_inv) {
                                        foreach ($ptb_inv as $data) {
                                            echo $data->NOMOR_DOKUMEN;
                                        }
                                    }
                                    ?>">
                                </div>
                                <div class="col-sm-4">
                                    <input readonly type="text" class="form-control" id="nama" value="<?php
                                    if ($ptb_inv) {
                                        foreach ($ptb_inv as $data) {
                                            echo $data->TANGGAL_DOKUMEN;
                                        }
                                    }
                                    ?>">
                                </div>
                            </li>
                        </div>

                        <div class="form-group row">
                            <li>
                                <label for="kppbc_pengawas" class="col-sm-3 col-form-label">Fasilitas Impor</label>
                                <div class="col-sm-3">
                                    <input readonly type="text" class="form-control" id="kppbc_pengawas" value="">
                                </div>
                                <div class="col-sm-3">
                                    <input readonly type="text" class="form-control" id="kppbc_pengawas" placeholder="DDMMYY">
                                </div>
                                <div class="col-sm-2">
                                    <input readonly type="text" class="form-control" id="kppbc_pengawas" value="">
                                </div>
                            </li>
                        </div>

                        <div class="form-group row">
                            <li>
                                <label for="tujuan" class="col-sm-10 col-form-label">Surat Keputusan / Dokumen Lainnya</label>
                            </li>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <table class="table table-sm">
                                    <thead>
                                        <tr class="bg-success">
                                            <th scope="col">No.</th>
                                            <th scope="col">Jenis Dokumen</th>
                                            <th scope="col">Nomor Dokumen</th>
                                            <th scope="col">Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $n = 0;
                                        if ($ptb_other) {
                                            foreach ($ptb_other as $data) {
                                                $n = $n + 1;
                                                ?>
                                                <tr><td width="30%"><?php echo $n; ?></td>
                                                    <td width="30%"><?php echo $data->URAIAN_DOKUMEN; ?></td>
                                                    <td width="30%"><?php echo $data->NOMOR_DOKUMEN; ?></td>
                                                    <td width="30%"><?php echo $data->TANGGAL_DOKUMEN; ?></td></tr>
                                                <?php
                                            }
                                        }
                                        ?>						    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group row">
                            <li>
                                <label for="Negara" class="col-sm-3 col-form-label">LC</label>
                                <div class="col-sm-4">
                                    <input readonly type="text" class="form-control" id="LC" placeholder="" value="<?php
                                    if ($ptb_lc) {
                                        foreach ($ptb_lc as $data) {
                                            echo $data->NOMOR_DOKUMEN;
                                        }
                                    }
                                    ?>">
                                </div>
                                <div class="col-sm-4">
                                    <input readonly type="text" class="form-control" id="alamat" placeholder="DDMMYY" value="<?php
                                    if ($ptb_lc) {
                                        foreach ($ptb_lc as $data) {
                                            echo $data->TANGGAL_DOKUMEN;
                                        }
                                    }
                                    ?>">
                                </div>
                            </li>
                        </div>
                        <div class="form-group row">
                            <li>

                                <div class="col-sm-4">
                                    <select id="inputState" readonly class="form-control">
                                        <option selected>B/L</option>
                                        <option>...</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <input readonly type="text" class="form-control" id="api" value ="" >
                                </div>
                                <div class="col-sm-4">
                                    <input readonly type="text" class="form-control" id="api" value ="" >
                                </div>
                            </li>
                        </div>
                        <div class="form-group row">
                            <li>

                                <div class="col-sm-4">
                                    <button type="submit" class="btn blue" name="submit_user">BC 1.1</button>
                                </div>
                                <div class="col-sm-4">
                                    <input readonly type="text" class="form-control" id="api" value ="" >
                                </div>
                                <div class="col-sm-4">
                                    <input readonly type="text" class="form-control" id="api" value ="" >
                                </div>
                            </li>
                        </div>
                        <!-- FORM PENIMBUNAN -->
                        <div>
                            <p style="margin-left: -38px"><b>PENIMBUNAN</b></p>

                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Tempat Penimbunan</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" class="form-control" id="nama" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->kode_tps;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                    <div class="col-sm-4">
                                        <input readonly type="text" class="form-control" id="nama" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->uraian_tps;
                                            }
                                        }
                                        ?>" >
                                    </div>

                                </li>
                            </div>	
                        </div>
                        <!-- FORM Harga -->
                        <div>
                             <li>
                                <a href="#modal6" class="btn btn-primary btn-sm" role="button" data-target="#modal6" data-toggle="modal">HARGA</a>
                            </li>
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Valuta</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" class="form-control" id="nama" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->kode_valuta . " - " . $data->uraian_valuta;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">NDPBM</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" class="form-control" id="nama" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->NDPBM;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">FOB</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" class="form-control" id="nama" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->FOB;
                                            }
                                        }
                                        ?>" >
                                    </div>


                                </li>
                            </div>	
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Freight</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" class="form-control" id="nama" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->freight;
                                            }
                                        }
                                        ?>" >
                                    </div>


                                </li>
                            </div>	
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Asuransi LN/DN</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" class="form-control" id="nama" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->asuransi;
                                            }
                                        }
                                        ?>" >
                                    </div>


                                </li>
                            </div>	
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Nilai CIF</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" class="form-control" id="nama" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->cif;
                                            }
                                        }
                                        ?>" >
                                    </div>


                                </li>
                            </div>
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Nilai CIF Rupiah</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" class="form-control" id="nama" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->cif_rupiah;
                                            }
                                        }
                                        ?>" >
                                    </div>


                                </li>
                            </div>
                        </div>
                        <!-- FORM KONTAINER-->
                        <div class="form-group row">
                            <li>
                                <a href="#modal2" class="btn btn-primary btn-sm" role="button" data-target="#modal2" data-toggle="modal">KONTAINER</a>
                            </li>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <table class="table table-sm">
                                    <thead>
                                        <tr class="bg-success">
                                            <th scope="col">No.</th>
                                            <th scope="col">Nomor Kontainer</th>
                                            <th scope="col">Ukuran</th>
                                            <th scope="col">Tipe</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $n = 0;
                                        if ($ptb_kontainer) {
                                            foreach ($ptb_kontainer as $data) {
                                                $n = $n + 1;
                                                ?>
                                                <tr><td width="30%"><?php echo $n; ?></td>
                                                    <td width="30%"><?php echo $data->NOMOR_KONTAINER; ?></td>
                                                    <td width="30%"><?php echo $data->URAIAN_UKURAN_KONTAINER; ?></td>
                                                    <td width="30%"><?php echo $data->URAIAN_TIPE_KONTAINER; ?></td></tr>
                                                <?php
                                            }
                                        }
                                        ?>						    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- FORM KEMASAN-->
                        <div class="form-group row">
                            <li>
                                <a href="#modal3" class="btn btn-primary btn-sm" role="button" data-target="#modal3" data-toggle="modal">KEMASAN</a>
                            </li>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <table class="table table-sm">
                                    <thead>
                                        <tr class="bg-success">
                                            <th scope="col">No.</th>
                                            <th scope="col">Jumlah</th>
                                            <th scope="col">Kode Jenis</th>
                                            <th scope="col">Jenis</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $n = 0;
                                        if ($ptb_kemasan) {
                                            foreach ($ptb_kemasan as $data) {
                                                $n = $n + 1;
                                                ?>
                                                <tr><td width="30%"><?php echo $n; ?></td>
                                                    <td width="30%"><?php echo $data->JUMLAH_KEMASAN; ?></td>
                                                    <td width="30%"><?php echo $data->KODE_JENIS_KEMASAN; ?></td>
                                                    <td width="30%"><?php echo $data->URAIAN_KEMASAN; ?></td></tr>
                                                <?php
                                            }
                                        }
                                        ?>						    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- FORM BARANG -->
                        <div>
                            <a href="./preview_BC23/form_detail_barang?id=<?php if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->id;
                                            }
                                        } ?>"><p style="margin-left: -38px"><b>BARANG</b></p></a>

                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Bruto (Kg)</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" class="form-control" id="nama" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->bruto;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Netto (Kg)</label>
                                    <div class="col-sm-4">
                                        <input readonly type="text" class="form-control" id="nama" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->netto;
                                            }
                                        }
                                        ?>" >
                                    </div>

                                </li>
                            </div>	
                        </div>
                        <!-- FORM PUNGUTAN-->
                        <!--							<div class="form-group row">
                                                                                <li>
                                                                                        <label for="tujuan" class="col-sm-10 col-form-label">Pngutan</label>
                                                                                </li>
                                                                        </div>-->







                </ol>	
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="bg-success">
                            <th scope="col">Jenis Pungutan</th>
                            <th scope="col">Ditangguhkan (Rp)</th>
                            <th scope="col">Dibebaskan (Rp)</th>
                            <th scope="col">Tidak Dipungut (Rp)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $n = 0;
                            if ($ptb_kemasan) {
                                foreach ($ptb_kemasan as $data) {
                                    $n = $n + 1;
                                    ?>
                                    <td width="30%"><?php echo $data->JUMLAH_KEMASAN; ?></td>
                                    <td width="30%"><?php echo $data->JUMLAH_KEMASAN; ?></td>
                                    <td width="30%"><?php echo $data->KODE_JENIS_KEMASAN; ?></td>
                                    <td width="30%"><?php echo $data->URAIAN_KEMASAN; ?></td>
        <?php
    }
}
?>						    
                        </tr>
                    </tbody>
                </table>
                <p>Dengan saya menyatakan bertanggung jawab atas kebenaran
                    <br></br>hal hal yang diberitahukan pada dokumen ini. </p>
                <div class="form-group row">
                    <!--<li>-->
                    <!--<label for="Negara" class="col-sm-3 col-form-label">Pelabuhan Bongkar</label>-->
                    <div class="col-sm-2">
                        <input readonly type="text" class="form-control" id="alamat" Placeholder="kota" value = "<?php
                        if ($ptb_edit) {
                            foreach ($ptb_edit as $data) {
                                echo $data->KOTA_TTD;
                            }
                        }
                        ?>">
                    </div>
                    <div class="col-sm-2">
                        <input readonly type="text" class="form-control" id="alamat" Placeholder="Tanggal" value = "<?php
                        if ($ptb_edit) {
                            foreach ($ptb_edit as $data) {
                                echo $data->TANGGAL_TTD;
                            }
                        }
                        ?>">
                    </div>
                    <!--</li>-->
                </div>
                <div class="form-group row">
                    <!--<li>-->
                    <label for="Negara" class="col-sm-2 col-form-label">Pemberitahu</label>
                    <div class="col-sm-2">
                        <input readonly type="text" class="form-control" id="alamat" Placeholder="nama" value = "<?php
                        if ($ptb_edit) {
                            foreach ($ptb_edit as $data) {
                                echo $data->NAMA_TTD;
                            }
                        }
                        ?>">
                    </div>
                    <!--</li>-->
                </div>
                <div class="form-group row">
                    <!--<li>-->
                    <label for="Negara" class="col-sm-2 col-form-label">Jabatan</label>
                    <div class="col-sm-2">
                        <input readonly type="text" class="form-control" id="alamat" Placeholder="jabatan" value = "<?php
                               if ($ptb_edit) {
                                   foreach ($ptb_edit as $data) {
                                       echo $data->JABATAN_TTD;
                                   }
                               }
                               ?>" >
                    </div>
                    <!--</li>-->
                </div>
            </div>


        </div>		
        <!-- TUTUP FORM RIGHT SIDE -->

</div>
</div>
</form>
<!-- MODAL respon -->

<div id="modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>RESPON</b></h3>
        </div>
        <div class="modal-body">
            
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                    <thead>
                        <tr class="bg-success">
                            <th scope="col">Kode</th>
                            <th scope="col">Uraian</th>
                            <th scope="col">Waktu</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            if ($ptb_respon) {
                                foreach ($ptb_respon as $data) {
                                    ?>
                                    <td width="30%"><?php echo $data->KODE_RESPON; ?></td>
                                    <td width="30%"><?php echo $data->NOMOR_RESPON; ?></td>
                                    <td width="30%"><?php echo $data->TANGGAL_RESPON; ?></td></tr>
        <?php
    }
}
?>						    
                    </tr>
                    </tbody>
                </table>
                    
                    
                    
                </div>
            </div>	
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>

<div id="modal4" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>DOKUMEN</b></h3>
        </div>
        <div class="modal-body">
            
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                    <thead>
                        <tr class="bg-success">
                                            <th scope="col">No.</th>
                                            <th scope="col">Jenis Dokumen</th>
                                            <th scope="col">Nomor Dokumen</th>
                                            <th scope="col">Tanggal</th>
                                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                                        $n = 0;
                                        if ($ptb_other) {
                                            foreach ($ptb_other as $data) {
                                                $n = $n + 1;
                                                ?>
                                                <tr><td width="30%"><?php echo $n; ?></td>
                                                    <td width="30%"><?php echo $data->URAIAN_DOKUMEN; ?></td>
                                                    <td width="30%"><?php echo $data->NOMOR_DOKUMEN; ?></td>
                                                    <td width="30%"><?php echo $data->TANGGAL_DOKUMEN; ?></td></tr>
                                                <?php
                                            }
                                        }
                                        ?>
                        </tr>	    
                    
                    </tbody>
                </table>
                    
                    
                    
                </div>
            </div>	
        </div>

        <div class="modal2-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>
<div id="modal6" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>HARGA</b></h3>
        </div>
        <div class="modal-body">
            
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                    <thead>
                        <tr class="bg-success">
                                            <th scope="col">No.</th>
                                            <th scope="col">Jenis Dokumen</th>
                                            <th scope="col">Nomor Dokumen</th>
                                            <th scope="col">Tanggal</th>
                                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            
                        </tr>	    
                    
                    </tbody>
                </table>
                    
                    
                    
                </div>
            </div>	
        </div>

        <div class="modal2-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>

<div id="modal2" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>KONTAINER</b></h3>
        </div>
        <div class="modal-body">
            
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                    <thead>
                        <tr class="bg-success">
                            <th scope="col">Nomor Kontainer</th>
                            <th scope="col">Ukuran</th>
                            <th scope="col">Tipe</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                           <?php
                                        $n = 0;
                                        if ($ptb_kontainer) {
                                            foreach ($ptb_kontainer as $data) {
                                                $n = $n + 1;
                                                ?>
                                            <td width="30%"><?php echo $data->NOMOR_KONTAINER; ?></td>
                                            <td width="30%"><?php echo $data->URAIAN_UKURAN_KONTAINER; ?></td>
                                            <td width="30%"><?php echo $data->URAIAN_TIPE_KONTAINER; ?></td></tr>
                                            <?php
                                        }
                                    }
                                    ?>
                        </tr>	    
                    
                    </tbody>
                </table>
                    
                    
                    
                </div>
            </div>	
        </div>

        <div class="modal2-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>

<div id="modal3" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>KEMASAN</b></h3>
        </div>
        <div class="modal-body">
            
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                    <thead>
                        <tr class="bg-success">
                                            <th scope="col">Jumlah</th>
                                            <th scope="col">Kode Jenis</th>
                                            <th scope="col">Uraian</th>
                                            <th scope="col">Merk Kemasan</th>
                                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                           <?php
                                        $n = 0;
                                        if ($ptb_kemasan) {
                                            foreach ($ptb_kemasan as $data) {
                                                $n = $n + 1;
                                                ?>
                                            <td width="30%"><?php echo $data->JUMLAH_KEMASAN; ?></td>
                                            <td width="30%"><?php echo $data->KODE_JENIS_KEMASAN; ?></td>
                                            <td width="30%"><?php echo $data->URAIAN_KEMASAN; ?></td>
                                            <td width="30%"><?php echo $data->MERK_KEMASAN; ?></td>
                                            <?php
                                        }
                                    }
                                    ?>
                        </tr>	    
                    
                    </tbody>
                </table>
                    
                    
                    
                </div>
            </div>	
        </div>

        <div class="modal2-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>

</div>
<!-- TUTUP CONTAINER -->
