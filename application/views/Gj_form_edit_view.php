<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Form General Journal</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">
          <form role="form" method="post">
              <div class="form-body">
                <div class="row">

                  <div class="col-sm-4">
                      <h2><?php echo $default['gl_date'];?></h2>

                          <h3><b><?php echo $default['gl_number'];?></b></h3>

                  </div>
                  <div class="col-sm-8" id="detail_cash_bank">
                      <div class="portlet light bordered">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Akun</label>
                                    <div class="input-group">
                                       <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="coa_id" required>
                                          <option value=<?php echo $default['coa_id'];?>><?php echo $default['coa_no'];?></option>
                                          <?php
                                            foreach($list_coa as $row){ ?>
                                           <option value="<?php echo $row['coa_id'];?>"><?php echo $row['nomor']." | ".$row['nama'];?></option>
                                         <?php } ?>
                                       </select>
                                 </div>
                               </div>
                            </div>

                            <div class="col-sm-6">
                              <div class="form-group">
                                  <label>Akun Lawan Transaksi</label>
                                  <div class="input-group">
                                     <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="coa_id_lawan" required>
                                       <option value=<?php echo $default['coa_id_lawan'];?>><?php echo $default['coa_no_lawan'];?></option>
                                       <?php 	foreach($list_coa as $row){ ?>
                                         <option value="<?php echo $row['coa_id'];?>"><?php echo $row['nomor']." | ".$row['nama'];?></option>
                                       <?php } ?>
                                     </select>
                               </div>
                             </div>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label>Keterangan Voucher</label>
                              <input type="text" class="form-control" placeholder="Keterangan" name="keterangan_journal" value='<?php echo $default['keterangan'];?>' required>
                            </div>
                          </div>
                          <div class="col-sm-12">
                              <div class="row">
                                <div class="col-sm-4">
                                  <div class="form-group">
                                  <label>Jumlah IDR</label>
                                  <?php
                                        $jml_idr_debet = str_replace(".", ",", $default['debet']);
                                        $jml_idr_kredit = str_replace(".", ",", $default['kredit']);
                                  ?>
                                  <input type="text" class="form-control" placeholder="Jumlah IDR" name="jumlah_idr" id="saldo" value='<?php if ($default['debet'] == 0){ echo $jml_idr_kredit;}else{ echo $jml_idr_debet;}?>' required>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>

                      </div>
                  </div>
              </div>
            </div>

            <div class="form-actions">
                <a href="<?php echo base_url();?>Gj_controller/List_gj" class="btn default"> Cancel</a>
                <button type="submit" class="btn red" name="submit_journal">Submit</button>
            </div>
          </form>
      </div>
    </div>


        <script type="text/javascript">

    		var saldo = document.getElementById('saldo');
    		saldo.addEventListener('keyup', function(e){
    			saldo.value = formatRupiah(this.value, 'Rp. ');
    		});

    		/* Fungsi formatRupiah */
    		function formatRupiah(angka, prefix){
    			var number_string = angka.replace(/[^,\d]/g, '').toString(),
    			split   		= number_string.split(','),
    			sisa     		= split[0].length % 3,
    			rupiah     		= split[0].substr(0, sisa),
    			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    			// tambahkan titik jika yang di input sudah menjadi angka ribuan
    			if(ribuan){
    				separator = sisa ? '.' : '';
    				rupiah += separator + ribuan.join('.');
    			}

    			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    		}
    	</script>
