<?php
  $tgl = $_GET['tgl'];

  $thn_awal = substr($tgl,6,4);
  $bln_awal = substr($tgl,0,2);
  $day_awal = substr($tgl,3,2);
  $tgl_formated = $thn_awal."-".$bln_awal."-".$day_awal;

  if (intval($bln_awal) == 1){
    $txtBulan = "JANUARI";
  }elseif (intval($bln_awal) == 2) {
    $txtBulan = "FEBRUARI";
  }elseif (intval($bln_awal) == 3) {
    $txtBulan = "MARET";
  }elseif (intval($bln_awal) == 4) {
    $txtBulan = "APRIL";
  }elseif (intval($bln_awal) == 5) {
    $txtBulan = "MEI";
  }elseif (intval($bln_awal) == 6) {
    $txtBulan = "JUNI";
  }elseif (intval($bln_awal) == 7) {
    $txtBulan = "JULI";
  }elseif (intval($bln_awal) == 8) {
    $txtBulan = "AGUSTUS";
  }elseif (intval($bln_awal) == 9) {
    $txtBulan = "SEPTEMBER";
  }elseif (intval($bln_awal) == 10) {
    $txtBulan = "OKTOBER";
  }elseif (intval($bln_awal) == 11) {
    $txtBulan = "NOVEMBER";
  }elseif (intval($bln_awal) == 12) {
    $txtBulan = "DESEMBER";
  }

  $tgl_awal_tahun = $thn_awal."-01-"."01";
?>

  <h1>
      <center><b>Report CALK</b></center>
  </h1>
  <hr />

      <div class="row">
          <div class="col-xs-12">
              <table class="table table-striped table-hover">
                  <thead>
                      <tr>
                          <th><b>AKUN</b></th>
                          <th><b>SALDO</b></th>
                      </tr>
                  </thead>

                  <tbody>
<!------------------------------------- KAS BANK -------------------------------------------------------------->
                    <tr>
                        <td><b>KAS DAN BANK</b></td>
                        <td><b></b></td>
                    </tr>
                    <tr>
                        <td colspan="2">Merupakan saldo kas dan bank perusahaan per <?php echo $day_awal." ".$txtBulan." ".$thn_awal;?> dengan rincian sebagai berikut :</td>
                    </tr>

                    <?php
                    $total_saldo_kas_bank = 0;

                    foreach($akun_kas_bank as $row){

                      //TOTAL MUTASI AKUN KAS BANK
                      $mutasi_kas_bank = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id=".intval($row['coa_id']));
                      $hasil_mutasi_kas_bank = $mutasi_kas_bank->row_array();

                      if ($row['dk'] == "D"){
                            $saldo = $row['debet_idr'] + $hasil_mutasi_kas_bank['totdebet'] - $hasil_mutasi_kas_bank['totkredit'];
                      }elseif($row['dk'] == "K"){
                          $saldo = $row['kredit_idr'] + $hasil_mutasi_kas_bank['totkredit'] - $hasil_mutasi_kas_bank['totdebet'];
                      }
                    ?>

                    <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['nama'];?></td>
                          <td><?php echo number_format($saldo, 2);?></td>
                    </tr>
                    <?php
                      $total_saldo_kas_bank = $total_saldo_kas_bank + $saldo;
                      $totalKasBank = $total_saldo_kas_bank;
                      }
                    ?>

                    <tr>
                          <td></td>
                          <td><b><?php echo number_format($total_saldo_kas_bank, 2);?></b></td>
                    </tr>
<!----------------------------------------- KAS BANK -------------------------------------------------------------->

<!----------------------------------------- PIUTANG -------------------------------------------------------------->
                    <tr>
                        <td><b>PIUTANG</b></td>
                        <td><b></b></td>
                    </tr>
                    <tr>
                        <td colspan="2">Merupakan saldo piutang perusahaan per <?php echo $day_awal." ".$txtBulan." ".$thn_awal;?> dengan rincian sebagai berikut :</td>
                    </tr>

                    <?php
                    $total_saldo_piutang = 0;

                    foreach($akun_piutang as $row){

                      //TOTAL MUTASI AKUN KAS BANK
                      $mutasi_piutang = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id=".intval($row['coa_id']));
                      $hasil_mutasi_piutang = $mutasi_piutang->row_array();

                      if ($row['dk'] == "D"){
                            $saldo = $row['debet_idr'] + $hasil_mutasi_piutang['totdebet'] - $hasil_mutasi_piutang['totkredit'];
                      }elseif($row['dk'] == "K"){
                          $saldo = $row['kredit_idr'] + $hasil_mutasi_piutang['totkredit'] - $hasil_mutasi_piutang['totdebet'];
                      }
                    ?>

                    <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['nama'];?></td>
                          <td><?php echo number_format($saldo, 2);?></td>
                    </tr>
                    <?php
                      $total_saldo_piutang = $total_saldo_piutang + $saldo;
                      }
                    ?>

                    <tr>
                          <td></td>
                          <td><b><?php echo number_format($total_saldo_piutang, 2);?></b></td>
                    </tr>
<!----------------------------------------- END PIUTANG -------------------------------------------------------------->

<!----------------------------------------- PERSEDIAAN -------------------------------------------------------------->
                    <tr>
                        <td><b>PERSEDIAAN</b></td>
                        <td><b></b></td>
                    </tr>
                    <tr>
                        <td colspan="2">Merupakan saldo persediaan perusahaan per <?php echo $day_awal." ".$txtBulan." ".$thn_awal;?> dengan rincian sebagai berikut :</td>
                    </tr>

                    <?php
                    $total_saldo_persediaan = 0;

                    foreach($akun_persediaan as $row){

                      //TOTAL MUTASI AKUN KAS BANK
                      $mutasi_persediaan = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id=".intval($row['coa_id']));
                      $hasil_mutasi_persediaan = $mutasi_persediaan->row_array();

                      if ($row['dk'] == "D"){
                            $saldo = $row['debet_idr'] + $hasil_mutasi_persediaan['totdebet'] - $hasil_mutasi_persediaan['totkredit'];
                      }elseif($row['dk'] == "K"){
                          $saldo = $row['kredit_idr'] + $hasil_mutasi_persediaan['totkredit'] - $hasil_mutasi_persediaan['totdebet'];
                      }
                    ?>

                    <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['nama'];?></td>
                          <td><?php echo number_format($saldo, 2);?></td>
                    </tr>
                    <?php
                      $total_saldo_persediaan = $total_saldo_persediaan + $saldo;
                      }
                    ?>

                    <tr>
                          <td></td>
                          <td><b><?php echo number_format($total_saldo_persediaan, 2);?></b></td>
                    </tr>
<!----------------------------------------- END PERSEDIAAN -------------------------------------------------------------->

<!----------------------------------------- UANG MUKA -------------------------------------------------------------->
                    <tr>
                        <td><b>UANG MUKA</b></td>
                        <td><b></b></td>
                    </tr>
                    <tr>
                        <td colspan="2">Merupakan saldo uang muka perusahaan per <?php echo $day_awal." ".$txtBulan." ".$thn_awal;?> dengan rincian sebagai berikut :</td>
                    </tr>

                    <?php
                    $total_saldo_uang_muka = 0;

                    foreach($akun_uang_muka as $row){

                      //TOTAL MUTASI AKUN KAS BANK
                      $mutasi_uang_muka = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id=".intval($row['coa_id']));
                      $hasil_mutasi_uang_muka = $mutasi_uang_muka->row_array();

                      if ($row['dk'] == "D"){
                            $saldo = $row['debet_idr'] + $hasil_mutasi_uang_muka['totdebet'] - $hasil_mutasi_uang_muka['totkredit'];
                      }elseif($row['dk'] == "K"){
                          $saldo = $row['kredit_idr'] + $hasil_mutasi_uang_muka['totkredit'] - $hasil_mutasi_uang_muka['totdebet'];
                      }
                    ?>

                    <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['nama'];?></td>
                          <td><?php echo number_format($saldo, 2);?></td>
                    </tr>
                    <?php
                      $total_saldo_uang_muka = $total_saldo_uang_muka + $saldo;
                      }
                    ?>

                    <tr>
                          <td></td>
                          <td><b><?php echo number_format($total_saldo_uang_muka, 2);?></b></td>
                    </tr>
<!----------------------------------------- END UANG MUKA -------------------------------------------------------------->

<!----------------------------------------- UANG MUKA PAJAK -------------------------------------------------------------->
                    <tr>
                        <td><b>UANG MUKA PAJAK</b></td>
                        <td><b></b></td>
                    </tr>
                    <tr>
                        <td colspan="2">Merupakan saldo uang muka pajak perusahaan per <?php echo $day_awal." ".$txtBulan." ".$thn_awal;?> dengan rincian sebagai berikut :</td>
                    </tr>

                    <?php
                    $total_saldo_uang_muka_pajak = 0;

                    foreach($akun_uang_muka_pajak as $row){

                      //TOTAL MUTASI UANG MUKA PAJAK
                      $mutasi_uang_muka_pajak = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id=".intval($row['coa_id']));
                      $hasil_mutasi_uang_muka_pajak = $mutasi_uang_muka_pajak->row_array();

                      if ($row['dk'] == "D"){
                            $saldo = $row['debet_idr'] + $hasil_mutasi_uang_muka_pajak['totdebet'] - $hasil_mutasi_uang_muka_pajak['totkredit'];
                      }elseif($row['dk'] == "K"){
                          $saldo = $row['kredit_idr'] + $hasil_mutasi_uang_muka_pajak['totkredit'] - $hasil_mutasi_uang_muka_pajak['totdebet'];
                      }
                    ?>

                    <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['nama'];?></td>
                          <td><?php echo number_format($saldo, 2);?></td>
                    </tr>
                    <?php
                      $total_saldo_uang_muka_pajak = $total_saldo_uang_muka_pajak + $saldo;
                      }
                    ?>

                    <tr>
                          <td></td>
                          <td><b><?php echo number_format($total_saldo_uang_muka_pajak, 2);?></b></td>
                    </tr>
<!----------------------------------------- END UANG MUKA PAJAK -------------------------------------------------------------->

<!----------------------------------------- ASET TETAP -------------------------------------------------------------->
                    <tr>
                        <td><b>ASET TETAP</b></td>
                        <td><b></b></td>
                    </tr>
                    <tr>
                        <td colspan="2">Merupakan saldo aset tetap perusahaan per <?php echo $day_awal." ".$txtBulan." ".$thn_awal;?> dengan rincian sebagai berikut :</td>
                    </tr>

                    <?php
                    $total_saldo_aset = 0;
                    $total_saldo_penyusutan = 0;

                    foreach($akun_aset as $row){

                      //TOTAL MUTASI AKUN KAS BANK
                      $mutasi_aset = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id=".intval($row['coa_id']));
                      $hasil_mutasi_aset = $mutasi_aset->row_array();

                      if ($row['dk'] == "D"){
                            $saldo = $row['debet_idr'] + $hasil_mutasi_aset['totdebet'] - $hasil_mutasi_aset['totkredit'];
                      }elseif($row['dk'] == "K"){
                          $saldo = $row['kredit_idr'] + $hasil_mutasi_aset['totkredit'] - $hasil_mutasi_aset['totdebet'];
                      }
                    ?>

                    <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['nama'];?></td>
                          <td><?php echo number_format($saldo, 2);?></td>
                    </tr>
                    <?php
                      $total_saldo_aset = $total_saldo_aset + $saldo;
                      }

                      //Akun Penyusutan
                      foreach($akun_penyusutan as $row){

                        //TOTAL MUTASI AKUN PENYUSUTAN
                        $mutasi_penyusutan = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id=".intval($row['coa_id']));
                        $hasil_mutasi_penyusutan = $mutasi_penyusutan->row_array();

                        if ($row['dk'] == "D"){
                              $saldo = $row['debet_idr'] + $hasil_mutasi_penyusutan['totdebet'] - $hasil_mutasi_penyusutan['totkredit'];
                        }elseif($row['dk'] == "K"){
                            $saldo = $row['kredit_idr'] + $hasil_mutasi_penyusutan['totkredit'] - $hasil_mutasi_penyusutan['totdebet'];
                        }
                      ?>

                      <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['nama'];?></td>
                            <td><?php echo number_format($saldo*-1, 2);?></td>
                      </tr>

                      <?php
                          $total_saldo_penyusutan = $total_saldo_penyusutan + ($saldo*-1);
                      }
                      ?>

                    <tr>
                          <td></td>
                          <td><b><?php echo number_format($total_saldo_aset+$total_saldo_penyusutan, 2);?></b></td>
                    </tr>
<!----------------------------------------- ASET TETAP -------------------------------------------------------------->

<!----------------------------------------- HUTANG -------------------------------------------------------------->
                    <tr>
                        <td><b>HUTANG USAHA</b></td>
                        <td><b></b></td>
                    </tr>
                    <tr>
                        <td colspan="2">Merupakan saldo hutang usaha perusahaan per <?php echo $day_awal." ".$txtBulan." ".$thn_awal;?> dengan rincian sebagai berikut :</td>
                    </tr>

                    <?php
                    $total_saldo_hutang = 0;

                    foreach($akun_hutang as $row){

                      //TOTAL MUTASI AKUN KAS BANK
                      $mutasi_hutang = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id=".intval($row['coa_id']));
                      $hasil_mutasi_hutang = $mutasi_hutang->row_array();

                      if ($row['dk'] == "D"){
                            $saldo = $row['debet_idr'] + $hasil_mutasi_hutang['totdebet'] - $hasil_mutasi_hutang['totkredit'];
                      }elseif($row['dk'] == "K"){
                            $saldo = $row['kredit_idr'] + $hasil_mutasi_hutang['totkredit'] - $hasil_mutasi_hutang['totdebet'];
                      }
                    ?>

                    <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['nama'];?></td>
                          <td><?php echo number_format($saldo, 2);?></td>
                    </tr>
                    <?php
                      $total_saldo_hutang = $total_saldo_hutang + $saldo;
                      }
                    ?>

                    <tr>
                          <td></td>
                          <td><b><?php echo number_format($total_saldo_hutang, 2);?></b></td>
                    </tr>

<!----------------------------------------- END HUTANG -------------------------------------------------------------->

<!----------------------------------------- HUTANG BIAYA -------------------------------------------------------------->
                    <tr>
                        <td><b>HUTANG BIAYA </b></td>
                        <td><b></b></td>
                    </tr>
                    <tr>
                        <td colspan="2">Merupakan saldo hutang biaya perusahaan per <?php echo $day_awal." ".$txtBulan." ".$thn_awal;?> dengan rincian sebagai berikut :</td>
                    </tr>

                    <?php
                    $total_saldo_hutang_biaya = 0;

                    foreach($akun_biaya_setor as $row){

                      //TOTAL MUTASI AKUN KAS BANK
                      $mutasi_hutang_biaya = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id=".intval($row['coa_id']));
                      $hasil_mutasi_hutang_biaya = $mutasi_hutang_biaya->row_array();

                      if ($row['dk'] == "D"){
                            $saldo = $row['debet_idr'] + $hasil_mutasi_hutang_biaya['totdebet'] - $hasil_mutasi_hutang_biaya['totkredit'];
                      }elseif($row['dk'] == "K"){
                          $saldo = $row['kredit_idr'] + $hasil_mutasi_hutang_biaya['totkredit'] - $hasil_mutasi_hutang_biaya['totdebet'];
                      }
                    ?>

                    <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['nama'];?></td>
                          <td><?php echo number_format($saldo, 2);?></td>
                    </tr>
                    <?php
                      $total_saldo_hutang_biaya = $total_saldo_hutang_biaya + $saldo;
                      }
                    ?>

                    <tr>
                          <td></td>
                          <td><b><?php echo number_format($total_saldo_hutang_biaya, 2);?></b></td>
                    </tr>
<!----------------------------------------- END BIAYA -------------------------------------------------------------->

<!----------------------------------------- HUTANG LAINNYA -------------------------------------------------------------->
                    <tr>
                        <td><b>HUTANG LAINNYA </b></td>
                        <td><b></b></td>
                    </tr>
                    <tr>
                        <td colspan="2">Merupakan saldo hutang lainnya perusahaan per <?php echo $day_awal." ".$txtBulan." ".$thn_awal;?> dengan rincian sebagai berikut :</td>
                    </tr>

                    <?php
                    $total_saldo_hutang_lainnya = 0;

                    foreach($akun_hutang_lainnya as $row){

                      //TOTAL MUTASI AKUN KAS BANK
                      $mutasi_hutang_lainnya = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id=".intval($row['coa_id']));
                      $hasil_mutasi_hutang_lainnya = $mutasi_hutang_lainnya->row_array();

                      if ($row['dk'] == "D"){
                            $saldo = $row['debet_idr'] + $hasil_mutasi_hutang_lainnya['totdebet'] - $hasil_mutasi_hutang_lainnya['totkredit'];
                      }elseif($row['dk'] == "K"){
                          $saldo = $row['kredit_idr'] + $hasil_mutasi_hutang_lainnya['totkredit'] - $hasil_mutasi_hutang_lainnya['totdebet'];
                      }
                    ?>

                    <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['nama'];?></td>
                          <td><?php echo number_format($saldo, 2);?></td>
                    </tr>
                    <?php
                      $total_saldo_hutang_lainnya = $total_saldo_hutang_lainnya + $saldo;
                      }
                    ?>

                    <tr>
                          <td></td>
                          <td><b><?php echo number_format($total_saldo_hutang_lainnya, 2);?></b></td>
                    </tr>
<!----------------------------------------- END BIAYA LAINNYA -------------------------------------------------------------->

<!----------------------------------------- HUTANG PAJAK -------------------------------------------------------------->
                    <tr>
                        <td><b>HUTANG PAJAK </b></td>
                        <td><b></b></td>
                    </tr>
                    <tr>
                        <td colspan="2">Merupakan saldo hutang pajak perusahaan per <?php echo $day_awal." ".$txtBulan." ".$thn_awal;?> dengan rincian sebagai berikut :</td>
                    </tr>

                    <?php
                    $total_saldo_hutang_pajak = 0;

                    foreach($akun_hutang_pajak as $row){

                      //TOTAL MUTASI AKUN KAS BANK
                      $mutasi_hutang_pajak = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id=".intval($row['coa_id']));
                      $hasil_mutasi_hutang_pajak = $mutasi_hutang_pajak->row_array();

                      if ($row['dk'] == "D"){
                            $saldo = $row['debet_idr'] + $hasil_mutasi_hutang_pajak['totdebet'] - $hasil_mutasi_hutang_pajak['totkredit'];
                      }elseif($row['dk'] == "K"){
                          $saldo = $row['kredit_idr'] + $hasil_mutasi_hutang_pajak['totkredit'] - $hasil_mutasi_hutang_pajak['totdebet'];
                      }
                    ?>

                    <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['nama'];?></td>
                          <td><?php echo number_format($saldo, 2);?></td>
                    </tr>
                    <?php
                      $total_saldo_hutang_pajak = $total_saldo_hutang_pajak + $saldo;
                      }
                    ?>

                    <tr>
                          <td></td>
                          <td><b><?php echo number_format($total_saldo_hutang_pajak, 2);?></b></td>
                    </tr>
<!----------------------------------------- END HUTANG PAJAK -------------------------------------------------------------->

<!----------------------------------------- UANG MUKA PENJUALAN -------------------------------------------------------------->
                    <tr>
                        <td><b>UANG MUKA PENJUALAN</b></td>
                        <td><b></b></td>
                    </tr>
                    <tr>
                        <td colspan="2">Merupakan saldo uang muka penjualan perusahaan per <?php echo $day_awal." ".$txtBulan." ".$thn_awal;?> dengan rincian sebagai berikut :</td>
                    </tr>

                    <?php
                    $total_saldo_uang_muka_penjualan = 0;

                    foreach($akun_uang_muka_penjualan as $row){

                      //TOTAL MUTASI UANG MUKA PAJAK
                      $mutasi_uang_muka_penjualan = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id=".intval($row['coa_id']));
                      $hasil_mutasi_uang_muka_penjualan = $mutasi_uang_muka_penjualan->row_array();

                      if ($row['dk'] == "D"){
                            $saldo = $row['debet_idr'] + $hasil_mutasi_uang_muka_penjualan['totdebet'] - $hasil_mutasi_uang_muka_penjualan['totkredit'];
                      }elseif($row['dk'] == "K"){
                          $saldo = $row['kredit_idr'] + $hasil_mutasi_uang_muka_penjualan['totkredit'] - $hasil_mutasi_uang_muka_penjualan['totdebet'];
                      }
                    ?>

                    <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['nama'];?></td>
                          <td><?php echo number_format($saldo, 2);?></td>
                    </tr>
                    <?php
                      $total_saldo_uang_muka_penjualan = $total_saldo_uang_muka_penjualan + $saldo;
                      }
                    ?>

                    <tr>
                          <td></td>
                          <td><b><?php echo number_format($total_saldo_uang_muka_penjualan, 2);?></b></td>
                    </tr>
<!----------------------------------------- END UANG MUKA PENJUALAN -------------------------------------------------------------->

<!----------------------------------------- MODAL -------------------------------------------------------------->
                    <tr>
                        <td><b>MODAL </b></td>
                        <td><b></b></td>
                    </tr>
                    <tr>
                        <td colspan="2">Merupakan saldo modal perusahaan per <?php echo $day_awal." ".$txtBulan." ".$thn_awal;?> dengan rincian sebagai berikut :</td>
                    </tr>

                    <?php
                    $total_saldo_modal = 0;

                    foreach($akun_modal as $row){

                      //TOTAL MUTASI AKUN KAS BANK
                      $mutasi_modal = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id=".intval($row['coa_id']));
                      $hasil_mutasi_modal = $mutasi_modal->row_array();

                      if ($row['dk'] == "D"){
                            $saldo = $row['debet_idr'] + $hasil_mutasi_modal['totdebet'] - $hasil_mutasi_modal['totkredit'];
                      }elseif($row['dk'] == "K"){
                            $saldo = $row['kredit_idr'] + $hasil_mutasi_modal['totkredit'] - $hasil_mutasi_modal['totdebet'];
                      }
                    ?>

                    <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['nama'];?></td>
                          <td><?php echo number_format($saldo, 2);?></td>
                    </tr>
                    <?php
                      $total_saldo_modal = $total_saldo_modal + $saldo;
                      }
                    ?>

                    <tr>
                          <td></td>
                          <td><b><?php echo number_format($total_saldo_modal, 2);?></b></td>
                    </tr>
<!----------------------------------------- END MODAL -------------------------------------------------------------->

<!----------------------------------------- SALDO LABA -------------------------------------------------------------->
                <?php
                // RUMUS LABA RUGI KOTOR = PENJUALAN - HPP
                // RUMUS LABA RUGI USAHA = LABA RUGI KOTOR - BIAYA ADMIN DAN UMUM
                // RUMUS LABA RUGI BERSIH = LABA RUGI USAHA + PENDAPATAN LAIN

                /************************************* PENJUALAN ********************************************************/
                $total_penjualan = 0; //TOTAL PENJUALAN = SALDO AWAL AKUN PENJALAN COA + MUTASI AKUN PENJUALAN DI GENERAL LEDGER

                foreach($saldo_awal_coa_penjualan as $row){
                    //nilai Saldo Awal dari COA Akun PENJUALAN
                    $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id =".intval($row['coa_id']));
                    $hasil_posisi_dk = $sql_posisi_dk->row_array();

                    //nilai Mutasi Akun Penjualan di General Ledger
                    $sql_total_mutasi_dk = $this->db->query("SELECT SUM(debet) as totmdebet, SUM(kredit) as totmkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id =".intval($row['coa_id']));
                    $hasil_total_mutasi_dk = $sql_total_mutasi_dk->row_array();

                    if ($hasil_posisi_dk['dk'] == "D"){
                          $saldo = $hasil_total_mutasi_dk['totmdebet'] - $hasil_total_mutasi_dk['totmkredit'];
                    }elseif($hasil_posisi_dk['dk'] == "K"){
                        $saldo = $hasil_total_mutasi_dk['totmkredit'] - $hasil_total_mutasi_dk['totmdebet'];
                    }

                    //HASIL TOTAL PENJUALAN
                    $total_penjualan = $total_penjualan + $saldo;
                  }
                /************************************* END PENJUALAN ********************************************************/

                /***************************************************** TOTAL DEBET KREDIT AKUN HPP *********************************************************/
                      //Tidak menggunakan foreach karena hanya menyorot 1 Akun (Akun HPP hanya ada 1)
                      $total_hpp = 0;

                      //posisi Debet Kredit Akun HPP
                      $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE nomor = '500-00'");
                      $hasil_posisi_dk = $sql_posisi_dk->row_array();

                      //nilai Mutasi Akun HPP di General Ledger
                      $sql_total_mutasi_dk = $this->db->query("SELECT SUM(debet) as totmdebet, SUM(kredit) as totmkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id =".intval($hasil_posisi_dk['coa_id']));
                      $hasil_total_mutasi_dk = $sql_total_mutasi_dk->row_array();

                      if ($hasil_posisi_dk['dk'] == "D"){
                            $saldo_HPP = $hasil_total_mutasi_dk['totmdebet']-$hasil_total_mutasi_dk['totmkredit'];
                      }elseif($hasil_posisi_dk['dk'] == "K"){
                          $saldo_HPP = $hasil_total_mutasi_dk['totmkredit']-$hasil_total_mutasi_dk['totmdebet'];
                      }
                /***************************************************** END TOTAL DEBET KREDIT AKUN HPP *********************************************************/

                      $rugi_laba_kotor = $total_penjualan - $saldo_HPP;

                /****************************************** ADMIN BIAYA UMUM ************************************/
                      $total_biaya_admin_umum = 0;

                      foreach($list_coa_biaya_admin_umum as $row){
                        //posisi Debet Kredit Akun HPP
                        $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id =".intval($row['coa_id']));
                        $hasil_posisi_dk = $sql_posisi_dk->row_array();

                        //nilai Mutasi Akun BIAYA ADMIN DAN UMUM di General Ledger
                        $sql_dk_admin_umum = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id =".intval($row['coa_id']));
                        $hasil_dk_admin_umum = $sql_dk_admin_umum->row_array();

                        if ($hasil_posisi_dk['dk'] == "D"){
                                $saldo_admin = $hasil_dk_admin_umum['totdebet'] - $hasil_dk_admin_umum['totkredit'];
                        }elseif($hasil_posisi_dk['dk'] == "K"){
                            $saldo_admin = $hasil_dk_admin_umum['totkredit'] - $hasil_dk_admin_umum['totdebet'];
                        }
                      $total_biaya_admin_umum = $total_biaya_admin_umum + $saldo_admin;
                      }
                /****************************************** ADMIN BIAYA UMUM ************************************/

                      $rugi_laba_usaha = $rugi_laba_kotor - $total_biaya_admin_umum;

                //**************************************** PENDAPATAN BIAYA LAIN ***************************
                $total_pendapatan_biaya_lain = 0;


                foreach($list_coa_pendapatan_lain as $row){
                  //posisi Debet Kredit Akun PENDAPATAN LAIN LAIN
                  $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id =".intval($row['coa_id']));
                  $hasil_posisi_dk = $sql_posisi_dk->row_array();

                  //nilai Mutasi Akun PENDAPATAN LAIN LAIN di General Ledger
                  $sql_dk_pendapatan = $this->db->query("SELECT SUM(debet) as totpdebet, SUM(kredit) as totpkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id =".intval($row['coa_id']));
                  $hasil_dk_pendapatan = $sql_dk_pendapatan->row_array();

                  if ($hasil_posisi_dk['dk'] == "D"){
                        $saldo_pendapatan = $hasil_dk_pendapatan['totpdebet'] - $hasil_dk_pendapatan['totpkredit'];
                  }elseif($hasil_posisi_dk['dk'] == "K"){
                      $saldo_pendapatan = $hasil_dk_pendapatan['totpkredit'] - $hasil_dk_pendapatan['totpdebet'];
                  }

                  $total_pendapatan_biaya_lain = $total_pendapatan_biaya_lain + $saldo_pendapatan;
                }
                //******************************************* END PENDAPATAN BIAYA LAIN ***************************

                  $laba_rugi_bersih_usaha = $rugi_laba_usaha + $total_pendapatan_biaya_lain;

                  //SALDO COA (LABA) / RUGI TAHUN LALU
                  $coa_laba_rugi_lalu = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_transaksi = 4");
                  $laba_rugi_thn_lalu = $coa_laba_rugi_lalu->row_array();

                  $coa_id_lr_thn_lalu = $laba_rugi_thn_lalu['coa_id'];

                  //TOTAL MUTASI LABA RUGI (DEFIDEN)
                  $mutasi_labar_rugi = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_tahun' AND '$tgl_formated' AND coa_id = '$coa_id_lr_thn_lalu'");
                  $hasil_mutasi_laba_rugi = $mutasi_labar_rugi->row_array();

                  $hasil_saldo_laba = ($laba_rugi_thn_lalu['kredit_idr']-$hasil_mutasi_laba_rugi['totdebet']) + $laba_rugi_bersih_usaha;
                ?>
<!----------------------------------------- END SALDO LABA -------------------------------------------------------------->

                  <tr>
                      <td><b>SALDO LABA </b></td>
                      <td><b></b></td>
                  </tr>
                  <tr>
                      <td colspan="2">Merupakan saldo laba perusahaan per <?php echo $day_awal." ".$txtBulan." ".$thn_awal;?> dengan rincian sebagai berikut :</td>
                  </tr>
                  <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SALDO LABA</td>
                        <td><?php echo number_format($hasil_saldo_laba, 2);?></td>
                  </tr>
                  <tr>
                        <td></td>
                        <td><b><?php echo number_format($hasil_saldo_laba, 2);?></b></td>
                  </tr>

                  </tbody>
              </table>

          </div>
      </div>
