<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>



<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Bill Of Material</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">

          <form role="form" method="get">
              <div class="form-body">

                <div class="row">
                  <!--<div class="col-sm-4"><h3><b><?php //echo $nomor_bom;?></b></h3></div>-->
                  <div name="txtnobom" id="txtnobom"><b>Nomor BOM</b></div>
                  <div class="col-sm-8">
                    <div class="form-group">
                          <label>Tanggal</label>
                          <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input class="form-control form-control-inline input-medium date-picker input-circle-right" size="16" type="text" name="tanggal" value="<?php echo date('m/d/Y');?>" onChange="tampilnomor(this.value);" readonly required/>
                            <span class="help-block"></span>
                          </div>
                    </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                            <label>Kode Biaya</label>
                            <div class="input-group">
                               <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="kode_biaya" required>
                                 <option value=""></option>
                                 <option value=1>PRODUKSI WIP</option>
                                 <option value=2>PRODUKSI BARANG JADI</option>
                               </select>
                         </div>
                       </div>
                    </div>

                      <div class="col-sm-8">
                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" class="form-control" placeholder="Keterangan" id="keterangan" name="keterangan" required>
                            <!--<input type="hidden" class="form-control" name="transfer_no" value='<?php //echo $nomor_bom;?>'>-->
                          </div>
                      </div>
                    </div>

              </div>
              <div class="form-actions">
                  <button type="submit" class="btn blue" id="submit_transfer" name="submit_transfer">Lanjut</button>
              </div>
          </form>
  </div>
</div>

<script>
    function tampilnomor(str)
    {
    if (str=="")
      {
      document.getElementById("txtnobom").innerHTML="";
      return;
      }
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
      document.getElementById("txtnobom").innerHTML=xmlhttp.responseText;
      }
      }
    xmlhttp.open("GET","Bom_controller/get_nomor_bom?q="+str,true);
    xmlhttp.send();
    }
    </script>
