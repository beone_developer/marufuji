<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Recalculation Inventory</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">
          <form role="form" method="post">
              <div class="form-body">
                <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                            <label>Tanggal Awal</label>
                            <div class="input-group">
                              <span class="input-group-addon input-circle-left">
                                  <i class="fa fa-calendar"></i>
                              </span>
                              <input class="form-control form-control-inline input-medium date-picker input-circle-right" name="tanggal_awal" size="16" type="text" value="<?php echo date('m/d/Y');?>" readonly>
                              <span class="help-block"></span>
                            </div>
                      </div>
                  </div>


                  <div class="col-sm-4">
                    <div class="form-group">
                          <label>Tanggal Akhir</label>
                          <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input class="form-control form-control-inline input-medium date-picker input-circle-right" name="tanggal_akhir" size="16" type="text" value="<?php echo date('m/d/Y');?>" readonly>
                            <span class="help-block"></span>
                          </div>
                    </div>
                  </div>

              </div>

            </div>

            <div class="form-actions">
                <button type="button" class="btn default">Cancel</button>
                <button type="submit" class="btn red" id="submit_recal_inventory" name="submit_recal_inventory">Submit</button>
            </div>
          </form>
      </div>
    </div>
