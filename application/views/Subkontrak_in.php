<h3>Subkontrak In</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_2">
        <thead>
          <tr>
              <th width="20%"><center><small>No Pengajuan</small></center></th>
              <th width="15%"><center><small>Tgl Pengajuan</small></center></th>
              <th width="5%"><center><small>Kode <br/> Pabean</small></center></th>
              <th width="15%"><center><small>Pengirim</small></center></th>
              <th width="10%"><center><small>Berat</small></center></th>
							<th width="20%"><center><small>Action</small></center></th>
          </tr>
        </thead>
        <tbody>
          <?php
                $no=0;
								foreach($list_import_header as $row){
					?>
            <tr>
								<td><small><?php echo $row['NOMOR_AJU'];?></small></td>
                <td><small><?php echo $row['TANGGAL_AJU'];?></small></td>
								<td><small><?php echo "BC ".$row['KODE_DOKUMEN_PABEAN'];?></small></td>
                <td><small><?php echo $row['NAMA_PENGIRIM'];?></small></td>
                <td><small><?php echo number_format($row['BRUTO'], 2);?></small></td>
                <?php
                    $sql_received = $this->db->query("SELECT COUNT(nomor_aju) as jml FROM public.beone_subkon_in_header WHERE nomor_aju =  '$row[NOMOR_AJU]'");
              			$hasil_received = $sql_received->row_array();
                    $id = $hasil_received['jml'];
              		  if ($id > 0){
                ?>
                    <td>Sudah Kirim </td>
              <?php }else{?>
                    <td><?php if(helper_security("terima_barang") == 1){?><a href='<?php echo base_url('Subkontrak_controller/Terima/'.$row['ID'].'');?>' class='btn btn-circle red'><small>Terima</small></a><?php }?> </td>
              <?php }?>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>
