
    <!-- jquery
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/counterup/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/counterup/waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/counterup/counterup-active.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- jvectormap JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/jvectormap/jvectormap-active.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/sparkline/sparkline-active.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/flot/curvedLines.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/flot/flot-active.js"></script>
    <!-- knob JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/knob/jquery.knob.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/knob/jquery.appear.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/knob/knob-active.js"></script>
    <!--  wave JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/wave/waves.min.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/wave/wave-active.js"></script>
    <!--  todo JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/todo/jquery.todo.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/plugins.js"></script>
	<!--  Chat JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/chat/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/chat/jquery.chat.js"></script>
    <!-- main JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/main.js"></script>
	
	 <!-- Data Table JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/data-table/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/data-table/data-table-act.js"></script>
	
</body>

</html>