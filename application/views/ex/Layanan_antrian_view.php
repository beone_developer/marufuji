<body style="background-image: url(<?php echo base_url();?>assets/green/img/Orthopedi_blur.jpg); background-size: cover;">


<!-- Start Status area -->
    		
			<div class="notika-status-area">
				<div class="container" >
				<div class="material-design-btn">
						<a href="<?php echo base_url();?>Beranda_controller" class="btn notika-btn-green btn-reco-mg btn-button-mg" style="padding:10px;" ><h2 style="font-family: Times new Roman;"><i class="fa fa-arrow-circle-left" style="font-size: 70px;"></i></h2></a>
				</div>
				</div>
			</div>
		   
		   <!-- BUTTON AREA -->
			<div class="notika-status-area">
				<div class="container" >
					<div class="row">
						<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12" ></div>
						
						<div class="col-lg-6 col-md-4 col-sm-12 col-xs-12" style="margin:0px; padding:0px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
							<div class="color-single nk-teal mg-t-30" style="width:100%; height:30px; margin:0px;"></div>
					
							<div style="background-color:white; width:100%; height:270px; padding:10px; background-image: url(<?php echo base_url();?>assets/green/img/logo/logo-soth-trans.png); background-size: cover;">
								<center>
								<h3 style="font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55);">NOMOR ANTRIAN</h3>
								<hr />
								<h1 style="font-size:130px; font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55);"><?php echo $list_layanan['nomor_antrian'];?></h1>
								<h4 style="font-family:EB+Garamond; text-shadow: 0px 5px 6px rgba(38,16,10,0.55);">- <?php echo $list_layanan['nama'];?> -</h4>
								</center>
							</div>
							
							<div class="color-single nk-teal mg-t-30" style="width:100%; height:30px; margin: 0px;"></div>
						</div>
						
						
						<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12" ></div>
					</div>
					<br />
					<br />
					<br />
					<div class="row">
					
					
					<?php 
						$no_antrian = $list_layanan['nomor_antrian'];
					
						$kd = substr($no_antrian,0,2);
						$digit1 = substr($no_antrian,2,1);
						$digit2 = substr($no_antrian,3,1);
						$digit3 = substr($no_antrian,4,1);
						
						$audio_digit1 = "";
						$audio_digit2 = "";
						$audio_digit3 = "";
						
						for ($x = 0; $x <= 10; $x++) {
							if ($digit1 == $x){
								$audio_digit1 = $x.".MP3";
							}
						} 
						
						for ($x = 0; $x <= 10; $x++) {
							if ($digit2 == $x){
								$audio_digit2 = $x.".MP3";
							}
						}
						
						for ($x = 0; $x <= 10; $x++) {
							if ($digit3 == $x){
								$audio_digit3 = $x.".MP3";
							}
						}
						
					?>
					<audio id="in"><source src="<?php echo base_url();?>assets/green/record/new/in.wav"></audio>
					<audio id="kode"><source src="<?php echo base_url();?>assets/green/record/new/<?php echo $kd.".MP3";?>"></audio>
					<audio id="nomor-urut"><source src="<?php echo base_url();?>assets/green/record/new/nomor-urut.wav"></audio>
					<audio id="satu"><source src="<?php echo base_url();?>assets/green/record/new/<?php echo $audio_digit1;?>"></audio>
					<audio id="dua"><source src="<?php echo base_url();?>assets/green/record/new/<?php echo $audio_digit2;?>"></audio>
					<audio id="tiga"><source src="<?php echo base_url();?>assets/green/record/new/<?php echo $audio_digit3;?>"></audio>
					<audio id="out"><source src="<?php echo base_url();?>assets/green/record/new/out.wav"></audio>
					
					<center>
						<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12"></div>
						
						<div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
							<div class="material-design-btn">
								<a href="<?php echo base_url();?>Layanan_controller/next_antrian/<?php echo $list_layanan['layanan_id'];?>/<?php echo $list_layanan['antrian_id'];?>/<?php echo $list_layanan['nomor_antrian'];?>" class="btn notika-btn-blue" style="margin:10px; padding:10px; width:100%;" onclick="playAudio()"><h2 style="font-family: Times new Roman;"><i class="fa fa-play" style="font-size: 70px;"></i><br/> Next </h2></a>
							</div>
						</div>
						
						<div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
							<div class="material-design-btn">
								<a href="#" class="btn notika-btn-blue" style="margin:10px; padding:10px; width:100%;" onclick="playAudio()"><h2 style="font-family: Times new Roman;"><i class="fa fa-refresh" style="font-size: 70px;"></i><br/> Sound </h2></a>
							</div>
						</div>
						
						<div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
							<div class="material-design-btn">
								<a href="<?php echo base_url();?>Layanan_controller/skip_antrian/<?php echo $list_layanan['layanan_id'];?>/<?php echo $list_layanan['antrian_id'];?>" class="btn notika-btn-red" style="margin:10px; padding:10px; width:100%;"><h2 style="font-family: Times new Roman;"><i class="fa fa-forward" style="font-size: 70px;"></i><br/> Skip </h2></a>
							</div>
						</div>
						
						<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12"></div>

					</center>
					</div>
			<!-- End BUTTON AREA -->
				</div>
			</div>
			
			<script>
			function playAudio() { 
			var totalwaktu = 8568.163;
					document.getElementById('in').pause();
					document.getElementById('in').currentTime=0;
					document.getElementById('in').play();
					totalwaktu=document.getElementById('in').duration*1000;
			setTimeout(function() {
							document.getElementById('nomor-urut').pause();
							document.getElementById('nomor-urut').currentTime=0;
							document.getElementById('nomor-urut').play();
					}, totalwaktu);
					totalwaktu=totalwaktu+1000;
			setTimeout(function() {
							document.getElementById('kode').pause();
							document.getElementById('kode').currentTime=0;
							document.getElementById('kode').play();
					}, totalwaktu);
					totalwaktu=totalwaktu+1500;
			setTimeout(function() {
							document.getElementById('satu').pause();
							document.getElementById('satu').currentTime=0;
							document.getElementById('satu').play();
					}, totalwaktu);
					totalwaktu=totalwaktu+1000;
			setTimeout(function() {
							document.getElementById('dua').pause();
							document.getElementById('dua').currentTime=0;
							document.getElementById('dua').play();
					}, totalwaktu);
					totalwaktu=totalwaktu+1000;
			setTimeout(function() {
							document.getElementById('tiga').pause();
							document.getElementById('tiga').currentTime=0;
							document.getElementById('tiga').play();
					}, totalwaktu);
					totalwaktu=totalwaktu+1000;
			setTimeout(function(){
						document.getElementById('out').pause();
						document.getElementById('out').currentTime=0;
						document.getElementById('out').play();
					}, totalwaktu);
					totalwaktu=totalwaktu+1000;
			}


			</script>
		
    <!-- End Status area-->