<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Report Filter</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">
          <form role="form" method="get">
              <div class="form-body">
                <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                            <label>Tanggal Awal</label>
                            <div class="input-group">
                              <span class="input-group-addon input-circle-left">
                                  <i class="fa fa-calendar"></i>
                              </span>
                              <input class="form-control form-control-inline input-medium date-picker input-circle-right" name="tanggal_awal" size="16" type="text" value="<?php echo date('m/d/Y');?>" readonly />
                              <span class="help-block"></span>
                            </div>
                      </div>
                  </div>


                  <div class="col-sm-4">
                    <div class="form-group">
                          <label>Tanggal Akhir</label>
                          <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input class="form-control form-control-inline input-medium date-picker input-circle-right" name="tanggal_akhir" size="16" type="text" value="<?php echo date('m/d/Y');?>" readonly />
                            <span class="help-block"></span>
                          </div>
                    </div>
                  </div>

              </div>

              <div class="row">
                <div class="col-sm-4">
                <div class="form-group">
                    <label>ITEM</label>
                    <div class="input-group">
                       <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="coa_id" required>
                          <option value=0>SEMUA ITEM</option>
                         <?php
                         $item = $this->db->query("SELECT * FROM public.beone_item");
                         foreach($item->result_array() as $row){
                          ?>
                           <option value="<?php echo $row['item_id'];?>"><?php echo $row['nama'];?></option>
                         <?php } ?>
                       </select>
                 </div>
               </div>
              </div>
              <div class="col-sm-4">
              <div class="form-group">
                  <label>TIPE ITEM</label>
                  <div class="input-group">
                     <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="tipe_item" required>
                        <option value=0>SEMUA TIPE</option>
                        <option value=1>BAHAN BAKU</option>
                        <option value=2>BARANG WIP</option>
                        <option value=3>BARANG JADI</option>
                     </select>
               </div>
             </div>
            </div>
            </div>

            </div>

            <div class="form-actions">
                <button type="button" class="btn default">Cancel</button>
                <button type="submit" class="btn red" name="submit_filter">Filter</button>
            </div>
          </form>
      </div>
    </div>
