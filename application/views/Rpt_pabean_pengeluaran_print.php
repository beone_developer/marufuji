<?php
$awal = $_GET['tglawal'];
$tgl_awal = substr($awal,8,2);
$bulan_awal = substr($awal,5,2);
$tahun_awal = substr($awal,0,4);

$bln_awal = $bulan_awal * 1;

$akhir = $_GET['tglakhir'];
$tgl_akhir = substr($akhir,8,2);
$bulan_akhir = substr($akhir,5,2);
$tahun_akhir = substr($akhir,0,4);

$bln_akhir = $bulan_akhir * 1;

if ($bln_awal == 1){
	$spell_bulan_awal = "Januari";
}else if($bln_awal == 2){
	$spell_bulan_awal = "Februari";
}else if($bln_awal == 3){
	$spell_bulan_awal = "Maret";
}else if($bln_awal == 4){
	$spell_bulan_awal = "April";
}else if($bln_awal == 5){
	$spell_bulan_awal = "Mei";
}else if($bln_awal == 6){
	$spell_bulan_awal = "Juni";
}else if($bln_awal == 7){
	$spell_bulan_awal = "Juli";
}else if($bln_awal == 8){
	$spell_bulan_awal = "Agustus";
}else if($bln_awal == 9){
	$spell_bulan_awal = "September";
}else if($bln_awal == 10){
	$spell_bulan_awal = "Oktober";
}else if($bln_awal == 11){
	$spell_bulan_awal = "November";
}else if($bln_awal == 12){
	$spell_bulan_awal = "Desember";
}



if ($bln_akhir == 1){
	$spell_bulan_akhir = "Januari";
}else if($bln_akhir == 2){
	$spell_bulan_akhir = "Februari";
}else if($bln_akhir == 3){
	$spell_bulan_akhir = "Maret";
}else if($bln_akhir == 4){
	$spell_bulan_akhir = "April";
}else if($bln_akhir == 5){
	$spell_bulan_akhir = "Mei";
}else if($bln_akhir == 6){
	$spell_bulan_akhir = "Juni";
}else if($bln_akhir == 7){
	$spell_bulan_akhir = "Juli";
}else if($bln_akhir == 8){
	$spell_bulan_akhir = "Agustus";
}else if($bln_akhir == 9){
	$spell_bulan_akhir = "September";
}else if($bln_akhir == 10){
	$spell_bulan_akhir = "Oktober";
}else if($bln_akhir == 11){
	$spell_bulan_akhir = "November";
}else if($bln_akhir == 12){
	$spell_bulan_akhir = "Desember";
}


// intance object dan memberikan pengaturan halaman PDF
$pdf = new FPDF('l','mm','A4');


// membuat halaman baru
$pdf->AddPage();
// setting jenis font yang akan digunakan
$pdf->SetFont('Arial','B',12);
// mencetak string
$pdf->Cell(270,6,'LAPORAN PENGELUARAN BARANG PER DOKUMEN PABEAN',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(270,6,'BUILDYET INDONESIA, PT',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(270,6,'PERIODE '.$tgl_awal.'-'.$spell_bulan_awal.'-'.$tahun_awal.' S.D '.$tgl_akhir.'-'.$spell_bulan_akhir.'-'.$tahun_akhir,0,1,'C');

// Memberikan space kebawah agar tidak terlalu rapat
$pdf->Cell(10,7,'',0,1);

$pdf->SetFont('Arial','B',8);
$pdf->Cell(10,12,'NO',1,0, 'C');
$pdf->Cell(55,6,'Dokumen Pabean',1,0,'C');
$pdf->Cell(45,6,'Surat Jalan',1,0, 'C');
$pdf->Cell(55,12,'Penerima',1,0, 'C');
$pdf->Cell(20,12,'Kode Barang',1,0, 'C');
$pdf->Cell(35,12,'Nama Barang',1,0, 'C');
$pdf->Cell(10,12,'Sat',1,0, 'C');
$pdf->Cell(20,12,'Jumlah',1,0, 'C');
$pdf->Cell(10,12,'Valas',1,0, 'C');
$pdf->Cell(20,12,'Nilai Barang',1,0, 'C');
$pdf->Cell(5,6,'',0,0);
$pdf->Ln();
$pdf->Cell(10,0,' ',0,0);
$pdf->Cell(15,6,'Jenis',1,0, 'C');
$pdf->Cell(20,6,'Nomor',1,0, 'C');
$pdf->Cell(20,6,'Tanggal',1,0, 'C');
$pdf->Cell(25,6,'Nomor',1,0, 'C');
$pdf->Cell(20,6,'Tanggal',1,0, 'C');

$pdf->Cell(10,10,'',0,1);

$pdf->SetFont('Arial','',7);

$this->mysql = $this ->load -> database('mysql', TRUE);

$sql = $this->db->query("SELECT h.jenis_bc, h.car_no, h.bc_no, h.bc_date, h.delivery_date, h.delivery_no, h.receiver_id, h.surat_jalan_no, h.surat_jalan_date, d.qty, d.price, i.nama, i.item_code, c.nama as customer
												FROM public.beone_export_header h INNER JOIN public.beone_export_detail d ON h.export_header_id = d.export_header_id INNER JOIN public.beone_item i ON d.item_id = i.item_id INNER JOIN public.beone_custsup c ON h.receiver_id = c.custsup_id
												WHERE h.bc_date BETWEEN '$awal' AND '$akhir' ORDER BY h.bc_no ASC, h.delivery_date ASC");

$no = 0;
$y = 0;
foreach($sql->result_array() as $row){
		$no = $no + 1;

		$cellWidth=55; //lebar sel
		$cellWidthUraian=35;
		$cellWidthSuratJalan=25;
		$cellHeight=4; //tinggi sel satu baris normal
		$over = 0;

		if($pdf->GetStringWidth($row['customer']) < $cellWidth AND $pdf->GetStringWidth($row['nama']) < $cellWidthUraian AND $pdf->GetStringWidth($row['delivery_no']) < $cellWidthSuratJalan){
			$line=1;		//tidak melakukan apa2
			$over=1;		//isi terserah asal terisi
		}else if($pdf->GetStringWidth($row['nama']) > $cellWidthUraian){
			$textLength=strlen($row['nama']);	//total panjang teks
			$errMargin=5;		//margin kesalahan lebar sel, untuk jaga-jaga
			$startChar=0;		//posisi awal karakter untuk setiap baris
			$maxChar=0;			//karakter maksimum dalam satu baris, yang akan ditambahkan nanti
			$textArray=array();	//untuk menampung data untuk setiap baris
			$tmpString="";		//untuk menampung teks untuk setiap baris (sementara)

				while($startChar < $textLength){ //perulangan sampai akhir teks
					//perulangan sampai karakter maksimum tercapai
					while(
					$pdf->GetStringWidth( $tmpString ) < ($cellWidthUraian-$errMargin) &&
					($startChar+$maxChar) < $textLength ) {
						$maxChar++;
						$tmpString=substr($row['nama'],$startChar,$maxChar);
					}
					//pindahkan ke baris berikutnya
					$startChar=$startChar+$maxChar;
					//kemudian tambahkan ke dalam array sehingga kita tahu berapa banyak baris yang dibutuhkan
					array_push($textArray,$tmpString);
					//reset variabel penampung
					$maxChar=0;
					$tmpString='';

				}
				//dapatkan jumlah baris
				$line=count($textArray);
				$over = 1; //nama item yg over
		}else if($pdf->GetStringWidth($row['customer']) > $cellWidth){
			$textLength=strlen($row['customer']);	//total panjang teks
			$errMargin=5;		//margin kesalahan lebar sel, untuk jaga-jaga
			$startChar=0;		//posisi awal karakter untuk setiap baris
			$maxChar=0;			//karakter maksimum dalam satu baris, yang akan ditambahkan nanti
			$textArray=array();	//untuk menampung data untuk setiap baris
			$tmpString="";		//untuk menampung teks untuk setiap baris (sementara)

				while($startChar < $textLength){ //perulangan sampai akhir teks
					//perulangan sampai karakter maksimum tercapai
					while(
					$pdf->GetStringWidth( $tmpString ) < ($cellWidth-$errMargin) &&
					($startChar+$maxChar) < $textLength ) {
						$maxChar++;
						$tmpString=substr($row['customer'],$startChar,$maxChar);
					}
					//pindahkan ke baris berikutnya
					$startChar=$startChar+$maxChar;
					//kemudian tambahkan ke dalam array sehingga kita tahu berapa banyak baris yang dibutuhkan
					array_push($textArray,$tmpString);
					//reset variabel penampung
					$maxChar=0;
					$tmpString='';

				}
				//dapatkan jumlah baris
				$line=count($textArray);
				$over = 2; //customer yg over
		}else if($pdf->GetStringWidth($row['delivery_no']) > $cellWidthSuratJalan){
			$textLength=strlen($row['delivery_no']);	//total panjang teks
			$errMargin=5;		//margin kesalahan lebar sel, untuk jaga-jaga
			$startChar=0;		//posisi awal karakter untuk setiap baris
			$maxChar=0;			//karakter maksimum dalam satu baris, yang akan ditambahkan nanti
			$textArray=array();	//untuk menampung data untuk setiap baris
			$tmpString="";		//untuk menampung teks untuk setiap baris (sementara)

				while($startChar < $textLength){ //perulangan sampai akhir teks
					//perulangan sampai karakter maksimum tercapai
					while(
					$pdf->GetStringWidth( $tmpString ) < ($cellWidthSuratJalan-$errMargin) &&
					($startChar+$maxChar) < $textLength ) {
						$maxChar++;
						$tmpString=substr($row['delivery_no'],$startChar,$maxChar);
					}
					//pindahkan ke baris berikutnya
					$startChar=$startChar+$maxChar;
					//kemudian tambahkan ke dalam array sehingga kita tahu berapa banyak baris yang dibutuhkan
					array_push($textArray,$tmpString);
					//reset variabel penampung
					$maxChar=0;
					$tmpString='';

				}
				//dapatkan jumlah baris
				$line=count($textArray);
				$over = 3; //no surat jalan yg over
		}

		if ($row['jenis_bc'] == 1){
				$view_bc = "BC 3.0";
		}elseif($row['jenis_bc'] == 2){
				$view_bc = 	"BC 2.7";
		}elseif($row['jenis_bc'] == 3){
				$view_bc = 	"BC 4.1";
		}elseif($row['jenis_bc'] == 4){
				$view_bc = 	"BC 2.5";
		}


		if ($over == 1){// nama item yg over
			$pdf->Cell(10,($line * $cellHeight),$no,1,0,'C');
	    $pdf->Cell(15,($line * $cellHeight),$view_bc,1,0);
	    $pdf->Cell(20,($line * $cellHeight),$row['bc_no'],1,0);
	    $pdf->Cell(20,($line * $cellHeight),$row['bc_date'],1,0);
	    $pdf->Cell(25,($line * $cellHeight),$row['delivery_no'],1,0);
	    $pdf->Cell(20,($line * $cellHeight),$row['delivery_date'],1,0);
	    $pdf->Cell(55,($line * $cellHeight),$row['customer'],1,0);
	    $pdf->Cell(20,($line * $cellHeight),$row['item_code'],1,0);
			$xPos=$pdf->GetX();
			$yPos=$pdf->GetY();
			$pdf->MultiCell($cellWidthUraian,$cellHeight,$row['nama'],1, 'L');
			$pdf->SetXY($xPos + $cellWidthUraian , $yPos);
	    $pdf->Cell(10,($line * $cellHeight),'KGM',1,0);
	    $pdf->Cell(20,($line * $cellHeight),number_format($row['qty'], 4),1,0, 'R');
	    $pdf->Cell(10,($line * $cellHeight),'USD',1,0);
	    $pdf->Cell(20,($line * $cellHeight),number_format($row['price']*$row['qty'], 4),1,0, 'R');
		}else if ($over == 2){// customer yg over
			$pdf->Cell(10,($line * $cellHeight),$no,1,0,'C');
	    $pdf->Cell(15,($line * $cellHeight),$view_bc,1,0);
	    $pdf->Cell(20,($line * $cellHeight),$row['bc_no'],1,0);
	    $pdf->Cell(20,($line * $cellHeight),$row['bc_date'],1,0);
	    $pdf->Cell(20,($line * $cellHeight),$row['delivery_no'],1,0);
	    $pdf->Cell(20,($line * $cellHeight),$row['delivery_date'],1,0);
			$xPos=$pdf->GetX();
			$yPos=$pdf->GetY();
			$pdf->MultiCell($cellWidth,$cellHeight,$row['customer'],1, 'L');
			$pdf->SetXY($xPos + $cellWidth , $yPos);
	    $pdf->Cell(20,($line * $cellHeight),$row['item_code'],1,0);
			$pdf->Cell(35,($line * $cellHeight),$row['nama'],1,0);
	    $pdf->Cell(10,($line * $cellHeight),'KGM',1,0);
	    $pdf->Cell(20,($line * $cellHeight),number_format($row['qty'], 4),1,0, 'R');
	    $pdf->Cell(10,($line * $cellHeight),'USD',1,0);
	    $pdf->Cell(20,($line * $cellHeight),number_format($row['price']*$row['qty'], 4),1,0, 'R');
		}else if ($over == 3){// no surat jalan yg over
			$pdf->Cell(10,($line * $cellHeight),$no,1,0,'C');
	    $pdf->Cell(15,($line * $cellHeight),$view_bc,1,0);
	    $pdf->Cell(20,($line * $cellHeight),$row['bc_no'],1,0);
	    $pdf->Cell(20,($line * $cellHeight),$row['bc_date'],1,0);
			$xPos=$pdf->GetX();
			$yPos=$pdf->GetY();
			$pdf->MultiCell($cellWidthSuratJalan,$cellHeight,$row['delivery_no'],1, 'L');
			$pdf->SetXY($xPos + $cellWidthSuratJalan , $yPos);
			$pdf->Cell(20,($line * $cellHeight),$row['delivery_date'],1,0);
			$pdf->Cell(55,($line * $cellHeight),$row['customer'],1,0);
	    $pdf->Cell(20,($line * $cellHeight),$row['item_code'],1,0);
			$pdf->Cell(35,($line * $cellHeight),$row['nama'],1,0);
	    $pdf->Cell(10,($line * $cellHeight),'KGM',1,0);
	    $pdf->Cell(20,($line * $cellHeight),number_format($row['qty'], 4),1,0, 'R');
	    $pdf->Cell(10,($line * $cellHeight),'USD',1,0);
	    $pdf->Cell(20,($line * $cellHeight),number_format($row['price']*$row['qty'], 4),1,0, 'R');
		}

    $pdf->Ln();
		$y = $y + $cellHeight;

		if ($y >= 110){
			$pdf->AddPage();
			// setting jenis font yang akan digunakan

			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(10,12,'NO',1,0, 'C');
			$pdf->Cell(47,6,'Dokumen Pabean',1,0,'C');
			$pdf->Cell(40,6,'Surat Jalan',1,0, 'C');
			$pdf->Cell(55,12,'Penerima',1,0, 'C');
			$pdf->Cell(20,12,'Kode Barang',1,0, 'C');
			$pdf->Cell(35,12,'Nama Barang',1,0, 'C');
			$pdf->Cell(10,12,'Sat',1,0, 'C');
			$pdf->Cell(20,12,'Jumlah',1,0, 'C');
			$pdf->Cell(10,12,'Valas',1,0, 'C');
			$pdf->Cell(20,12,'Nilai Barang',1,0, 'C');
			$pdf->Cell(5,6,'',0,0);
			$pdf->Ln();
			$pdf->Cell(10,0,' ',0,0);
			$pdf->Cell(15,6,'Jenis',1,0, 'C');
			$pdf->Cell(12,6,'Nomor',1,0, 'C');
			$pdf->Cell(20,6,'Tanggal',1,0, 'C');
			$pdf->Cell(20,6,'Nomor',1,0, 'C');
			$pdf->Cell(20,6,'Tanggal',1,0, 'C');

			$pdf->Cell(10,10,'',0,1);
		}
}
date_default_timezone_set('Asia/Jakarta');
$jam=date("H_i_s");
$pdf->Output('','KELUAR_PERIODE_'.$tgl_awal.'-'.$spell_bulan_awal.'-'.$tahun_awal.'_S.D_'.$tgl_akhir.'-'.$spell_bulan_akhir.'-'.$tahun_akhir.'_'.$jam.'.pdf');
?>
