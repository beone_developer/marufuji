<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Form Kredit Note</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">
          <form role="form" method="post">
              <div class="form-body">
                <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                            <label>Tanggal</label>
                            <div class="input-group">
                              <span class="input-group-addon input-circle-left">
                                  <i class="fa fa-calendar"></i>
                              </span>
                              <input class="form-control form-control-inline input-medium date-picker input-circle-right" name="tanggal_dk_note" size="16" type="text" value="<?php echo date('m/d/Y');?>" />
                              <span class="help-block"></span>
                            </div>
                      </div>

                          <div name="txtkdnote" id="txtkdnote"><b>Nomor Kredit Note</b></div>

                  </div>


                  <div class="col-sm-8" id="detail_cash_bank">
                      <div class="portlet light bordered">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Akun Biaya</label>
                                    <div class="input-group">
                                       <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="coa_id" required>
                                            <option value=""></option>
                                         <?php 	foreach($list_coa as $row){ ?>
                                           <option value="<?php echo $row['coa_id'];?>"><?php echo $row['nomor']." | ".$row['nama'];?></option>
                                         <?php } ?>
                                       </select>
                                 </div>
                               </div>
                            </div>

                            <div class="col-sm-6">
                              <div class="form-group">
                                  <label>Supplier</label>
                                  <div class="input-group">
                                     <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="supplier" onChange="tampilkode(this.value);" required>
                                        <option value=""></option>
                                       <?php 	foreach($list_supplier as $row){ ?>
                                         <option value="<?php echo $row['custsup_id'];?>"><?php echo $row['nama'];?></option>
                                       <?php } ?>
                                     </select>
                               </div>
                             </div>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label>Keterangan</label>
                              <input type="text" class="form-control" placeholder="Keterangan" name="keterangan_kredit_note" required>
                            </div>
                          </div>
                          <div class="col-sm-12">
                              <div class="row">
                                <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Posisi</label>
                                      <div class="input-group">
                                         <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="posisi" required>
                                              <option value=""></option>
                                              <option value=0>MINUS (-)</option>
                                              <option value=1>PLUS (+)</option>
                                         </select>
                                   </div>
                                 </div>
                                </div>
                                <div class="col-sm-4">
                                  <div class="form-group">
                                  <label>Jumlah IDR</label>
                                  <input type="text" class="form-control" placeholder="Jumlah IDR" name="jumlah_idr" id="saldo" required>
                                  </div>
                                </div>
                                <div class="col-sm-4">
                                  <div class="form-group">
                                  <label>Valas</label>
                                  <input type="text" class="form-control" placeholder="Jumlah Valas" name="jumlah_valas" id="saldo_valas" required>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>

                      </div>
                  </div>
              </div>
            </div>

            <div class="form-actions">
                <button type="button" class="btn default">Cancel</button>
                <?php if(helper_security("kredit_note_add") == 1){?>
                <button type="submit" class="btn red" name="submit_kredit_note">Submit</button>
                <?php }?>
            </div>
          </form>
      </div>
    </div>

    <script>
				function tampilkode(str)
				{
				if (str=="")
				  {
				  document.getElementById("txtkdnote").innerHTML="";
				  return;
				  }
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
					document.getElementById("txtkdnote").innerHTML=xmlhttp.responseText;
					}
				  }
				xmlhttp.open("GET","Kredit_note_controller/get_nomor_kredit_note?q="+str,true);
				xmlhttp.send();
				}
				</script>


        <script type="text/javascript">

    		var saldo = document.getElementById('saldo');
    		saldo.addEventListener('keyup', function(e){
    			saldo.value = formatRupiah(this.value, 'Rp. ');
    		});

        var saldo_valas = document.getElementById('saldo_valas');
    		saldo_valas.addEventListener('keyup', function(e){
    			saldo_valas.value = formatRupiah(this.value, 'Rp. ');
    		});

    		/* Fungsi formatRupiah */
    		function formatRupiah(angka, prefix){
    			var number_string = angka.replace(/[^,\d]/g, '').toString(),
    			split   		= number_string.split(','),
    			sisa     		= split[0].length % 3,
    			rupiah     		= split[0].substr(0, sisa),
    			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    			// tambahkan titik jika yang di input sudah menjadi angka ribuan
    			if(ribuan){
    				separator = sisa ? '.' : '';
    				rupiah += separator + ribuan.join('.');
    			}

    			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    		}
    	</script>
