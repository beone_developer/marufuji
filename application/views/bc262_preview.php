<div class="container">
    <div class="row">
        <p class="text-center" style="font-size: 17px"><b>PEMBERITAHUAN PEMASUKAN KEMBALI BARANG YANG DIKELUARKAN DARI <br> TEMPAT PENIMBUNAN BERIKAT DENGAN JAMINAN</b></p>
    </div>

    <form action="">
        <div class="form-group row">
            <label for="status" class="col-sm-2 col-form-label">Status</label>
            <div class="col-sm-2">
                <input style="border: none;" type="text" readonly class="form-control" id="status" value = "<?php
                $n = 0;
                if ($ptb_edit) {
                    foreach ($ptb_edit as $data) {
                        $n = $n + 1;
                        echo $data->URAIAN_STATUS;
                    }
                }
                ?>">
            </div>
        </div>

        <div class="form-group row">
            <label for="status_perbaikan" class="col-sm-2 col-form-label">Status Perbaikan</label>
            <div class="col-sm-2">
                <input style="border: none;" type="text" readonly class="form-control" id="status_perbaikan" value="-">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-2">
                <a href="#modal" class="btn btn-primary btn-sm" role="button" data-target="#modal" data-toggle="modal">DAFTAR RESPON</a>
            </div>
        </div>

        <!-- FORM NOMOR PENGAJUAN -->
        <div style="margin-top: 30px;">
            <div class="form-group row">
                <label for="nomor_pengajuan" class="col-sm-2 col-form-label">Nomor Pengajuan</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="nomor_pengajuan" value = "<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->nomor_aju;
                        }
                    }
                    ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="nomor_pendaftaran" class="col-sm-2 col-form-label">Nomor Pendaftaran</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="nomor_pendaftaran" value="<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->nomor_daftar;
                        }
                    }
                    ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="tanggal_pendaftaran" class="col-sm-2 col-form-label">Tanggal Pendaftaran</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="tanggal_pendaftaran" value="<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->tanggal_daftar;
                        }
                    }
                    ?>" placeholder="DDMMYY">
                </div>
            </div>		
        </div>
        <!-- TUTUP FORM NOMOR PENGAJUAN -->

        <!-- FORM KPPBC BONGKAR -->
        <div style="margin-top: 30px;">


            <div class="form-group row">
                <label for="kppbc_pengawas" class="col-sm-2 col-form-label">Kantor Pabean</label>
                <div class="col-sm-4">
                    <input type="text" readonly class="form-control" id="kppbc_pengawas" value="<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->kode_kantor_pabean . " - " . $data->kantor_pabean;
                        }
                    }
                    ?>">
                </div>
                <label for="tujuan" class="col-sm-2 col-form-label">Kode Gudang PLB </label>
                <div class="col-sm-2">
                    <input type="text" readonly class="form-control" id="nama" value ="<?php
                    $n = 0;
                    if ($ptb_edit) {
                        foreach ($ptb_edit as $data) {
                            $n = $n + 1;
                            echo $data->KODE_TPS;
                        }
                    }
                    ?>" >
                </div>
            </div>

            <div class="form-group row">
                <label for="tujuan" class="col-sm-2 col-form-label">Tujuan Pemasukan </label>
                <div class="col-sm-4">
                    <select id="inputState" readonly class="form-control">
                        <option selected>Choose...</option>
                        <option>...</option>
                    </select>
                </div>
            </div>		
        </div>
        <!-- TUTUP FORM KPPBC BONGKAR -->


        <div class="row">
            <div class="col-sm-12">

                <!-- FORM LEFT SIDE -->
                <ol>
                    <div class="col-sm-6">
                        <p><b>PENGUSAHA TPB</b></p>
                        <!-- FORM PENGUSAHA -->
                        <div>
                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">NPWP</label>
                                    <div class="col-sm-4">
                                        <select id="inputState" readonly class="form-control">
                                            <option selected>NPWP</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="nama" value = "<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->id_pengusaha;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>	
                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control" id="nama" value = "<?php
                                        $n = 0;
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                $n = $n + 1;
                                                echo $data->NAMA_PENGUSAHA;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                <div class="col-sm-8">
                                    <input type="text" readonly class="form-control" id="alamat" value = "<?php
                                    $n = 0;
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            $n = $n + 1;
                                            echo $data->ALAMAT_PENGUSAHA;
                                        }
                                    }
                                    ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Negara" class="col-sm-3 col-form-label">IJIN PTB</label>
                                <div class="col-sm-4">
                                    <input type="text" readonly class="form-control" id="negara" value = "<?php
                                    $n = 0;
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            $n = $n + 1;
                                            echo $data->NOMOR_IJIN_TPB;
                                        }
                                    }
                                    ?>">
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" readonly class="form-control" id="negara" value = "<?php
                                    $n = 0;
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            $n = $n + 1;
                                            echo $data->TANGGAL_IJIN_TPB;
                                        }
                                    }
                                    ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">API</label>
                                    <div class="col-sm-4">
                                        <select id="inputState" readonly class="form-control">
                                            <option selected>API</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="nama" value = "<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->api_pengusaha;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>

                        </div>
                        <!-- TUTUP FORM PEMASOK -->		

                        <!-- FORM Penerima barang -->
                        <div>
                            <p style="margin-left: -38px"><b>PENGIRIM BARANG</b></p>

                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">NPWP</label>
                                    <div class="col-sm-4">
                                        <select id="inputState" readonly class="form-control">
                                            <option selected>NPWP</option>
                                            <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="nama" value = "<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->ID_PENGIRIM;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>	

                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control" id="alamat" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->NAMA_PENGIRIM;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>

                            </div>
                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control" id="alamat" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->ALAMAT_PENGIRIM;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>

                            </div>
                        </div>
                        <!-- TUTUP FORM IMPORTIR -->

                        <!-- FORM PEMILIK -->
                        <div>
                           <li>
                                <a href="#modal4" class="btn btn-primary btn-sm" role="button" data-target="#modal4" data-toggle="modal">DOKUMEN</a>
                            </li>
                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Packing List</label>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="nama" value ="<?php
                                        if ($ptb_packing) {
                                            foreach ($ptb_packing as $data) {
                                                echo $data->nomor_dokumen;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="nama" value ="<?php
                                        if ($ptb_packing) {
                                            foreach ($ptb_packing as $data) {
                                                echo $data->tanggal_dokumen;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>	

                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Surat Keputusan</label>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="nama" value ="" >
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="nama" value ="" >
                                    </div>
                                </li>
                            </div>	
                            <div class="form-group row">
                                <li>
                                    <label for="nama" class="col-sm-3 col-form-label">Dok. BC 2.6.1</label>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="nama" value ="" >
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="nama" value ="" >
                                    </div>
                                </li>
                            </div>	

                            <div class="form-group row">
                                <li>
                                    <div class="col-sm-10">
                                        <table class="table table-sm">
                                            <thead>
                                                <tr class="bg-success">
                                                    <th scope="col">Jenis Dokumen</th>
                                                    <th scope="col">Nomor Dokumen</th>
                                                    <th scope="col">Tanggal</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php
                                                $n = 0;
                                                if ($ptb_other) {
                                                    foreach ($ptb_other as $data) {
                                                        $n = $n + 1;
                                                        ?>
                                                    <td width="30%"><?php echo $data->uraian_dokumen; ?></td>
                                                    <td width="30%"><?php echo $data->nomor_dokumen; ?></td>
                                                    <td width="30%"><?php echo $data->tanggal_dokumen; ?></td></tr>
                                                    <?php
                                                }
                                            }
                                            ?>						    

                                            </tbody>
                                        </table>
                                    </div>
                                </li>
                            </div>
                        </div>


                        <!-- FORM HARGA -->
                        <div>
                            <p style="margin-left: -38px"><b>HARGA</b></p>


                            <div class="form-group row">
                                <li>
                                    <label for="alamat" class="col-sm-3 col-form-label">Valuta</label>
                                    <div class="col-sm-3">
                                        <input type="text" readonly class="form-control" id="alamat" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->KODE_VALUTA;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">
                                <li>
                                    <div class="col-sm-3">
                                        <button type="submit" class="btn blue" name="submit_user">NDPBM</button>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="api" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->NDPBM;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                                <!--									<div class="col-sm-4">
                                                                                                                <input type="text" class="form-control" id="alamat" >
                                                                                                        </div>-->
                            </div>

                            <div class="form-group row">
                                <li>
                                    <label for="Negara" class="col-sm-3 col-form-label">Nilai CIF</label>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="alamat" value="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->CIF;
                                            }
                                        }
                                        ?>">
                                    </div>
                                </li>
                            </div>

                            <div class="form-group row">

                                <label for="Negara" class="col-sm-3 col-form-label">Nilai CIF (Rp)</label>
                                <div class="col-sm-4">
                                    <input type="text" readonly class="form-control" id="alamat" value="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->CIF_RUPIAH;
                                        }
                                    }
                                    ?>">
                                </div>

                            </div>


                        </div>
                        <!-- TUTUP FORM HARGA -->	
                    </div>
                    <!-- TUTUP FORM LEFT SIDE -->


                    <!-- FORM RIGHT SIDE -->
                    <div class="col-sm-6" style="padding-right: 50px;">
                        <p><b>PENGANGKUTAN</b></p>
                        <div class="form-group row">
                            <li>
                                <label for="nama" class="col-sm-4 col-form-label">Jenis Sarana Pengangkut</label>
                                <div class="col-sm-6">
                                    <select id="inputState" readonly class="form-control">
                                        <option selected></option>
                                        <option>...</option>
                                    </select>
                                </div>
                            </li>
                        </div>


                        <div class="form-group row">
                            <li>
                                <a href="#modal2" class="btn btn-primary btn-sm" role="button" data-target="#modal2" data-toggle="modal">KONTAINER</a>
                            </li>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <table class="table table-sm">
                                    <thead>
                                        <tr class="bg-success">
                                            <th scope="col">No Cont</th>
                                            <th scope="col">Ukuran</th>
                                            <th scope="col">Tipe</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $n = 0;
                                        if ($ptb_kontainer) {
                                            foreach ($ptb_kontainer as $data) {
                                                $n = $n + 1;
                                                ?>
                                            <td width="30%"><?php echo $data->NOMOR_KONTAINER; ?></td>
                                            <td width="30%"><?php echo $data->URAIAN_UKURAN_KONTAINER; ?></td>
                                            <td width="30%"><?php echo $data->URAIAN_TIPE_KONTAINER; ?></td></tr>
                                            <?php
                                        }
                                    }
                                    ?>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group row">
                            <li>
                                <a href="#modal3" class="btn btn-primary btn-sm" role="button" data-target="#modal3" data-toggle="modal">KEMASAN</a>
                            </li>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-10">
                                <table class="table table-sm">
                                    <thead>
                                        <tr class="bg-success">
                                            <th scope="col">Jumlah</th>
                                            <th scope="col">Kode Jenis</th>
                                            <th scope="col">Jenis</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $n = 0;
                                        if ($ptb_kemasan) {
                                            foreach ($ptb_kemasan as $data) {
                                                $n = $n + 1;
                                                ?>
                                            <td width="30%"><?php echo $data->JUMLAH_KEMASAN; ?></td>
                                            <td width="30%"><?php echo $data->KODE_JENIS_KEMASAN; ?></td>
                                            <td width="30%"><?php echo $data->URAIAN_KEMASAN; ?></td></tr>
                                            <?php
                                        }
                                    }
                                    ?>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- FORM BARANG -->
                        <div>
                            <a href="./preview_BC262/form_detail_barang?id=<?php if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->id;
                                            }
                                        } ?>"><p style="margin-left: -38px"><b>BARANG</b></p></a>
                            
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Bruto (Kg)</label>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="nama" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->bruto;
                                            }
                                        }
                                        ?>" >
                                    </div>
                                </li>
                            </div>
                            <div class="form-group row">
                                <li>
                                    <label class="col-sm-3 col-form-label">Netto (Kg)</label>
                                    <div class="col-sm-4">
                                        <input type="text" readonly class="form-control" id="nama" value ="<?php
                                        if ($ptb_edit) {
                                            foreach ($ptb_edit as $data) {
                                                echo $data->NETTO;
                                            }
                                        }
                                        ?>" >
                                    </div>

                                </li>

                            </div>	
                            <div class="form-group row">

                                <label class="col-sm-3 col-form-label">Jumlah Barang</label>
                                <div class="col-sm-2">
                                    <input type="text" readonly class="form-control" id="nama" value ="<?php
                                    if ($ptb_edit) {
                                        foreach ($ptb_edit as $data) {
                                            echo $data->JUMLAH_BARANG;
                                        }
                                    }
                                    ?>" >
                                </div>



                            </div>	
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <table class="table table-sm">
                                    <thead>
                                        <tr class="bg-success">
                                            <th scope="col">Jenis Pungutan</th>
                                            <th scope="col">Ditangguhkan (Rp)</th>

                                        </tr>
                                    </thead>
                                    <tbody>



                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- FORM PUNGUTAN-->
                        <!--							<div class="form-group row">
                                                                                <li>
                                                                                        <label for="tujuan" class="col-sm-10 col-form-label">Pngutan</label>
                                                                                </li>
                                                                        </div>-->







                </ol>
                <div class="form-group row">
                    <li>
                                <a href="#modal5" class="btn btn-primary btn-sm" role="button" data-target="#modal5" data-toggle="modal">JAMINAN</a>
                            </li>
                </div>
                <table class="table table-striped table-bordered table-hover">

                    <thead>
                        <tr class="bg-success">
                            <th scope="col">Jenis</th>
                            <th scope="col">Nomor</th>
                            <th scope="col">Tanggal</th>
                            <th scope="col">Nilai</th>
                            <th scope="col">Jatuh Tempo</th>
                            <th scope="col">Penjamin</th>
                            <th scope="col">Nomor BPJ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            $n = 0;
                            if ($ptb_jaminan) {
                                foreach ($ptb_jaminan as $data) {
                                    $n = $n + 1;
                                    ?>
                                <tr><td width="30%"><?php echo $n; ?></td>
                                    <td width="30%"><?php echo $data->URAIAN_JENIS_JAMINAN; ?></td>
                                    <td width="30%"><?php echo $data->NOMOR_JAMINAN; ?></td>
                                    <td width="30%"><?php echo $data->TANGGAL_JAMINAN; ?></td></tr>
                            <td width="30%"><?php echo $data->NILAI_JAMINAN; ?></td>
                            <td width="30%"><?php echo $data->TANGGAL_JATUH_TEMPO; ?></td>
                            <td width="30%"><?php echo $data->PENJAMIN; ?></td></tr>
                            <td width="30%"><?php echo $data->NOMOR_BPJ; ?></td></tr>
        <?php
    }
}
?>						    
                    </tr>
                    </tbody>
                </table>
                <p>Dengan saya menyatakan bertanggung jawab atas kebenaran
                    <br></br>hal hal yang diberitahukan dalam pemberitahuan pabean ini. </p>
                <div class="form-group row">
                    <!--<li>-->
                    <!--<label for="Negara" class="col-sm-3 col-form-label">Pelabuhan Bongkar</label>-->
                    <div class="col-sm-2">
                        <input type="text" readonly class="form-control" id="alamat" Placeholder="kota" value = "<?php
                        if ($ptb_edit) {
                            foreach ($ptb_edit as $data) {
                                echo $data->KOTA_TTD;
                            }
                        }
                        ?>">
                    </div>
                    <div class="col-sm-2">
                        <input type="text" readonly class="form-control" id="alamat" Placeholder="Tanggal" value = "<?php
                        if ($ptb_edit) {
                            foreach ($ptb_edit as $data) {
                                echo $data->TANGGAL_TTD;
                            }
                        }
                        ?>">
                    </div>
                    <!--</li>-->
                </div>
                <div class="form-group row">
                    <!--<li>-->
                    <label for="Negara" class="col-sm-2 col-form-label">Pemberitahu</label>
                    <div class="col-sm-2">
                        <input type="text" readonly class="form-control" id="alamat" Placeholder="nama" value = "<?php
                        if ($ptb_edit) {
                            foreach ($ptb_edit as $data) {
                                echo $data->NAMA_TTD;
                            }
                        }
                        ?>">
                    </div>
                    <!--</li>-->
                </div>
                <div class="form-group row">
                    <!--<li>-->
                    <label for="Negara" class="col-sm-2 col-form-label">Jabatan</label>
                    <div class="col-sm-2">
                        <input type="text" readonly class="form-control" id="alamat" Placeholder="jabatan" value = "<?php
                               if ($ptb_edit) {
                                   foreach ($ptb_edit as $data) {
                                       echo $data->JABATAN_TTD;
                                   }
                               }
                               ?>" >
                    </div>
                    <!--</li>-->
                </div>
            </div>


        </div>		
        <!-- TUTUP FORM RIGHT SIDE -->

</div>
</div>
</form>
<!-- MODAL DOKUMEN -->

<div id="modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>RESPON</b></h3>
        </div>
        <div class="modal-body">
            
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                    <thead>
                        <tr class="bg-success">
                            <th scope="col">Kode</th>
                            <th scope="col">Uraian</th>
                            <th scope="col">Waktu</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php
                            if ($ptb_respon) {
                                foreach ($ptb_respon as $data) {
                                    ?>
                                    <td width="30%"><?php echo $data->KODE_RESPON; ?></td>
                                    <td width="30%"><?php echo $data->NOMOR_RESPON; ?></td>
                                    <td width="30%"><?php echo $data->TANGGAL_RESPON; ?></td></tr>
        <?php
    }
}
?>						    
                    </tr>
                    </tbody>
                </table>
                    
                    
                    
                </div>
            </div>	
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>
<!-- MODAL KONTAINER -->

<div id="modal2" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>KONTAINER</b></h3>
        </div>
        <div class="modal-body">
            
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                    <thead>
                        <tr class="bg-success">
                            <th scope="col">Nomor Kontainer</th>
                            <th scope="col">Ukuran</th>
                            <th scope="col">Tipe</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                           <?php
                                        $n = 0;
                                        if ($ptb_kontainer) {
                                            foreach ($ptb_kontainer as $data) {
                                                $n = $n + 1;
                                                ?>
                                            <td width="30%"><?php echo $data->NOMOR_KONTAINER; ?></td>
                                            <td width="30%"><?php echo $data->URAIAN_UKURAN_KONTAINER; ?></td>
                                            <td width="30%"><?php echo $data->URAIAN_TIPE_KONTAINER; ?></td></tr>
                                            <?php
                                        }
                                    }
                                    ?>
                        </tr>	    
                    
                    </tbody>
                </table>
                    
                    
                    
                </div>
            </div>	
        </div>

        <div class="modal2-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>
 <!-- MODAL KEMASAN -->

<div id="modal3" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>KEMASAN</b></h3>
        </div>
        <div class="modal-body">
            
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                    <thead>
                        <tr class="bg-success">
                                            <th scope="col">Jumlah</th>
                                            <th scope="col">Kode Jenis</th>
                                            <th scope="col">Uraian</th>
                                            <th scope="col">Merk Kemasan</th>
                                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                           <?php
                                        $n = 0;
                                        if ($ptb_kemasan) {
                                            foreach ($ptb_kemasan as $data) {
                                                $n = $n + 1;
                                                ?>
                                            <td width="30%"><?php echo $data->JUMLAH_KEMASAN; ?></td>
                                            <td width="30%"><?php echo $data->KODE_JENIS_KEMASAN; ?></td>
                                            <td width="30%"><?php echo $data->URAIAN_KEMASAN; ?></td>
                                            <td width="30%"><?php echo $data->MERK_KEMASAN; ?></td>
                                            <?php
                                        }
                                    }
                                    ?>
                        </tr>	    
                    
                    </tbody>
                </table>
                    
                    
                    
                </div>
            </div>	
        </div>

        <div class="modal2-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>
 
  <!-- MODAL DOKUMEN -->

<div id="modal4" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>DOKUMEN</b></h3>
        </div>
        <div class="modal-body">
            
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                    <thead>
                        <tr class="bg-success">
                                            <th scope="col">Kode Dokumen</th>
                                            <th scope="col">Jenis Dokumen</th>
                                            <th scope="col">Nomor</th>
                                            <th scope="col">Tanggal</th>
                                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                           <?php
                                                $n = 0;
                                                if ($ptb_other) {
                                                    foreach ($ptb_other as $data) {
                                                        $n = $n + 1;
                                                        ?>
                                                    <td width="30%"><?php echo $data->kode_jenis_dokumen; ?></td>
                                                    <td width="30%"><?php echo $data->uraian_dokumen; ?></td>
                                                    <td width="30%"><?php echo $data->nomor_dokumen; ?></td>
                                                    <td width="30%"><?php echo $data->tanggal_dokumen; ?></td></tr>
                                                    <?php
                                                }
                                            }
                                            ?>	
                        </tr>	    
                    
                    </tbody>
                </table>
                    
                    
                    
                </div>
            </div>	
        </div>

        <div class="modal2-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>
  <div id="modal5" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>JAMINAN</b></h3>
        </div>
        <div class="modal-body">
            
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                    <thead>
                        <tr class="bg-success">
                                            <th scope="col">Jenis</th>
                                            <th scope="col">Nomor</th>
                                            <th scope="col">Tanggal</th>
                                            <th scope="col">Nilai</th>
                                            <th scope="col">Jatuh Tempo</th>
                                            <th scope="col">Penjamin</th>
                                            <th scope="col">Nomor BPJ</th>
                                            <th scope="col">Tanggal BPJ</th>
                                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <tr> <?php
                            $n = 0;
                            if ($ptb_jaminan) {
                                foreach ($ptb_jaminan as $data) {
                                    $n = $n + 1;
                                    ?>
                                <td width="30%"><?php echo $n; ?></td>
                                    
                                    <td width="30%"><input value = "<?php echo $data->URAIAN_JENIS_JAMINAN; ?>"></input></td>
                                    <td width="30%"><input value="<?php echo $data->NOMOR_JAMINAN; ?>"></input></td>
                                    <td width="30%"><input value ="<?php echo $data->TANGGAL_JAMINAN; ?>"></input></td>
                                    <td width="30%"><input value = "<?php echo $data->NILAI_JAMINAN; ?>"></input></td>
                                    <td width="30%"><input value="<?php echo $data->TANGGAL_JATUH_TEMPO; ?>"></input></td>
                                    <td width="30%"><input value ="<?php echo $data->PENJAMIN; ?>"></input></td>
                                    <td width="30%"><input value = "<?php echo $data->NOMOR_BPJ; ?>"></input></td>
                                           
        <?php
    }
}
?>		
                        </tr>	    
                    
                    </tbody>
                </table>
                    
                    
                    
                </div>
            </div>	
        </div>

        <div class="modal2-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>

</div>
</div>
<!-- TUTUP CONTAINER -->
