<h3>Master Kode Kas Bank <b><?=$tipe?></b></h3>
<div class="portlet light bordered">
  <div class="portlet-title">


    <form role="form" method="post">
        <div class="form-body">
          <div class="row">
            <div class="col-sm-4">
                <label>Nama</label>
                <input type="text" class="form-control" placeholder="Nama Kas / Bank" id="nama" value="<?=isset($default['nama'])? $default['nama'] : ""?>" name="nama_item" required>
            </div>
            <div class="col-sm-4">
                <label>Kode</label>
                <input type="text" class="form-control" placeholder="Kode" id="kode" value="<?=isset($default['kode_bank'])? $default['kode_bank'] : ""?>" name="kode" required>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Akun Kas Bank</label>
                    <div class="input-group">
                       <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="akun" required>
                         <option value="<?=isset($default['coa_id'])? $default['coa_id'] : ""?>"><?=isset($default['ncoa'])? $default['ncoa'] : ""?></option>
                         <?php 	foreach($list_coa_cashbank as $row){ ?>
                           <option value="<?php echo $row['coa_id'];?>"><?php echo $row['nama'];?></option>
                         <?php } ?>
                       </select>
                 </div>
               </div>
            </div>
            </div>
            <div class="row">
              <div class="col-sm-8">
                  <label>Transaksi In / Out</label>
                  <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="inout" required>
                    <?php if ($tipe == "Ubah"){if ($default['in_out']){$txt = "TRANSAKSI IN";}else{$txt = "TRANSAKSI OUT";} }?>
                    <option value="<?=isset($default['in_out'])? $default['in_out'] : ""?>"><?=isset($txt)? $txt : ""?></option>
                    <option value="1">TRANSAKSI IN</option>
                    <option value="0">TRANSAKSI OUT</option>
                  </select>
              </div>
              <div class="col-sm-8"></div>
          </div>
        </div>
        <br />
        <div class="form-actions">
            <a href='<?php echo base_url('Cashbank_controller/index_kode_cashbank');?>' class='btn default'> Cancel</a>
            <button type="submit" class="btn blue" name="submit_kode_cashbank">Submit</button>
        </div>
      </form>

      <hr />
      <br />
      <div class="tools"> </div>
</div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th><center>Nama</center></th>
              <th><center>Coa</center></th>
              <th><center>Kode</center></th>
              <th><center>In / Out</center></th>
              <th><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_kode_cashbank as $row){ ?>
            <tr>
                <td><?php echo $row['nama'];?></td>
                <td><?php echo $row['ncoa'];?></td>
                <td><?php echo $row['kode_bank'];?></td>
                <td><?php if ($row['in_out'] == 1){echo "IN";}else{echo "OUT";} ?></td>
                <td><a href='<?php echo base_url('Cashbank_controller/Edit_kode_cashbank/'.$row['kode_trans_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i></a><a href="javascript:dialogHapus('<?php echo base_url('Cashbank_controller/delete_kode_cashbank/'.$row['kode_trans_id'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i></a></td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
