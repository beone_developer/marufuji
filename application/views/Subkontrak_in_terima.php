<h3>Subkontrak In</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>


  <form role="form" method="post">
      <div class="form-body">
            <div class="row">
                <div class="col-sm-4">
                <div class="form-group">
                      <label>Tgl Penerima</label>
                      <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-calendar"></i>
                        </span>
                        <input class="form-control form-control-inline input-medium date-picker input-circle-right" size="16" type="text" name="tgl_penerimaan" value="<?php echo date('m/d/Y');?>" readonly required/>
                        <span class="help-block"></span>
                      </div>
                </div>
              </div>
              <div class="col-sm-4">
                    <div class="form-group">
                      <label>Nomor Subkontrak In</label>
                      <input type="text" class="form-control" placeholder="Nomor Subkontrak In" name="nomor_sk_in" required>
                    </div>
                </div>
              <div class="col-sm-4">
                    <div class="form-group">
                        <label>Nomor Subkontrak Out</label>
                        <div class="input-group">
                           <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="nomor_sk_out" required>
                              <option value=""></option>
                             <?php 	foreach($list_subkontrak_out as $row){ ?>
                               <option value="<?php echo $row['subkon_out_id'];?>"><?php echo $row['no_subkon_out'];?></option>
                             <?php } ?>
                           </select>
                     </div>
                   </div>
                </div>
            </div>

            <div class="row">
              <div class="col-sm-4">
                    <div class="form-group">
                    <label>Supplier TPB</label>
                      <input type='text' class='form-control' name='supplier_tpb' id='supplier_tpb' value="<?php echo $supplier['nama_pengirim'];?>" required readonly>
                    </div>
                </div>
                <div class="col-sm-4">
                      <div class="form-group">
                        <label>Supplier</label>
                        <div class="input-group">
                           <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="supplier" required>
                              <option value=""></option>
                             <?php 	foreach($list_supplier as $row){ ?>
                               <option value='<?php echo $row['custsup_id'];?>'><?php echo $row['nama'];?></opiton>
                             <?php } ?>
                           </select>
                     </div>
                      </div>
                  </div>
            </div>

            <div class="row">
              <div class="col-sm-4">
                    <div class="form-group">
                      <label>Biaya Subkontrak</label>
                      <input type="text" class="form-control" placeholder="Biaya Subkontrak" name="biaya_subkontrak" id="biaya_subkontrak" required>
                    </div>
                </div>
                <div class="col-sm-4">
                      <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" class="form-control" placeholder="Keterangan" name="keterangan" required>
                      </div>
                  </div>
            </div>

            <hr />

            <h4>List Barang Penerimaan</h4>
            <?php
              $no = 0;
              $harga_invoice = 0;
              /********************************/
              $list_item = $this->db->query("SELECT * FROM public.beone_item");
              /********************************/

              foreach($item as $row){
              $no = $no + 1;
            ?>

            <div class="row">
              <div class="col-sm-4">
                    <div class="form-group">
                    <label>Item TPB</label>
                      <input type='text' class='form-control' name='item_tpb' id='item_tpb' value="<?php echo $row['uraian'];?>" required readonly>
                    </div>
                </div>
                <div class="col-sm-2">
                      <div class="form-group">
                      <label>Qty</label>
                        <input type='text' class='form-control' name='<?php echo "qty_".$no;?>' id='qty' value="<?php echo $row['jumlah_satuan'];?>" required readonly>
                      </div>
                  </div>
                <div class="col-sm-6">
                      <div class="form-group">
                        <label>Item</label>
                        <div class="input-group">
                          <select id='item' class='form-control input-sm select2-multiple' name='<?php echo "item_".$no;?>' required>
                            <option value=""></option>
                            <?php  foreach($list_item->result_array() as $row2){?>
                                <option value="<?php echo $row2['item_id'];?>"><?php echo $row2['nama']." (".$row2['item_code'].")";?></option>
                            <?php
                              }
                            ?>
                          </select>
                     </div>
                      </div>
                  </div>
            </div>
            <?php
              }
            ?>
            <input type='hidden' class='form-control' name='jml_item' id='jml_item' value="<?php echo $no;?>" required readonly>
            <input type='hidden' class='form-control' name='nomor_aju' id='nomor_aju' value="<?php echo $nomor_aju['nomor_aju'];?>" required readonly>
            <input type='hidden' class='form-control' name='harga_invoice' id='harga_invoice' value="<?php echo $nomor_aju['harga_invoice'];?>" required readonly>
      </div>
      <div class="form-actions">
          <a href='<?php echo base_url('Subkontrak_controller/Subkontrak_out');?>' class='btn default'><i class="fa fa-arrow-circle-o-left"></i> Back</a>
          <button type="submit" class="btn red" name="submit_in">Submit</button>
      </div>
  </form>



  <script type="text/javascript">
  var biaya_subkontrak = document.getElementById('biaya_subkontrak');
    biaya_subkontrak.addEventListener('keyup', function(e){
    biaya_subkontrak.value = formatRupiah(this.value, 'Rp. ');
  });


  var kurs = document.getElementById('kurs');
    kurs.addEventListener('keyup', function(e){
    kurs.value = formatRupiah(this.value, 'Rp. ');
  });

  /* Fungsi formatRupiah */
  function formatRupiah(angka, prefix){
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split   		= number_string.split(','),
    sisa     		= split[0].length % 3,
    rupiah     		= split[0].substr(0, sisa),
    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
  }
  </script>
