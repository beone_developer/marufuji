<h3>Master Periode Fix Asset <b><?=$tipe?></b></h3>
<div class="portlet light bordered">
  <div class="portlet-title">


    <form role="form" method="post">
        <div class="form-body">
          <div class="row">
            <div class="col-sm-4">
                <label>Nama Periode</label>
                <input type="text" class="form-control" placeholder="Nama Periode" id="periode" value="<?=isset($default['nama_periode'])? $default['nama_periode'] : ""?>" name="periode" required>
            </div>
            <div class="col-sm-4"></div>
            </div>
            </div>
        <br />
        <div class="form-actions">
            <a href='<?php echo base_url('Fixasset_controller');?>' class='btn default'> Cancel</a>
            <button type="submit" class="btn blue" name="submit_periode">Submit</button>
        </div>
      </form>

      <hr />
      <br />

      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th><center>Nama Periode</center></th>
              <th><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_periode as $row){ ?>
            <tr>
                <td><?php echo $row['nama_periode'];?></td>
                <td>
                    <a href='<?php echo base_url('Fixasset_controller/Fixed_asset_akm/'.$row['periode_id'].'');?>' class='btn yellow'><i class="fa fa-eye"></i></a>
                    <a href='<?php echo base_url('Fixasset_controller/Edit_periode/'.$row['periode_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i></a>
                    <a href="javascript:dialogHapus('<?php echo base_url('Fixasset_controller/delete_periode/'.$row['periode_id'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
