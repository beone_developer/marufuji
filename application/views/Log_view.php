<h3>Data Log</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
							<th><center>No</center></th>
              <th><center>User</center></th>
              <th><center>Waktu</center></th>
              <th><center>Tindakan</center></th>
          </tr>
        </thead>
        <tbody>
          <?php
					 		$no = 0;
							foreach($list_log as $row){
							$no = $no + 1;
					?>
            <tr>
								<td><?php echo $no;?></td>
                <td><?php echo $row['log_user'];?></td>
                <td><?php echo $row['log_time'];?></td>
                <td><?php echo $row['log_desc'];?></td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>
