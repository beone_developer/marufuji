<div class="row">
	<div class="col-sm-3">
		<a href="<?php echo base_url('Bc27_detail_controller/form/'.$default['ID_HEADER'].'');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Detail Barang</a>
	</div>
	<div style="margin-left: -176px;" class="col-sm-3">
		<a href="#" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Penggunaan Bahan Baku Impor</a>
	</div>
	<!-- <div style="margin-left: -75px;" class="col-sm-3">
		<a href="<?php echo base_url('Bc27_detail_penggunaan_bb_lokal_controller');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Penggunaan Bahan Baku Lokal</a>
	</div>	 -->
</div>
<br>
<a href="<?php echo base_url('Bc27_detail_penggunaan_bb_impor_controller/prev/'.$default['ID_HEADER'].'/'.$default['ID'].'/'.$default['ID_BARANG'].'');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">prev</a>
<a href="<?php echo base_url('Bc27_detail_penggunaan_bb_impor_controller/next/'.$default['ID_HEADER'].'/'.$default['ID'].'/'.$default['ID_BARANG'].'');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">next</a>

<br>

<div class="portlet light bordered">
	<div class="portlet-title">
		
		<form role="form" method="post">
			<input type="hidden" class="form-control" id="ID" 
						name="ID" value="<?=isset($default['ID'])? $default['ID'] : ""?>">
			<div class="row">
				<label for="status" class="col-sm-2 col-form-label">Status</label>
				<div class="col-sm-2">
					<input style="border: none; margin-top: -5px;" type="text" readonly class="form-control" id="status" name="status" value="<?=isset($default[''])? $default[''] : ""?>">
				</div>
			</div>
			<br>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<label style="margin-top: 5px;">Detail Ke</label>
					</div>

					<div class="col-sm-1">
						<input type="text" readonly class="form-control" id="status" 
						name="status" value="<?=isset($default[''])? $default[''] : ""?>">
					</div>

					<div class="col-sm-1">
						<label style="margin-top: 5px;">Dari</label>
					</div>

					<div class="col-sm-1">
						<input type="text" readonly class="form-control" id="status" 
						name="status" value="<?=isset($default[''])? $default[''] : ""?>">
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-1">
						<label style="margin-top: 5px;">Bahan Baku Ke</label>
					</div>

					<div class="col-sm-1">
						<input type="text" readonly class="form-control" id="status" 
						name="status" value="<?=isset($default[''])? $default[''] : ""?>">
					</div>

					<div class="col-sm-1">
						<label style="margin-top: 5px;">Dari</label>
					</div>
					<div class="col-sm-1">
						<input type="text" readonly class="form-control" id="status" 
						name="status" value="<?=isset($default[''])? $default[''] : ""?>">
					</div>
				</div>
			</div>
		
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label>Dok Asal</label>
						<input type="text" class="form-control" id="dok_asal" 
						name="dok_asal" value="<?=isset($default['KODE_JENIS_DOK_ASAL'])? $default['KODE_JENIS_DOK_ASAL'] : ""?>">
					</div>
					<div class="col-sm-3">
						<input style="margin-top: 24px; border: none;" type="text" readonly class="form-control" id="status" 
						name="status" value="<?=isset($default[''])? $default[''] : ""?>">
					</div>

					<div class="col-sm-3">
						<label>KPPBC Dok</label>
						<input type="text" class="form-control" id="kppbc_dok" 
						name="kppbc_dok" value="<?=isset($default['KODE_KANTOR'])? $default['KODE_KANTOR'] : ""?>">
					</div>
					<div class="col-sm-3">
						<input style="margin-top: 24px; border: none;" type="text" readonly class="form-control" id="status" 
						name="status" value="<?=isset($default[''])? $default[''] : ""?>">
					</div>
					
				</div>
			</div>
				
			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label>No / Tanggal Dok</label>
						<input type="text" class="form-control" id="nomor_daftar" 
						name="nomor_daftar" value="<?=isset($default['NOMOR_DAFTAR_DOK_ASAL'])? $default['NOMOR_DAFTAR_DOK_ASAL'] : ""?>" placeholder="No">
					</div>
					<div class="col-sm-2">
						<input style="margin-top: 24px;" type="text" readonly class="form-control" id="tgl_daftar" name="tgl_daftar" value="<?=isset($default['TANGGAL_DAFTAR_DOK_ASAL'])? $default['TANGGAL_DAFTAR_DOK_ASAL'] : ""?>" placeholder="Tgl Dok">
					</div>

					<div class="col-sm-5"> 
						<label>No Aju</label>
						<input type="text" class="form-control" id="nomor_aju" 
						name="nomor_aju" value="<?=isset($default['NOMOR_AJU_DOK_ASAL'])? $default['NOMOR_AJU_DOK_ASAL'] : ""?>">
					</div>
					<div class="col-sm-3">
						<label>Urut Ke</label>
						<input type="text" readonly class="form-control" id="seri_barang" 
						name="seri_barang" value="<?=isset($default['SERI_BARANG_DOK_ASAL'])? $default['SERI_BARANG_DOK_ASAL'] : ""?>">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-6">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
					<label>Kode Barang</label>
					<input type="text" class="form-control" name="kode" value="<?=isset($default['KODE_BARANG'])? $default['KODE_BARANG'] : ""?>">
				</div>
				<div class="col-sm-4">
					<label>Nomor HS</label>
					<input type="text" class="form-control" name="nomor_hs" value="<?=isset($default['POS_TARIF'])? $default['POS_TARIF'] : ""?>">
				</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<label>Uraian Barang</label>
						<input type="text" readonly class="form-control" id="uraian_barang" 
						name="uraian_barang" value="<?=isset($default['URAIAN'])? $default['URAIAN'] : ""?>">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
				<div class="col-sm-3">
					<label>Merk</label>
					<input type="text" class="form-control" name="merk" value="<?=isset($default['MERK'])? $default['MERK'] : ""?>">
				</div>
				<div class="col-sm-3">
					<label>Tipe</label>
					<input type="text" class="form-control" name="tipe" value="<?=isset($default['TIPE'])? $default['TIPE'] : ""?>">
				</div>
				<div class="col-sm-3">
					<label>Ukuran</label>
					<input type="text" class="form-control" name="ukuran" value="<?=isset($default['UKURAN'])? $default['UKURAN'] : ""?>">
				</div>
				<div class="col-sm-3">
					<label>Spf Lain</label>
					<input type="text" class="form-control" name="spf_lain" value="<?=isset($default['SPESIFIKASI_LAIN'])? $default['SPESIFIKASI_LAIN'] : ""?>">
				</div>
				</div>
			</div>
			<br>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-6">
						<label><b>HARGA & SATUAN</b></label>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label>Harga CIF</label>
						<input type="text"  class="form-control" id="cif_usd" name="cif_usd" value="<?=isset($default['CIF'])? $default['CIF'] : ""?>">
					</div>
					<div class="col-sm-4">
						<label>Jumlah Satuan</label>
						<input type="text"  class="form-control" id="jumlah_satuan" name="jumlah_satuan" value="<?=isset($default['JUMLAH_SATUAN'])? $default['JUMLAH_SATUAN'] : ""?>">
					</div>
					<div class="col-sm-2">
						<label>Jenis Satuan</label>
						<input type="text"  class="form-control" id="satuan" name="satuan" value="<?=isset($default['JENIS_SATUAN'])? $default['JENIS_SATUAN'] : ""?>">
					</div>
					<div class="col-sm-2">
						<input style="margin-top: 24px; border: none;" type="text" class="form-control" id="status" name="status" value="">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label>Harga Penyerahan</label>
						<input type="text"  class="form-control" id="harga_penyerahan" name="harga_penyerahan" value="<?=isset($default['HARGA_PENYERAHAN'])? $default['HARGA_PENYERAHAN'] : ""?>">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-6">
						<p><b>NILAI BM</b></p>
					</div>
				</div>
			</div>
			
			<div class="form-body">
				<div class="row">
					<!-- FORM LEFT SIDE -->
					<div class="col-sm-6">
						<div class="form-group">
							<div class="row">
								<div class="col-sm-4">
									<label>BM Bahan Baku</label>
									<input type="text" class="form-control" name="kode_tarif_bm" value="<?=isset($default_tarifBM['KODE_TARIF'])? $default_tarifBM['KODE_TARIF'] : ""?>">
								</div>

								<div class="col-sm-4">
									<input style="margin-top: 24px" type="text" class="form-control" name="tarif_bm" value="<?=isset($default_tarifBM['TARIF'])? $default_tarifBM['TARIF'] : ""?>">
								</div>
								<div class="col-sm-4">
									<label style="margin-top: 24px; font-size: 15px;">%</label>
								</div>

							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-sm-4">
									<input type="text" class="form-control" name="kode_fasilitas_bm" value="<?=isset($default_tarifBM['KODE_FASILITAS'])? $default_tarifBM['KODE_FASILITAS'] : ""?>">
								</div>

								<div class="col-sm-4">
									<input type="text" class="form-control" name="tarif_fasilitas_bm" value="<?=isset($default_tarifBM['TARIF_FASILITAS'])? $default_tarifBM['TARIF_FASILITAS'] : ""?>">
								</div>
								<div class="col-sm-4">
									<label style="margin-top: 7px; font-size: 15px;">%</label>
								</div>

							</div>
						</div>
						
						<div class="form-group">
							<div class="row">
								<div class="col-sm-3">
									<label>PPN</label>
									<input type="text" class="form-control" name="tarif_ppn" value="<?=isset($default_tarifPPN['TARIF'])? $default_tarifPPN['TARIF'] : ""?>">
								</div>
								
								<div class="col-sm-1">
									<p style="margin-top: 30px; font-size: 15px;">%</p>
								</div>

								<div class="col-sm-4">
								<input style="margin-top: 25px;" type="text" class="form-control" name="kode_fasilitas_ppn" value="<?=isset($default_tarifPPN['KODE_FASILITAS'])? $default_tarifPPN['KODE_FASILITAS'] : ""?>">
							</div>

							<div class="col-sm-3">
								<input style="margin-top: 25px;" type="text" class="form-control" name="tarif_fasilitas_ppn" value="<?=isset($default_tarifPPN['TARIF_FASILITAS'])? $default_tarifPPN['TARIF_FASILITAS'] : ""?>">
							</div>
								
								<div class="col-sm-1">
									<p style="margin-top: 30px; font-size: 15px;">%</p>
								</div>
							</div>
						</div>
						
						<div style="margin-top: -10px" class="form-group">
							<div class="row">
								<div class="col-sm-3">
									<label>PPnBM</label>
									<input type="text" class="form-control" name="tarif_ppnbm" value="<?=isset($default_tarifPPNBM['TARIF'])? $default_tarifPPNBM['TARIF'] : ""?>">
								</div>
								
								<div class="col-sm-1">
									<p style="margin-top: 30px; font-size: 15px;">%</p>
								</div>

								<div class="col-sm-4">
								<input style="margin-top: 25px;" type="text" class="form-control" name="kode_fasilitas_ppnbm" value="<?=isset($default_tarifPPNBM['KODE_FASILITAS'])? $default_tarifPPNBM['KODE_FASILITAS'] : ""?>">
							</div>

							<div class="col-sm-3">
								<input style="margin-top: 25px;" type="text" class="form-control" name="tarif_fasilitas_ppnbm" value="<?=isset($default_tarifPPNBM['TARIF_FASILITAS'])? $default_tarifPPNBM['TARIF_FASILITAS'] : ""?>">
							</div>
								
								<div class="col-sm-1">
									<p style="margin-top: 30px; font-size: 15px;">%</p>
								</div>
							</div>
						</div>

						<div style="margin-top: -10px" class="form-group">
							<div class="row">
								<div class="col-sm-3">
									<label>PPh</label>
									<input type="text" class="form-control" name="tarif_pph" value="<?=isset($default_tarifPPH['TARIF'])? $default_tarifPPH['TARIF'] : ""?>">
								</div>
								
								<div class="col-sm-1">
									<p style="margin-top: 30px; font-size: 15px;">%</p>
								</div>

								<div class="col-sm-4">
								<input style="margin-top: 25px;" type="text" class="form-control" name="kode_fasilitas_pph" value="<?=isset($default_tarifPPH['KODE_FASILITAS'])? $default_tarifPPH['KODE_FASILITAS'] : ""?>">
							</div>

							<div class="col-sm-3">
								<input style="margin-top: 25px;" type="text" class="form-control" name="tarif_fasilitas_pph" value="<?=isset($default_tarifPPH['TARIF_FASILITAS'])? $default_tarifPPH['TARIF_FASILITAS'] : ""?>">
							</div>
								
								<div class="col-sm-1">
									<p style="margin-top: 30px; font-size: 15px;">%</p>
								</div>
							</div>
						</div>
						
						<div style="margin-top: -10px" class="form-group">
							<div class="row">
								<div class="col-sm-12">
									<label>Cukai</label>
									<input type="hidden" class="form-control" name="id_cukai" value="<?=isset($default_tarifcukai['ID'])? $default_tarifcukai['ID'] : ""?>">
								<input type="text" class="form-control" name="kode_komoditi_cukai" value="<?=isset($default_tarifcukai['KODE_KOMODITI_CUKAI'])? $default_tarifcukai['KODE_KOMODITI_CUKAI'] : ""?>">
								</div>
							</div>
						</div>
						
						<div style="margin-top: -20px" class="form-group">
							<div class="row">
								<div class="col-sm-4">
								<input style="margin-top: 30px;" type="text" class="form-control" name="kode_tarif_cukai" value="<?=isset($default_tarifcukai['KODE_TARIF'])? $default_tarifcukai['KODE_TARIF'] : ""?>">
								</div>

								<div class="col-sm-4">
								<input style="margin-top: 30px;" type="text" class="form-control" name="tarif_cukai" value="<?=isset($default_tarifcukai['TARIF'])? $default_tarifcukai['TARIF'] : ""?>">
								</div>

								<div class="col-sm-1">
									<p style="margin-top: 33px; font-size: 18px;">/</p>
								</div>

								<div class="col-sm-3">
									<input style="margin-top: 30px;" type="text" class="form-control" name="kode_satuan_cukai" value="<?=isset($default_tarifcukai['KODE_SATUAN'])? $default_tarifcukai['KODE_SATUAN'] : ""?>">
								</div>
							</div>
						</div>

						<div style="margin-top: -10px" class="form-group">
							<div class="row">
								<div class="col-sm-4">
									<label>Jumlah Satuan</label>
									<input type="text" class="form-control" name="jumlah_satuan_cukai" value="<?=isset($default_tarifcukai['JUMLAH_SATUAN'])? $default_tarifcukai['JUMLAH_SATUAN'] : ""?>">
								</div>

								<div class="col-sm-4">
								<input style="margin-top: 25px;" type="text" class="form-control" name="kode_fasilitas_cukai" value="<?=isset($default_tarifcukai['KODE_FASILITAS'])? $default_tarifcukai['KODE_FASILITAS'] : ""?>">
								</div>

								<div class="col-sm-3">
								<input style="margin-top: 25px;" type="text" class="form-control" name="tarif_fasilitas_cukai" value="<?=isset($default_tarifcukai['TARIF_FASILITAS'])? $default_tarifcukai['TARIF_FASILITAS'] : ""?>">
								</div>

								<div class="col-sm-1">
									<p style="margin-top: 30px; font-size: 15px;">%</p>
								</div>
							</div>
						</div>
						

					</div>
					<!-- TUTUP FORM LEFT SIDE -->
					
					
					<!-- TUTUP FORM RIGHT SIDE -->
				</div>
			</div>
			<div class="form-actions">
            <!-- <a href='<?php echo base_url('Bc27_detail_penggunaan_bb_impor_controller');?>' class='btn default'> Cancel</a> -->
            <button type="submit" class="btn blue" name="submit_bbimpor">Simpan</button>
        </div>
		</form>

	</div>
</div>