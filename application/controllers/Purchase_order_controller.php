<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_order_controller extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Purchase_order_model');
		$this->load->model('Custsup_model');
		$this->load->view('Header');

		$data['list_supplier'] = $this->Custsup_model->load_supplier();
		if (isset($_POST['submit_purchase'])) {
			$this->Purchase_order_model->simpan($_POST);
			redirect("Purchase_order_controller");
		}

		$this->load->view('Purchase_order_form_view', $data);
		$this->load->view('Footer');
	}

	public function index_purchase_order()
	{
		$this->load->model('Purchase_order_model');
		$this->load->view('Header');

		$data['list_purchase_header'] = $this->Purchase_order_model->load_purchase_header();

		$this->load->view('Purchase_order_list_view', $data);
		$this->load->view('Footer');
	}


	public function edit($purchase_id)
	{
		$this->load->model('Item_model');
		$this->load->model('Purchase_order_model');
		$this->load->view('Header');

		$data['list_item'] = $this->Item_model->load_item();
		$data['default'] = $this->Purchase_order_model->get_default_header($purchase_id);
		$data['default_detail'] = $this->Purchase_order_model->get_default_detail($purchase_id);

		if (isset($_POST['submit_purchase'])) {
			$this->Purchase_order_model->update($_POST, $purchase_id);
			redirect("Purchase_order_controller/index_purchase_order");
		}

		$this->load->view('Purchase_form_edit_view', $data);
		$this->load->view('Footer');
	}

	public function realisasi($purchase_id)
	{
		$this->load->model("Purchase_order_model");
		$this->Purchase_order_model->realisasi($purchase_id);
		redirect("Purchase_order_controller/index_purchase_order");
	}

	public function delete($purchase_id, $purchase_no)
	{
		$this->load->model("Purchase_order_model");
		$this->Purchase_order_model->delete($purchase_id, $purchase_no);
		redirect("Purchase_order_controller/index_purchase_order");
	}

	public function purchase_order_print($purchase_id)
	{
		$this->load->library('pdfgenerator');
		$this->load->model('Purchase_order_model');

		$data['default'] = $this->Purchase_order_model->get_default_header($purchase_id);
		$data['default_detail'] = $this->Purchase_order_model->get_default_detail($purchase_id);

		// $new_array = array();
		// for ($x = 0; $x <= 14; $x++) {
		// 	$new_array[] = array('kode_barang'=>'B001','ket'=>'Perabotan rumah','jumlah'=>'10','sat'=>'Pcs','harga'=>'1.000.000,-','total'=>'10.000.000,-');   
		// }
		// $data['users']=$new_array;

		$html = $this->load->view('Rpt_purchase_order_print', $data, true);

		$this->pdfgenerator->generate($html, 'Purchase Order');
	}

}
