<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Sales_model');
		$this->load->model('Custsup_model');
		$this->load->view('Header');

		$data['list_customer'] = $this->Custsup_model->load_receiver();
		if(isset($_POST['submit_sales'])){
			$this->Sales_model->simpan($_POST);
			redirect("Sales_controller");
		}

		$this->load->view('Sales_form_view', $data);
		$this->load->view('Footer');
	}


	public function index_sales()
	{
		$this->load->model('Sales_model');
		$this->load->view('Header');

		$data['list_sales_header'] = $this->Sales_model->load_sales_header();

		$this->load->view('Sales_list_view', $data);
		$this->load->view('Footer');
	}

	public function edit($sales_id)
	{
		$this->load->model('Item_model');
		$this->load->model('Sales_model');
		$this->load->view('Header');

		$data['list_item'] = $this->Item_model->load_item();
		$data['default'] = $this->Sales_model->get_default_header($sales_id);
		$data['default_detail'] = $this->Sales_model->get_default_detail($sales_id);

		if(isset($_POST['submit_sales'])){
			$this->Sales_model->update($_POST, $sales_id);
			redirect("Sales_controller/index_sales");
		}

		$this->load->view('Sales_form_edit_view', $data);
		$this->load->view('Footer');
	}

	public function delete($sales_id, $sales_no){
		$this->load->model("Sales_model");
		$this->Sales_model->delete($sales_id, $sales_no);
		redirect("Sales_controller/index_sales");
	}


}
