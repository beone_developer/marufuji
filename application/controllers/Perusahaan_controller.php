<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perusahaan_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Perusahaan_model');
		$this->load->view('Header');

		$data['data_perusahaan'] = $this->Perusahaan_model->load_data_perusahaan();

		if(isset($_POST['submit_perusahaan'])){
			$this->Perusahaan_model->simpan($_POST);
			redirect("Perusahaan_controller");
		}

		$this->load->view('Perusahaan_view',$data);
		$this->load->view('Footer');
	}

}
