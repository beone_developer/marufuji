<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subkontrak_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Gudang_model');
		$this->load->model('Custsup_model');
		$this->load->view('Header');

		$data['list_supplier'] = $this->Custsup_model->load_supplier();

		if(isset($_POST['submit_subkontrak'])){
			$this->Gudang_model->simpan_pindah_gudang_subkontrak($_POST);
			redirect("Subkontrak_controller");
		}

		$this->load->view('Subkon_form_view', $data);
		$this->load->view('Footer');
	}

	public function Subkontrak_out()
	{
		$this->load->model('Subkontrak_model');
		$this->load->view('Header');
		$data['list_import_header'] = $this->Subkontrak_model->load_import_header_subkon_out();
		$this->load->view('Subkontrak_out', $data);
		$this->load->view('Footer');
	}

	public function Subkontrak_in()
	{
		$this->load->model('Subkontrak_model');
		$this->load->view('Header');
		$data['list_import_header'] = $this->Subkontrak_model->load_import_header_subkon_in();
		$this->load->view('Subkontrak_in', $data);
		$this->load->view('Footer');
	}

	public function Kirim($import_header_id)
	{
		$this->load->model('Inventin_model');
		$this->load->model('Custsup_model');
		$this->load->model('Subkontrak_model');
		$this->load->view('Header');

		$data['kurs'] = $this->Inventin_model->get_kurs($import_header_id);
		$data['supplier'] = $this->Inventin_model->get_penerima($import_header_id);
		$data['item'] = $this->Inventin_model->get_item($import_header_id);
		$data['nomor_aju'] = $this->Inventin_model->get_nomor_aju($import_header_id);
		$data['list_supplier'] = $this->Custsup_model->load_supplier();

		if(isset($_POST['submit_out'])){
			$this->Subkontrak_model->kirim($_POST, $import_header_id);
			redirect("Subkontrak_controller/Subkontrak_out");
		}

		$this->load->view('Subkontrak_out_kirim', $data);
		$this->load->view('Footer');
	}

	public function Terima($import_header_id)
	{
		$this->load->model('Inventin_model');
		$this->load->model('Custsup_model');
		$this->load->model('Subkontrak_model');
		$this->load->view('Header');

		$data['supplier'] = $this->Inventin_model->get_pengirim($import_header_id);
		$data['item'] = $this->Inventin_model->get_item($import_header_id);
		$data['nomor_aju'] = $this->Inventin_model->get_nomor_aju($import_header_id);
		$data['list_supplier'] = $this->Custsup_model->load_supplier();
		$data['list_subkontrak_out'] = $this->Subkontrak_model->load_subkontrak_out();

		if(isset($_POST['submit_in'])){
			$this->Subkontrak_model->terima($_POST, $import_header_id);
			redirect("Subkontrak_controller/Subkontrak_in");
		}

		$this->load->view('Subkontrak_in_terima', $data);
		$this->load->view('Footer');
	}

	public function Subkontrak_out_list()
	{
		$this->load->view('Header');
		$this->load->view('Subkontrak_out_list');
		$this->load->view('Footer');
	}

}
