<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gudang_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Gudang_model');
		$this->load->view('Header');

		$data['list_gudang'] = $this->Gudang_model->load_gudang();
		$data['tipe'] = "Tambah";

		/*if(isset($_POST['submit_gudang'])){
			$this->Gudang_model->simpan($_POST);
			redirect("Gudang_controller");
		}*/

		if(isset($_POST['preview'])){
			redirect("Gudang_controller/import");
		}

		$this->load->view('Gudang_view',$data);
		$this->load->view('Footer');
	}

	public function Edit($gudang_id)
	{
		$this->load->model('Gudang_model');
		$this->load->view('Header');

		$data['list_gudang'] = $this->Gudang_model->load_gudang();
		$data['default'] = $this->Gudang_model->get_default($gudang_id);
		$data['tipe'] = "Ubah";

		if(isset($_POST['submit_gudang'])){
			$this->Gudang_model->update($_POST, $gudang_id);
			redirect("Gudang_controller");
		}

		$this->load->view('Gudang_view',$data);
		$this->load->view('Footer');
	}

	public function delete($gudang_id){
		$this->load->model("Gudang_model");
		$this->Gudang_model->delete($gudang_id);
		redirect("Gudang_controller");
	}


	/***************************************************************************/
	public function import(){
		$this->load->model('Gudang_model');
		// Load plugin PHPExcel nya
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/'.$this->filename.'gudang_detail.xlsx'); // Load file yang telah diupload ke folder excel
		//$loadexcel = $excelreader->load('excel/format.xlsx');

		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();

		$numrow = 1;
		foreach($sheet as $row){
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					'gudang_detail_id'=>$row['A'],
					'gudang_id'=>$row['B'],
					'trans_date'=>$row['C'],
					'item_id'=>$row['D'],
					'qty_in'=>$row['E'],
					'qty_out'=>$row['F'],
					'nomor_transaksi'=>$row['G'],
					'update_by'=>$row['H'],
					'update_date'=>$row['I'],
					'flag'=>$row['J'],
					'keterangan'=>$row['K']
				));
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->Gudang_model->insert_multiple($data);

		redirect("Gudang_controller"); // Redirect ke halaman awal (ke controller fungsi index)
	}
	/**************************************************************************/

}
