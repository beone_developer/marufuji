<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bc_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('pdf');
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}

		 else{
             	$this->load->model('bc23_model');
             }
	}

	public function index() {


        $data['judul'] = 'Dokumen 2.3';
        $this->load->view('Header', $data);

        $data['ptb'] = $this->bc23_model->select();

        $this->load->view('bc23_tree_view', $data);
        $this->load->view('Footer');
    }

    public function bc23_form() {

        $data['judul'] = 'Dokumen 2.3';
        $this->load->view('Header', $data);

	    $data['ptb'] = $this->bc23_model->select();

        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }

    public function insert_form() {

        $data['judul'] = 'Insert dokumen 2.3';
        $this->load->view('Header', $data);

        $this->bc23_model->insert($_POST);
        redirect("Bc_controller");

        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }

    public function delete($id) {

        $data['judul'] = 'Delete 2.3';
        $this->load->view('Header', $data);
        $data['ptb'] = $this->bc23_model->delete($id);
        redirect("Bc_controller");
        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }

		public function daftar_respon() {
        $data['judul'] = 'Respon 2.3';
        $this->load->view('Header', $data);
//        $data['ptb'] = $this->bc23_model->daftar_respon($id);
        $this->load->view('bc23_respon', $data);
        $this->load->view('Footer');
    }

public function filter_pabean_pemasukan()
{
	$this->load->view('Header');

	$data['judul'] = 'Pabean Pemasukan';

	if(isset($_GET['submit_filter'])){
			redirect("Bc_controller/Rpt_pabean_pemasukan?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}
	$this->load->view('Report_filter_tanggal', $data);
	$this->load->view('Rpt_pabean_pemasukan_sekarang');
	$this->load->view('Footer');
}

public function Rpt_pabean_pemasukan()
{
	$this->load->view('Header');

	$data['judul'] = 'Pabean Pemasukan';

	if(isset($_GET['submit_filter'])){
			redirect("Bc_controller/Rpt_pabean_pemasukan?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}
	$this->load->view('Report_filter_tanggal', $data);
	$this->load->view('Rpt_pabean_pemasukan');
	$this->load->view('Footer');
}


public function filter_pabean_pengeluaran()
{
	$this->load->view('Header');

	$data['judul'] = 'Pabean Pengeluaran';

	if(isset($_GET['submit_filter'])){
			redirect("Bc_controller/Rpt_pabean_pengeluaran?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}
	$this->load->view('Report_filter_tanggal', $data);
	$this->load->view('Rpt_pabean_pengeluaran_sekarang');
	$this->load->view('Footer');
}

public function Rpt_pabean_pengeluaran()
{
	$this->load->view('Header');

	$data['judul'] = 'Pabean Pengeluaran';

	if(isset($_GET['submit_filter'])){
			redirect("Bc_controller/Rpt_pabean_pengeluaran?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}
	$this->load->view('Report_filter_tanggal', $data);
	$this->load->view('Rpt_pabean_pengeluaran');
	$this->load->view('Footer');
}

public function filter_mutasi_bahan_baku()
{
	$this->load->view('Header');

	$data['judul'] = 'Mutasi Bahan Baku';

	if(isset($_GET['submit_filter'])){
			redirect("Bc_controller/Rpt_mutasi_bahan_baku?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}
	$this->load->view('Report_filter_tanggal', $data);
	$this->load->view('Rpt_mutasi_bahan_baku_sekarang');
	$this->load->view('Footer');
}

public function Rpt_mutasi_bahan_baku()
{
	$this->load->view('Header');

	$data['judul'] = 'Mutasi Bahan Baku';

	if(isset($_GET['submit_filter'])){
			redirect("Bc_controller/Rpt_mutasi_bahan_baku?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}
	$this->load->view('Report_filter_tanggal', $data);
	$this->load->view('Rpt_mutasi_bahan_baku');
	$this->load->view('Footer');
}

public function filter_mutasi_barang_jadi()
{
	$this->load->view('Header');

	$data['judul'] = 'Mutasi Barang Jadi';

	if(isset($_GET['submit_filter'])){
			redirect("Bc_controller/Rpt_mutasi_barang_jadi?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}
	$this->load->view('Report_filter_tanggal', $data);
	$this->load->view('Rpt_mutasi_barang_jadi_sekarang');
	$this->load->view('Footer');
}

public function Rpt_mutasi_barang_jadi()
{
	$this->load->view('Header');
	$data['judul'] = 'Mutasi Barang Jadi';

	if(isset($_GET['submit_filter'])){
			redirect("Bc_controller/Rpt_mutasi_barang_jadi?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}
	$this->load->view('Report_filter_tanggal', $data);
	$this->load->view('Rpt_mutasi_barang_jadi');
	$this->load->view('Footer');
}

public function filter_mutasi_mesin()
{
	$this->load->view('Header');

	$data['judul'] = 'Mutasi Mesin';

	if(isset($_GET['submit_filter'])){
			redirect("Bc_controller/Rpt_mutasi_mesin?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}
	$this->load->view('Report_filter_tanggal', $data);
	$this->load->view('Rpt_mutasi_mesin_sekarang');
	$this->load->view('Footer');
}

public function Rpt_mutasi_mesin()
{
	$this->load->view('Header');

	$data['judul'] = 'Mutasi Mesin';

	if(isset($_GET['submit_filter'])){
			redirect("Bc_controller/Rpt_mutasi_mesin?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}
	$this->load->view('Report_filter_tanggal', $data);
	$this->load->view('Rpt_mutasi_mesin');
	$this->load->view('Footer');
}

public function filter_pabean_wip()
{
	$this->load->view('Header');

	$data['judul'] = 'Mutasi WIP';

	if(isset($_GET['submit_filter'])){
			redirect("Bc_controller/Rpt_pabean_wip?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}
	$this->load->view('Report_filter_tanggal', $data);
	$this->load->view('Rpt_mutasi_wip_sekarang');
	$this->load->view('Footer');
}

public function Rpt_pabean_wip()
{
	$this->load->view('Header');

	$data['judul'] = 'Mutasi WIP';

	if(isset($_GET['submit_filter'])){
			redirect("Bc_controller/Rpt_pabean_wip?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}
	$this->load->view('Report_filter_tanggal', $data);
	$this->load->view('Rpt_mutasi_wip');
	$this->load->view('Footer');
}


public function filter_pabean_mutasi_scrapt()
{
	$this->load->view('Header');

	$data['judul'] = 'Mutasi Scrapt';

	if(isset($_GET['submit_filter'])){
			redirect("Bc_controller/Rpt_pabean_mutasi_scrapt?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}
	$this->load->view('Report_filter_tanggal', $data);
	$this->load->view('Rpt_mutasi_scrapt_sekarang');
	$this->load->view('Footer');
}

public function Rpt_pabean_mutasi_scrapt()
{
	$this->load->view('Header');

	$data['judul'] = 'Mutasi Scrapt';

	if(isset($_GET['submit_filter'])){
			redirect("Bc_controller/Rpt_pabean_mutasi_scrapt?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}
	$this->load->view('Report_filter_tanggal', $data);
	$this->load->view('Rpt_mutasi_scrapt');
	$this->load->view('Footer');
}

public function Print_pabean_pemasukan()
{
	$this->load->view('Rpt_pabean_pemasukan_print');
}

public function Print_pabean_pengeluaran()
{
	$this->load->view('Rpt_pabean_pengeluaran_print');
}

public function Print_mutasi_wip()
{
	$this->load->view('Rpt_mutasi_wip_print');
}

public function Print_mutasi_bahan_baku()
{
	$this->load->view('Rpt_mutasi_bahan_baku_print');
}

public function Print_mutasi_scrapt()
{
	$this->load->view('Rpt_mutasi_scrapt_print');
}

public function Print_mutasi_barang_jadi()
{
	$this->load->view('Rpt_mutasi_barang_jadi_print');
}

public function Print_mutasi_mesin()
{
	$this->load->view('Rpt_mutasi_mesin_print');
}


}
