<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemakaian_wip_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Pemakaian_model');
		$this->load->view('Header');

		$data['list_pm'] = $this->Pemakaian_model->load_header_pm();

		if(isset($_POST['submit_pemakaian'])){
			$this->Pemakaian_model->simpan($_POST);
			redirect("Pemakaian_wip_controller");
		}

		$this->load->view('Pemakaian_wip_form_view', $data);
		$this->load->view('Footer');
	}

	public function get_nomor_pemakaian_wip(){
		$this->load->view('get_nomor_pemakaian_wip');
	}


}
