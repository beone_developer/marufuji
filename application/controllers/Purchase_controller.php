<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Purchase_model');
		$this->load->model('Custsup_model');
		$this->load->view('Header');

		$data['list_supplier'] = $this->Custsup_model->load_supplier();
		if(isset($_POST['submit_purchase'])){
			$this->Purchase_model->simpan($_POST);
			redirect("Purchase_controller");
		}

		$this->load->view('Purchase_form_view', $data);
		$this->load->view('Footer');
	}

	public function index_purchase()
	{
		$this->load->model('Purchase_model');
		$this->load->view('Header');

		$data['list_purchase_header'] = $this->Purchase_model->load_purchase_header();

		$this->load->view('Purchase_list_view', $data);
		$this->load->view('Footer');
	}


	public function edit($purchase_id)
	{
		$this->load->model('Item_model');
		$this->load->model('Purchase_model');
		$this->load->view('Header');

		$data['list_item'] = $this->Item_model->load_item();
		$data['default'] = $this->Purchase_model->get_default_header($purchase_id);
		$data['default_detail'] = $this->Purchase_model->get_default_detail($purchase_id);

		if(isset($_POST['submit_purchase'])){
			$this->Purchase_model->update($_POST, $purchase_id);
			redirect("Purchase_controller/index_purchase");
		}

		$this->load->view('Purchase_form_edit_view', $data);
		$this->load->view('Footer');
	}

	public function delete($purchase_id, $purchase_no){
		$this->load->model("Purchase_model");
		$this->Purchase_model->delete($purchase_id, $purchase_no);
		redirect("Purchase_controller/index_purchase");
	}


}
