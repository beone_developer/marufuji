<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BC_261 extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('status') != "login") {
            redirect(base_url("Login_controller"));
        } else {
            $this->load->model('BC_261_model');
        }
    }

    public function index() {

        $data['judul'] = 'Dokumen 2.6.1';
        $this->load->view('Header', $data);

        $data['ptb_261'] = $this->BC_261_model->select();

        $this->load->view('bc261_tree_view', $data);
        $this->load->view('Footer');
    }

    public function bc261_form() {

        $data['judul'] = 'Dokumen 2.6.1';
        $this->load->view('Header', $data);

//		$data['ptb'] = $this->bc23_model->select();

        $this->load->view('bc261_form_view', $data);
        $this->load->view('Footer');
    }
    
        public function delete($id) {

        $data['judul'] = 'Delete 2.6.1';
        $this->load->view('Header', $data);
        $data['ptb'] = $this->BC_261_model->delete($id);
        redirect("BC_261");
//        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }
    public function daftar_respon() {
        $data['judul'] = 'Respon 2.6.1';
        $this->load->view('Header', $data);
//        $data['ptb'] = $this->bc23_model->daftar_respon($id);
        $this->load->view('bc23_respon', $data);
        $this->load->view('Footer');
    }
   
}
