<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class BC40_detail_barang_controller extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
		 else{
            $this->load->model('BC_40_detail_barang_model');
        }
	}

	public function index(){
		
		$id = $_GET['id'];

		$data['judul'] = 'Detail Barang';
		$data['tipe'] = "New";
		$this->load->view('Header', $data);

		$data['default'] = $this->BC_40_detail_barang_model->get_default_barang($id);

		if(isset($_POST['submit_detail'])){
			$this->BC_40_detail_barang_model->update25($_POST);
			redirect("BC40_detail_barang_controller/$id");
		}

		$this->load->view('bc40_detail_barang_view', $data);
		$this->load->view('Footer'); 

	}

	public function next($ID_HEADER, $ID)
	{
		$data['judul'] = 'Detail Barang';
		$data['tipe'] = "New";
		$this->load->view('Header', $data);

		$data['default'] = $this->BC_40_detail_barang_model->get_default_barangnext($ID_HEADER, $ID);
		$data['default_tarifcukai'] = $this->BC_40_detail_barang_model->get_default_barang_tarifcukainext($ID_HEADER, $ID);
		$data['default_tarifBM'] = $this->BC_40_detail_barang_model->get_default_barang_tarifBMnext($ID_HEADER, $ID);
		$data['default_tarifPPNBM'] = $this->BC_40_detail_barang_model->get_default_barang_tarifPPNBMnext($ID_HEADER, $ID);
		$data['default_tarifPPN'] = $this->BC_40_detail_barang_model->get_default_barang_tarifPPNnext($ID_HEADER, $ID);
		$data['default_tarifPPH'] = $this->BC_40_detail_barang_model->get_default_barang_tarifPPHnext($ID_HEADER, $ID);
		
		if(isset($_POST['submit_detail'])){
			$this->BC_40_detail_barang_model->update_detail_barang($_POST);
			redirect("BC40_detail_barang_controller/next/$ID_HEADER/$ID");
		}
		
		$this->load->view('bc40_detail_barang_view', $data);
		$this->load->view('Footer'); 
	}

	public function prev($ID_HEADER, $ID)
	{
		$data['judul'] = 'Detail Barang';
		$data['tipe'] = "New";
		$this->load->view('Header', $data);

		$data['default'] = $this->BC_40_detail_barang_model->get_default_barangprev($ID_HEADER, $ID);
		$data['default_tarifcukai'] = $this->BC_40_detail_barang_model->get_default_barang_tarifcukaiprev($ID_HEADER, $ID);
		$data['default_tarifBM'] = $this->BC_40_detail_barang_model->get_default_barang_tarifBMprev($ID_HEADER, $ID);
		$data['default_tarifPPNBM'] = $this->BC_40_detail_barang_model->get_default_barang_tarifPPNBMprev($ID_HEADER, $ID);
		$data['default_tarifPPN'] = $this->BC_40_detail_barang_model->get_default_barang_tarifPPNprev($ID_HEADER, $ID);
		$data['default_tarifPPH'] = $this->BC_40_detail_barang_model->get_default_barang_tarifPPHprev($ID_HEADER, $ID);
		
		if(isset($_POST['submit_detail'])){
			$this->BC_40_detail_barang_model->update_detail_barang($_POST);
			redirect("BC40_detail_barang_controller/prev/$ID_HEADER/$ID");
		}
		
		$this->load->view('bc40_detail_barang_view', $data);
		$this->load->view('Footer'); 
	}
}


 ?>