<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pindah_gudang_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Gudang_model');
		$this->load->view('Header');

		$data['list_gudang'] = $this->Gudang_model->load_gudang();

		if(isset($_GET['submit_pindah'])){
			redirect("Pindah_gudang_controller/detail?tgl=".$_GET['tanggal']."&gudang=".$_GET['gudang_asal']);
		}

		$this->load->view('Pindah_gudang_form_view', $data);
		$this->load->view('Footer');
	}

	public function detail()
	{
		$this->load->model('Gudang_model');
		$this->load->view('Header');

		$data['list_gudang'] = $this->Gudang_model->load_gudang();

		if(isset($_POST['submit_pindah'])){
			$this->Gudang_model->simpan_pindah_gudang($_POST);
			redirect("Pindah_gudang_controller");
		}

		$this->load->view('Pindah_gudang_detail_view', $data);
		$this->load->view('Footer');
	}

	public function get_item_pindah_gudang(){
		$this->load->view('get_item_pindah_gudang');
	}

	public function List_pindah_gudang()
	{
		$this->load->model('Gudang_model');
		$this->load->view('Header');

		$data['List_pindah_gudang'] = $this->Gudang_model->load_pindah_gudang();

		$this->load->view('Pindah_gudang_view', $data);
		$this->load->view('Footer');
	}

	public function List_pindah_gudang_penerimaan()
	{
		$this->load->model('Gudang_model');
		$this->load->view('Header');

		$data['List_pindah_gudang'] = $this->Gudang_model->load_pindah_gudang_penerimaan();

		$this->load->view('Pindah_gudang_penerimaan_view', $data);
		$this->load->view('Footer');
	}

	public function pindah_print($pindah_id)
	{
		$this->load->model('Gudang_model');
		$this->load->view('Header');

		$data['print_pindah'] = $this->Gudang_model->load_print_pindah_gudang($pindah_id);

		$this->load->view('Pindah_gudang_print_view', $data);
		$this->load->view('Footer');
	}

	public function pindah_print_penerimaan($no_aju)
	{
		$this->load->model('Gudang_model');
		$this->load->view('Header');

		$data['print_pindah'] = $this->Gudang_model->load_print_pindah_gudang_penerimaan($no_aju);

		$this->load->view('Pindah_gudang_print_penerimaan_view', $data);
		$this->load->view('Footer');
	}


}
