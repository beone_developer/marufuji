<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Bc41_detail_controller extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
		 else{
                    $this->load->model('bc25_detail_model');
                }
	}

	public function form($ID)
	{
		$data['judul'] = 'Detail Barang';
		$data['tipe'] = "New";
		$this->load->view('Header', $data);

		$data['default'] = $this->bc25_detail_model->get_default_barang($ID);
		$data['default_tarifcukai'] = $this->bc25_detail_model->get_default_barang_tarifcukai($ID);
		$data['default_tarifBM'] = $this->bc25_detail_model->get_default_barang_tarifBM($ID);
		$data['default_tarifPPNBM'] = $this->bc25_detail_model->get_default_barang_tarifPPNBM($ID);
		$data['default_tarifPPN'] = $this->bc25_detail_model->get_default_barang_tarifPPN($ID);
		$data['default_tarifPPH'] = $this->bc25_detail_model->get_default_barang_tarifPPH($ID);

		if(isset($_POST['submit_detail'])){
			$this->bc25_detail_model->update41($_POST);
			redirect("Bc41_detail_controller/form/$ID");
		}

		$this->load->view('bc41_detail_view', $data);
		$this->load->view('Footer'); 
	}
	public function next($ID_HEADER, $ID)
	{
		$data['judul'] = 'Detail Barang';
		$data['tipe'] = "New";
		$this->load->view('Header', $data);

		$data['default'] = $this->bc25_detail_model->get_default_barangnext($ID_HEADER, $ID);
		$data['default_tarifcukai'] = $this->bc25_detail_model->get_default_barang_tarifcukainext($ID_HEADER, $ID);
		$data['default_tarifBM'] = $this->bc25_detail_model->get_default_barang_tarifBMnext($ID_HEADER, $ID);
		$data['default_tarifPPNBM'] = $this->bc25_detail_model->get_default_barang_tarifPPNBMnext($ID_HEADER, $ID);
		$data['default_tarifPPN'] = $this->bc25_detail_model->get_default_barang_tarifPPNnext($ID_HEADER, $ID);
		$data['default_tarifPPH'] = $this->bc25_detail_model->get_default_barang_tarifPPHnext($ID_HEADER, $ID);
		if(isset($_POST['submit_detail'])){
			$this->bc25_detail_model->update41($_POST);
			redirect("Bc41_detail_controller/next/$ID_HEADER/$ID");
		}

		$this->load->view('bc41_detail_view', $data);
		$this->load->view('Footer'); 
	}
	public function prev($ID_HEADER, $ID)
	{
		$data['judul'] = 'Detail Barang';
		$data['tipe'] = "New";
		$this->load->view('Header', $data);

		$data['default'] = $this->bc25_detail_model->get_default_barangprev($ID_HEADER, $ID);
		$data['default_tarifcukai'] = $this->bc25_detail_model->get_default_barang_tarifcukaiprev($ID_HEADER, $ID);
		$data['default_tarifBM'] = $this->bc25_detail_model->get_default_barang_tarifBMprev($ID_HEADER, $ID);
		$data['default_tarifPPNBM'] = $this->bc25_detail_model->get_default_barang_tarifPPNBMprev($ID_HEADER, $ID);
		$data['default_tarifPPN'] = $this->bc25_detail_model->get_default_barang_tarifPPNprev($ID_HEADER, $ID);
		$data['default_tarifPPH'] = $this->bc25_detail_model->get_default_barang_tarifPPHprev($ID_HEADER, $ID);
		if(isset($_POST['submit_detail'])){
			$this->bc25_detail_model->update41($_POST);
			redirect("Bc41_detail_controller/next/$ID_HEADER/$ID");
		}

		$this->load->view('bc41_detail_view', $data);
		$this->load->view('Footer'); 
	}
}


 ?>