<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class preview_BC261 extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('status') != "login") {
            redirect(base_url("Login_controller"));
        } else {
            $this->load->model('preview_bc261_model');
        }
    }

    public function index() {
        $aju = $_GET['aju'];
        $id = $_GET['id'];
        $kode = $_GET['kode'];

        $data['judul'] = 'Preview BC 2.6.1';
        $this->load->view('Header', $data);

        $data['ptb_edit'] = $this->preview_bc261_model->select($aju, $id);
        $data['ptb_tujuan'] = $this->preview_bc261_model->get_tujuan($aju, $id);
        $data['ptb_packing'] = $this->preview_bc261_model->get_packing($aju, $id);
        $data['ptb_other'] = $this->preview_bc261_model->get_another($aju, $id);
        $data['ptb_jaminan'] = $this->preview_bc261_model->get_jaminan($aju);
        $data['ptb_kontainer'] = $this->preview_bc261_model->get_kontainer($aju);
        $data['ptb_kemasan'] = $this->preview_bc261_model->get_kemasan($aju);
        $data['ptb_respon'] = $this->preview_bc261_model->get_respon($aju, $id);

        if ($kode == 2) {
            $this->load->view('bc261_preview', $data);
        }
        if ($kode == 1) {
            $this->load->view('bc261_update', $data);
        }

        $this->load->view('Footer');
    }

    public function edit() {
        $id = $_POST['id'];
//        echo $id;
        $nomor_pendaftaran = $_POST['nomor_pendaftaran'];
        $tanggal_pendaftaran = $_POST['tanggal_pendaftaran'];
        $kppbc_pengawas = $_POST['kppbc_pengawas'];
        $tujuan_kirim = $_POST['tujuan_kirim'];
        $nama_pengusaha = $_POST['nama_pengusaha'];
        $alamat_pengusaha = $_POST['alamat_pengusaha'];
        $ptb_pengusaha = $_POST['ptb_pengusaha'];
        $tgl_ijinptb_pengusaha = $_POST['tgl_ijinptb_pengusaha'];
        $jenis_api_pengusaha = $_POST['jenis_api_pengusaha'];
        $nomor_api_pengusaha = $_POST['nomor_api_pengusaha'];
        $jenis_npwp_penerima = $_POST['jenis_npwp_penerima'];
        $nomor_npwp_penerima = $_POST['nomor_npwp_penerima'];
        $nama_penerima = $_POST['nama_penerima'];
        $alamat_penerima = $_POST['alamat_penerima'];
        $nomor_dokumen = $_POST['nomor_dokumen'];
        $tanggal_dokumen = $_POST['tanggal_dokumen'];
        $fasim_1 = $_POST['fasim_1'];
        $fasim_2 = $_POST['fasim_2'];
        $fasim_3 = $_POST['fasim_3'];
        $kode_valuta = $_POST['kode_valuta'];
        $ndpbm = $_POST['ndpbm'];
        $cif = $_POST['cif'];
        $cif_rp = $_POST['cif_rp'];
        $sarana_angkut = $_POST['sarana_angkut'];
        $bruto = $_POST['bruto'];
        $netto = $_POST['netto'];
        $jml_barang = $_POST['jml_barang'];
        $kota_ttd = $_POST['kota_ttd'];
        $tgl_ttd = $_POST['tgl_ttd'];
        $pemberitahu = $_POST['pemberitahu'];
        $jabatan = $_POST['jabatan'];

        $data['judul'] = 'Update 2.6.1';
        $this->load->view('Header', $data);
        $data['ptb'] = $this->preview_bc261_model->update(
                $id, $nomor_pendaftaran, $tanggal_pendaftaran, $kppbc_pengawas, $tujuan_kirim, $nama_pengusaha, $alamat_pengusaha, $ptb_pengusaha, $tgl_ijinptb_pengusaha, $jenis_api_pengusaha, $nomor_api_pengusaha, $jenis_npwp_penerima, $nomor_npwp_penerima, $nama_penerima, $nama_penerima, $alamat_penerima, $nomor_dokumen, $tanggal_dokumen, $fasim_1, $fasim_2, $fasim_3, $kode_valuta, $ndpbm, $cif, $cif_rp, $sarana_angkut, $bruto, $netto, $jml_barang, $kota_ttd, $tgl_ttd, $pemberitahu, $jabatan);
        redirect("BC_261");
        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }

    public function delete_respon($id) {

        $data['judul'] = 'Delete Respon 2.6.1';
        $this->load->view('Header', $data);
        $data['ptb'] = $this->preview_bc261_model->delete_respon($id);
        redirect("BC_261");
//        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }

    public function edit_respon($id) {
        $data['judul'] = 'Update Respon 2.6.1';
        $this->load->view('Header', $data);
        $data['ptb'] = $this->preview_bc261_model->select();
        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }

    public function form_detail_barang() {
        $ID = $_GET['id'];
        $data['judul'] = 'Detail Barang';
        $data['tipe'] = "New";
        $this->load->view('Header', $data);

        $data['default'] = $this->preview_bc261_model->get_default_barang($ID);
        $data['default_tarifcukai'] = $this->preview_bc261_model->get_default_barang_tarifcukai($ID);
        $data['default_tarifBM'] = $this->preview_bc261_model->get_default_barang_tarifBM($ID);
        $data['ptb_other'] = $this->preview_bc261_model->get_another_id($ID);
        $data['default_tarifPPNBM'] = $this->preview_bc261_model->get_default_barang_tarifPPNBM($ID);
        $data['default_tarifPPN'] = $this->preview_bc261_model->get_default_barang_tarifPPN($ID);
        $data['default_tarifPPH'] = $this->preview_bc261_model->get_default_barang_tarifPPH($ID);

        $this->load->view('bc261_detail_view', $data);
        $this->load->view('Footer');
    }

    public function update_modal_kemasan() {
        $jumlah_kemasan = $_POST['jumlah_kemasan'];
        $jenis_kemasan = $_POST['jenis_kemasan'];
        $uraian_kemasan = $_POST['uraian_kemasan'];
        $merk_kemasan = $_POST['merk_kemasan'];
        $id_kemasan = $_POST['id_kemasan'];

        $data['judul'] = 'Update Modal kemasan 2.6.2';
        $this->load->view('Header', $data);
        $data['ptb'] = $this->preview_bc262_model->update_modal_kemasan($id_kemasan, $jumlah_kemasan);
        redirect("BC_261");
//        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }

    public function delete_dokumen($id) {
//        $ID = $_GET['id'];
        $data['judul'] = 'Detail Barang';
        $data['tipe'] = "New";
        $this->load->view('Header', $data);

        $data['default'] = $this->preview_bc261_model->delete_dokumen($id);

        redirect("BC_261");
        $this->load->view('Footer');
    }

    public function delete_kemasan($id) {
//        $ID = $_GET['id'];
        $data['judul'] = 'Detail Barang';
        $data['tipe'] = "New";
        $this->load->view('Header', $data);

        $data['default'] = $this->preview_bc261_model->delete_kemasan($id);

        redirect("BC_261");
        $this->load->view('Footer');
    }

    public function delete_kontainer($id) {
//        $ID = $_GET['id'];
        $data['judul'] = 'Detail Barang';
        $data['tipe'] = "New";
        $this->load->view('Header', $data);

        $data['default'] = $this->preview_bc261_model->delete_kontainer($id);

        redirect("BC_261");
        $this->load->view('Footer');
    }

    public function delete_jaminan($id) {
//        $ID = $_GET['id'];
        $data['judul'] = 'Detail Barang';
        $data['tipe'] = "New";
        $this->load->view('Header', $data);

        $data['default'] = $this->preview_bc261_model->delete_jaminan($id);

        redirect("BC_261");
        $this->load->view('Footer');
    }

    public function editmodal($ID) {

        $data['dokumenedit'] = $this->preview_bc261_model->get_default_modal_dokumen($ID);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function editmodal_kemasan($ID) {

        $data['dokumenedit'] = $this->preview_bc261_model->get_default_modal_kemasan($ID);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function update_modal_dokumen() {
        $id = $_POST['id'];
        $jenis_dokumen = $_POST['dokumen'];
        $nomor_dokumen = $_POST['nomor'];
        $tgl_dokumen = $_POST['tanggal'];

        $data['judul'] = 'Update modal dokumen 2.6.1';
        $this->load->view('Header', $data);
        $data['ptb'] = $this->preview_bc261_model->update_modal_dokumen($id, $jenis_dokumen, $nomor_dokumen, $tgl_dokumen);
        redirect("BC_261");
//        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }

    public function update_popup_kemasan() {
        $id = $_POST['id_kemasan'];
        $jml_kemasan = $_POST['jml_kemasan'];
        $kode_kemasan = $_POST['kode_kemasan'];
        $uraian_kemasan = $_POST['uraian_kemasan'];
        $merk_kemasan = $_POST['merk_kemasan'];

//        echo ($id);

        $data['judul'] = 'Update modal kemasan 2.6.1';
        $this->load->view('Header', $data);
        $data['ptb'] = $this->preview_bc261_model->update_popup_kemasan($id, $jml_kemasan, $kode_kemasan, $merk_kemasan, $uraian_kemasan);
        redirect("BC_261");
//        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }

    public function update_popup_kontainer() {
        $id = $_POST['id_kontainer'];
        $nomor_kontainer = $_POST['nomor_kontainer'];
        $ukuran_kontainer = $_POST['ukuran_kontainer'];
        $tipe_kontainer = $_POST['tipe_kontainer'];

//        echo ($id);

        $data['judul'] = 'Update modal kontainer 2.3';
        $this->load->view('Header', $data);
        $data['ptb'] = $this->preview_bc261_model->update_popup_kontainer($id, $nomor_kontainer, $ukuran_kontainer, $tipe_kontainer);
        redirect("BC_261");
//        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }

    public function editmodal_kontainer($ID) {

        $data['dokumenedit'] = $this->preview_bc261_model->get_default_modal_kontainer($ID);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function update_popup_jaminan() {
        $id = $_POST['id_jaminan'];
        $jenis_jaminan = $_POST['jenis_jaminan'];
        $nomor_jaminan = $_POST['nomor_jaminan'];
        $tgl_jaminan = $_POST['tgl_jaminan'];
        $nilai_jaminan = $_POST['nilai_jaminan'];
        $jatuh_tempo = $_POST['jatuh_tempo'];
        $penjamin = $_POST['penjamin'];
        $nomor_bpj = $_POST['nomor_bpj'];

//        echo ($id);

        $data['judul'] = 'Update modal jaminan 2.6.1';
        $this->load->view('Header', $data);
        $data['ptb'] = $this->preview_bc261_model->update_popup_jaminan($id, $jenis_jaminan, $nomor_jaminan, $tgl_jaminan,$nilai_jaminan,$jatuh_tempo,$penjamin,$nomor_bpj);
        redirect("BC_261");
//        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }
    public function editmodal_jaminan($ID) {

        $data['dokumenedit'] = $this->preview_bc261_model->get_default_modal_jaminan($ID);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

}
