<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cashbank_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function get_nomor(){
		$this->load->view('get_nomor_kas');
	}


	public function index_voucher()
	{
		$this->load->model('Cashbank_model');
		$this->load->view('Header');

		$data['list_voucher'] = $this->Cashbank_model->load_voucher();

		$this->load->view('Voucher_view', $data);
		$this->load->view('Footer');
	}

	public function voucher_print($voucher_id)
	{
		$this->load->model('Cashbank_model');
		$this->load->view('Header');

		$data['print_voucher'] = $this->Cashbank_model->load_voucher_print($voucher_id);

		$this->load->view('Voucher_print_view',$data);
		$this->load->view('Footer');
	}

	public function index()
	{
		$this->load->model("Cashbank_model");
		$this->load->model('COA_model');
		$this->load->view('Header');

		$data['list_coa_kas_bank'] = $this->COA_model->load_COA_kas_bank();

		if(isset($_GET['submit_header'])){
			//proses simpan dilakukan
				redirect("Cashbank_controller/detail?tgl=".$_GET['tanggal_voucher']."&tipe=".$_GET['tipe_voucher']."&coa_id=".$_GET['coa_id_cash_bank']."&ket=".$_GET['keterangan_header']);
		}

		$this->load->view('Cashbank_form_view',$data);
		$this->load->view('Footer');
	}

	public function detail()
	{
		$this->load->model("Cashbank_model");
		$this->load->model('COA_model');
		$this->load->model('Custsup_model');
		$this->load->view('Header');

		$data['list_coa'] = $this->COA_model->load_COA();
		$data['list_custsup'] = $this->Custsup_model->load_custsup();
		$data['list_hutang'] = $this->Cashbank_model->load_hutang();
		$data['list_piutang'] = $this->Cashbank_model->load_piutang();
		$data['list_saldo_awal_hutang'] = $this->Cashbank_model->load_saldo_awal_hutang();
		$data['list_saldo_awal_piutang'] = $this->Cashbank_model->load_saldo_awal_piutang();

		if(isset($_POST['submit_voucher'])){
			//proses simpan dilakukan
			$this->Cashbank_model->simpan($_POST);
			redirect("Cashbank_controller");
		}

		$this->load->view('Cashbank_detail_view',$data);
		$this->load->view('Footer');
	}

	public function edit($voucher_id){
		$this->load->model("Cashbank_model");
		$this->load->model('COA_model');
		$this->load->model('Custsup_model');
		$this->load->view('Header');

		$data['list_coa'] = $this->COA_model->load_COA();
		$data['list_custsup'] = $this->Custsup_model->load_custsup();
		$data['list_hutang'] = $this->Cashbank_model->load_hutang();
		$data['list_piutang'] = $this->Cashbank_model->load_piutang();
		$data['list_saldo_awal_hutang'] = $this->Cashbank_model->load_saldo_awal_hutang();
		$data['list_saldo_awal_piutang'] = $this->Cashbank_model->load_saldo_awal_piutang();
		$data['default'] = $this->Cashbank_model->get_default_header($voucher_id);
		$data['default_detail'] = $this->Cashbank_model->get_default_detail($voucher_id);
		//yang penting diletakkan sebelum load view

		if(isset($_POST['submit_voucher'])){
			$this->Cashbank_model->update($_POST, $voucher_id);
			redirect("Cashbank_controller/index_voucher");
		}
		$this->load->view("Cashbank_edit_view",$data);
		$this->load->view('Footer');
	}

	public function delete($voucher_id, $voucher_number){
		$this->load->model("Cashbank_model");
		$this->Cashbank_model->delete($voucher_id, $voucher_number);
		redirect("Cashbank_controller/index_voucher");
	}


	public function index_kode_cashbank()
	{
		$this->load->model('Cashbank_model');
		$this->load->model('COA_model');
		$this->load->view('Header');

		$data['list_kode_cashbank'] = $this->Cashbank_model->load_kode_cashbank();
		$data['list_coa_cashbank'] = $this->COA_model->load_COA_kas_bank();
		$data['tipe'] = "Tambah";

		if(isset($_POST['submit_kode_cashbank'])){
			$this->Cashbank_model->simpan_kode_cashbank($_POST);
			redirect("Cashbank_controller/index_kode_cashbank");
		}

		$this->load->view('Kode_cashbank_view',$data);
		$this->load->view('Footer');
	}


	public function Edit_kode_cashbank($kode_trans_id)
	{
		$this->load->model('Cashbank_model');
		$this->load->model('COA_model');
		$this->load->view('Header');

		$data['list_kode_cashbank'] = $this->Cashbank_model->load_kode_cashbank();
		$data['list_coa_cashbank'] = $this->COA_model->load_COA_kas_bank();
		$data['default'] = $this->Cashbank_model->get_default_kode_cashbank($kode_trans_id);
		$data['tipe'] = "Ubah";

		if(isset($_POST['submit_kode_cashbank'])){
			$this->Cashbank_model->update_kode_cashbank($_POST, $kode_trans_id);
			redirect("Cashbank_controller/index_kode_cashbank");
		}

		$this->load->view('Kode_cashbank_view',$data);
		$this->load->view('Footer');
	}

	public function delete_kode_cashbank($kode_trans_id){
		$this->load->model("Cashbank_model");
		$this->Cashbank_model->delete_kode_cashbank($kode_trans_id);
		redirect("Cashbank_controller/index_kode_cashbank");
	}

}
