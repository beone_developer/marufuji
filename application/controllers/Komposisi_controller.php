<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komposisi_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Item_model');
		$this->load->model('Komposisi_model');
		$this->load->view('Header');

		$data['list_item'] = $this->Item_model->load_item();
		$data['list_komposisi'] = $this->Komposisi_model->load_komposisi();

		if(isset($_POST['submit_komposisi'])){
			$this->Komposisi_model->simpan($_POST);
			redirect("Komposisi_controller");
		}

		$this->load->view('Komposisi_form_view',$data);
		$this->load->view('Footer');
	}

	public function delete($komposisi_id){
		$this->load->model('Komposisi_model');
		$this->Komposisi_model->delete($komposisi_id);
		redirect("Komposisi_controller");
	}

}
