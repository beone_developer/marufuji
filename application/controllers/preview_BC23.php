<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class preview_BC23 extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('status') != "login") {
            redirect(base_url("Login_controller"));
        } else {
            $this->load->model('preview_bc23_model');
        }
    }

    public function index() {
        $aju = $_GET['aju'];
        $kode = $_GET['kode'];
        $id = $_GET['id'];

        $data['judul'] = 'Preview BC 2.3';
        $this->load->view('Header', $data);

        $data['ptb_edit'] = $this->preview_bc23_model->select($aju);
        $data['ptb_inv'] = $this->preview_bc23_model->get_inv($aju);
        $data['ptb_other'] = $this->preview_bc23_model->get_another($aju);
        $data['ptb_lc'] = $this->preview_bc23_model->get_lc($aju);
        $data['ptb_kontainer'] = $this->preview_bc23_model->get_kontainer($aju);
        $data['ptb_kemasan'] = $this->preview_bc23_model->get_kemasan($aju);
        $data['ptb_respon'] = $this->preview_bc23_model->get_respon($id);

        if ($kode == 2) {
            $this->load->view('bc23_preview', $data);
        }
        if ($kode == 1) {
            $this->load->view('bc23_update', $data);
        }
        $this->load->view('Footer');
    }

    public function edit() {
        $id = $_POST['id'];
        $nomor_pendaftaran = $_POST['nomor_pendaftaran'];
        $tanggal_pendaftaran = $_POST['tanggal_pendaftaran'];
        $kppbc_bongkar = $_POST['kppbc_bongkar'];
        $kppbc_pengawas = $_POST['kppbc_pengawas'];
        $tujuan = $_POST['tujuan'];
        $nama_pemasok = $_POST['nama_pemasok'];
        $alamat_pemasok = $_POST['alamat_pemasok'];
        $negara_pemasok = $_POST['negara_pemasok'];
        $jenis_identitas_importir = $_POST['jenis_identitas_importir'];
        $nomor_identitas_importir = $_POST['nomor_identitas_importir'];
        $nama_importir = $_POST['nama_importir'];
        $nomor_ijin_tpb = $_POST['nomor_ijin_tpb'];
        $alamat_importir = $_POST['alamat_importir'];
        $jenis_api_importir = $_POST['jenis_api_importir'];
        $nomor_api_importir = $_POST['nomor_api_importir'];
        $jenis_identitas_pemilik = $_POST['jenis_identitas_pemilik'];
        $nomor_identitas_pemilik = $_POST['nomor_identitas_pemilik'];
        $nama_pemilik = $_POST['nama_pemilik'];
        $alamat_pemilik = $_POST['alamat_pemilik'];
        $jenis_api_pemilik = $_POST['jenis_api_pemilik'];
        $nomor_api_pemilik = $_POST['nomor_api_pemilik'];
        $jenis_identitas_pemilik = $_POST['jenis_identitas_pemilik'];
        $nomor_identitas_pemilik = $_POST['nomor_identitas_pemilik'];
        $nama_pemilik = $_POST['nama_pemilik'];
        $alamat_pemilik = $_POST['alamat_pemilik'];
        $nomor_npppjk = $_POST['nomor_npppjk'];
        $tanggal_npppjk = $_POST['tanggal_npppjk'];
        $cara_angkut = $_POST['cara_angkut'];
        $nama_sarana_angkut = $_POST['nama_sarana_angkut'];
        $nomor_vf = $_POST['nomor_vf'];
        $kode_bendera = $_POST['kode_bendera'];
        $kode_pel_muat = $_POST['kode_pel_muat'];
        $pel_muat = $_POST['pel_muat'];
        $kode_pel_transit = $_POST['kode_pel_transit'];
        $kode_pel_bongkar = $_POST['kode_pel_bongkar'];
        $pel_bongkar = $_POST['pel_bongkar'];
        $nomor_dokumen = $_POST['nomor_dokumen'];
        $tgl_dokumen = $_POST['tgl_dokumen'];
        $fasim_1 = $_POST['fasim_1'];
        $fasim_2 = $_POST['fasim_2'];
        $fasim_3 = $_POST['fasim_3'];
        $LC = $_POST['LC'];
        $tgl_lc = $_POST['tgl_lc'];
        $BL = $_POST['BL'];
        $BL_1 = $_POST['BL_1'];
        $BL_2 = $_POST['BL_2'];
        $bc11_1 = $_POST['bc11_1'];
        $bc11_2 = $_POST['bc11_2'];
        $kode_tps = $_POST['kode_tps'];
        $uraian_tps = $_POST['uraian_tps'];
        $kode_valuta = $_POST['kode_valuta'];
        $ndpbm = $_POST['ndpbm'];
        $fob = $_POST['fob'];
        $freight = $_POST['freight'];
        $asuransi = $_POST['asuransi'];
        $cif = $_POST['cif'];
        $cif_rp = $_POST['cif_rp'];
        $bruto = $_POST['bruto'];
        $netto = $_POST['netto'];
        $kota_ttd = $_POST['kota_ttd'];
        $tgl_ttd = $_POST['tgl_ttd'];
        $nama_ttd = $_POST['nama_ttd'];
        $jabatan_ttd = $_POST['jabatan_ttd'];

        $data['judul'] = 'Update 2.3';
        $this->load->view('Header', $data);
        $data['ptb'] = $this->preview_bc23_model->update(
                $id, $nomor_pendaftaran, $tanggal_pendaftaran, $kppbc_bongkar, $kppbc_pengawas, $tujuan, $nama_pemasok, $alamat_pemasok, $negara_pemasok, $jenis_identitas_importir, $nomor_identitas_importir, $nama_importir, $nomor_ijin_tpb, $alamat_importir, $jenis_api_importir, $nomor_api_importir, $jenis_identitas_pemilik, $nomor_identitas_pemilik, $nama_pemilik, $alamat_pemilik, $jenis_api_pemilik, $nomor_api_pemilik, $nomor_npppjk, $tanggal_npppjk, $cara_angkut, $nama_sarana_angkut, $nomor_vf, $kode_bendera, $kode_pel_muat, $pel_muat, $kode_pel_transit, $kode_pel_bongkar, $pel_bongkar, $nomor_dokumen, $tgl_dokumen, $fasim_1, $fasim_2, $fasim_3, $LC, $tgl_lc, $BL, $BL_1, $BL_2, $bc11_1, $bc11_2, $kode_tps, $uraian_tps, $kode_valuta, $ndpbm, $fob, $freight, $asuransi, $cif, $cif_rp, $bruto, $netto, $kota_ttd, $tgl_ttd, $nama_ttd, $jabatan_ttd);
        redirect("Bc_controller");
        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }

    public function form_detail_barang() {
        $ID = $_GET['id'];
        $data['judul'] = 'Detail Barang';
        $data['tipe'] = "New";
        $this->load->view('Header', $data);

        $data['default'] = $this->preview_bc23_model->get_default_barang($ID);
        $data['ptb_other'] = $this->preview_bc23_model->get_another_id($ID);
        $data['default_tarifcukai'] = $this->preview_bc23_model->get_default_barang_tarifcukai($ID);
        $data['default_tarifBM'] = $this->preview_bc23_model->get_default_barang_tarifBM($ID);
        $data['default_tarifPPNBM'] = $this->preview_bc23_model->get_default_barang_tarifPPNBM($ID);
        $data['default_tarifPPN'] = $this->preview_bc23_model->get_default_barang_tarifPPN($ID);
        $data['default_tarifPPH'] = $this->preview_bc23_model->get_default_barang_tarifPPH($ID);

        $this->load->view('bc23_detail_barang', $data);
        $this->load->view('Footer');
    }

    public function delete_dokumen($id) {
//        $ID = $_GET['id'];
        $data['judul'] = 'Detail Barang';
        $data['tipe'] = "New";
        $this->load->view('Header', $data);

        $data['default'] = $this->preview_bc23_model->delete_dokumen($id);

        redirect("Bc_controller");
        $this->load->view('Footer');
    }

    public function delete_kemasan($id) {
//        $ID = $_GET['id'];
        $data['judul'] = 'Detail Barang';
        $data['tipe'] = "New";
        $this->load->view('Header', $data);

        $data['default'] = $this->preview_bc23_model->delete_kemasan($id);

        redirect("Bc_controller");
        $this->load->view('Footer');
    }

    public function delete_kontainer($id) {
//        $ID = $_GET['id'];
        $data['judul'] = 'Detail Barang';
        $data['tipe'] = "New";
        $this->load->view('Header', $data);

        $data['default'] = $this->preview_bc23_model->delete_kontainer($id);

        redirect("Bc_controller");
        $this->load->view('Footer');
    }

    public function update_modal_dokumen() {
        $id = $_POST['id'];
        $jenis_dokumen = $_POST['dokumen'];
        $nomor_dokumen = $_POST['nomor'];
        $tgl_dokumen = $_POST['tanggal'];

        $data['judul'] = 'Update modal dokumen 2.3';
        $this->load->view('Header', $data);
        $data['ptb'] = $this->preview_bc23_model->update_modal_dokumen($id, $jenis_dokumen, $nomor_dokumen, $tgl_dokumen);
        redirect("Bc_controller");
//        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }

    public function update_popup_kemasan() {
        $id = $_POST['id_kemasan'];
        $jml_kemasan = $_POST['jml_kemasan'];
        $kode_kemasan = $_POST['kode_kemasan'];
        $uraian_kemasan = $_POST['uraian_kemasan'];
        $merk_kemasan = $_POST['merk_kemasan'];

//        echo ($id);

        $data['judul'] = 'Update modal kemasan 2.6.2';
        $this->load->view('Header', $data);
        $data['ptb'] = $this->preview_bc23_model->update_popup_kemasan($id, $jml_kemasan, $kode_kemasan, $merk_kemasan, $uraian_kemasan);
        redirect("Bc_controller");
//        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }

    public function update_popup_kontainer() {
        $id = $_POST['id_kontainer'];
        $nomor_kontainer = $_POST['nomor_kontainer'];
        $ukuran_kontainer = $_POST['ukuran_kontainer'];
        $tipe_kontainer = $_POST['tipe_kontainer'];

//        echo ($id);

        $data['judul'] = 'Update modal kontainer 2.3';
        $this->load->view('Header', $data);
        $data['ptb'] = $this->preview_bc23_model->update_popup_kontainer($id, $nomor_kontainer, $ukuran_kontainer, $tipe_kontainer);
        redirect("Bc_controller");
//        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }

    public function editmodal_kemasan($ID) {

        $data['dokumenedit'] = $this->preview_bc23_model->get_default_modal_kemasan($ID);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function editmodal($ID) {

        $data['dokumenedit'] = $this->preview_bc23_model->get_default_modal_dokumen($ID);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function editmodal_kontainer($ID) {

        $data['dokumenedit'] = $this->preview_bc23_model->get_default_modal_kontainer($ID);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function update_modal_harga() {
        $id = $_POST['id_header'];
        $harga = $_POST['harga'];
        $valuta = $_POST['valuta'];
        $modal_ndpbm = $_POST['modal_ndpbm'];
        $harga_fob = $_POST['harga_fob'];
        $biaya_tambahan = $_POST['biaya_tambahan'];
        $diskon = $_POST['diskon'];
        $asuransi_bayar_di = $_POST['asuransi_bayar_di'];
        $nilai_asuransi = $_POST['nilai_asuransi'];
        $freight = $_POST['freight'];
        $fob = $_POST['fob'];
        $cif = $_POST['cif'];
        $cif_rp = $_POST['cif_rp'];

//        echo ($id);

        $data['judul'] = 'Update modal harga 2.3';
        $this->load->view('Header', $data);
        $data['ptb'] = $this->preview_bc23_model->update_popup_harga($id, $harga, $valuta, $modal_ndpbm,$harga_fob,
                $biaya_tambahan,$diskon,$asuransi_bayar_di,$nilai_asuransi,$freight,$fob,$cif,$cif_rp);
        redirect("Bc_controller");
//        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }

}
