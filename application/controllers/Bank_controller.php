<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function get_nomor(){
		$this->load->view('get_nomor_bank');
	}

	public function index()
	{
		$this->load->model("Cashbank_model");
		$this->load->model('COA_model');
		$this->load->view('Header');

		$data['list_coa_bank'] = $this->COA_model->load_COA_bank();
		$data['list_coa'] = $this->COA_model->load_COA();

		if(isset($_POST['submit_voucher'])){
			//proses simpan dilakukan
			$this->Cashbank_model->simpan($_POST);
			redirect("Bank_controller");
		}

		$this->load->view('Bank_form_view',$data);
		$this->load->view('Footer');
	}


}
