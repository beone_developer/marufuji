<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Bc25_detail_controller extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
		 else{
                    $this->load->model('bc25_detail_model');
                }
	}

	public function form($ID)
	{
		$data['judul'] = 'Detail Barang';
		$data['tipe'] = "New";
		$this->load->view('Header', $data);

		$data['default'] = $this->bc25_detail_model->get_default_barang($ID);
		$data['default_tarifcukai'] = $this->bc25_detail_model->get_default_barang_tarifcukai($ID);
		$data['default_tarifBM'] = $this->bc25_detail_model->get_default_barang_tarifBM($ID);
		$data['default_tarifPPNBM'] = $this->bc25_detail_model->get_default_barang_tarifPPNBM($ID);
		$data['default_tarifPPN'] = $this->bc25_detail_model->get_default_barang_tarifPPN($ID);
		$data['default_tarifPPH'] = $this->bc25_detail_model->get_default_barang_tarifPPH($ID);

		$this->load->view('bc25_detail_view', $data);
		$this->load->view('Footer'); 
	}
	public function next($ID_HEADER, $ID)
	{
		$data['judul'] = 'Detail Barang';
		$data['tipe'] = "New";
		$this->load->view('Header', $data);

		$data['default'] = $this->bc23_detail_model->get_default_barangnext($ID_HEADER, $ID);

		$this->load->view('bc23_detail_view', $data);
		$this->load->view('Footer'); 
	}
	public function prev($ID_HEADER, $ID)
	{
		$data['judul'] = 'Detail Barang';
		$data['tipe'] = "New";
		$this->load->view('Header', $data);

		$data['default'] = $this->bc23_detail_model->get_default_barangprev($ID_HEADER, $ID);

		$this->load->view('bc23_detail_view', $data);
		$this->load->view('Footer'); 
	}
}


 ?>