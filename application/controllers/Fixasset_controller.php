<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fixasset_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index(){
		$this->load->model('Fixasset_model');
		$this->load->view('Header');

		$data['list_periode'] = $this->Fixasset_model->load_periode();
		$data['tipe'] = "Tambah";

		if(isset($_POST['submit_periode'])){
			$this->Fixasset_model->simpan_periode($_POST);
			redirect("Fixasset_controller");
		}

		$this->load->view('Fixasset_periode_view',$data);
		$this->load->view('Footer');
	}

	public function Edit_periode($periode_id){
		$this->load->model('Fixasset_model');
		$this->load->view('Header');

		$data['list_periode'] = $this->Fixasset_model->load_periode();
		$data['default'] = $this->Fixasset_model->get_default_periode($periode_id);
		$data['tipe'] = "Ubah";

		if(isset($_POST['submit_periode'])){
			$this->Fixasset_model->update_periode($_POST, $periode_id);
			redirect("Fixasset_controller");
		}

		$this->load->view('Fixasset_periode_view',$data);
		$this->load->view('Footer');
	}

	public function delete_periode($periode_id){
		$this->load->model("Fixasset_model");
		$this->Fixasset_model->delete_periode($periode_id);
		redirect("Fixasset_controller");
	}

	/*********************************************************************/


	public function Fixed_asset_akm($periode_id){
		$this->load->model('Fixasset_model');
		$this->load->view('Header');

		$data['list_fixed_asset_akm'] = $this->Fixasset_model->load_fixed_asset_akm($periode_id);
		$data['tipe'] = "List";

		$this->load->view('Fixasset_akm_view',$data);
		$this->load->view('Footer');
	}

	public function Edit_asset_akm($periode_id, $fix_asset_id)
	{
		$this->load->model('Fixasset_model');
		$this->load->view('Header');

		$data['list_fixed_asset_akm'] = $this->Fixasset_model->load_fixed_asset_akm($periode_id);
		$data['default'] = $this->Fixasset_model->get_default_fix_asset($periode_id, $fix_asset_id);
		$data['tipe'] = "Ubah";

		if(isset($_POST['submit_fixasset'])){
			$this->Fixasset_model->proses_fixasset($_POST);
			redirect("Fixasset_controller");
		}

		$this->load->view('Fixasset_akm_view',$data);
		$this->load->view('Footer');
	}

	/********************************/

	public function fixasset(){
		$this->load->model('Fixasset_model');
		$this->load->view('Header');

		$data['list_fixasset'] = $this->Fixasset_model->load_fixasset();
		$data['tipe'] = "Tambah";

		if(isset($_POST['submit_fixasset'])){
			$this->Fixasset_model->simpan_fixasset($_POST);
			redirect("Fixasset_controller/fixasset");
		}

		$this->load->view('Fixasset_view',$data);
		$this->load->view('Footer');
	}

	public function Edit_fixasset($fix_asset_id){
		$this->load->model('Fixasset_model');
		$this->load->view('Header');

		$data['list_fixasset'] = $this->Fixasset_model->load_fixasset();
		$data['tipe'] = "Ubah";
		$data['default'] = $this->Fixasset_model->get_default_fixasset($fix_asset_id);

		if(isset($_POST['submit_fixasset'])){
			$this->Fixasset_model->update_fixasset($_POST, $fix_asset_id);
			redirect("Fixasset_controller/fixasset");
		}

		$this->load->view('Fixasset_view',$data);
		$this->load->view('Footer');
	}

	public function delete_fixasset($fix_asset_id){
		$this->load->model("Fixasset_model");
		$this->Fixasset_model->delete_fixasset($fix_asset_id);
		redirect("Fixasset_controller/fixasset");
	}


}
