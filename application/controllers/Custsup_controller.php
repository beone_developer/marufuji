<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custsup_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Custsup_model');
		$this->load->view('Header');

		$data['tipe'] = "Tambah";

		if(isset($_POST['submit_custsup'])){
			$this->Custsup_model->simpan($_POST);
			redirect("Custsup_controller?tipe=".intval($_POST['tipe_custsup']));
		}

		/* import custsup
		if(isset($_POST['preview'])){
			redirect("Custsup_controller/import");
		}*/

		$this->load->view('Custsup_view',$data);
		$this->load->view('Footer');
	}

	public function Edit($custsup_id)
	{
		$this->load->model('Custsup_model');
		$this->load->view('Header');

		$data['default'] = $this->Custsup_model->get_default($custsup_id);
		$data['tipe'] = "Ubah";

		if(isset($_POST['submit_custsup'])){
			$this->Custsup_model->update($_POST, $custsup_id);
			redirect("Custsup_controller?tipe=".intval($_POST['tipe_custsup']));
		}

		$this->load->view('Custsup_view',$data);
		$this->load->view('Footer');
	}

	public function delete($custsup_id){
		$this->load->model("Custsup_model");
		$this->Custsup_model->delete($custsup_id);

		$tipe = $this->db->query("SELECT * FROM public.beone_custsup WHERE custsup_id =".intval($custsup_id));
		$hasil_tipe = $tipe->row_array();

		redirect("Custsup_controller?tipe=".intval($hasil_tipe['tipe_custsup']));
	}

	/***************************************************************************/
	public function import(){
		$this->load->model('Custsup_model');
		// Load plugin PHPExcel nya
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/'.$this->filename.'custsup.xlsx'); // Load file yang telah diupload ke folder excel
		//$loadexcel = $excelreader->load('excel/format.xlsx');

		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();

		$numrow = 1;
		foreach($sheet as $row){
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					'custsup_id'=>$row['A'], // Insert data nis dari kolom A di excel
					'nama'=>$row['B'], // Insert data nama dari kolom B di excel
					'alamat'=>$row['C'], // Insert data jenis kelamin dari kolom C di excel
					'tipe_custsup'=>$row['D'], // Insert data alamat dari kolom D di excel
					'negara'=>$row['E'],
					'saldo_hutang_idr'=>$row['F'],
					'saldo_hutang_valas'=>$row['G'],
					'saldo_piutang_idr'=>$row['H'],
					'saldo_piutang_valas'=>$row['I'],
					'flag'=>$row['J'],
					'pelunasan_hutang_idr'=>$row['K'],
					'pelunasan_hutang_valas'=>$row['L'],
					'pelunasan_piutang_idr'=>$row['M'],
					'pelunasan_piutang_valas'=>$row['N'],
					'status_lunas_piutang'=>$row['O'],
					'status_lunas_hutang'=>$row['P'],
				));
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->Custsup_model->insert_multiple($data);

		redirect("Custsup_controller?tipe=2"); // Redirect ke halaman awal (ke controller fungsi index)
	}
	/**************************************************************************/


}
