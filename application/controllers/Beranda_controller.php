<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda_controller extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login_controller"));
		}
	}

	public function sql_example()
	{
		$this->cb->begin_trans();
		$this->cb->query("select 0/0");
		$this->cb->query("select 0/0");
		$this->cb->query("select 0/0");
		$result = $this->cb->commit_trans();
		echo json_encode($result);
		return '';
	}

	public function index()
	{
		$this->load->model('Beranda_model');
		$this->load->view('Header');
		//$this->load->view('Dashboard');
		$this->load->view('Footer');
	}

	public function test()
	{
		$this->load->library('pdf');
		$this->load->view('test-print');
	}
}
