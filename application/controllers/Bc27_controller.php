<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bc27_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
		 else{
                    $this->load->model('bc27_model');
                }
	}

	public function index()
	{
		$data['judul'] = 'Dokumen 2.7';
		$this->load->view('Header', $data);

		$data['ptb'] = $this->bc27_model->select();

		$this->load->view('bc27_tree_view',$data);
		$this->load->view('Footer'); 
	}

	public function form()
	{
		$data['judul'] = 'Dokumen 2.7';
		$this->load->view('Header', $data);
		$data['jenis_tpb'] = $this->bc27_model->get_jenis_tpb();
		$data['tujuan_tpb'] = $this->bc27_model->get_tujuan_tpb();
		$data['tujuan_pengiriman'] = $this->bc27_model->get_tujuan_pengiriman();
		$data['default_pengusaha'] = $this->bc27_model->get_default_pengusaha();

		$data['kemasan'] = $this->bc27_model->get_kemasan('0');
		$data['kontainer'] = $this->bc27_model->get_kontainer('0');
		$data['dokumen_luar'] = $this->bc27_model->get_dokumen('0');

		// $data['ptb'] = $this->bc25_model->select();
		if(isset($_POST['submit_bc27'])){
			$this->bc27_model->simpan($_POST);
			redirect("Bc27_controller");
		}

		$this->load->view('bc27_form_view',$data);
		$this->load->view('Footer'); 
	}

	public function edit($ID)
	{
		$data['judul'] = 'Dokumen 2.7';
		$this->load->view('Header', $data);

		$data['default'] = $this->bc27_model->get_default($ID);
		$data['jenis_tpb'] = $this->bc27_model->get_jenis_tpb();
		$data['tujuan_tpb'] = $this->bc27_model->get_tujuan_tpb();
		$data['tujuan_pengiriman'] = $this->bc27_model->get_tujuan_pengiriman();


		$data['jenis_api'] = $this->bc27_model->get_jenis_api();
		$data['jenis_id'] = $this->bc27_model->get_jenis_id();
		$data['jenis_lokasi_bayar'] = $this->bc27_model->get_jenis_lokasi_bayar();
		$data['jenis_pembayar'] = $this->bc27_model->get_jenis_pembayar();
		$data['jenis_cara_angkut'] = $this->bc27_model->get_jenis_cara_angkut();
		$data['kemasan'] = $this->bc27_model->get_kemasan($ID);
		$data['kontainer'] = $this->bc27_model->get_kontainer($ID);
		$data['dokumen_luar'] = $this->bc27_model->get_dokumen($ID);
		$data['default_dok_inv'] = $this->bc27_model->get_default_dokinv($ID);
		$data['default_dok_pl'] = $this->bc27_model->get_default_dokpl($ID);
		$data['default_dok_ktr'] = $this->bc27_model->get_default_dokktr($ID);
		$data['default_dok_sj'] = $this->bc27_model->get_default_doksj($ID);
		$data['default_dok_skep'] = $this->bc27_model->get_default_dokskep($ID);
		$data['dokumen_modal_luar'] = $this->bc27_model->get_dokumen_modal($ID);
		$data['kemasan_modal_luar'] = $this->bc27_model->get_kemasan_modal($ID);
		$data['modal_daftar_respon'] = $this->bc27_model->get_daftar_respon($ID);
		$data['modal_daftar_status'] = $this->bc27_model->get_daftar_status($ID);
		$data['ptb_kontainer'] = $this->bc27_model->get_kontainer($ID);

		if(isset($_POST['submit_bc27'])){
			$this->bc27_model->update($_POST, $ID);
			redirect("Bc27_controller");
		}

		$this->load->view('bc27_form_view',$data);
		$this->load->view('Footer');
	}

	public function delete($id){
		$this->bc27_model->delete($id);
		redirect("Bc27_controller");
	}

	public function delete_dokumen($id, $ID_HEADER) {

		$this->bc27_model->delete_dokumen($id);
		redirect("Bc27_controller/edit/$ID_HEADER");
    }

    public function delete_kemasan($id, $ID_HEADER) {
$this->bc27_model->delete_kemasan($id);
		redirect("Bc27_controller/edit/$ID_HEADER");
    }

    public function delete_kontainer($id, $ID_HEADER) {
//        $ID = $_GET['id'];
        $this->bc27_model->delete_kontainer($id);
		redirect("Bc27_controller/edit/$ID_HEADER");
    }



	public function editmodal($ID){

		$data['dokumenedit'] = $this->bc27_model->get_default_modal_dokumen($ID);
		header('Content-Type: application/json');
    	echo json_encode( $data );
	}

	public function editkemasan($ID){

		$data['kemasanedit'] = $this->bc27_model->get_default_modal_kemasan($ID);
		header('Content-Type: application/json');
    	echo json_encode( $data );
	}

	public function update_dokumen($ID_HEADER){
		if(isset($_POST['submit_modaldokumen'])){
			$this->bc27_model->updatedokumen($_POST);
			redirect("Bc27_controller/edit/$ID_HEADER");
		}
	}

	public function update_kemasan($ID_HEADER){
		if(isset($_POST['submit_modalkemasan'])){
			$this->bc27_model->updatekemasan($_POST);
			redirect("Bc27_controller/edit/$ID_HEADER");
		}
	}
// --------------------------------------------------------

public function filter_pabean_pemasukan()
{
	$this->load->view('Header');
	if(isset($_GET['submit_filter'])){
			redirect("Bc_controller/Rpt_pabean_pemasukan?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}
	$this->load->view('Report_filter_tanggal');
	$this->load->view('Footer');
}

public function Rpt_pabean_pemasukan()
{
	$this->load->view('Header');
	$this->load->view('Rpt_pabean_pemasukan');
	$this->load->view('Footer');
}


public function filter_pabean_pengeluaran()
{
	$this->load->view('Header');
	if(isset($_GET['submit_filter'])){
			redirect("Bc_controller/Rpt_pabean_pengeluaran?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}
	$this->load->view('Report_filter_tanggal');
	$this->load->view('Footer');
}

public function Rpt_pabean_pengeluaran()
{
	$this->load->view('Header');
	$this->load->view('Rpt_pabean_pengeluaran');
	$this->load->view('Footer');
}


}
