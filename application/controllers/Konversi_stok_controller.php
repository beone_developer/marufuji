<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konversi_stok_controller extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Item_model');
		$this->load->model('Komposisi_model');
		$this->load->model('Konversi_stok_model');
		$this->load->model('Gudang_model');

		$data['list_item'] = $this->Item_model->load_item();
		$data['list_satuan'] = $this->Item_model->load_satuan();
		$data['list_gudang'] = $this->Gudang_model->load_gudang();
		

		if (isset($_POST['submit_konversi'])) {
			$result = $this->Konversi_stok_model->simpan($_POST);
			if ($result['is_error']) {
				$this->session->set_flashdata('error', $result['message']);
			}
			redirect("Konversi_stok_controller");
		}

		$this->load->view('Header');
		$this->load->view('Konversi_stok_form_view', $data);
		$this->load->view('Footer');
	}

	public function index_konversi_stok()
	{
		$this->load->model('Konversi_stok_model');
		$this->load->view('Header');

		$data['list_konversi_stok'] = $this->Konversi_stok_model->load_konversi();

		$this->load->view('Konversi_stok_list_view', $data);
		$this->load->view('Footer');
	}

	public function delete($konversi_stok_header_id)
	{
		$this->load->model('Konversi_stok_model');
		$this->Konversi_stok_model->delete($konversi_stok_header_id);
		redirect("Konversi_stok_controller/index_konversi_stok");
	}

	public function loadKomposisi($item_jadi_id)
	{
		$this->load->model('Komposisi_model');
		$data = $this->Komposisi_model->get_default_komposisi($item_jadi_id);
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function edit($konversi_stok_header_id)
	{
		$this->load->model('Konversi_stok_model');
		$this->load->model('Item_model');
		$this->load->view('Header');

		$data['list_satuan'] = $this->Item_model->load_satuan();
		$data['default'] = $this->Konversi_stok_model->get_default_header($konversi_stok_header_id);
		$data['default_detail'] = $this->Konversi_stok_model->get_default_detail($konversi_stok_header_id);

		if (isset($_POST['submit_konversi'])) {
			$this->Konversi_stok_model->update($_POST, $konversi_stok_header_id);
			redirect("Konversi_stok_controller/index_konversi_stok");
		}

		$this->load->view('Konversi_stok_form_edit_view', $data);
		$this->load->view('Footer');
	}

}
