<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Debit_note_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Custsup_model');
		$this->load->model('COA_model');
		$this->load->model('Debit_note_model');
		$this->load->view('Header');

		$data['list_customer'] = $this->Custsup_model->load_receiver();
		$data['list_coa'] = $this->COA_model->load_COA();

		if(isset($_POST['submit_debit_note'])){
			$this->Debit_note_model->simpan_debit_note($_POST);
			redirect("Debit_note_controller");
		}

		$this->load->view('Debit_note_form_view',$data);
		$this->load->view('Footer');
	}

	public function get_nomor_debit_note(){
		$this->load->view('get_nomor_debit_note');
	}

	public function List_debit_note()
	{
		$this->load->model('Debit_note_model');
		$this->load->view('Header');

		$data['List_debit_note'] = $this->Debit_note_model->load_debit_note();

		$this->load->view('Debit_note_view', $data);
		$this->load->view('Footer');
	}

	public function edit($hp_id, $dbn_no){
		$this->load->model('Custsup_model');
		$this->load->model('COA_model');
		$this->load->model('Debit_note_model');
		$this->load->view('Header');

		$data['list_customer'] = $this->Custsup_model->load_receiver();
		$data['list_coa'] = $this->COA_model->load_COA();
		$data['default'] = $this->Debit_note_model->get_default($hp_id);

		if(isset($_POST['submit_debit_note'])){
			$this->Debit_note_model->update($_POST, $dbn_no);
			redirect("Debit_note_controller/List_debit_note");
		}
		$this->load->view("Debit_note_form_edit_view",$data);
		$this->load->view('Footer');
	}

	public function delete($hp_id, $dbn_no){
		$this->load->model("Debit_note_model");
		$this->Debit_note_model->delete($hp_id, $dbn_no);
		redirect("Debit_note_controller/List_debit_note");
	}


}
