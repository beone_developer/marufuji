<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_order_controller extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Sales_order_model');
		$this->load->model('Custsup_model');
		$this->load->view('Header');

		$data['list_customer'] = $this->Custsup_model->load_receiver();
		if (isset($_POST['submit_sales'])) {
			$this->Sales_order_model->simpan($_POST);
			redirect("Sales_order_controller");
		}

		$this->load->view('Sales_order_form_view', $data);
		$this->load->view('Footer');
	}


	public function index_sales()
	{
		$this->load->model('Sales_order_model');
		$this->load->view('Header');

		$data['list_sales_order_header'] = $this->Sales_order_model->load_sales_header();

		$this->load->view('Sales_order_list_view', $data);
		$this->load->view('Footer');
	}

	public function edit($sales_id)
	{
		$this->load->model('Item_model');
		$this->load->model('Sales_order_model');
		$this->load->view('Header');

		$data['list_item'] = $this->Item_model->load_item();
		$data['default'] = $this->Sales_order_model->get_default_header($sales_id);
		$data['default_detail'] = $this->Sales_order_model->get_default_detail($sales_id);

		if (isset($_POST['submit_sales'])) {
			$this->Sales_order_model->update($_POST, $sales_id);
			redirect("Sales_order_controller/index_sales");
		}

		$this->load->view('Sales_order_form_edit_view', $data);
		$this->load->view('Footer');
	}

	public function realisasi($sales_id)
	{
		$this->load->model("Sales_order_model");
		$this->Sales_order_model->realisasi($sales_id);
		redirect("Sales_order_controller/index_sales");
	}

	public function delete($sales_id, $sales_no)
	{
		$this->load->model("Sales_order_model");
		$this->Sales_order_model->delete($sales_id, $sales_no);
		redirect("Sales_order_controller/index_sales");
	}

	public function sales_order_print($sales_id)
	{
		$this->load->library('pdfgenerator');
		$this->load->model('Sales_order_model');

		$data['default'] = $this->Sales_order_model->get_default_header($sales_id);
		$data['default_detail'] = $this->Sales_order_model->get_default_detail($sales_id);

		$html = $this->load->view('Rpt_sales_order_print', $data, true);

		$this->pdfgenerator->generate($html, 'Sales Order');
	}

	public function sales_order_printinv($sales_id){
		$this->load->library('pdfgenerator');
		$this->load->model('Sales_order_model');

		$data['default'] = $this->Sales_order_model->get_default_header($sales_id);
		$data['default_detail'] = $this->Sales_order_model->get_default_detail($sales_id);
 
	    $html = $this->load->view('Rpt_sales_order_invoice_print', $data, true);
	    
	    $this->pdfgenerator->generate($html,'Sales Order');
	}
}
