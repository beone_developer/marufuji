<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produksi_controller extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Produksi_model');
		$this->load->model('Pemakaian_model');
		$this->load->view('Header');

		$data['list_pemakaian'] = $this->Pemakaian_model->load_pemakaian();
		$data['list_pm'] = $this->Pemakaian_model->load_header_pm();

		if (isset($_POST['submit_proudksi'])) {
			$this->Produksi_model->simpan($_POST);
			redirect("Produksi_controller");
		}

		$this->load->view('Produksi_form_view', $data);
		$this->load->view('Footer');
	}

	public function get_nomor_produksi()
	{
		$this->load->view('get_nomor_produksi');
	}


}
